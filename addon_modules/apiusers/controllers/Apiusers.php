<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Apiusers extends MY_Controller
{

	function __construct()
	{
		parent::__construct();

		// First check if logged in
		if (!$this->session->userdata('user_id'))
		{
			// set the url they were trtying to go to in session
			$this->session->set_userdata('page_url', current_url());

			//Redirect to login
			redirect('user/login');
		}

		// Get site wide settings first
		$this->Settings_model->loadSitewideSettings();

		// Load This Model
		$this->load->model('Apiusers_model');
	}

	public function admin_index()
	{
		$this->data = array();
		$this->data['title'] = 'API Users Index';

		// load the config from the current module
		$this->load->config('apiusers');
		$this->data['apiusers_config'] = $this->config->config['apiusers'];

		// Added for developer dropdown
		$apiusers = $this->db->get($this->db->dbprefix . "apiusers");
		$this->data['dev_data']['apiusers'] = $apiusers->result_array();

		$this->load->view('layout/admin/header.php', $this->data);
		$this->load->view('admin/index', $this->data);
		$this->load->view('layout/admin/footer.php', $this->data);
	}

	public function getapiusers()
	{
		$filter = new stdClass();
		// Set filter data based on post input, none by default
		//$filter->first_name = ($this->input->get_post('filterfirstname') != '') ? $this->input->get_post('filterfirstname') : null;

		$data['apiusers'] = $this->Apiusers_model->filter($filter);

		return json_encode($this->load->view('admin/filter', $data));
	}

	public function add()
	{
		$this->data = array();

		if ($this->input->post()) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('name', 'Name', 'trim|required');
			if ($this->form_validation->run() !== FALSE) {
				$data = array(
					'name' => $this->input->post('name')
				);
				if ($this->db->insert($this->db->dbprefix . "apiusers", $data)) {
					$this->data['msg'] = "The API User was added successfully.";
					$this->data['msgtype'] = "success";
				} else {
					$this->data['msg'] = "There was a problem adding the API User.";
					$this->data['msgtype'] = "danger";
				}
			} else {
				$this->data['msg'] = "Your submission has the following Problems:<br>" . validation_errors();
				$this->data['msgtype'] = "danger";
			}
		}

		$this->data['title'] = 'API User Add';

		$this->load->view('layout/admin/header.php', $this->data);
		$this->load->view('admin/add', $this->data);
		$this->load->view('layout/admin/footer.php', $this->data);
	}

	public function edit()
	{
		$apiuserid = $this->uri->segment(4);
		$this->data = array();

		if ($this->input->post()) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('name', 'Name', 'trim|required');
			if ($this->form_validation->run() !== FALSE) {
                $data = array(
                    'name' => $this->input->post('name')
                );
                $this->db->where("id", $apiuserid);
                if ($this->db->update($this->db->dbprefix . "apiusers", $data)) {
                    $this->data['msg'] = "The API User was updated successfully.";
                    $this->data['msgtype'] = "success";
                } else {
                    $this->data['msg'] = "There was a problem updating the API User.";
                    $this->data['msgtype'] = "danger";
                }
			} else {
				$this->data['msg'] = "Your submission has the following Problems:<br>" . validation_errors();
				$this->data['msgtype'] = "danger";
			}
		}

		$this->data['title'] = 'API User Edit';

		$this->data['apiuser'] = $this->Apiusers_model->getapiuser($apiuserid);

		// Added for developer dropdown
		$this->data['dev_data']['apiuser'] = $this->data['apiuser'];

		$this->load->view('layout/admin/header.php', $this->data);
		$this->load->view('admin/edit', $this->data);
		$this->load->view('layout/admin/footer.php', $this->data);
	}


	public function delete()
	{
        $apiuserid = $this->uri->segment(4);
        if ($this->Apiusers_model->delete($apiuserid) !== false) {
            $response_array['status'] = 'success';
            echo json_encode($response_array);
        } else {
            $response_array['status'] = 'error';
            echo json_encode($response_array);
        }
    }
}
