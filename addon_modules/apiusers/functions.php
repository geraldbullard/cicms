<?php
/* Module functions file */

// If installing the module run this function to create db table(s)
if (!function_exists('bp_installModule')) {
    function bp_installModule() {
        $CI =& get_instance();

        // Update the module status
        $CI->db->set('status', 1);
        $CI->db->where("module", "apiusers");
        $CI->db->update($CI->db->dbprefix . "modules");

        // Create table(s)
	    /*$CI->db->query("
			CREATE TABLE IF NOT EXISTS `" . $CI->db->dbprefix . "apiusers` (
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`vname` varchar(64) NOT NULL,
				PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	    ");*/

        return true;
    }
}

// If uninstalling the module run this function to remove db entries if selected
if (!function_exists('bp_uninstallModule')) {
    function bp_uninstallModule() {
        $CI =& get_instance();

        // Update the module status
        $CI->db->set('status', -1);
        $CI->db->where("module", "apiusers");
        $CI->db->update($CI->db->dbprefix . "modules");

        // Drop table(s)
        /*$CI->db->query("
            DROP TABLE IF EXISTS `" . $CI->db->dbprefix . "apiusers`;
        ");*/

        return true;
    }
}
