<?php
/* Vanilla module initialization file */

// If uninstalling the content module run this function to remove db entries if selected
if (!function_exists('bp_initializeModule')) {
    function bp_initializeModule($db) {
        $mysqli = new mysqli($db->hostname, $db->username, $db->password, $db->database);
        // Module table entry
        $mysqli->query("
            INSERT IGNORE INTO `" . $db->dbprefix . "modules` (`module`, `title`, `summary`, `theme`, `icon`, `sort`, `visibility`, `status`, `system`)
            VALUES ('vanillas', 'Vanillas', 'The Vanillas Module Summary', 'adminTheme', 'lnr lnr-leaf', 99, 1, -1, 0);
        ");
        // Developer Permissions
        $mysqli->query("
            INSERT IGNORE INTO `" . $db->dbprefix . "permissions` (`groups_id`, `module`, `access`, `override`)
            VALUES (1, 'vanillas', 5, NULL);
        ");
        // Admin Permissions
        $mysqli->query("
            INSERT IGNORE INTO `" . $db->dbprefix . "permissions` (`groups_id`, `module`, `access`, `override`)
            VALUES (2, 'vanillas', 4, NULL);
        ");
        // Settings (If Needed)
        $mysqli->query("
            INSERT IGNORE INTO `" . $db->dbprefix . "settings` (`define`, `title`, `summary`, `value`, `type`, `edit`, `system`, `visibility`)
            VALUES ('vanillasAdminPagination', 'Vanillas Module Pagination Default', 'Vanillas Module Pagination Default', '3', 'settings', 1, 0, 1);
        ");
        return true;
    }
}