<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
        <h3 class="page-title pull-left">Vanillas</h3>
        <div class="pull-right">
            <a href="/admin/vanillas/add/">
                <button class="btn btn-sm btn-primary" id="dt-add-row">
                    <i class="fa fa-plus"></i>&nbsp;&nbsp;Add Vanilla
                </button>
            </a>
        </div>
        <?php if ($this->session->flashdata('msg')) { ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="alert alert-<?php echo $this->session->flashdata('msgtype'); ?> alert-dismissible action-alert" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">x</button>
                    <?php echo $this->session->flashdata('msg'); ?>
                </div>
            </div>
        </div>
        <?php } ?>
        <div class="row">
            <div class="col-lg-12">
                <div id="vanillasListingTable"></div><hr />
            </div>
        </div>
        <script>
            $(document).ready(function () {
                $("#clearFilter").on("click", function (evt) {
                    evt.preventDefault();
                    $('#vanillas_filter').trigger("reset");
                    $.fn.loadVanillas();
                    $("#clearFilter").blur();
                });
                $("#vanillasFilter").on("click", function (evt) {
                    evt.preventDefault();
                    $.fn.loadVanillas();
                    $("#vanillasFilter").blur();
                });
                $.fn.loadVanillas = function () {
                    $.blockUI({target: "#vanillasListingTable"});
                    $params = $("#vanillas_filter").serialize();
                    $.get('<?php echo base_url('/admin/vanillas/getVanillas'); ?>?' + $params, function (data) {
                        $("#vanillasListingTable").html(data);
                        $.unblockUI({target: "#vanillasListingTable"});
                    }).fail(function () {
                        alert("Error!", "Something went wrong loading the Vanillas!", "error");
                    });
                };
                $.fn.loadVanillas();
            });
        </script>
