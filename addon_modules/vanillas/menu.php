<?php
/* Kanban module menu file */

$_menu = array(
    'url' => '/admin/vanillas/',
    'title' => 'Vanillas',
    'icon' => 'lnr lnr-leaf',
    'access' => 1,
    'sub_items' => array(
    	// Repeat main menu since it has subs
	    'vanillas' => array(
		    'url' => '/admin/vanillas/',
		    'title' => 'Vanillas',
		    'icon' => 'lnr lnr-leaf',
		    'access' => 1,
	    ),
	    'add' => array(
		    'url' => '/admin/vanillas/add/',
		    'title' => 'Add',
		    'icon' => 'lnr lnr-plus-circle',
		    'access' => 3,
	    ),
    ),
);