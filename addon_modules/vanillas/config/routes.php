<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* Vanillas Routes */
$route['admin/vanillas'] = 'vanillas/admin_index';
$route['admin/vanillas/add'] = 'vanillas/add';
$route['admin/vanillas/getVanillas'] = 'vanillas/getVanillas';
$route['admin/vanillas/edit/(:any)'] = 'vanillas/edit/$1';
$route['admin/vanillas/update/(:any)'] = 'vanillas/update/$1';
$route['admin/vanillas/delete/(:any)'] = 'vanillas/delete/$1';
