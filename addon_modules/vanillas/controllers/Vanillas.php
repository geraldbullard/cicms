<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vanillas extends MY_Controller
{

	function __construct()
	{
		parent::__construct();

		// First check if logged in
		if (!$this->session->userdata('user_id'))
		{
			// set the url they were trtying to go to in session
			$this->session->set_userdata('page_url', current_url());

			//Redirect to login
			redirect('user/login');
		}

		// Get site wide settings first
		$this->Settings_model->loadSitewideSettings();

		// Load This Model
		$this->load->model('Vanillas_model');
	}

	public function admin_index()
	{
		$this->data = array();
		$this->data['title'] = 'Vanillas Index';

		// load the config from the current module
		$this->load->config('vanillas');
		$this->data['vanillas_config'] = $this->config->config['vanillas'];

		// Added for developer dropdown
		$vanillas = $this->db->get($this->db->dbprefix . "vanillas");
		$this->data['dev_data']['vanillas'] = $vanillas->result_array();

		$this->load->view('layout/admin/header.php', $this->data);
		$this->load->view('admin/index', $this->data);
		$this->load->view('layout/admin/footer.php', $this->data);
	}

	public function getVanillas()
	{
		$filter = new stdClass();
		// Set filter data based on post input, none by default
		//$filter->first_name = ($this->input->get_post('filterfirstname') != '') ? $this->input->get_post('filterfirstname') : null;

		$data['vanillas'] = $this->Vanillas_model->filter($filter);

		return json_encode($this->load->view('admin/filter', $data));
	}

	public function add()
	{
		$this->data = array();

		if ($this->input->post()) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('vname', 'Name', 'trim|required');
			if ($this->form_validation->run() !== FALSE) {
				$data = array(
					'vname' => $this->input->post('vname')
				);
				if ($this->db->insert($this->db->dbprefix . "vanillas", $data)) {
					$this->data['msg'] = "The Vanilla was added successfully.";
					$this->data['msgtype'] = "success";
				} else {
					$this->data['msg'] = "There was a problem adding the Vanilla.";
					$this->data['msgtype'] = "danger";
				}
			} else {
				$this->data['msg'] = "Your submission has the following Problems:<br>" . validation_errors();
				$this->data['msgtype'] = "danger";
			}
		}

		$this->data['title'] = 'Vanilla Add';

		$this->load->view('layout/admin/header.php', $this->data);
		$this->load->view('admin/add', $this->data);
		$this->load->view('layout/admin/footer.php', $this->data);
	}

	public function edit()
	{
		$vanillaid = $this->uri->segment(4);
		$this->data = array();

		if ($this->input->post()) {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('vname', 'Name', 'trim|required');
			if ($this->form_validation->run() !== FALSE) {
                $data = array(
                    'vname' => $this->input->post('vname')
                );
                $this->db->where("id", $vanillaid);
                if ($this->db->update($this->db->dbprefix . "vanillas", $data)) {
                    $this->data['msg'] = "The Vanilla was updated successfully.";
                    $this->data['msgtype'] = "success";
                } else {
                    $this->data['msg'] = "There was a problem updating the Vanilla.";
                    $this->data['msgtype'] = "danger";
                }
			} else {
				$this->data['msg'] = "Your submission has the following Problems:<br>" . validation_errors();
				$this->data['msgtype'] = "danger";
			}
		}

		$this->data['title'] = 'Vanilla Edit';

		$this->data['vanilla'] = $this->Vanillas_model->getVanilla($vanillaid);

		// Added for developer dropdown
		$this->data['dev_data']['vanilla'] = $this->data['vanilla'];

		$this->load->view('layout/admin/header.php', $this->data);
		$this->load->view('admin/edit', $this->data);
		$this->load->view('layout/admin/footer.php', $this->data);
	}


	public function delete()
	{
        $vanillaid = $this->uri->segment(4);
        if ($this->Vanillas_model->delete($vanillaid) !== false) {
            $response_array['status'] = 'success';
            echo json_encode($response_array);
        } else {
            $response_array['status'] = 'error';
            echo json_encode($response_array);
        }
    }
}
