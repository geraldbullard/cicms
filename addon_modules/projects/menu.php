<?php
/* Projects module menu file */

$_menu = array(
    'url' => '/admin/projects/',
    'title' => 'Projects',
    'icon' => 'lnr lnr-paperclip',
    'access' => 1,
    'sub_items' => array(
	    'projects' => array(
		    'url' => '/admin/projects/',
		    'title' => 'Projects',
		    'icon' => 'lnr lnr-mustache',
		    'access' => 1,
	    ),
	    'add' => array(
		    'url' => '/admin/projects/add/',
		    'title' => 'Add',
		    'icon' => 'lnr lnr-plus-circle',
		    'access' => 3,
	    ),
	    'archived' => array(
		    'url' => '/admin/projects/archived/',
		    'title' => 'Archived',
		    'icon' => 'lnr lnr-trash',
		    'access' => 3,
	    ),
    ),
);