<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Projects Model
 */
class Projects_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	public function getActiveProjects()
	{
		$this->db->select('
            P.id as id,
            P.clients_id as client_id,
            P.project_statuses_id as status_id,
            P.categories_id as cat_id,
            P.domains_id as domain_id,
            P.contract_info as contract_info,
            P.time_estimate as time_estimate,
            
            CL.client_name as client_name,
            
            CA.name as cat_name,
            
            D.domain as domain,
            D.ada as ada,
        ');

		$this->db->from($this->db->dbprefix . 'projects P');
		$this->db->join($this->db->dbprefix . 'clients CL', 'P.clients_id = CL.id', 'left');
		$this->db->join($this->db->dbprefix . 'categories CA', 'P.categories_id = CA.id', 'left');
		$this->db->join($this->db->dbprefix . 'domains D', 'P.domains_id = D.id', 'left');

		$this->db->order_by('CL.client_name', 'ASC');
		$query = $this->db->get();

		return $query->result_array();
	}

	public function getProjectCategoryName($cid)
	{
		$this->db->select("name");
		$this->db->where("id", $cid);
		return $this->db->get($this->db->dbprefix . "projects")->first_row();
	}

	public function getStatusColor($sid)
	{
		if ($sid == 1) return 'success';
		if ($sid == 2) return 'danger';
		if ($sid == 3) return 'primary';
		if ($sid == 4) return 'warning';
		if ($sid == 5) return 'default';
		if ($sid == 6) return 'info';
	}

    public function addNote($post) {
        $data = array(
            'projects_id' => $post['projects_id'],
            'tasks_id' => ($post['tasks_id'] != '' ? $post['tasks_id'] : 0),
            'users_id' => $this->session->userdata('user_id'),
            'parent_notes_id' => 0,
            'note' => $post['n_note'],
            'date' => date("Y-m-d H:i:s"),
            'date_updated' => date("Y-m-d H:i:s"),
        );
        if ($this->db->insert($this->db->dbprefix . "project_notes", $data)) {
            return true;
        }
        return false;
    }

    public function editNote($post) {
        $data = array(
            'note' => $post['en_note'],
            'date_updated' => date("Y-m-d H:i:s"),
        );
        $this->db->where("id", $post['noteid']);
        if ($this->db->update($this->db->dbprefix . "project_notes", $data)) {
            return true;
        }
        return false;
    }

    public function deleteNote($id) {
        $this->db->where("id", $id);
        if ($this->db->delete($this->db->dbprefix . "project_notes")) {
            return true;
        }
        return false;
    }

    public function addLInk($post) {
        $data = array(
            'projects_id' => $post['al_pid'],
            'link' => $post['al_link'],
            'label' => $post['al_label']
        );
        if ($this->db->insert($this->db->dbprefix . "project_links", $data)) {
            return true;
        }
        return false;
    }

    public function editLInk($post) {
        $data = array(
            'link' => $post['el_link'],
            'label' => $post['el_label'],
        );
        $this->db->where("id", $post['el_id']);
        if ($this->db->update($this->db->dbprefix . "project_links", $data)) {
            return true;
        }
        return false;
    }

    public function deleteLink($id) {
        $this->db->where("id", $id);
        if ($this->db->delete($this->db->dbprefix . "project_links")) {
            return true;
        }
        return false;
    }

}
