    <?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-title">Projects Add</h3>
            <?php if (isset($msg)) { ?>
                <div class="alert alert-<?php echo $msgtype; ?> action-alert" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <?php echo $msg; ?>
                </div>
            <?php } ?>
            <?php if ($this->session->flashdata('msg')) { ?>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-<?php echo $this->session->flashdata('msgtype'); ?> alert-dismissible action-alert" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <?php echo $this->session->flashdata('msg'); ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div class="panel">
                <div class="panel-body margin-bottom-15">
                    <form action="" method="post" id="project_add">
                        <div class="clearfix">
                            <h4>Details</h4>
                            <input type="hidden" id="domains_id" name="domains_id" value="">
                            <p>
                                <label for="project_name">Name</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    id="clients_id"
                                    name="clients_id"
                                    value=""
                                >
                            </p>
                            <p>
                                <label for="categories_id">
                                    Category
                                </label>
                                <select
                                    class="form-control"
                                    id="categories_id"
                                    name="categories_id"
                                ></select>
                            </p>
                            <p>
                                <label for="project_statuses_id">
                                    Status
                                </label>
                                <select
                                    class="form-control"
                                    id="project_statuses_id"
                                    name="project_statuses_id"
                                ></select>
                            </p>
                            <p>
                                <label for="time_estimate">Time Estimate</label>
                                <input
                                    type="number"
                                    class="form-control"
                                    id="time_estimate"
                                    name="time_estimate"
                                    value=""
                                    step="0.25"
                                >
                            </p>
                            <p>
                                <label for="contract_info">Contract Info</label>
                                <textarea
                                    class="form-control"
                                    id="contract_info"
                                    name="contract_info"
                                    value=""
                                ></textarea>
                            </p>
                        </div>
                        <div class="margin-top-15 text-right">
                            <button type="submit" class="btn btn-primary margin-right-15">Save</button>
                            <button type="button" class="btn btn-danger" onclick="confirmProjectAddCancel();">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        function confirmProjectAddCancel() {
            if (confirm("Are you sure? Any unsaved information will be lost.")) window.location = '/admin/projects/';
        }
        function roundNearQtr(number) {
            return (Math.round(number * 4) / 4).toFixed(2);
        }
        function parseFloatToFixed(string) {
            return parseFloat(string).toFixed(string.split('.')[1].length);
        }
        $(document).ready(function() {
            $('#clients_id').bind("change keyup input cut", function() {
                if ($('#clients_id').val() != '') {
                    let search = $('#clients_id').val();

                }
            });
            $('#time_estimate').bind("blur", function() {
                if ($('#time_estimate').val() != '') {
                    $('#time_estimate').val(parseFloatToFixed(roundNearQtr($('#time_estimate').val())));
                }
            });
        });
    </script>

