<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
            <div class="row">
                <div class="col-lg-1">
                    <h3 class="page-title"><?php echo $title; ?></h3>
                </div>
                <div class="col-lg-5 filter-buttons">
                    <a href="javascript:;"><span class="label label-all">All</span></a>
                    <a href="javascript:;"><span class="label label-success">New</span></a>
                    <a href="javascript:;"><span class="label label-danger">Urgent</span></a>
                    <a href="javascript:;"><span class="label label-primary">In Progress</span></a>
                    <a href="javascript:;"><span class="label label-warning">Complete</span></a>
                    <a href="javascript:;"><span class="label label-info">Pending</span></a>
                    <a href="javascript:;"><span class="label label-default">Archived</span></a>
                </div>
                <div class="col-lg-6 text-right">
                    <a href="/admin/projects/add/">
                        <button class="btn btn-sm btn-primary" id="project-add">
                            <i class="fa fa-plus"></i><span class="hide-below-480">&nbsp;&nbsp;Add<span class="hide-below-768"> Project</span></span>
                        </button>
                    </a>
                    <a class="hide margin-left-10" href="javascript:;" id="project-edit">
                        <button class="btn btn-sm btn-default project-edit">
                            <i class="fa fa-pencil"></i><span class="hide-below-480">&nbsp;&nbsp;Edit<span class="hide-below-768"> Project</span></span>
                        </button>
                    </a>
                    <?php /*if ($this->session->userdata['system_access']['projects'] > 3) { ?>
                    <a class="hide margin-left-10" href="javascript:;" id="project-status" onclick="return confirm('Are you sure?');">
                        <button class="btn btn-sm btn-warning project-status">
                            <i class="fa fa-power-off"></i><span class="hide-below-480"><span class="hide-below-768"> Project</span>&nbsp;&nbsp;Status</span>
                        </button>
                    </a>
                    <?php }*/ ?>
                    <?php /*if ($this->session->userdata['system_access']['projects'] > 4) { ?>
                    <a class="hide margin-left-10" href="javascript:;" id="project-delete" onclick="deleteProject();">
                        <button class="btn btn-sm btn-danger project-delete">
                            <i class="fa fa-trash"></i><span class="hide-below-480">&nbsp;&nbsp;Delete<span class="hide-below-768"> Project</span></span>
                        </button>
                    </a>
                    <?php }*/ ?>
                </div>
            </div>
            <?php if ($this->session->flashdata('msg')) { ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-<?php echo $this->session->flashdata('msgtype'); ?> alert-dismissible action-alert" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <?php echo $this->session->flashdata('msg'); ?>
                    </div>
                </div>
            </div>
            <?php } ?>
            <?php if (isset($msg)) { ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-<?php echo $msg['msgtype']; ?> alert-dismissible action-alert" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <?php echo $msg['msg']; ?>
                    </div>
                </div>
            </div>
            <?php } ?>
			<div class="row projects-main-page">
				<div class="col-sm-4 col-lg-3">
                    <div class="panel panel-headline" style="height:65vh;">
                        <div class="panel-heading padding-10">
                            <span class="clearable">
                                <input
                                    type="text"
                                    placeholder="Find Project/Project Name"
                                    class="form-control form-input-small"
                                    id="filterProjects"
                                    value="<?php echo (
                                        isset($_GET['s']) && $_GET['s'] != '' ? $_GET['s'] : null
                                    ); ?>"
                                >
                                <i class="clearable__clear">&times;</i>
                            </span>
                        </div>
                        <div class="panel-body margin-bottom-15" style="padding-left:10px;padding-bottom:15px;border-left:1px solid #eee;height:87%;overflow-y:scroll;margin-left:10px;margin-right:10px;-moz-box-shadow: inset 0 -10px 10px -10px #ddd;-webkit-box-shadow: inset 0 -10px 10px -10px #ddd;box-shadow: inset 0 -10px 10px -10px #ddd;">
                            <?php
                                foreach ($projects as $p => $project) {
	                                $CI =& get_instance();
	                                $CI->load->model('Projects_model');
                                    $color = $CI->Projects_model->getStatusColor($project['status_id']);
                                    /*
                                        [id] => 13
                                        [client_id] => 1104
                                        [status_id] => 1
                                        [cat_id] => 1
                                        [domain_id] => 843
                                        [contract_info] => Contract
                                        [time_estimate] => 1
                                        [client_name] => ACME
                                        [cat_name] => Research/Estimation
                                        [domain] => www.google.com
                                        [ada] => 2020-10-20
                                    */
                                    echo '<a href="javascript:;" class="load-projects-data';
                                    echo ($project['status_id'] == 5 ? ' hide' : null) . '"';
	                                echo ' data-id="' . $project['id'] . '">';
                                    echo '<div class="metric">';
                                    echo '<button type="button" class="btn btn-' . $color . ' margin-right-10 pull-left">';
                                    echo $project['id'];
                                    echo '</button>';
                                    echo '<div>' . $project['client_name'] . '<br>' . $project['cat_name'] . '</div>';
                                    echo '</div></a>' . "\n";
                                }
                            ?>
                        </div>
                    </div>
				</div>
                <div class="col-sm-8 col-lg-9 margin-bottom-30" id="projectdata">
                    <div class="panel panel-headline">
                        <div class="panel-heading padding-bottom-15">
                            <div class="row">
                                <div class="col-xs-12 col-lg-3">
                                    <h3 class="panel-title project-panel-title">Select a Project</h3>
                                </div>
                                <div class="col-xs-12 col-lg-5 text-center">
                                    <p class="project-panel-links text-right" style="display:none;">Links</p>
                                </div>
                                <div class="col-xs-12 col-lg-4 text-right">
                                    <p class="project-panel-selects" style="display:none;">SELECTS</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body project-panel-body margin-bottom-15" style="display:none;">
                            <div id="project_status" class="hidden"></div>
                            <div id="project_client_id" class="hidden"></div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <a href="javascript:;" id="load-notes-data" data-id="">
                                        <div class="metric">
                                            <span class="icon">
                                                <i class="fa fa-sticky-note"></i>
                                            </span>
                                            <p>
                                                <span class="number">&nbsp;</span>
                                                <span class="title">Notes</span>
                                            </p>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-sm-6">
                                    <a href="javascript:;" id="load-tasks-data" data-id="">
                                        <div class="metric">
                                            <span class="icon">
                                                <i class="fas fa-pencil"></i>
                                            </span>
                                            <p>
                                                <span class="number">&nbsp;</span>
                                                <span class="title">Tasks</span>
                                            </p>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-sm-6">
                                    <a href="javascript:;" id="load-links-data" data-id="">
                                        <div class="metric">
                                            <span class="icon">
                                                <i class="fas fa-link"></i>
                                            </span>
                                            <p>
                                                <span class="number">&nbsp;</span>
                                                <span class="title">Links</span>
                                            </p>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-sm-6">
                                    <a href="javascript:;" id="load-developers-data" data-id="">
                                        <div class="metric">
                                            <span class="icon">
                                                <i class="fa fa-database"></i>
                                            </span>
                                            <p>
                                                <span class="number">&nbsp;</span>
                                                <span class="title">Developers</span>
                                            </p>
                                        </div>
                                    </a>
                                </div>
                            </div><hr style="margin:0 0 10px;">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="col-lg-12" id="cardtasks"></div>
                                    <div id="projectcontent"></div>
                                </div>
                            </div><hr>
                            <div class="row">
                                <div class="col-lg-12 text-right">
                                    <style>
                                        .pt-3, .py-3 {
                                            padding-top: 1rem!important;
                                        }
                                        .container-fluid {
                                            width: 100%;
                                            padding-right: 15px;
                                            padding-left: 15px;
                                            margin-right: auto;
                                            margin-left: auto;
                                        }
                                        .bg-light {
                                            background-color: #f8f9fa!important;
                                        }
                                        .card {
                                            position: relative;
                                            display: -ms-flexbox;
                                            display: flex;
                                            -ms-flex-direction: column;
                                            flex-direction: column;
                                            min-width: 0;
                                            word-wrap: break-word;
                                            background-color: #fff;
                                            background-clip: border-box;
                                            border: 1px solid rgba(0,0,0,.125);
                                            border-radius: .25rem;
                                        }
                                        .card-body {
                                            -ms-flex: 1 1 auto;
                                            flex: 1 1 auto;
                                            padding: 1.25rem;
                                        }
                                        .card.draggable {
                                            margin-bottom: 1rem;
                                            cursor: grab;
                                        }
                                        .card-title {
                                            margin-bottom: .5rem;
                                        }
                                        .float-right {
                                            float: right!important;
                                        }
                                        .rounded-circle {
                                            border-radius: 50%!important;
                                            background-color: #00AAFF;
                                            width: 30px;
                                            height: 30px;
                                            text-align: center;
                                            color: #fff;
                                        }
                                        img {
                                            vertical-align: middle;
                                            border-style: none;
                                        }
                                        .droppable {
                                            background-color: green;
                                            min-height: 120px;
                                            margin-bottom: 1rem;
                                        }
                                        .card.draggable {
                                            margin-bottom: 1rem;
                                            cursor: grab;
                                        }
                                    </style>
                                    <script>
                                        const drag = (event) => {
                                            event.dataTransfer.setData("text/plain", event.target.id);
                                        };
                                        const allowDrop = (ev) => {
                                            ev.preventDefault();
                                            if (hasClass(ev.target, "dropzone")) {
                                                addClass(ev.target, "droppable");
                                            }
                                        };
                                        const clearDrop = (ev) => {
                                            removeClass(ev.target, "droppable");
                                        };
                                        const drop = (event) => {
                                            event.preventDefault();
                                            const data = event.dataTransfer.getData("text/plain");
                                            const element = document.querySelector(`#${data}`);
                                            try {
                                                // remove the spacer content from dropzone
                                                event.target.removeChild(event.target.firstChild);
                                                // add the draggable content
                                                event.target.appendChild(element);
                                                // remove the dropzone parent
                                                unwrap(event.target);
                                            } catch (error) {
                                                console.warn("can't move the item to the same place")
                                            }
                                            updateDropzones();
                                        };
                                        const updateDropzones = (eid) => {
                                            /* after dropping, refresh the drop target areas
                                            so there is a dropzone after each item
                                            using jQuery here for simplicity */

                                            var dz = $('<div class="dropzone rounded" ondrop="drop(event)" ondragover="allowDrop(event)" ondragleave="clearDrop(event)"> &nbsp; </div>');

                                            // delete old dropzones
                                            $('.dropzone').remove();

                                            // insert new dropdzone after each item
                                            dz.insertAfter('.card.draggable');

                                            // insert new dropzone in any empty swimlanes
                                            $(".items:not(:has(.card.draggable))").append(dz);
                                            // START - Here is where we will put the ajax to update the db after every drop ////////////
                                            setTimeout(function() {
                                                var cardTasks = [];
                                                $('.card-column').each(function() {
                                                    var colId = $(this).attr('id');
                                                    $(this).find('.draggable').each(function() {
                                                        cardTasks.push({
                                                            col : colId,
                                                            task : $(this).attr('id')
                                                        });
                                                    });
                                                });
                                                $('#cardtasks').html("");
                                                for (i = 0; i < cardTasks.length; i++) {
                                                    //$('#cardtasks').append(jQuery.param(cardTasks[i]) + '<br>');
                                                    let pid = $("#load-tasks-data").attr("data-id");
                                                    let tid = cardTasks[i].task.replace("t_", "");
                                                    let usr = cardTasks[i].col.replace("user_", "");
                                                    let url = "<?php echo base_url(); ?>projects/dragAndDropProjectTask/" + pid + "/" + tid + "/" + usr + "/" + i;
                                                    $.post(url).done(function(data) {
                                                        //swal('Tasks Updated');
                                                    });
                                                }
                                            }, 100);
                                            // END - Here is where we will put the ajax to update the db after every drop //////////////
                                        };
                                        // helpers
                                        function hasClass(target, className) {
                                            return new RegExp('(\\s|^)' + className + '(\\s|$)').test(target.className);
                                        }
                                        function addClass(ele, cls) {
                                            if (!hasClass(ele, cls)) ele.className += " " + cls;
                                        }
                                        function removeClass(ele, cls) {
                                            if (hasClass(ele, cls)) {
                                                var reg = new RegExp('(\\s|^)' + cls + '(\\s|$)');
                                                ele.className = ele.className.replace(reg, ' ');
                                            }
                                        }
                                        function unwrap(node) {
                                            node.replaceWith(...node.childNodes);
                                        }
                                    </script>
                                    <div id="projectcontentadd"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
            <!-- New Project Note Modal -->
            <div class="modal fade" id="addNote" tabindex="-1" role="dialog"
                 aria-labelledby="addNoteLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="addNoteLabel">
                                Add Project Note
                            </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form id="addNoteForm">
                            <input type="hidden" name="projects_id" id="n_pid" value="">
                            <input type="hidden" name="tasks_id" id="n_tid" value="">
                            <input type="hidden" name="redirect" id="n_redirect" value="">
                            <div class="modal-body">
                                <p class="form-group">
                                    <label for="n_note">Note <small>(Required)</small></label>
                                    <textarea name="n_note" id="n_note" class="form-control" rows="5"></textarea>
                                </p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" id="addNoteClose" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" id="addNoteSubmit" class="btn btn-primary disabled" disabled>Create Note</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Edit Project Note Modal -->
            <div class="modal fade" id="editNote" tabindex="-1" role="dialog"
                 aria-labelledby="editNoteLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="editNoteLabel">
                                Edit Note
                            </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form id="editNoteForm">
                            <input type="hidden" name="projects_id" id="en_pid" value="">
                            <input type="hidden" name="noteid" id="en_id" value="">
                            <input type="hidden" name="redirect" id="en_redirect" value="">
                            <div class="modal-body">
                                <p class="form-group">
                                    <label for="en_note">Note <small>(Required)</small></label>
                                    <textarea name="en_note" id="en_note" class="form-control" rows="5"></textarea>
                                </p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" id="editNoteClose" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" id="editNoteSubmit" class="btn btn-primary">Save Note</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Add Project Link Modal -->
            <div class="modal fade" id="addLink" tabindex="-1" role="dialog"
                 aria-labelledby="addLinkLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="addLinkLabel">
                                Add Link
                            </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form id="addLinkForm">
                            <input type="hidden" name="al_pid" id="al_pid" value="">
                            <div class="modal-body">
                                <p class="form-group">
                                    <label for="al_link">Link <small>(Required)</small></label>
                                    <input type="text" name="al_link" id="al_link" class="form-control" value="https://" />
                                </p>
                                <p class="form-group">
                                    <label for="al_label">Label <small>(Required)</small></label>
                                    <input type="text" name="al_label" id="al_label" class="form-control" />
                                </p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" id="addLinkClose" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" id="addLinkSubmit" class="btn btn-primary">Save Link</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Edit Project Link Modal -->
            <div class="modal fade" id="editLink" tabindex="-1" role="dialog"
                 aria-labelledby="editLinkLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="editLinkLabel">
                                Add Link
                            </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form id="editLinkForm">
                            <input type="hidden" name="el_id" id="el_id" value="">
                            <div class="modal-body">
                                <p class="form-group">
                                    <label for="el_link">Link <small>(Required)</small></label>
                                    <input type="text" name="el_link" id="el_link" class="form-control" value="https://" />
                                </p>
                                <p class="form-group">
                                    <label for="el_label">Label <small>(Required)</small></label>
                                    <input type="text" name="el_label" id="el_label" class="form-control" />
                                </p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" id="editLinkClose" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" id="editLinkSubmit" class="btn btn-primary">Save Link</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <script>
                // Get the post project id
                let getPid = '<?php echo ((isset($_POST['pid']) && $_POST['pid'] != '') ? $_POST['pid'] : "none"); ?>';
                // Get the post content id
                let getContentId = '<?php echo ((isset($_POST['coid']) && $_POST['coid'] != '') ? $_POST['coid'] : "none"); ?>';
                let projectAccess = '<?php echo $this->session->userdata['system_access']['projects']; ?>';
                function subDomain(url) {
                    // IF THERE, REMOVE WHITE SPACE FROM BOTH ENDS
                    url = url.replace(new RegExp(/^\s+/), ""); // START
                    url = url.replace(new RegExp(/\s+$/), ""); // END
                    // IF FOUND, CONVERT BACK SLASHES TO FORWARD SLASHES
                    url = url.replace(new RegExp(/\\/g), "/");
                    // IF THERE, REMOVES 'http://', 'https://' or 'ftp://' FROM THE START
                    url = url.replace(new RegExp(/^http\:\/\/|^https\:\/\/|^ftp\:\/\//i), "");
                    // IF THERE, REMOVES 'www.' FROM THE START OF THE STRING
                    url = url.replace(new RegExp(/^www\./i), "");
                    // REMOVE COMPLETE STRING FROM FIRST FORWARD SLASH ON
                    url = url.replace(new RegExp(/\/(.*)/), "");
                    // REMOVES '.??.??' OR '.???.??' FROM END - e.g. '.CO.UK', '.COM.AU'
                    if (url.match(new RegExp(/\.[a-z]{2,3}\.[a-z]{2}$/i))) {
                        url = url.replace(new RegExp(/\.[a-z]{2,3}\.[a-z]{2}$/i), "");
                    // REMOVES '.??' or '.???' or '.????' FROM END - e.g. '.US', '.COM', '.INFO'
                    } else if (url.match(new RegExp(/\.[a-z]{2,4}$/i))) {
                        url = url.replace(new RegExp(/\.[a-z]{2,4}$/i), "");
                    }
                    // CHECK TO SEE IF THERE IS A DOT '.' LEFT IN THE STRING
                    var subDomain = (url.match(new RegExp(/\./g))) ? true : false;
                    return(subDomain);
                }
                function nl2br(str, is_xhtml) {
                    if (typeof str === 'undefined' || str === null) {
                        return '';
                    }
                    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
                    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
                }
                function scrollToView(area) {
                    // ScrollTo for mobile view
                    if ($(window).innerWidth() < 1024) {
                        $('html, body').animate({
                            scrollTop: ($("#" + area).offset().top - 80)
                        }, 1000);
                    }
                }
                function tConvert(time) {
                    // Check correct time format and split into components
                    time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
                    if (time.length > 1) { // If time format correct
                        time = time.slice(1); // Remove full string match value
                        time[5] = +time[0] < 12 ? ' am' : ' pm'; // Set AM/PM
                        time[0] = +time[0] % 12 || 12; // Adjust hours
                    }
                    return time.join(''); // return adjusted time or original string
                }
                function addProjectNote(pid) {
                    $("#n_pid").val(pid);
                    $("#addNoteLabel").html("Add Project Note");
                    $("#addNote").modal("show");
                }
                function editProjectNote(id) {
                    $("#en_pid").val($("#load-notes-data").data("id"));
                    $("#en_id").val(id);
                    $.ajax({
                        url: '/projects/loadSingleNoteData/' + id,
                        type: 'GET',
                        success: function(response) {
                            var result = JSON.parse(response);
                            $("#en_note").val(result.note[0].note);
                        }
                    });
                    $("#editNote").modal("show");
                }
                function deleteProjectNote(id) {
                    swal({
                        title: "Are you sure?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-success",
                        confirmButtonText: "Yes",
                        cancelButtonText: "Cancel",
                        closeOnConfirm: true,
                        closeOnCancel: true
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            let url = "<?php echo base_url(); ?>projects/deleteNote/" + id;
                            $.post(url).done(function(data) {
                                let result = JSON.parse(data);
                                if (result.msg == 'success') {
                                    let curNotes = $("#load-notes-data .metric p .number").html();
                                    $("#load-notes-data .metric p .number").html(curNotes-1);
                                    $(".well_" + id).parent().remove();
                                }
                                if (result.msg == 'failure') swal("Note Not Deleted!");
                            });
                        }
                    });
                }
                function addTaskNote(pid, tid) {
                    $("#n_pid").val(pid);
                    $("#n_tid").val(tid);
                    $("#n_redirect").val(2);
                    $("#addNoteLabel").html("Add Task Note");
                    $("#addNote").modal("show");
                }
                function editTaskNote(id) {
                    $("#en_pid").val($("#load-notes-data").data("id"));
                    $("#en_id").val(id);
                    $("#en_redirect").val(2);
                    $.ajax({
                        url: '/projects/loadSingleNoteData/' + id,
                        type: 'GET',
                        success: function(response) {
                            var result = JSON.parse(response);
                            $("#en_note").val(result.note[0].note);
                        }
                    });
                    $("#editNote").modal("show");
                }
                function deleteTaskNote(pid, id) {
                    swal({
                        title: "Are you sure?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-success",
                        confirmButtonText: "Yes",
                        cancelButtonText: "Cancel",
                        closeOnConfirm: true,
                        closeOnCancel: true
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            let url = "<?php echo base_url(); ?>projects/deleteNote/" + id;
                            $.post(url).done(function(data) {
                                let result = JSON.parse(data);
                                if (result.msg == 'success') {
                                    //$("#tn_" + id).remove();
                                    let redirectForm = $(
                                        '<form action="/admin/projects/" method="post">' +
                                        '<input type="hidden" name="pid" value="' + pid + '" />' +
                                        '<input type="hidden" name="coid" value="2" />' +
                                        '</form>'
                                    );
                                    $('body').append(redirectForm);
                                    redirectForm.submit();
                                }
                                if (result.msg == 'failure') swal("Task Note Not Deleted!");
                            });
                        }
                    });
                }
                function addProjectLink(pid) {
                    $("#al_pid").val(pid);
                    $("#addLink").modal("show");
                }
                function editProjectLink(id) {
                    $("#el_id").val(id);
                    $("#el_link").val($(".link_" + id + " a").html());
                    $("#el_label").val($(".link_" + id + " span").html());
                    $("#editLink").modal("show");
                }
                function deleteProjectLink(id) {
                    swal({
                        title: "Are you sure?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-success",
                        confirmButtonText: "Yes",
                        cancelButtonText: "Cancel",
                        closeOnConfirm: true,
                        closeOnCancel: true
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            var url = "<?php echo base_url(); ?>projects/deleteLink/" + id;
                            $.post(url).done(function(data) {
                                var result = JSON.parse(data);
                                if (result.msg == 'success') {
                                    var curLinks = $("#load-links-data .metric p .number").html();
                                    $("#load-links-data .metric p .number").html(curLinks-1);
                                    $(".link_" + id).parent().remove();
                                }
                                if (result.msg == 'failure') swal("Link was not Deleted!");
                            });
                        }
                    });
                }
                function addProjectTask() {
                    let pid = $("#load-notes-data").data("id");
                    /*
                    $("#at_pid").val(pid);
                    $("#addTask").modal("show");
                    */
                }
                function editProjectTask(tid) {
                    /*$("#n_id").val(id);
                    $.ajax({
                        url: '/projects/loadSingleNoteData/' + id,
                        type: 'GET',
                        success: function(response) {
                            var results = JSON.parse(response);
                            $("#en_note").val(results.notes);
                        }
                    });
                    $("#editNote").modal("show");*/
                }
                function archiveProjectTask(tid) {
                    swal({
                        title: "Are you sure?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-success",
                        confirmButtonText: "Yes",
                        cancelButtonText: "Cancel",
                        closeOnConfirm: true,
                        closeOnCancel: true
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            alert("Archive Task");
                            /*var url = "<?php echo base_url(); ?>projects/deleteLink/" + id;
                            $.post(url).done(function(data) {
                                var result = JSON.parse(data);
                                if (result.msg == 'success') {
                                    var curNotes = $("#load-links-data .metric p .number").html();
                                    $("#load-links-data .metric p .number").html(curLinks-1);
                                    $(".link_" + id).parent().remove();
                                }
                                if (result.msg == 'failure') swal("Link was not Deleted!");
                            });*/
                        }
                    });
                }
                function toggleProjectAssign(tmval) {
                    let checked = $("#teamm-" + tmval).is(":checked"); // backwards from visible
                    setTimeout(function() {
                        if (checked) {
                            // assign to project
                            $.ajax({
                                url: '/projects/toggleProjectUserAssign/' + $('#load-notes-data').data('id') +
                                    '/' + tmval + '/' + (checked ? 1 : 0),
                                type: 'GET',
                                success: function(response) {
                                    let result = JSON.parse(response);
                                    if (result.success == 'true') {
                                        //swal("Successfully Added User to Project");
                                    } else {
                                        swal("Something went wrong!");
                                    }
                                }
                            });
                            // Show Check Mark
                            $("#teamm-" + tmval).prop("checked", true);
                        } else {
                            // un-assign from project
                            $.ajax({
                                url: '/projects/toggleProjectUserAssign/' + $('#load-notes-data').data('id') +
                                    '/' + tmval + '/' + (checked ? 1 : 0),
                                type: 'GET',
                                success: function(response) {
                                    let result = JSON.parse(response);
                                    if (result.success == 'true') {
                                        //swal("Successfully Removed User from Project");
                                    } else {
                                        swal("Something went wrong!");
                                    }
                                }
                            });
                            // Remove Check Mark
                            $("#teamm-" + tmval).prop("checked", false);
                        }
                    }, 100);
                    setTimeout(function() {
                        let redirectForm = $(
                            '<form action="/admin/projects/" method="post">' +
                            '<input type="hidden" name="pid" value="' + $('#load-notes-data').data('id') + '" />' +
                            //'<input type="hidden" name="coid" value="2" />' +
                            '</form>'
                        );
                        $('body').append(redirectForm);
                        redirectForm.submit();
                    }, 150);
                }
                function setProjectStatus() {
                    //alert("Project " + $('#load-notes-data').data('id') + " Status " + $('#p_status').val());
                    $.ajax({
                        url: '/projects/editProjectStatus/' + $('#load-notes-data').data('id') + '/' + $('#p_status').val(),
                        type: 'GET',
                        success: function(response) {
                            /*let result = JSON.parse(response);
                            if (result.success == 'true') {
                                swal("Successfully Changed Project Status");
                            } else {
                                swal("Something went wrong!");
                            }*/
                        }
                    });
                    let redirectForm = $(
                        '<form action="/admin/projects/" method="post">' +
                        '<input type="hidden" name="pid" value="' + $('#load-notes-data').data('id') + '" />' +
                        //'<input type="hidden" name="coid" value="2" />' +
                        '</form>'
                    );
                    $('body').append(redirectForm);
                    redirectForm.submit();
                }
                function projectClientInfo(cid) {
                    let redirectForm = $(
                        '<form action="/admin/clients/" method="post">' +
                        '<input type="hidden" name="pid" value="' + $("#load-notes-data").data("id") + '" />' +
                        '<input type="hidden" name="cid" value="' + cid + '" />' +
                        '<input type="hidden" name="coid" value="1" />' +
                        '</form>'
                    );
                    $('body').append(redirectForm);
                    redirectForm.submit();
                }
                function toggleTaskComplete(tid, state) {
                    let change = (state == true ? 1 : 0);
                    $.ajax({
                        url: '/projects/toggleTaskComplete/' + tid + '/' + change,
                        type: 'GET',
                        success: function(response) {
                            /*let result = JSON.parse(response);
                            if (result.success == 'true') {
                                swal("Successfully Changed Project Status");
                            } else {
                                swal("Something went wrong!");
                            }*/
                        }
                    });
                }
                function toggleTaskPriority(tid, state) {
                    let change = (state == true ? 1 : 0);
                    $.ajax({
                        url: '/projects/toggleTaskPriority/' + tid + '/' + change,
                        type: 'GET',
                        success: function(response) {
                            /*let result = JSON.parse(response);
                            if (result.success == 'true') {
                                swal("Successfully Changed Project Status");
                            } else {
                                swal("Something went wrong!");
                            }*/
                        }
                    });
                }
                /*function deactivateProject(id) {
                    alert("Deactivate Project " + id);
                }*/
                /*function activateProject(id) {
                    alert("Activate Client " + id);
                }*/
                // Check all project modal selects for enabling ssave button
                /*function checkAllAPSelects() {
                    var count = 0;
                    $('.p-selection').each(function() {
                        if ($(this).val() != '') {
                            count++;
                        }
                        if ($(this).attr('id') == 'p_category' && $(this).val() == '-1') {
                            $("#p-custom-category").removeClass("hidden");
                        }
                        if ($(this).attr('id') == 'p_category' && $(this).val() != '-1') {
                            $("#p-custom-category").addClass("hidden");
                        }
                    });
                    if (count > 2) {
                        $("#addProjectSubmit").removeClass("disabled").removeAttr("disabled");
                    } else {
                        return false;
                    }
                }*/
                $(document).ready(function() {
                    $('.panel-heading').click(false);
                    if (getPid !== 'none') {
                        setTimeout(function() {
                            $('.project-panel-body .metric').css("color", "#3287B2").css("background-color", "#ffffff");
                            var thisid = getPid;
                            // set the data-id values around the main content
                            $('#load-projects-data').attr('data-id', thisid);
                            $('#load-notes-data').attr('data-id', thisid);
                            $('#load-tasks-data').attr('data-id', thisid);
                            $('#load-links-data').attr('data-id', thisid);
                            $('#load-developers-data').attr('data-id', thisid);
                            $('.project-panel-body').show();
                            $('#projectcontent').html(
                                '<div class="overlay" style="text-align:center;">\n' +
                                '<img class="loading" src="/assets/img/loader.gif" alt="Loading" /><br/>\n' +
                                '<h2>Loading the Projects Data...</h2>\n' +
                                '</div>'
                            );
                            let pUrl = "<?php echo base_url(); ?>projects/loadProjectData/" + thisid;
                            $.post(pUrl).done(function(data) {
                                let result = JSON.parse(data);
                                $("#project_status").html(result.status);
                                $("#project_client_id").html(result.client_id);
                                $(".project-panel-title").html(result.client_name != 'undefined' ? '(' + result.id + ') ' +
                                    result.client_name : '-- None --');
                                $("#filterProjects").val(result.client_name);
                                $(".clearable__clear").show();
                                $(".project-panel-links").html(
                                    //'<a href="javascript:;"><span class="label label-default">' +
                                    //'<i class="fas fa-pencil-alt margin-right-5"></i> Add Task' +
                                    //'</span></a>' +
                                    //'<a href="javascript:;"><span class="label label-default">' +
                                    //'<i class="fas fa-paperclip margin-right-5"></i> Edit Project' +
                                    //'</span></a>' +
                                    //'<a href="javascript:;" onclick="trackProjectTime();"><span class="label label-default">' +
                                    //'<i class="far fa-clock margin-right-5"></i> Track Time' +
                                    //'</span></a>' +
                                    '<a href="javascript:void(0);" onclick="projectClientInfo($(\'#project_client_id\').html());"><span class="label label-default">' +
                                    '<i class="fas fa-user-circle margin-right-5"></i> Client Info' +
                                    '</span></a>'
                                ).show();
                                $(".project-panel-selects").html(
                                    '<select name="p_status" id="p_status" class="form-control-sm"' +
                                    ' onchange="setProjectStatus();">' +
                                    '    <option value="1"' + (result.status == 1 ? ' selected' : '') + '>New</option>' +
                                    '    <option value="2"' + (result.status == 2 ? ' selected' : '') + '>Urgent</option>' +
                                    '    <option value="3"' + (result.status == 3 ? ' selected' : '') + '>In Progress</option>' +
                                    '    <option value="4"' + (result.status == 4 ? ' selected' : '') + '>Complete</option>' +
                                    '    <option value="6"' + (result.status == 6 ? ' selected' : '') + '>Pending</option>' +
                                    '    <option value="5"' + (result.status == 5 ? ' selected' : '') + '>Archive</option>' +
                                    '</select>' +
                                    '<ul id="p_team_members" class="form-control-sm margin-left-10"></ul>'
                                ).show();
                                if (result.team_members !== "undefined" && result.team_members.length > 0) {
                                    let thisLength = result.team_members.length;
                                    let emailMailTo = 'mailto:';
                                    $("#p_team_members").append(
                                        '<li>Team Members (' + result.developers.length + ')</li>'
                                    );
                                    for (let i = 0; i < result.team_members.length; i++) {
                                        /*
                                            id: "35", // ignored
                                            user_id: "1", // Used to get user info
                                            group_id: "1" // ignored
                                        */
                                        //console.log(result.developers[i]);
                                        $.ajax({
                                            url: '/users/getUserDetailsById/' + result.team_members[i].user_id,
                                            type: 'GET',
                                            success: function(response) {
                                                let tm = JSON.parse(response);
                                                //console.log(tm);
                                                /*
                                                    "id": "1",
                                                    "ip_address": "127.0.0.1",
                                                    "username": "developer",
                                                    "password": "$2y$12$tLnWNdF4Y7wVp4hmhmVh/urx0iWVmf5K0E7dk4pOAxsDqT7pncWXm",
                                                    "email": "dev@mail.ext",
                                                    "activation_selector": null,
                                                    "activation_code": null,
                                                    "forgotten_password_selector": null,
                                                    "forgotten_password_code": null,
                                                    "forgotten_password_time": null,
                                                    "remember_selector": null,
                                                    "remember_code": null,
                                                    "created_on": "1268889823",
                                                    "last_login": "1628009429",
                                                    "active": "1",
                                                    "first_name": "Top",
                                                    "last_name": "Developer",
                                                    "company": "Development Company",
                                                    "phone": "2147483647"
                                                */
                                                let isAssigned = false;
                                                for (let d = 0; d < result.developers.length; d++) {
                                                    if (result.developers[d].users_id == tm[0].id) isAssigned = true;
                                                }
                                                let emailHTML = '';
                                                emailMailTo = emailMailTo + tm[0].email + ',';
                                                if ((thisLength-1) == i) {
                                                    emailHTML = '<li><a href="' + emailMailTo + '" target="_blank">' +
                                                    'Email All Members &raquo;</a></li>';
                                                }
                                                $("#p_team_members").append(
                                                    '<li data-id="' + tm[0].id + '">' +
                                                    '<input id="teamm-' + tm[0].id + '" type="checkbox" ' +
                                                    'class="teamm-checkbox" ' +
                                                    'onclick="toggleProjectAssign(' + tm[0].id + ');"' +
                                                    (isAssigned ? ' checked' : '') + '>' +
                                                    '<a href="mailto:' + tm[0].email + '">' +
                                                    tm[0].first_name + ' ' + tm[0].last_name + '</a>' +
                                                    '</li>'
                                                );
                                                setTimeout(function() {
                                                    $("#p_team_members").append(
                                                        emailHTML
                                                    );
                                                }, 200);
                                            }
                                        });
                                    }
                                }
                                $("#load-notes-data .number").html((
                                    result.notes.length > 0 && result.notes != 'undefined') ? result.notes.length : 0);
                                $("#load-tasks-data .number").html((
                                    result.tasks.length > 0 && result.tasks != 'undefined') ? result.tasks.length : 0);
                                $("#load-links-data .number").html((
                                    result.links.length > 0 && result.links != 'undefined') ? result.links.length : 0);
                                $("#load-developers-data .number").html((
                                    result.developers.length > 0 && result.developers != 'undefined') ? result.developers.length : 0);
                                $(".project-panel-body .metric").css("color", "#3287B2").css("background-color", "#ffffff");
                                $("#projectcontent").html('');
                                $("#projectcontent").append(
                                    '<div class="text-right margin-bottom-15">' +
                                    '    <a href="javascript:;" id="add_project_note" onclick="addProjectNote(' + thisid + ');">' +
                                    '        <button class="btn btn-sm btn-info">' +
                                    '            <i class="fa fa-plus"></i>&nbsp;&nbsp;Add Project Note' +
                                    '        </button>' +
                                    '    </a>' +
                                    '</div>' +
                                    '<div class="row hidden margin-top-5"></div>'
                                );
                                if (result.notes !== "undefined" && result.notes.length > 0) {
                                    for (let i = 0; i < result.notes.length; i++) {
                                        if (result.notes[i].tasks_id == 0) {
                                            let nDateUp = '--';
                                            let nTimeUp = '--';
                                            if (result.notes[i].date_updated) {
                                                nDateUp = result.notes[i].date_updated.slice(0, result.notes[i].date_updated.indexOf(' '));
                                                nTimeUp = tConvert(result.notes[i].date_updated.slice(result.notes[i].date_updated.lastIndexOf(' ')+1));
                                            }
                                            $('#projectcontent .row').append(
                                                '<div class="col-sm-12">' +
                                                '<div class="well well_' + result.notes[i].id + ' note-card relative"' +
                                                ' style="min-height:47px;">' +
                                                '<a href="javascript:;" onclick="editProjectNote(' + result.notes[i].id + ');">' +
                                                '<h4><i class="fa fa-pencil"></i>&nbsp; <strong>Last Updated: ' +
                                                nDateUp + ' ' + nTimeUp + ' (' + result.notes[i].users_id + ')' +
                                                '</strong></h4></a>' +
                                                '<p class="service-note">' +
                                                (result.notes[i].note !== 'undefined' ? result.notes[i].note : '') +
                                                '</p>' +
                                                '<i class="fa fa-trash pull-right client-content-well-trash" ' +
                                                'onclick="deleteProjectNote(' + result.notes[i].id + ');"></i>' +
                                                '</div>' +
                                                '</div>'
                                            );
                                        }
                                    }
                                } else {
                                    $("#projectcontent .row").html('').append('<div class="col-sm-12">No Notes</div>');
                                }
                                $("#projectcontent .row").removeClass("hidden");
                                // Load Notes by default ///////////////////////////////////////////////////////////////////
                                $('#load-notes-data .metric').css("color", "#ffffff").css("background-color", "#2B333E");
                                $('#projectcontentadd').html(
                                    '<a href="javascript:;" id="add_project_note" onclick="addProjectNote(' + thisid + ');">' +
                                    '    <button class="btn btn-sm btn-info">' +
                                    '        <i class="fa fa-plus"></i>&nbsp;&nbsp;Add Project Note' +
                                    '    </button>' +
                                    '</a>'
                                );
                            });
                            $('#project-edit').attr("href", "/admin/projects/edit/" + thisid).removeClass("hide");
                            $('#project-delete').attr("onclick", "deleteProject(" + thisid + ");").removeClass("hide");
                            $('#project-deactivate').attr("href", "/admin/projects/deactivate/" + thisid).removeClass("hide");
                            scrollToView("projectdata");
                        }, 250);
                        // added to switch to the content you were on before refresh
                        setTimeout(function() {
                            if (getContentId != '') {
                                if (getContentId == '1') { // Notes
                                    $('#load-notes-data').click();
                                } else if (getContentId == '2') { // Tasks
                                    $('#load-tasks-data').click();
                                } else if (getContentId == '3') { // Links
                                    $('#load-links-data').click();
                                } else if (getContentId == '4') { // Developers
                                    $('#load-developers-data').click();
                                }
                            }
                        }, 400);
                    }
                    $(".filter-buttons a").on("click", "span", function () {
                        let className = $(this).attr("class").replace("label","").replace("label-","");
                        $(".load-projects-data .metric button").each(function() {
                            let hasClass = $(this).attr("class").indexOf("btn-" + className.trim());
                            if (className.trim() == 'all') {
                                $(this).parent().parent().removeClass("hide");
                                let defaultClass = $(this).attr("class").indexOf("btn-default");
                                if (defaultClass !== -1)
                                    $(this).parent().parent().addClass("hide");
                            } else if (hasClass == -1) {
                                $(this).parent().parent().addClass("hide");
                            } else {
                                $(this).parent().parent().removeClass("hide");
                            }
                        });
                    });
                    $(".clearable").each(function() {
                        var $inp = $(this).find("input:text"),
                            $cle = $(this).find(".clearable__clear");
                        $inp.on("input", function() {
                            $cle.toggle(!!this.value);
                        });
                        $cle.on("touchstart click", function(e) {
                            e.preventDefault();
                            $inp.val("").trigger("input");
                            $(".col-sm-4.col-lg-3 .load-projects-data").each(function() {
                                $(this).removeClass("hide").show();
                                let defaultClass = $(this).find(".metric button").attr("class").indexOf("btn-default");
                                if (defaultClass !== -1)
                                    $(this).addClass("hide");
                            });
                            $('.project-panel-title').html("Select a Project");
                            $('.project-panel-links').html("");
                            $('.project-panel-selects').html("");
                            $(".project-panel-body").hide();
                            $inp.focus();
                        });
                    });
                    // This function kept being very troublesome and time consuming therefore a manual click on the
                    // project once the list is down to a single result was neccessary for now.
                    $('#filterProjects').bind("keyup input", function() {
                        if ($('#filterProjects').val() != '') {
                            var search = $('#filterProjects').val();
                            var show = 0;
                            var hide = 0;
                            $(".load-projects-data").each(function() {
                                let projectsName = $(this).find(".metric div").html().replace("&amp;","&").split('<br>').shift();
                                if (projectsName.toLowerCase().indexOf(search.toLowerCase()) === -1) {
                                    hide++;
                                    $(this).hide();
                                } else {
                                    show++;
                                    $(this).show();
                                }
                            });
                        } else {
                            $(".row.projects-main-page .col-sm-8.col-lg-9.margin-bottom-30 .panel-headline .panel-heading .row").hide();
                            $(".col-sm-4.col-lg-3 .load-projects-data").each(function() {
                                $(this).show();
                            });
                            $('#filterProjects').focus();
                            $('#project-edit').attr("href", "javascript:;").addClass("hide");
                            $('#project-delete').attr("onclick", "deleteProject();").addClass("hide");
                            $('#project-deactivate').attr("href", "javascript:;").addClass("hide");
                            setTimeout(function() {
                                $('.project-panel-title').html("Select a Project");
                                $('.project-panel-links').html("");
                                $('.project-panel-selects').html("");
                                $(".project-panel-body").hide();
                            }, 100);
                            setTimeout(function() {
                                $(".row.projects-main-page .col-sm-8.col-lg-9.margin-bottom-30 .panel-headline .panel-heading .row").show();
                            }, 200);
                        }
                    });
                    $('.load-projects-data').click(function() {
                        $('.project-panel-body .metric').css("color", "#3287B2").css("background-color", "#ffffff");
                        let thisid = $(this).attr("data-id");
                        // set the data-id values around the main content
                        $('#load-notes-data').attr('data-id', thisid);
                        $('#load-tasks-data').attr('data-id', thisid);
                        $('#load-links-data').attr('data-id', thisid);
                        $('#load-developers-data').attr('data-id', thisid);
                        $('.project-panel-body').show();
                        $('#projectcontent').html(
                            '<div class="overlay" style="text-align:center;">\n' +
                            '<img class="loading" src="/assets/img/loader.gif" alt="Loading" /><br/>\n' +
                            '<h2>Loading the Project Data...</h2>\n' +
                            '</div>'
                        );
                        let pUrl = "<?php echo base_url(); ?>projects/loadProjectData/" + thisid;
                        $.post(pUrl).done(function(data) {
                            let result = JSON.parse(data);
                            //console.log(result);
                            $("#project_status").html(result.status);
                            $("#project_client_id").html(result.client_id);
                            $(".project-panel-title").html(result.client_name != 'undefined' ? '(' + result.id + ') ' +
                                result.client_name : '-- None --');
                            $("#filterProjects").val(result.client_name);
                            $(".clearable__clear").show();
                            $(".project-panel-links").html(
                                //'<a href="javascript:;"><span class="label label-default">' +
                                //'<i class="fas fa-pencil-alt margin-right-5"></i> Add Task' +
                                //'</span></a>' +
                                //'<a href="javascript:;"><span class="label label-default">' +
                                //'<i class="fas fa-paperclip margin-right-5"></i> Edit Project' +
                                //'</span></a>' +
                                //'<a href="javascript:;" onclick="trackProjectTime();"><span class="label label-default">' +
                                //'<i class="far fa-clock margin-right-5"></i> Track Time' +
                                //'</span></a>' +
                                '<a href="javascript:void(0);" onclick="projectClientInfo($(\'#project_client_id\').html());"><span class="label label-default">' +
                                '<i class="fas fa-user-circle margin-right-5"></i> Client Info' +
                                '</span></a>'
                            ).show();
                            $(".project-panel-selects").html(
                                '<select name="p_status" id="p_status" class="form-control-sm"' +
                                ' onchange="setProjectStatus();">' +
                                '    <option value="1"' + (result.status == 1 ? ' selected' : '') + '>New</option>' +
                                '    <option value="2"' + (result.status == 2 ? ' selected' : '') + '>Urgent</option>' +
                                '    <option value="3"' + (result.status == 3 ? ' selected' : '') + '>In Progress</option>' +
                                '    <option value="4"' + (result.status == 4 ? ' selected' : '') + '>Complete</option>' +
                                '    <option value="6"' + (result.status == 6 ? ' selected' : '') + '>Pending</option>' +
                                '    <option value="5"' + (result.status == 5 ? ' selected' : '') + '>Archive</option>' +
                                '</select>' +
                                '<ul id="p_team_members" class="form-control-sm margin-left-10"></ul>'
                            ).show();
                            if (result.team_members !== "undefined" && result.team_members.length > 0) {
                                let thisLength = result.team_members.length;
                                let emailMailTo = 'mailto:';
                                $("#p_team_members").append(
                                    '<li>Team Members (' + result.developers.length + ')</li>'
                                );
                                for (let i = 0; i < result.team_members.length; i++) {
                                    /*
                                        id: "35", // ignored
                                        user_id: "1", // Used to get user info
                                        group_id: "1" // ignored
                                    */
                                    //console.log(result.developers[i]);
                                    $.ajax({
                                        url: '/users/getUserDetailsById/' + result.team_members[i].user_id,
                                        type: 'GET',
                                        success: function(response) {
                                            let tm = JSON.parse(response);
                                            //console.log(tm);
                                            /*
                                                "id": "1",
                                                "ip_address": "127.0.0.1",
                                                "username": "developer",
                                                "password": "$2y$12$tLnWNdF4Y7wVp4hmhmVh/urx0iWVmf5K0E7dk4pOAxsDqT7pncWXm",
                                                "email": "dev@mail.ext",
                                                "activation_selector": null,
                                                "activation_code": null,
                                                "forgotten_password_selector": null,
                                                "forgotten_password_code": null,
                                                "forgotten_password_time": null,
                                                "remember_selector": null,
                                                "remember_code": null,
                                                "created_on": "1268889823",
                                                "last_login": "1628009429",
                                                "active": "1",
                                                "first_name": "Top",
                                                "last_name": "Developer",
                                                "company": "Development Company",
                                                "phone": "2147483647"
                                            */
                                            let isAssigned = false;
                                            for (let d = 0; d < result.developers.length; d++) {
                                                if (result.developers[d].users_id == tm[0].id) isAssigned = true;
                                            }
                                            let emailHTML = '';
                                            emailMailTo = emailMailTo + tm[0].email + ',';
                                            if ((thisLength-1) == i) {
                                                emailHTML = '<li><a href="' + emailMailTo + '" target="_blank">' +
                                                'Email All Members &raquo;</a></li>';
                                            }
                                            $("#p_team_members").append(
                                                '<li data-id="' + tm[0].id + '">' +
                                                '<input id="teamm-' + tm[0].id + '" type="checkbox" ' +
                                                'class="teamm-checkbox" ' +
                                                'onclick="toggleProjectAssign(' + tm[0].id + ');"' +
                                                (isAssigned ? ' checked' : '') + '>' +
                                                '<a href="mailto:' + tm[0].email + '">' +
                                                tm[0].first_name + ' ' + tm[0].last_name + '</a>' +
                                                '</li>'
                                            );
                                            setTimeout(function() {
                                                $("#p_team_members").append(
                                                    emailHTML
                                                );
                                            }, 200);
                                        }
                                    });
                                }
                            }
                            $("#load-notes-data .number").html((
                                result.notes.length > 0 && result.notes != 'undefined') ? result.notes.length : 0);
                            $("#load-tasks-data .number").html((
                                result.tasks.length > 0 && result.tasks != 'undefined') ? result.tasks.length : 0);
                            $("#load-links-data .number").html((
                                result.links.length > 0 && result.links != 'undefined') ? result.links.length : 0);
                            $("#load-developers-data .number").html((
                                result.developers.length > 0 && result.developers != 'undefined') ? result.developers.length : 0);
                            $(".project-panel-body .metric").css("color", "#3287B2").css("background-color", "#ffffff");
                            $("#projectcontent").html('');
                            $("#projectcontent").append(
                                '<div class="text-right margin-bottom-15">' +
                                '    <a href="javascript:;" id="add_project_note" onclick="addProjectNote(' + thisid + ');">' +
                                '        <button class="btn btn-sm btn-info">' +
                                '            <i class="fa fa-plus"></i>&nbsp;&nbsp;Add Project Note' +
                                '        </button>' +
                                '    </a>' +
                                '</div>' +
                                '<div class="row hidden margin-top-5"></div>'
                            );
                            if (result.notes !== "undefined" && result.notes.length > 0) {
                                for (let i = 0; i < result.notes.length; i++) {
                                    if (result.notes[i].tasks_id == 0) {
                                        let nDateUp = '--';
                                        let nTimeUp = '--';
                                        if (result.notes[i].date_updated) {
                                            nDateUp = result.notes[i].date_updated.slice(0, result.notes[i].date_updated.indexOf(' '));
                                            nTimeUp = tConvert(result.notes[i].date_updated.slice(result.notes[i].date_updated.lastIndexOf(' ')+1));
                                        }
                                        $('#projectcontent .row').append(
                                            '<div class="col-sm-12">' +
                                            '<div class="well well_' + result.notes[i].id + ' note-card relative"' +
                                            ' style="min-height:47px;">' +
                                            '<a href="javascript:;" onclick="editProjectNote(' + result.notes[i].id + ');">' +
                                            '<h4><i class="fa fa-pencil"></i>&nbsp; <strong>Last Updated: ' +
                                            nDateUp + ' ' + nTimeUp + ' (' + result.notes[i].users_id + ')' +
                                            '</strong></h4></a>' +
                                            '<p class="service-note">' +
                                            (result.notes[i].note !== 'undefined' ? result.notes[i].note : '') +
                                            '</p>' +
                                            '<i class="fa fa-trash pull-right client-content-well-trash" ' +
                                            'onclick="deleteProjectNote(' + result.notes[i].id + ');"></i>' +
                                            '</div>' +
                                            '</div>'
                                        );
                                    }
                                }
                            } else {
                                $("#projectcontent .row").html('').append('<div class="col-sm-12">No Notes</div>');
                            }
                            $("#projectcontent .row").removeClass("hidden");
                            // Load Notes by default ///////////////////////////////////////////////////////////////////
                            $('#load-notes-data .metric').css("color", "#ffffff").css("background-color", "#2B333E");
                            $('#projectcontentadd').html(
                                '<a href="javascript:;" id="add_project_note" onclick="addProjectNote(' + thisid + ');">' +
                                '    <button class="btn btn-sm btn-info">' +
                                '        <i class="fa fa-plus"></i>&nbsp;&nbsp;Add Project Note' +
                                '    </button>' +
                                '</a>'
                            );
                        });
                        scrollToView("projectdata");
                        $('#project-edit').attr("href", "/admin/projects/edit/" + thisid).removeClass("hide");
                        $('#project-delete').attr("onclick", "deleteProject(" + thisid + ");").removeClass("hide");
                        $('#project-deactivate').attr("href", "/admin/projects/deactivate/" + thisid).removeClass("hide");
                    });
                    $("#n_note").on('keyup', function() {
                        if ($(this).val().length > 10) {
                            $("#addNoteSubmit").removeClass("disabled").removeAttr("disabled");
                        } else {
                            $("#addNoteSubmit").addClass("disabled").attr("disabled", "disabled");
                        }
                    });
                    $("#addNoteClose").on('click', function() {
                        $("#n_note").val("");
                    });
                    $("#addNote button.close").on('click', function() {
                        $("#n_note").val("");
                    });
                    $("#addNoteSubmit").on('click', function() {
                        let redirect = ($("#n_redirect").val() == 2 ? 2 : 1);
                        let pid = $("#n_pid").val();
                        let formData = $("#addNoteForm").serialize();
                        $.ajax({
                            url: '/projects/addNote/',
                            type: 'POST',
                            data: formData,
                            success: function(response) {
                                let redirectForm = $(
                                    '<form action="/admin/projects/" method="post">' +
                                    '<input type="hidden" name="pid" value="' + pid + '" />' +
                                    '<input type="hidden" name="coid" value="' + redirect + '" />' +
                                    '</form>'
                                );
                                $('body').append(redirectForm);
                                redirectForm.submit();
                            }
                        });
                    });
                    $("#editNoteClose").on('click', function() {
                        $("#n_id").val("");
                        $("#en_note").val("");
                    });
                    $("#editNote button.close").on('click', function() {
                        $("#n_id").val("");
                        $("#en_note").val("");
                    });
                    $("#editNoteSubmit").on('click', function() {
                        let redirect = ($("#en_redirect").val() == 2 ? 2 : 1);
                        let pid = $("#en_pid").val();
                        let id = $("#en_id").val();
                        let formData = $("#editNoteForm").serialize();
                        $.ajax({
                            url: '/projects/editNote/' + id,
                            type: 'POST',
                            data: formData,
                            success: function(response) {
                                let redirectForm = $(
                                    '<form action="/admin/projects/" method="post">' +
                                    '<input type="hidden" name="pid" value="' + pid + '" />' +
                                    '<input type="hidden" name="coid" value="' + redirect + '" />' +
                                    '</form>'
                                );
                                $('body').append(redirectForm);
                                redirectForm.submit();
                            }
                        });
                    });
                    $("#addLinkClose").on('click', function() {
                        $("#al_pid").val("");
                        $("#al_link").val("https://");
                        $("#al_label").val("");
                    });
                    $("#addLink button.close").on('click', function() {
                        $("#al_pid").val("");
                        $("#al_link").val("https://");
                        $("#al_label").val("");
                    });
                    $("#addLinkSubmit").on('click', function() {
                        let pid = $("#al_pid").val();
                        let formData = $("#addLinkForm").serialize();
                        $.ajax({
                            url: '/projects/addLink/',
                            type: 'POST',
                            data: formData,
                            success: function(response) {
                                let redirectForm = $(
                                    '<form action="/admin/projects/" method="post">' +
                                    '<input type="hidden" name="pid" value="' + pid + '" />' +
                                    '<input type="hidden" name="coid" value="3" />' +
                                    '</form>'
                                );
                                $('body').append(redirectForm);
                                redirectForm.submit();
                            }
                        });
                    });
                    $("#editLinkClose").on('click', function() {
                        $("#el_id").val("");
                        $("#el_link").val("");
                        $("#el_label").val("");
                    });
                    $("#editLink button.close").on('click', function() {
                        $("#el_id").val("");
                        $("#el_link").val("");
                        $("#el_label").val("");
                    });
                    $("#editLinkSubmit").on('click', function() {
                        let pid = $("#load-links-data").attr("data-id");
                        let formData = $("#editLinkForm").serialize();
                        $.ajax({
                            url: '/projects/editLink/',
                            type: 'POST',
                            data: formData,
                            success: function(response) {
                                let redirectForm = $(
                                    '<form action="/admin/projects/" method="post">' +
                                    '<input type="hidden" name="pid" value="' + pid + '" />' +
                                    '<input type="hidden" name="coid" value="3" />' +
                                    '</form>'
                                );
                                $('body').append(redirectForm);
                                redirectForm.submit();
                            }
                        });
                    });
                    $('#load-links-data').click(function() {
                        $('.project-panel-body .metric').css("color", "#3287B2").css("background-color", "#ffffff");
                        $('#load-links-data .metric').css("color", "#ffffff").css("background-color", "#2B333E");
                        let thisid = $(this).attr("data-id");
                        $('#projectcontent').html(
                            '<div class="overlay" style="text-align:center;">\n' +
                            '<img class="loading" src="/assets/img/loader.gif" alt="Loading" /><br/>\n' +
                            '<h2>Loading the Links Data...</h2>\n' +
                            '</div>'
                        );
                        let lUrl = "<?php echo base_url(); ?>projects/loadLinksData/" + thisid;
                        $.post(lUrl).done(function(data) {
                            let result = JSON.parse(data);
                            $('#projectcontent').append(
                                '<div class="text-right margin-bottom-15">' +
                                '    <a href="javascript:;" id="add-link" onclick="addProjectLink(' + thisid + ');">' +
                                '        <button class="btn btn-sm btn-info">' +
                                '            <i class="fa fa-plus"></i>&nbsp;&nbsp;Add Link' +
                                '        </button>' +
                                '    </a>' +
                                '</div>' +
                                '<div class="row hidden margin-top-5"></div>'
                            );
                            if (result.links != 'undefined') {
                                $("#load-links-data .metric .number").html(result.links.length);
                                for (let i = 0; i < result.links.length; i++) {
                                    if (result.links[i].id != 'undefined') {
                                        $('#projectcontent .row').append(
                                            '<div class="col-sm-12 margin-bottom-10">' +
                                            '<div class="link_' + result.links[i].id +
                                            ' project-card padding-left-5 relative">' +
                                            '<a href="' + result.links[i].link + '" target="_blank">' +
                                            result.links[i].link + '</a>' +
                                            ' - <span>' + result.links[i].label + '</span>' +
                                            '<a href="javascript:void(0);" class="margin-left-15" ' +
                                            'onclick="editProjectLink(' + result.links[i].id + ');">' +
                                            '<i class="fa fa-pencil"></i>' +
                                            '</a>' +
                                            '<a href="javascript:void(0);" class="margin-left-15" ' +
                                            'onclick="deleteProjectLink(' + result.links[i].id + ');">' +
                                            '<i class="fa fa-times-circle red"></i>' +
                                            '</a>' +
                                            // Add edit and delete icons here //////////////////////////////////////////
                                            '</div>' +
                                            '</div>'
                                        );
                                    }
                                }
                            } else {
                                $('#projectcontent .row').append('<div class="col-sm-12">No Links</div>');
                            }
                            $('#projectcontentadd').html(
								'<a href="javascript:;" id="add-link" onclick="addProjectLink(' + thisid + ');">' +
								'    <button class="btn btn-sm btn-info">' +
								'        <i class="fa fa-plus"></i>&nbsp;&nbsp;Add Link' +
								'    </button>' +
								'</a>'
							);
                            $.when(
                                $('#projectcontent .overlay').fadeOut()
                            ).then(
                                setTimeout(function() {
                                    $('#projectcontent .row').removeClass("hidden");
                                    scrollToView("projectcontent");
                                }, 300)
                            );
                        });
                    });
                    $('#load-notes-data').click(function() {
                        $('.project-panel-body .metric').css("color", "#3287B2").css("background-color", "#ffffff");
                        $('#load-notes-data .metric').css("color", "#ffffff").css("background-color", "#2B333E");
                        let thisid = $(this).attr("data-id");
                        $('#projectcontent').html(
                            '<div class="overlay" style="text-align:center;">\n' +
                            '<img class="loading" src="/assets/img/loader.gif" alt="Loading" /><br/>\n' +
                            '<h2>Loading the Notes Data...</h2>\n' +
                            '</div>'
                        );
                        let nUrl = "<?php echo base_url(); ?>projects/loadNotesData/" + thisid;
                        $.post(nUrl).done(function(data) {
                            $('#projectcontent').html("");
                            let result = JSON.parse(data);
                            $('#projectcontent').append(
                                '<div class="text-right margin-bottom-15">' +
                                '    <a href="javascript:;" id="add_project_note" onclick="addProjectNote(' + thisid + ');">' +
                                '        <button class="btn btn-sm btn-info">' +
                                '            <i class="fa fa-plus"></i>&nbsp;&nbsp;Add Project Note' +
                                '        </button>' +
                                '    </a>' +
                                '</div>' +
                                '<div class="row hidden margin-top-5"></div>'
                            );
                            if (result.notes != 'undefined' && result.notes.length > 0) {
                                $("#load-notes-data .metric .number").html(result.notes.length);
                                for (let i = 0; i < result.notes.length; i++) {
                                    if (result.notes[i].tasks_id == 0) {
                                        let nDateUp = result.notes[i].date_updated.slice(0, result.notes[i].date_updated.indexOf(' '));
                                        let nTimeUp = tConvert(result.notes[i].date_updated.slice(result.notes[i].date_updated.lastIndexOf(' ')+1));
                                        $('#projectcontent .row').append(
                                            '<div class="col-sm-12">' +
                                            '<div class="well well_' + result.notes[i].id + ' note-card relative"' +
                                            ' style="min-height:47px;">' +
                                            '<a href="javascript:;" onclick="editProjectNote(' + result.notes[i].id + ');">' +
                                            '<h4><i class="fa fa-pencil"></i>&nbsp; <strong>Last Updated: ' +
                                            nDateUp + ' ' + nTimeUp + ' (' + result.notes[i].users_id + ')' +
                                            '</strong></h4></a>' +
                                            '<p class="service-note">' +
                                            (result.notes[i].note !== 'undefined' ? result.notes[i].note : '') +
                                            '</p>' +
                                            '<i class="fa fa-trash pull-right client-content-well-trash" ' +
                                            'onclick="deleteProjectNote(' + result.notes[i].id + ');"></i>' +
                                            '</div>' +
                                            '</div>'
                                        );
                                    }
                                }
                            } else {
                                $('#projectcontent .row').append('<div class="col-sm-12">No Notes</div>');
                            }
                            $('#projectcontentadd').html(
                                '<a href="javascript:;" id="add-note" onclick="addProjectNote(' + thisid + ');">' +
                                '    <button class="btn btn-sm btn-info">' +
                                '        <i class="fa fa-plus"></i>&nbsp;&nbsp;Add Project Note' +
                                '    </button>' +
                                '</a>'
                            );
                            $('#projectcontent .row').removeClass("hidden");
                            scrollToView("projectcontent");
                        });
                    });
                    $('#load-tasks-data').click(function() {
                        $('.project-panel-body .metric').css("color", "#3287B2").css("background-color", "#ffffff");
                        $('#load-tasks-data .metric').css("color", "#ffffff").css("background-color", "#2B333E");
                        let thisid = $(this).attr("data-id");
                        $('#projectcontent').html(
                            '<div class="overlay" style="text-align:center;">\n' +
                            '<img class="loading" src="/assets/img/loader.gif" alt="Loading" /><br/>\n' +
                            '<h2>Loading the Tasks Data...</h2>\n' +
                            '</div>'
                        );
                        let tasksHTML = '';
                        let tuUrl = "<?php echo base_url(); ?>projects/loadProjectsUsers/" + thisid;
                        $.ajax(tuUrl).done(function(data) {
                            let result = JSON.parse(data);
                            $('#projectcontent').append(
                                '<div class="text-right">' +
                                '    <a href="javascript:;" id="add-task-top" onclick="addProjectTask();">' +
                                '        <button class="btn btn-sm btn-info">' +
                                '            <i class="fa fa-plus"></i>&nbsp;&nbsp;Add Task' +
                                '        </button>' +
                                '    </a>' +
                                '</div>' +
                                '<div class="row hidden margin-top-5 sTreeWrap" id="sTreeWrap"></div>'
                            );
                            if (result != 'undefined' && result.length > 0) {
                                for (let i = 0; i < result.length; i++) {
                                    let thisUserId = result[i].users_id;
                                    let udUrl = "<?php echo base_url(); ?>users/getUserDetailsById/" + thisUserId;
                                    $.ajax(udUrl).done(function (data) {
                                        let uResult = JSON.parse(data);
                                        /*
                                        activation_code: null
                                        activation_selector: null
                                        active: "1"
                                        company: "ELYK Innovation, Inc."
                                        created_on: "1607629122"
                                        email: "cedwards@elykinnovation.com"
                                        first_name: "Chris"
                                        forgotten_password_code: null
                                        forgotten_password_selector: null
                                        forgotten_password_time: null
                                        id: "5"
                                        ip_address: "173.12.142.236"
                                        last_login: "1608319381"
                                        last_name: "Edwards"
                                        password: "$2y$10$1CXnHXpXAlvv2pkAzGA7duE11N9vq4dIPLncgGmuh/qKL.E9DJIhu"
                                        phone: "9048685134"
                                        remember_code: null
                                        remember_selector: null
                                        username: "cedwards"
                                        */
                                        $("#sTreeWrap").append(
                                            '    <div class="row flex-row flex-sm-nowrap py-3">' +
                                            '        <div class="col-xs-12">' +
                                            '            <div class="card bg-light margin-bottom-30">' +
                                            '                <div class="card-body card-column" id="user_' + thisUserId + '">' +
                                            '                    <div class="clearfix">' +
                                            '                        <div class="card-h4 pull-left" style="width:50%;">' +
                                            '                            <h4 class="card-title text-uppercase text-truncate py-2">' + uResult[0].first_name + '\'s Tasks</h4>' +
                                            '                        </div>' +
                                            '                        <div class="pull-right" style="width:50%;">' +
                                            '                            <button class="btn btn-primary btn-sm btn-xxs pull-right text-right">Add Task to ' + uResult[0].first_name + '</button>' +
                                            '                        </div>' +
                                            '                    </div>' +
                                            '                    <div class="items border border-light">' +
                                            '                        <div class="dropzone rounded" ondrop="drop(event)" ondragover="allowDrop(event)" ondragleave="clearDrop(event)"> &nbsp; </div>' +
                                            '                    </div>' +
                                            '                </div>' +
                                            '            </div>' +
                                            '        </div>' +
                                            '    </div>'
                                        );

                                        let tbuUrl = "<?php echo base_url(); ?>projects/loadTasksDataByUser/" + thisid + "/" + thisUserId;
                                        $.ajax(tbuUrl).done(function (data) {
                                            let tbuResult = JSON.parse(data);
                                            if (tbuResult != 'undefined' && tbuResult.tasks.length > 0) {
                                                //console.log(tbuResult.tasks);
                                                /*
                                                Array:
                                                    archived: "0"
                                                    calendar_date: "2021-06-16"
                                                    created_timestamp: "2021-06-03 13:02:12"
                                                    date_modified: "2021-07-29 19:18:25"
                                                    deadline: "2021-06-16"
                                                    id: "3"
                                                    is_completed: "0"
                                                    is_priority: "1"
                                                    out_of_scope: "0"
                                                    projects_id: "15"
                                                    sort_order: "2"
                                                    task: "Joe's Task"
                                                    time_estimate: null
                                                    users_id: "4"
                                                    users_id_created_by: "1"
                                                    users_id_updated_by: "1"
                                                    weighted: "0"
                                                */
                                                for (let i = 0; i < tbuResult.tasks.length; i++) {
                                                    if (tbuResult.tasks[i].users_id_created_by) {
                                                        let cbUsersImage = '';
                                                        let cbUsersFirstName = '';
                                                        let cbUsersLastName = '';
                                                        let cbdfUrl = "<?php echo base_url(); ?>users/getUserDefaultFile/" + tbuResult.tasks[i].users_id_created_by;
                                                        $.ajax(cbdfUrl).done(function (data) {
                                                            let createdByFileResult = JSON.parse(data);
                                                            //console.log(createdByFileResult);
                                                            /*
                                                            activation_code: null
                                                            activation_selector: null
                                                            active: "1"
                                                            client_name: "jlemire.png"
                                                            company: "ELYK Innovation, Inc."
                                                            created_on: "1607629187"
                                                            email: "jlemire@elykinnovation.com"
                                                            file_ext: ".png"
                                                            file_name: "jlemire.png"
                                                            file_path: "/home/bonfire4/public_html/bp_uploads/"
                                                            file_size: "4.39"
                                                            file_type: "image/png"
                                                            first_name: "Joe"
                                                            forgotten_password_code: null
                                                            forgotten_password_selector: null
                                                            forgotten_password_time: null
                                                            full_path: "/home/bonfire4/public_html/bp_uploads/jlemire.png"
                                                            id: "6"
                                                            image_height: "40"
                                                            image_size_str: "width=\"40\" height=\"40\""
                                                            image_type: "png"
                                                            image_width: "40"
                                                            ip_address: "173.12.142.236"
                                                            is_default: "1"
                                                            is_image: "1"
                                                            last_login: "1608122420"
                                                            last_name: "Lemire"
                                                            orig_name: "jlemire.png"
                                                            password: "$2y$10$DQTYwbxS2z.39tY8HsOMF.nNUqT1sAqelcbwD/K30hkGxqgOU6WL."
                                                            phone: "9044222823"
                                                            raw_name: "jlemire"
                                                            remember_code: null
                                                            remember_selector: null
                                                            userid: "6"
                                                            username: "jlemire"
                                                        */
                                                            let cbDefaultFile = '';
                                                            if (createdByFileResult.length > 0) {
                                                                for (let i = 0; i < createdByFileResult.length; i++) {
                                                                    cbDefaultFile = createdByFileResult[0].file_name;
                                                                    cbUsersFirstName = createdByFileResult[0].first_name;
                                                                    cbUsersLastName = createdByFileResult[0].last_name;
                                                                }
                                                                if (cbDefaultFile != 'undefined') {
                                                                    cbUsersImage = '<?php echo base_url('bp_uploads/'); ?>' + cbDefaultFile;
                                                                } else {
                                                                    cbUsersImage = '<?php echo base_url('assets/klorofil/img/no-user-file.png'); ?>';
                                                                }
                                                            }

                                                            let thisTID = tbuResult.tasks[i].id;
                                                            let tDateUp = tbuResult.tasks[i].date_modified.slice(0, tbuResult.tasks[i].date_modified.indexOf(' '));
                                                            let tTimeUp = tConvert(tbuResult.tasks[i].date_modified.slice(tbuResult.tasks[i].date_modified.lastIndexOf(' ') + 1));
                                                            $("#user_" + thisUserId + " .items").append(
                                                                '                        <div class="card draggable shadow-sm" id="t_' + thisTID + '" draggable="true" ondragstart="drag(event)">' +
                                                                '                            <div class="card-body p-2">' +
                                                                '                                <div class="padding-left-5 padding-bottom-10 border-bottom-1 margin-bottom-10 task-title">' +
                                                                tbuResult.tasks[i].task +
                                                                '                                </div>' +
                                                                '                                <div class="card-info padding-bottom-1">' +
                                                                '                                    <div class="card-handle">' +
                                                                '                                        <i class="lnr lnr-move" title="Drag N Drop"></i>' +
                                                                '                                    </div>' +
                                                                '                                    <div class="card-complete">' +
                                                                '                                        <input type="checkbox"' +
                                                                (tbuResult.tasks[i].is_completed == 1 ? ' checked' : '') +
                                                                ' onclick="toggleTaskComplete(' + thisTID + ',this.checked);"' +
                                                                ' title="Set Task Complete" />' +
                                                                '                                    </div>' +
                                                                '                                    <div class="card-id" title="Task ID">' +
                                                                '                                        T0000' + tbuResult.tasks[i].id +
                                                                '                                    </div>' +
                                                                '                                    <div class="card-scope">' +
                                                                '                                        <i class="lnr lnr-warning' +
                                                                (tbuResult.tasks[i].out_of_scope != '0' ? ' not" title="Out of Scope' : '" title="In Scope') +
                                                                '"></i>' +
                                                                '                                    </div>' +
                                                                '                                    <div class="card-priority">' +
                                                                '                                        PRI&nbsp;&nbsp;<input type="checkbox"' +
                                                                (tbuResult.tasks[i].is_priority == 1 ? ' checked' : '') +
                                                                ' onclick="toggleTaskPriority(' + thisTID + ',this.checked);"' +
                                                                ' title="Set as Priority" />' +
                                                                '                                    </div>' +
                                                                //'                                    <div class="card-title hide-below-768">' +
                                                                //'                                        ' + tbuResult.tasks[i].task +
                                                                //'                                    </div>' +
                                                                '                                    <div class="card-archive cursor-pointer pull-right">' +
                                                                '                                        <i class="fa fa-minus-circle" title="Archive Task"></i>' +
                                                                '                                    </div>' +
                                                                '                                    <div class="card-edit pull-right">' +
                                                                '                                        <i class="fa fa-pencil cursor-pointer pull-right" title="Edit Task"' +
                                                                '                                           onclick="editProjectTask(' + tbuResult.tasks[i].id + ');">' +
                                                                '                                        </i>' +
                                                                '                                    </div>' +
                                                                '                                    <div class="card-deadline pull-right" title="Task Deadline">' +
                                                                '                                        ' + tbuResult.tasks[i].deadline +
                                                                '                                    </div>' +
                                                                '                                    <div class="card-estimate pull-right" title="Task Estimate">' +
                                                                '                                        ' +
                                                                (!tbuResult.tasks[i].time_estimate ? '--' : tbuResult.tasks[i].time_estimate) +
                                                                '                                    </div>' +
                                                                '                                    <div class="card-created pull-right">' +
                                                                '                                        <span class="card-created-img">' + // tbuResult.tasks[i].users_id_updated_by
                                                                '                                            <img src="' + cbUsersImage + '" title="Created by: ' + cbUsersFirstName + ' ' + cbUsersLastName + '" width="30" height="30">' +
                                                                '                                        </span>' +
                                                                '                                    </div>' +
                                                                '                                    <div class="card-updated pull-right">' +
                                                                '                                        <span class="card-updated-img">' +
                                                                '                                            <img src="" title="" width="30" height="30">' +
                                                                '                                        </span>' +
                                                                '                                    </div>' +
                                                                '                                </div>' +
                                                                '                                <div class="card-notes">' +
                                                                //'                                    <div class="margin-right-50"></div>' +
                                                                //'                                    <button class="btn btn-warning btn-sm btn-xxs pull-right margin-left-10">Archive</button>' +
                                                                //'                                    <button class="btn btn-primary btn-sm btn-xxs pull-right margin-left-10">Edit</button>' +
                                                                '                                </div>' +
                                                                '                            </div>' +
                                                                '                        </div>' +
                                                                '                        <div class="dropzone rounded" ondrop="drop(event)" ondragover="allowDrop(event)" ondragleave="clearDrop(event)"> &nbsp; </div>'
                                                            );

                                                            let tnUrl = "<?php echo base_url(); ?>projects/loadTaskNotesData/" + thisTID;
                                                            $.ajax(tnUrl).done(function (data) {
                                                                let notesResults = JSON.parse(data);
                                                                //console.log(notesResults.notes);
                                                                let taskNotesHTML = '';
                                                                if (notesResults.notes !== 'undefined') {
                                                                    for (let i = 0; i < notesResults.notes.length; i++) {
                                                                        /*
                                                                        date: "2021-09-22 11:49:57"
                                                                        date_updated: "2021-09-22 11:49:57"
                                                                        id: "13"
                                                                        note: "This is a task specific note."
                                                                        parent_notes_id: "0"
                                                                        projects_id: "15"
                                                                        tasks_id: "1"
                                                                        users_id: "5"
                                                                        */
                                                                        //console.log(notesResults.notes[i]);
                                                                        taskNotesHTML += '<div class="full-width" id="tn_' + notesResults.notes[i].id + '">' +
                                                                        '<div class="full-width pull-left margin-bottom-10">' +
                                                                        '<a href="javascript:;" onclick="editTaskNote(' + notesResults.notes[i].id + ');">' +
                                                                        '<h5><i class="fa fa-pencil"></i>&nbsp; <strong>User ' +
                                                                        notesResults.notes[i].users_id + ' (' +
                                                                        notesResults.notes[i].date_updated.slice(0, 10) +
                                                                        ')' +
                                                                        '</strong></h5></a>' +
                                                                        notesResults.notes[i].note + '</div>' +
                                                                        '<div class="full-width pull-right margin-top-5">' +
                                                                        (
                                                                            projectAccess > 3 ?
                                                                            '<button onclick="deleteTaskNote(' + thisid + ',' + notesResults.notes[i].id + ');" ' +
                                                                            'class="btn btn-danger btn-sm btn-xxs pull-right margin-left-10">Delete</button>' :
                                                                            ''
                                                                        ) +
                                                                        '<button onclick="addTaskNote(' + thisid + ',' + thisTID + ');" ' +
                                                                        'class="btn btn-primary btn-sm btn-xxs pull-right margin-left-10">Reply</button>' +
                                                                        '</div>' +
                                                                        '</div>';

                                                                    }
                                                                } else {
                                                                    taskNotesHTML += 'No Notes' +
                                                                    '<div class="pull-right margin-top-5">' +
                                                                    '<button onclick="addTaskNote(' + thisid + ',' + thisTID + ');" ' +
                                                                    'class="btn btn-primary btn-sm btn-xxs pull-right margin-left-10">Add Note</button>' +
                                                                    '</div>';
                                                                    //$("#t_" + thisTID + " .card-notes").html("No Notes");
                                                                }
                                                                setTimeout(function() {
                                                                    $("#t_" + thisTID + " .card-notes").html(taskNotesHTML);
                                                                }, 100);
                                                            });

                                                            let ubdfUrl = "<?php echo base_url(); ?>users/getUserDefaultFile/" + tbuResult.tasks[i].users_id_updated_by;
                                                            $.ajax(ubdfUrl).done(function (data) {
                                                                let updatedByFileResult = JSON.parse(data);
                                                                let ubDefaultFile = '';
                                                                let ubUsersImage = '';
                                                                let ubUsersFirstName = '';
                                                                let ubUsersLastName = '';
                                                                //console.log(updatedByFileResult);
                                                                if (updatedByFileResult.length > 0) {
                                                                    for (let i = 0; i < updatedByFileResult.length; i++) {
                                                                        ubDefaultFile = updatedByFileResult[0].file_name;
                                                                        ubUsersFirstName = updatedByFileResult[0].first_name;
                                                                        ubUsersLastName = updatedByFileResult[0].last_name;
                                                                    }
                                                                    if (ubDefaultFile != 'undefined') {
                                                                        ubUsersImage = '<?php echo base_url('bp_uploads/'); ?>' + ubDefaultFile;
                                                                    } else {
                                                                        ubUsersImage = '<?php echo base_url('assets/klorofil/img/no-user-file.png'); ?>';
                                                                    }
                                                                    $("#t_" + thisTID + " .card-updated .card-updated-img img").attr(
                                                                        "src", ubUsersImage
                                                                    ).attr("title", "Updated by: " + ubUsersFirstName + " " + ubUsersLastName);
                                                                }
                                                            });
                                                        });
                                                    }
                                                }
                                            }
                                        });
                                    });
                                }
                            } else {
                                $("#sTreeWrap").append('<div class="col-sm-12">No Tasks</div>');
                            }
                            $('#projectcontentadd').html(
                                '<a href="javascript:;" id="add-task-bottom" onclick="addProjectTask();">' +
                                '    <button class="btn btn-sm btn-info">' +
                                '        <i class="fa fa-plus"></i>&nbsp;&nbsp;Add Task' +
                                '    </button>' +
                                '</a>'
                            );
                            //runSortableTasksFunctions();
                            $('#projectcontent .overlay').remove();
                            $('#projectcontent .row').removeClass("hidden");
                            scrollToView("projectcontent");
                        });
                    });
                    $('#load-developers-data').click(function() {
                        swal("Coming Soon");
                    });
                    /*$("#contactAddEmail").on('click', function() {
                        $("#editEmailsGroup").append(
                            '<p class="form-group input-group relative">\n' +
                            '    <span class="input-group-addon"><i class="fas fa-envelope" title="Email"></i></span>\n' +
                            '    <input type="text" name="ce_emails[]" class="form-control c-input ce-email">\n' +
                            '    <span class="input-group-btn">\n' +
                            '        <button class="btn btn-danger modal-email-delete" type="button">Delete</button>\n' +
                            '    </span>\n' +
                            '</p>'
                        );
                        setModalEmailDeleteOnClick();
                    });
                    $("#editAddEmail").on('click', function() {
                        $("#addEmailsGroup").append(
                            '<p class="form-group input-group relative">\n' +
                            '    <span class="input-group-addon"><i class="fas fa-envelope" title="Email"></i></span>\n' +
                            '    <input type="text" name="ce_emails[]" class="form-control c-input ce-email">\n' +
                            '    <span class="input-group-btn">\n' +
                            '        <button class="btn btn-danger modal-email-delete" type="button">Delete</button>\n' +
                            '    </span>\n' +
                            '</p>'
                        );
                        setModalEmailDeleteOnClick();
                    });*/
                    /*$("#c_firstname").on('keyup blur', function(){
                        if ($(this).val().length > 2) {
                            $("#addContactSubmit").removeClass("disabled").removeAttr("disabled");
                        } else {
                            $("#addContactSubmit").addClass("disabled").attr("disabled", "disabled");
                        }
                    });*/
                    /*$(".p-selection").each(function() {
                        $(this).on('change', function() {
                            checkAllAPSelects();
                        });
                    });*/
                });
                /*function sortableTasks1() {
                    var options = {
                        placeholderCss: {'background-color': '#ff8'},
                        hintCss: {'background-color':'#bbf'},
                        onChange: function(cEl) {
                            // nothing here yet
                        },
                        complete: function(cEl) {
                            // console.log($('#sTree1').sortableListsToArray());
                        },
                        maxLevels: 1,
                        ignoreClass: 'clickable'
                    };
                    $('#sTree1').sortableLists(options);
                    $('.clickable').on('click', function(e)	{
                        // swal('Click works fine!');
                    });
                }
                function sortableTasks2() {
                    var options = {
                        placeholderCss: {'background-color': '#ff8'},
                        hintCss: {'background-color':'#bbf'},
                        onChange: function(cEl) {
                            // nothing here yet
                        },
                        complete: function(cEl) {
                            // console.log($('#sTree2').sortableListsToArray());
                        },
                        maxLevels: 1,
                        ignoreClass: 'clickable'
                    };
                    $('#sTree2').sortableLists(options);
                    $('.clickable').on('click', function(e)	{
                        // swal('Click works fine!');
                    });
                }
                function sortableTasks3() {
                    var options = {
                        placeholderCss: {'background-color': '#ff8'},
                        hintCss: {'background-color':'#bbf'},
                        onChange: function(cEl) {
                            // nothing here yet
                        },
                        complete: function(cEl) {
                            // console.log($('#sTree3').sortableListsToArray());
                        },
                        maxLevels: 1,
                        ignoreClass: 'clickable'
                    };
                    $('#sTree3').sortableLists(options);
                    $('.clickable').on('click', function(e)	{
                        // swal('Click works fine!');
                    });
                }
                function sortableTasks4() {
                    var options = {
                        placeholderCss: {'background-color': '#ff8'},
                        hintCss: {'background-color':'#bbf'},
                        onChange: function(cEl) {
                            // nothing here yet
                        },
                        complete: function(cEl) {
                            // console.log($('#sTree4').sortableListsToArray());
                        },
                        maxLevels: 1,
                        ignoreClass: 'clickable'
                    };
                    $('#sTree4').sortableLists(options);
                    $('.clickable').on('click', function(e)	{
                        // swal('Click works fine!');
                    });
                }
                function sortableTasks5() {
                    var options = {
                        placeholderCss: {'background-color': '#ff8'},
                        hintCss: {'background-color':'#bbf'},
                        onChange: function(cEl) {
                            // nothing here yet
                        },
                        complete: function(cEl) {
                            // console.log($('#sTree5').sortableListsToArray());
                        },
                        maxLevels: 1,
                        ignoreClass: 'clickable'
                    };
                    $('#sTree5').sortableLists(options);
                    $('.clickable').on('click', function(e)	{
                        // swal('Click works fine!');
                    });
                }
                function sortableTasks6() {
                    var options = {
                        placeholderCss: {'background-color': '#ff8'},
                        hintCss: {'background-color':'#bbf'},
                        onChange: function(cEl) {
                            // nothing here yet
                        },
                        complete: function(cEl) {
                            // console.log($('#sTree6').sortableListsToArray());
                        },
                        maxLevels: 1,
                        ignoreClass: 'clickable'
                    };
                    $('#sTree6').sortableLists(options);
                    $('.clickable').on('click', function(e)	{
                        // swal('Click works fine!');
                    });
                }
                function sortableTasks7() {
                    var options = {
                        placeholderCss: {'background-color': '#ff8'},
                        hintCss: {'background-color':'#bbf'},
                        onChange: function(cEl) {
                            // nothing here yet
                        },
                        complete: function(cEl) {
                            // console.log($('#sTree7').sortableListsToArray());
                        },
                        maxLevels: 1,
                        ignoreClass: 'clickable'
                    };
                    $('#sTree7').sortableLists(options);
                    $('.clickable').on('click', function(e)	{
                        // swal('Click works fine!');
                    });
                }
                function sortableTasks8() {
                    var options = {
                        placeholderCss: {'background-color': '#ff8'},
                        hintCss: {'background-color':'#bbf'},
                        onChange: function(cEl) {
                            // nothing here yet
                        },
                        complete: function(cEl) {
                            // console.log($('#sTree8').sortableListsToArray());
                        },
                        maxLevels: 1,
                        ignoreClass: 'clickable'
                    };
                    $('#sTree8').sortableLists(options);
                    $('.clickable').on('click', function(e)	{
                        // swal('Click works fine!');
                    });
                }
                function sortableTasks9() {
                    var options = {
                        placeholderCss: {'background-color': '#ff8'},
                        hintCss: {'background-color':'#bbf'},
                        onChange: function(cEl) {
                            // nothing here yet
                        },
                        complete: function(cEl) {
                            // console.log($('#sTree9').sortableListsToArray());
                        },
                        maxLevels: 1,
                        ignoreClass: 'clickable'
                    };
                    $('#sTree9').sortableLists(options);
                    $('.clickable').on('click', function(e)	{
                        // swal('Click works fine!');
                    });
                }
                function sortableTasks10() {
                    var options = {
                        placeholderCss: {'background-color': '#ff8'},
                        hintCss: {'background-color':'#bbf'},
                        onChange: function(cEl) {
                            // nothing here yet
                        },
                        complete: function(cEl) {
                            // console.log($('#sTree10').sortableListsToArray());
                        },
                        maxLevels: 1,
                        ignoreClass: 'clickable'
                    };
                    $('#sTree10').sortableLists(options);
                    $('.clickable').on('click', function(e)	{
                        // swal('Click works fine!');
                    });
                }
                sortableTasks1();
                sortableTasks2();
                sortableTasks3();
                sortableTasks4();
                sortableTasks5();
                sortableTasks6();
                sortableTasks7();
                sortableTasks8();
                sortableTasks9();
                sortableTasks10();
                function runSortableTasksFunctions() {
                    sortableTasks1();
                    sortableTasks2();
                    sortableTasks3();
                    sortableTasks4();
                    sortableTasks5();
                    sortableTasks6();
                    sortableTasks7();
                    sortableTasks8();
                    sortableTasks9();
                    sortableTasks10();
                }*/
            </script>
