<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Projects extends MY_Controller
{
	/* BF3 Lazy Load Statuses
	const NEW_PROJECT=1; // Green
	const URGENT=2; // Red
	const INPROGRESS=3; // Dark Blue
	const COMPLETE=4; // Orange
	const ARCHIVE=5; // Purple (BF3) / Dark Grey (BF4)
	const PENDING=6; // Light Blue
	*/

	function __construct()
	{
		parent::__construct();
		// Get site wide settings first
		$this->Settings_model->loadSitewideSettings();

		// Load This Model
		$this->load->model('Projects_model');

		// Make sure the user is logged in for this module
		if (!$this->session->userdata('user_id'))
		{
			// set the url they were trtying to go to in session
			$this->session->set_userdata('page_url', current_url());

			//Redirect to login
			redirect('user/login');
		}
	}

	public function admin_index()
	{
		$this->data = array();
		$this->data['title'] = 'Projects';

		// load the config from the current module
		$this->load->config('projects');
		$this->data['projects_config'] = $this->config->config['projects'];

		// Get projects, not archived
		$projects = $this->Projects_model->getActiveProjects();
		$this->data['projects'] = $projects;

		// Added for developer dropdown
		$this->data['dev_data']['projects'] = $projects;

		$this->load->view('layout/admin/header.php', $this->data);
		$this->load->view('admin/index', $this->data);
		$this->load->view('layout/admin/footer.php', $this->data);
	}

	public function add()
	{
		if ($this->input->post()) {
			$projectinfo = array(
				'clients_id' => $this->input->post('clientid'),
				'project_statuses_id' => $this->input->post('p_status'),
				'categories_id' => $this->input->post('p_category'),
				'domains_id' => $this->input->post('p_domain'),
				'contract_info' => $this->input->post('p_contract_info'),
				'time_estimate' => $this->input->post('p_estimate'),
			);
			$this->db->insert($this->db->dbprefix . "projects", $projectinfo);
			$projectid = $this->db->insert_id();
			foreach ($this->input->post('p_members') as $m => $member) {
				$data = array(
					'users_id' => $member,
					'projects_id' => $projectid,
					'last_accessed' => NULL,
					'saw_changes' => 0,
					'priority' => 0,
				);
				$this->db->insert($this->db->dbprefix . "users_to_projects", $data);
			}
			return true;
		}

		$this->data = array();
		$this->data['title'] = 'Projects Add';

		$this->load->view('layout/admin/header.php', $this->data);
		$this->load->view('admin/add', $this->data);
		$this->load->view('layout/admin/footer.php', $this->data);
	}

	public function edit()
	{
        $projid = $this->uri->segment(4);

		$this->data = array();
		$this->data['title'] = 'Projects Edit';

		$this->db->where("id", $projid);
		$project = $this->db->get($this->db->dbprefix . "projects");
		$this->data['project'] = $project->result_array();

		// added for developer dropdown
		$this->data['dev_data']['project'] = $project->result_array();

		$this->load->view('layout/admin/header.php', $this->data);
		$this->load->view('admin/edit', $this->data);
		$this->load->view('layout/admin/footer.php', $this->data);
	}

	public function archived()
	{
        $this->data = array();
		$this->data['title'] = 'Archived Projects';

		$this->db->where("project_statuses_id", 5);
		$projects = $this->db->get($this->db->dbprefix . "projects");
		$this->data['projects'] = $projects->result_array();

		// added for developer dropdown
		$this->data['dev_data']['projects'] = $projects->result_array();

		$this->load->view('layout/admin/header.php', $this->data);
		$this->load->view('admin/archived', $this->data);
		$this->load->view('layout/admin/footer.php', $this->data);
	}

	public function delete() // ONLY TOP DEVELOPER, DANGEROUS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	{
		/*$clientid = $this->uri->segment(4);
		if (!$clientid) $clientid = $this->uri->segment(3);

		// first delete all the related data
		$this->Clients_model->deleteClientAuditTrail($clientid);
		$this->Clients_model->deleteClientLogins($clientid);
		$this->Clients_model->deleteClientNotes($clientid);
		$this->Clients_model->deleteClientContacts($clientid);
		$this->Clients_model->deleteClientDomains($clientid);
		// priorities
		// project links
		// project notes
		// project tasks
		// users to projects
		$this->Clients_model->deleteClientProjects($clientid);
		$this->Clients_model->deleteClientServices($clientid);
		// time records to tasks
		$this->Clients_model->deleteClientTimeRecords($clientid);
		// users
		// users files

		if (!$this->Clients_model->delete($clientid)) {
			// Set the error to the flashdata array:
			$this->session->set_flashdata(array("msg" => "Error Deleting the Client!", "msgtype" => "danger"));
		} else {
			// Add Audit Trail
			$this->load->model('Audit_model');
			$this->Audit_model->addAuditLog(
				$this->config->config['settings']['auditTypeUserLogout'],
				'/client/delete/' . $clientid,
				'Client Deleted',
				'clients',
				$this->session->userdata('user_id')
			);
			// Set the success message to the flashdata array:
			$this->session->set_flashdata(array("msg" => "Client Successfully Deleted!", "msgtype" => "success"));
		}
		// Redirect to Clients Page
		redirect('/admin/clients/','refresh');*/
	} // ONLY TOP DEVELOPER, DANGEROUS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	public function loadProjectData()
	{
		$projectid = $this->uri->segment(3);

		// Project Info
		$this->db->where("id", $projectid);
		$this->db->limit(1);
		$project = $this->db->get($this->db->dbprefix . "projects")->result_array();

		// Client Info
		$this->db->where("id", $project[0]['clients_id']);
		$this->db->limit(1);
		$client = $this->db->get($this->db->dbprefix . "clients")->result_array();

		// Notes Info
		$this->db->where("projects_id", $projectid);
		$notes = $this->db->get($this->db->dbprefix . "project_notes")->result_array();
		if (empty($notes)) $notes = 'undefined';

		// Tasks Info
		$this->db->where("projects_id", $projectid);
		$tasks = $this->db->get($this->db->dbprefix . "tasks")->result_array();
		if (empty($tasks)) $tasks = 'undefined';

		// Links Info
		$this->db->where("projects_id", $projectid);
		$links = $this->db->get($this->db->dbprefix . "project_links")->result_array();
		if (empty($links)) $links = 'undefined';

		// Developers (Assigned to this project)
        $this->db->distinct();
        $this->db->where("projects_id", $projectid);
        $developers = $this->db->get($this->db->dbprefix . "users_to_projects")->result_array();
        if (empty($developers)) $developers = 'undefined';

        // Team Members
        $this->db->where("user_id !=", 1); // Top Developer
        $this->db->where("user_id !=", 2); // Admin User
        $this->db->where("group_id", 3);
		$team_members = $this->db->get($this->db->dbprefix . "users_groups")->result_array();
		if (empty($team_members)) $team_members = 'undefined';

		$data = array(
			'id' => $project[0]['id'],
			'client_id' => $project[0]['clients_id'],
			'status' => $project[0]['project_statuses_id'],
			'client_name' => ($client[0]['client_name'] != '' ? $client[0]['client_name'] : 'undefined'),
			'notes' => $notes,
			'tasks' => $tasks,
			'links' => $links,
            'developers' => $developers,
            'team_members' => $team_members
		);

		echo json_encode($data);
	}

    public function loadNotesData()
    {
        $projectid = $this->uri->segment(3);

        // Notes Info
        $this->db->where("projects_id", $projectid);
        $notes = $this->db->get($this->db->dbprefix . "project_notes")->result_array();
        if (empty($notes)) $notes = 'undefined';

        $data = array(
            'notes' => $notes,
        );

        echo json_encode($data);
    }

    public function addNote()
    {
        if (!$this->Projects_model->addNote($this->input->post())) {
            return false;
        } else {
            // Add Audit Trail
            $this->load->model('Audit_model');
            $this->Audit_model->addAuditLog(
                $this->config->config['settings']['auditTypeRecordCreated'],
                '/projects/addNote/',
                'Project Note Added',
                'projects',
                $this->session->userdata('user_id')
            );
            return true;
        }
    }

    public function editNote()
    {
        $noteid = $this->uri->segment(3);

        if (!$this->Projects_model->editNote($this->input->post())) {
            return false;
        } else {
            // Add Audit Trail
            $this->load->model('Audit_model');
            $this->Audit_model->addAuditLog(
                $this->config->config['settings']['auditTypeRecordUpdated'],
                '/projects/editNote/' . $noteid,
                'Project Note Edited',
                'projects',
                $this->session->userdata('user_id')
            );
            return true;
        }
        return true;
    }

	public function deleteNote()
    {
        $id = $this->uri->segment(3);

        if (!$this->Projects_model->deleteNote($id)) {
            echo json_encode(array("msg" => "failure"));
        } else {
            // Add Audit Trail
            $this->load->model('Audit_model');
            $this->Audit_model->addAuditLog(
                $this->config->config['settings']['auditTypeRecordDeleted'],
                '/projects/deleteNote/' . $id,
                'Project Note Deleted',
                'projects',
                $this->session->userdata('user_id')
            );
            echo json_encode(array("msg" => "success"));
        }
    }

    public function loadLinksData()
    {
        $projectid = $this->uri->segment(3);

        // Links Info
        $this->db->where("projects_id", $projectid);
        $links = $this->db->get($this->db->dbprefix . "project_links")->result_array();
        if (empty($links)) $links = 'undefined';

        $data = array(
            'links' => $links
        );

        echo json_encode($data);
    }

    public function addLink()
    {
        if (!$this->Projects_model->addLink($this->input->post())) {
            return false;
        } else {
            // Add Audit Trail
            $this->load->model('Audit_model');
            $this->Audit_model->addAuditLog(
                $this->config->config['settings']['auditTypeRecordCreated'],
                '/projects/addLink/',
                'Project Link Added',
                'projects',
                $this->session->userdata('user_id')
            );
            return true;
        }
    }

    public function editLink()
    {
        if (!$this->Projects_model->editLink($this->input->post())) {
            echo json_encode(array("msg" => "failure"));
        } else {
            // Add Audit Trail
            $this->load->model('Audit_model');
            $this->Audit_model->addAuditLog(
                $this->config->config['settings']['auditTypeRecordUpdated'],
                '/projects/editLink/' . $this->input->post('el_id'),
                'Project Link Updated',
                'projects',
                $this->session->userdata('user_id')
            );
            echo json_encode(array("msg" => "success"));
        }
        return true;
    }

    public function deleteLink()
    {
        $id = $this->uri->segment(3);

        if (!$this->Projects_model->deleteLink($id)) {
            echo json_encode(array("msg" => "failure"));
        } else {
            // Add Audit Trail
            $this->load->model('Audit_model');
            $this->Audit_model->addAuditLog(
                $this->config->config['settings']['auditTypeRecordDeleted'],
                '/projects/deleteLink/' . $id,
                'Project Link Deleted',
                'projects',
                $this->session->userdata('user_id')
            );
            echo json_encode(array("msg" => "success"));
        }
    }

    public function editProjectStatus()
    {
        $pid = $this->uri->segment(3);
        $sid = $this->uri->segment(4);

        $success = 'false';

        $this->db->set("project_statuses_id", $sid);
        $this->db->where('id', $pid);
        if ($this->db->update($this->db->dbprefix . "projects")) $success = 'true';

        // Add Audit Trail
        $this->load->model('Audit_model');
        $this->Audit_model->addAuditLog(
            $this->config->config['settings']['auditTypeRecordUpdated'],
            '/projects/editProjectStatus/',
            'Project Status Updated',
            'projects',
            $this->session->userdata('user_id')
        );

        echo json_encode(array("success" => $success));
    }

    public function loadTasksData()
    {
        $projectid = $this->uri->segment(3);

        // Tasks Info
        $this->db->where("projects_id", $projectid);
        $tasks = $this->db->get($this->db->dbprefix . "tasks")->result_array();
        if (empty($tasks)) $tasks = 'undefined';

        $data = array(
            'tasks' => $tasks
        );

        echo json_encode($data);
    }

    public function loadTasksDataByUser()
    {
        $projectid = $this->uri->segment(3);
        $userid = $this->uri->segment(4);

        // Tasks Info
        $this->db->where("projects_id", $projectid);
        $this->db->where("users_id", $userid);
        $this->db->order_by("sort_order", "ASC");
        $tasks = $this->db->get($this->db->dbprefix . "tasks")->result_array();
        if (empty($tasks)) $tasks = 'undefined';

        $data = array(
            'tasks' => $tasks
        );

        echo json_encode($data);
    }

    public function loadTaskNotesData()
    {
        $taskid = $this->uri->segment(3);

        // Task Info
        $this->db->where("tasks_id", $taskid);
        $this->db->order_by("id", "ASC");
        $notes = $this->db->get($this->db->dbprefix . "project_notes")->result_array();

        if (empty($notes)) $notes = 'undefined';

        $data = array(
            'notes' => $notes
        );

        echo json_encode($data);
    }

    public function loadSingleNoteData()
    {
        $noteid = $this->uri->segment(3);

        // Note Info
        $this->db->where("id", $noteid);
        $note = $this->db->get($this->db->dbprefix . "project_notes")->result_array();

        if (empty($note)) $note = 'undefined';

        $data = array(
            'note' => $note
        );

        echo json_encode($data);
    }

    public function loadTasksUsers()
    {
        $projectid = $this->uri->segment(3);

        // Tasks Info
        $this->db->distinct();
        $this->db->select('users_id as users_id');
        $this->db->where("projects_id", $projectid);
        $this->db->order_by("users_id", "ASC");
        $users = $this->db->get($this->db->dbprefix . "tasks")->result_array();

        echo json_encode($users);
    }

    public function loadProjectsUsers()
    {
        $projectid = $this->uri->segment(3);

        // Tasks Info
        $this->db->distinct();
        $this->db->select('users_id as users_id');
        $this->db->where("projects_id", $projectid);
        $this->db->order_by("users_id", "ASC");
        $users = $this->db->get($this->db->dbprefix . "users_to_projects")->result_array();

        echo json_encode($users);
    }

    public function toggleTaskComplete()
    {
        $taskid = $this->uri->segment(3);
        $state = $this->uri->segment(4);

        $success = 'false';

        // Tasks Info
        $this->db->where("id", $taskid);
        $this->db->set("is_completed", "$state");
        if ($this->db->update($this->db->dbprefix . "tasks")) $success = 'true';

        // Add Audit Trail
        $this->load->model('Audit_model');
        $this->Audit_model->addAuditLog(
            $this->config->config['settings']['auditTypeRecordUpdated'],
            '/projects/toggleTaskComplete/',
            'Project Tasks Completed Status Updated',
            'projects',
            $this->session->userdata('user_id')
        );

        echo json_encode(array("success" => $success));
    }

    public function toggleTaskPriority()
    {
        $taskid = $this->uri->segment(3);
        $state = $this->uri->segment(4);

        $success = 'false';

        // Tasks Info
        $this->db->where("id", $taskid);
        $this->db->set("is_priority", "$state");
        if ($this->db->update($this->db->dbprefix . "tasks")) $success = 'true';

        // Add Audit Trail
        $this->load->model('Audit_model');
        $this->Audit_model->addAuditLog(
            $this->config->config['settings']['auditTypeRecordUpdated'],
            '/projects/toggleTaskPriority/',
            'Project Tasks Priority Status Updated',
            'projects',
            $this->session->userdata('user_id')
        );

        echo json_encode(array("success" => $success));
    }

    public function dragAndDropProjectTask()
    {
        $pid = $this->uri->segment(3);
        $tid = $this->uri->segment(4);
        $uid = $this->uri->segment(5);
        $srt = $this->uri->segment(6);

        $success = 'false';

        $this->db->set("users_id", $uid);
        $this->db->set("date_modified", date("Y-m-d H:i:s"));
        $this->db->set("sort_order", $srt);
        //$this->db->set("users_id_updated_by", $this->session->userdata('user_id')); // not needed for drag n drop
        $this->db->where('id', $tid);
        $this->db->where('projects_id', $pid);
        if ($this->db->update($this->db->dbprefix . "tasks")) $success = 'true';

        // Add Audit Trail
        $this->load->model('Audit_model');
        $this->Audit_model->addAuditLog(
            $this->config->config['settings']['auditTypeRecordUpdated'],
            '/projects/dragAndDropProjectTask/',
            'Project Tasks (Drag n Drop) Updated',
            'projects',
            $this->session->userdata('user_id')
        );

        echo json_encode(array("success" => $success));
    }

    public function toggleProjectUserAssign()
    {
        $pid = $this->uri->segment(3);
        $uid = $this->uri->segment(4);
        $val = $this->uri->segment(5);

        // Remove all for this user first, then add back if "1"
        $this->db->where('projects_id', $pid);
        $this->db->where('users_id', $uid);
        $this->db->delete($this->db->dbprefix . "users_to_projects");

        if ($val == true) { // Add
            $data = array(
                'users_id' => $uid,
                'projects_id' => $pid
            );
            $this->db->insert($this->db->dbprefix . "users_to_projects", $data);
        }

        // Add Audit Trail
        $this->load->model('Audit_model');
        $this->Audit_model->addAuditLog(
            $this->config->config['settings']['auditTypeRecordUpdated'],
            '/projects/toggleProjectUserAssign/',
            'Project Assigned Users Updated',
            'projects',
            $this->session->userdata('user_id')
        );

        echo json_encode(array("success" => "true"));
    }

}
