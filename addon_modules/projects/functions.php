<?php
/* Module functions file */

// If installing the module run this function to create db table(s)
if (!function_exists('bp_installModule')) {
    function bp_installModule() {
        $CI =& get_instance();

        // Update the module status
        $CI->db->set('status', 1);
        $CI->db->where("module", "projects");
        $CI->db->update($CI->db->dbprefix . "modules");

        // Create table(s)
        $CI->db->query("
            CREATE TABLE IF NOT EXISTS `" . $CI->db->dbprefix . "projects` (
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`clients_id` int(10) UNSIGNED NOT NULL,
				`project_statuses_id` int(11) NOT NULL,
				`categories_id` int(11) NOT NULL,
				`domains_id` int(11) NOT NULL,
				`contract_info` text,
				`time_estimate` int(8) NOT NULL DEFAULT '0',
				PRIMARY KEY (`id`),
				KEY `bp_projects_clid_idx` (`clients_id`),
				KEY `bp_projects_psid_idx` (`project_statuses_id`),
				KEY `bp_projects_caid_idx` (`categories_id`),
				KEY `bp_projects_did_idx` (`domains_id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");
        $CI->db->query("
			CREATE TABLE IF NOT EXISTS `" . $CI->db->dbprefix . "project_links` (
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`projects_id` int(11) NOT NULL,
				`link` varchar(512) NOT NULL,
				`label` varchar(512) NOT NULL,
				PRIMARY KEY (`id`),
				KEY `bp_project_links_pid_idx` (`projects_id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");
        $CI->db->query("
			CREATE TABLE IF NOT EXISTS `" . $CI->db->dbprefix . "project_notes` (
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`projects_id` int(11) NOT NULL,
				`tasks_id` int(11) NOT NULL,
				`users_id` int(10) UNSIGNED NOT NULL,
				`parent_notes_id` int(11) NOT NULL,
				`note` text,
				`date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
				`date_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
				PRIMARY KEY (`id`),
				KEY `bp_project_notes_pid_idx` (`projects_id`),
				KEY `bp_project_notes_tid_idx` (`tasks_id`),
				KEY `bp_project_notes_uid_idx` (`users_id`),
				KEY `bp_project_notes_date_idx` (`date`),
				KEY `project_notes_pnid_idx` (`parent_notes_id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");
	    $CI->db->query("
			CREATE TABLE IF NOT EXISTS `" . $CI->db->dbprefix . "project_statuses` (
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`status` varchar(45) NOT NULL,
				PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");
	    $CI->db->query("
			CREATE TABLE IF NOT EXISTS `" . $CI->db->dbprefix . "users_to_projects` (
                `users_id` int(11) NOT NULL,
                `projects_id` int(11) NOT NULL,
                `last_accessed` datetime DEFAULT NULL,
                `saw_changes` tinyint(1) NOT NULL DEFAULT '0',
                `priority` int(11) NOT NULL DEFAULT '0',
                PRIMARY KEY (`users_id`,`projects_id`),
                KEY `priority` (`priority`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");

        return true;
    }
}

// If uninstalling the module run this function to remove db entries if selected
if (!function_exists('bp_uninstallModule')) {
    function bp_uninstallModule() {
        $CI =& get_instance();

        // Update the module status
        $CI->db->set('status', -1);
        $CI->db->where("module", "projects");
        $CI->db->update($CI->db->dbprefix . "modules");

        // Drop table(s)
        $CI->db->query("
            DROP TABLE IF EXISTS `" . $CI->db->dbprefix . "projects`;
        ");
        $CI->db->query("
            DROP TABLE IF EXISTS `" . $CI->db->dbprefix . "project_links`;
        ");
        $CI->db->query("
            DROP TABLE IF EXISTS `" . $CI->db->dbprefix . "project_notes`;
        ");
        $CI->db->query("
            DROP TABLE IF EXISTS `" . $CI->db->dbprefix . "project_statuses`;
        ");
        $CI->db->query("
            DROP TABLE IF EXISTS `" . $CI->db->dbprefix . "users_to_projects`;
        ");

        return true;
    }
}
