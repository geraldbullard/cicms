<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* Projects Routing */
$route['admin/projects'] = 'projects/admin_index';
$route['admin/projects/add'] = 'projects/add';
$route['admin/projects/archived'] = 'projects/archived';
$route['admin/projects/getProjects'] = 'projects/getProjects';
$route['admin/projects/edit/(:any)'] = 'projects/edit/$1';
$route['admin/projects/update/(:any)'] = 'projects/update/$1';
$route['admin/projects/delete/(:any)'] = 'projects/delete/$1';
$route['admin/projects/activate/(:any)'] = 'projects/activate/$1';
$route['admin/projects/deactivate/(:any)'] = 'projects/deactivate/$1';
$route['admin/projects/archive/(:any)'] = 'projects/archive/$1';

