<?php
/* Content module menu file */

$_menu = array(
    'url' => '/admin/contents/',
    'title' => 'Content',
    'icon' => 'lnr lnr-list',
    'access' => 1,
    'sub_items' => array(
    	// Repeat main menu since it has subs
	    'contents' => array(
		    'url' => '/admin/contents/',
		    'title' => 'Content',
		    'icon' => 'lnr lnr-list',
		    'access' => 1,
	    ),
	    'menus' => array(
		    'url' => '/admin/contents/menus/',
		    'title' => 'Menus',
		    'icon' => 'lnr lnr-menu',
		    'access' => 3,
	    ),
		'forms' => array(
			'url' => '/admin/contents/forms/',
			'title' => 'Forms',
			'icon' => 'lnr lnr-paperclip',
			'access' => 3,
		),
		'submissions' => array(
			'url' => '/admin/contents/submissions/',
			'title' => 'Submissions',
			'icon' => 'lnr lnr-inbox',
			'access' => 3,
		),
    ),
);