<?php
// Custom Contents Module Shortcodes

// Manually include the Shortcode class file for debugging
if (!class_exists('Shortcode')) {
    require_once APPPATH . 'libraries/Shortcode.php';
}

// brokenPIXEL Form Builder $_POST submission Support
function bpForms($atts) {
    $CI =& get_instance(); // Always include the CodeIgniter Instance
    // get the form data based on the shortcode id attribute
    $CI->db->where("id", $atts['id']);
    $formData = $CI->db->get($CI->db->dbprefix . "forms")->first_row();
    $success = false;
    $hideAfter = false;
    // start the output buffer here to cover html generation and $_POST
    ob_start();
    // Handle the form $_POST and actions etc...
    if (isset($_POST['verb']) && !empty($_POST['verb'])) { // if we have $_POST
        /*echo '<pre>';
        print_r($_POST); // $formData
        echo '</pre>';
        die();*/
        if ($_POST['verb'] === $formData->formVerb) {
            // Check for required field values first
            // TODO: later we will add other validations like only digits, or min/max length etc ///////////////////////
            // Start an empty $error string
            $errors = '';
            // Loop thru each form filed entry for a "required" parameter
            foreach(json_decode($formData->data) as $i => $input) {
                // If the required param is found
                if (isset($input->required)) {
                    // now check if the related $_POST field value is empty
                    if (empty($_POST[$input->name]))
                        // if empty, add to the $errors string for later
                        $errors .= '<div>' . $input->label . ' cannot be empty.</div>';
                }
            }
            // If no $errors proceed with normal functions
            if (empty($errors)) {
                // storeSubmission
                if ($formData->storeSubmission == 1) {
                    // Unset the unwanted values from $_POST
                    foreach ($_POST as $key => $val) {
                        unset($_POST['verb']);
                        unset($_POST['g-recaptcha-response']);
                    }
                    //echo json_encode($_POST); // {"fullname":"John D","subject":"Feedback","message":"Message."}
                    $subData = array(
                        'form_id' => $formData->id,
                        'user_id' => $CI->session->userdata('user_id'),
                        'data' => json_encode($_POST),
                        'created' => date("Y-m-d H:i:s"),
                    );
                    $CI->db->insert($CI->db->dbprefix . "form_submissions", $subData);
                }
                // adminNotify
                if ($formData->adminNotify == 1) {
                    $local = array("localhost","127.0.0.1","::1");
                    if (!in_array($_SERVER['REMOTE_ADDR'], $local)) {
                        $headers  = "From: " . $CI->config->item('siteName', 'settings') . " <no-reply@" . $_SERVER['HTTP_HOST'] . ">\n";
                        //$headers .= "Cc: testsite < mail@testsite.com >\n";
                        //$headers .= "X-Sender: testsite < mail@testsite.com >\n";
                        //$headers .= 'X-Mailer: PHP/' . phpversion();
                        //$headers .= "X-Priority: 1\n"; // Urgent message!
                        $headers .= "Return-Path: " . $CI->config->item('adminEmail', 'settings') . "\n"; // Return path for errors
                        $headers .= "MIME-Version: 1.0\r\n";
                        $headers .= "Content-Type: text/html; charset=iso-8859-1\n";
                        mail(
                            $CI->config->item('adminEmail', 'settings'),
                            $formData->title . " submission from " . $CI->config->item('siteName', 'settings'),
                            $formData->successMsg,
                            $headers
                        );
                    }
                }
                // notifyEmail & ccEmails
                if ($formData->notifyEmail != '') {
                    $local = array("localhost","127.0.0.1","::1");
                    if (!in_array($_SERVER['REMOTE_ADDR'], $local)) {
                        $headers  = "From: " . $CI->config->item('siteName', 'settings') . " <no-reply@" . $_SERVER['HTTP_HOST'] . ">\n";
                        if ($formData->ccEmails != '') {
                            $headers .= "Cc: " . $formData->ccEmails . "\n";
                        }
                        $headers .= "Return-Path: " . $CI->config->item('adminEmail', 'settings') . "\n";
                        $headers .= "MIME-Version: 1.0\r\n";
                        $headers .= "Content-Type: text/html; charset=iso-8859-1\n";
                        mail(
                            $CI->config->item('adminEmail', 'settings'),
                            $formData->title . " submission from " . $CI->config->item('siteName', 'settings'),
                            $formData->successMsg,
                            $headers
                        );
                    }
                }
                // redirect
                if ($formData->redirect != '') {
                    // later
                    header("Location: " . $formData->redirect);
                    exit();
                }
                // redirect
                if ($formData->successMsg != '') {
                    $success = true;
                }
                // hideAfter
                if ($formData->hideAfter != '') {
                    $hideAfter = true;
                }
            }
        }
    }

    // if success, show message
    if ($success) {
        echo '<div class="alert alert-success" role="alert"><i class="icofont-checked"></i>&nbsp; ' .
            $formData->successMsg . '</div>';
    }

    // if success, and hide form after successful submission
    if ($success && $hideAfter) {
        // show nothing, hide the form
    } else {
        // Generate the HTML for the form content if the data exists
        if (isset($formData->data) && $formData->data != '') {
            // Show the Title or not
            if ($formData->showTitle === 1) echo '<h3 class="float-left">' . $formData->title . '</h3>';
            if (!empty($errors)) {
                echo '<div class="alert alert-danger">';
                echo '<p><i class="icofont-warning"></i> Your submission has the following issues:</p>' . $errors . '</div>';
            }
            // Open the <form> tag
            echo form_open_multipart('', array('id' => $formData->formId));
            // Form Verb
            if (isset($formData->formVerb)) echo '<input type="hidden" name="verb" value="' . $formData->formVerb . '" />';
            // Show the Required Fields text
            echo '<div class="form-required float-right"><span class="required">Required Fields *</span></div>';
            // The field loop
            foreach(json_decode($formData->data) as $i => $input) {
                if ($input->type === 'autocomplete') { // TODO /////////////////////////////////////////////////////////
                    //echo 'Autocomplete Input Placeholder<br>';
                } elseif ($input->type === 'button') { // DONE /////////////////////////////////////////////////////////
                    /* JSON PARAMS
                    "type": "button",
                    "subtype": "submit",
                    "label": "Button",
                    "className": "btn btn-primary",
                    "name": "button-1638411034424",
                    "value": "Send",
                    "access": false,
                    "style": "primary"
                    */
                    echo '<div class="form-group">';
                    $extra = '';
                    if ($formData->recaptcha == 1) {
                        // set only if "reCAPCTHA" setting is true
                        $extra = array(
                            'class' => 'btn btn-info g-recaptcha', // TODO: add g-recaptcha instead of replace class ///
                            'data-sitekey' => '', // Move to Site Settings or Form Globals etc
                            'data-callback' => 'onSubmit',
                            'data-action' => 'submit'
                        );
                        ?>
                        <!-- Google (Invisible/Badge) reCAPTCHA v2 -->
                        <script src="https://www.google.com/recaptcha/api.js"></script>
                        <script>
                            // After successful return submit the form
                            function onSubmit(token) {
                                document.getElementById("<?php echo $formData->formId; ?>").submit();
                            }
                        </script>
                        <?php
                    } else {
                        $extra = array(
                            'class' => 'btn btn-primary btn-sm'
                        );
                    }
                    if ($input->subtype === 'button') {
                        echo form_button($input->name, $input->label, $extra);
                    } elseif ($input->subtype === 'submit') {
                        echo form_submit($input->name, $input->label, $extra);
                    }
                    echo '</div>';
                } elseif ($input->type === 'checkbox-group') { // DONE /////////////////////////////////////////////////
                    $required = ($input->required == true ? '<span class="required"> *</span>' : '');
                    $help = (isset($input->description) ? '<span class="tooltip-element float-right" title="' . $input->description . '">?</span>' : '');
                    echo '<div class="form-group' . (isset($input->className) ? ' ' . $input->className : null) . '">';
                    echo form_label($input->label . $required . $help, $input->name, array('class' => 'control-label full-width'));
                    echo form_error($input->name, '<div class="alert alert-danger" role="alert">', '</div>');
                    foreach ($input->values as $i => $val) {
                        echo '<div class="form-check' . ($input->inline == 1 ? '-inline' : null) . '">';
                        echo '<label class="form-check-label" for="' . $input->name . '">';
                        echo '<input type="checkbox" name="' . $input->name . (count($input->values) > 1 ? '[]' : null) . '" class="form-check-input" value="' . $val->value . '"' .
                            ($val->selected == 1 ? ' checked' : null) . '>' . $val->label;
                        echo '</label>';
                        echo '</div>';
                    }
                    echo '</div>';
                } elseif ($input->type === 'date') { // TODO ///////////////////////////////////////////////////////////
                    echo 'Date Input Placeholder<br>';
                } elseif ($input->type === 'file') { // TODO ///////////////////////////////////////////////////////////
                    /*echo '<div class="form-group">';
                    echo form_label('Upload:', 'upload', array('class' => 'control-label'));
                    echo form_error('upload', '<div class="alert alert-danger" role="alert">', '</div>');
                    echo form_upload('', '', array('class' => 'form-control btn btn-default'));
                    echo '</div>';*/
                } elseif ($input->type === 'header') { // DONE /////////////////////////////////////////////////////////
                    echo '<' . $input->subtype . '>' . $input->label . '</' . $input->subtype . '>';
                } elseif ($input->type === 'hidden') { // DONE /////////////////////////////////////////////////////////
                    echo '<input type="hidden" name="' . $input->name . '" value="' . $input->value . '" />';
                    //echo form_hidden('verb', 'contact');
                } elseif ($input->type === 'number') { // TODO /////////////////////////////////////////////////////////
                    echo 'Number Input Placeholder<br>';
                } elseif ($input->type === 'paragraph') { // DONE //////////////////////////////////////////////////////
                    echo '<p>' . $input->label . '</p>';
                } elseif ($input->type === 'radio-group') { // TODO ////////////////////////////////////////////////////
                    /*
                    [type] => radio-group
                    [required] =>
                    [label] => Radio Group
                    [inline] => 1
                    [name] => radio-group-1607399750037
                    [access] =>
                    [other] =>
                    [values] => Array(
                        [0] => stdClass Object(
                            [label] => Option 1
                            [value] => option-1
                            [selected] =>
                    */
                    // Add for Inline
                    /*echo '<div class="form-group">';
                    echo form_label('Recurring ' . $required, 'recurring', array('class' => 'control-label'));
                    //echo form_radio('recurring', 'yes', FALSE, array('class' => 'form-control pull-left'));
                    echo form_error('recurring', '<div class="alert alert-danger" role="alert">', '</div>');
                    echo '<div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" id="defaultGroupExample1" name="groupOfDefaultRadios">
                            <!--<label class="custom-control-label" for="defaultGroupExample1">Option 1</label>-->
                        </div>
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" id="defaultGroupExample2" name="groupOfDefaultRadios">
                            <!--<label class="custom-control-label" for="defaultGroupExample2">Option 2</label>-->
                        </div>
                        <div class="custom-control custom-radio">
                            <input type="radio" class="custom-control-input" id="defaultGroupExample3" name="groupOfDefaultRadios">
                            <!--<label class="custom-control-label" for="defaultGroupExample3">Option 3</label>-->
                        </div>';
                    echo '</div>';*/
                } elseif ($input->type === 'select') { // DONE /////////////////////////////////////////////////////////
                    // Add for Multi Select
                    $required = ($input->required == true ? '<span class="required"> *</span>' : '');
                    $help = (isset($input->description) ? '<span class="tooltip-element float-right" title="' . $input->description . '">?</span>' : '');
                    $values = array();
                    foreach ($input->values as $val) {
                        $values[$val->value] = $val->label;
                    }
                    echo '<div class="form-group">';
                    echo form_label($input->label . $required . $help, $input->name, array('class' => 'control-label full-width'));
                    echo form_error($input->name, '<div class="alert alert-danger" role="alert">', '</div>');
                    echo form_dropdown(
                        $input->name,
                        $values,
                        $CI->input->post($input->name),
                        array(
                            'class' => $input->className,
                        )
                    );
                    echo '</div>';
                } elseif ($input->type === 'text') { // DONE ///////////////////////////////////////////////////////////
                    $required = ($input->required == true ? '<span class="required"> *</span>' : '');
                    $help = (isset($input->description) ? '<span class="tooltip-element float-right" title="' . $input->description . '">?</span>' : '');
                    //$value = ($input->value != '' ? $input->value : ''); // not sure how we will use this yet???
                    $maxlength = (isset($input->maxlength) ? $input->maxlength : null);
                    echo '<div class="form-group">';
                    echo form_label($input->label . $required . $help, $input->name, array('class' => 'control-label full-width'));
                    echo form_error($input->name, '<div class="alert alert-danger" role="alert">', '</div>');
                    // Build the extra params array
                    $text_params = array();
                    if (isset($input->className)) $text_params['class'] = $input->className;
                    if (isset($input->placeholder)) $text_params['placeholder'] = $input->placeholder;
                    $text_params['maxlength'] = $maxlength;
                    echo form_input(
                        $input->name,
                        ($CI->input->post($input->name) != '' ? $CI->input->post($input->name) : ''),
                        $text_params
                    );
                    echo '</div>';
                } elseif ($input->type === 'textarea') { // DONE ///////////////////////////////////////////////////////
                    $required = ($input->required == true ? '<span class="required"> *</span>' : '');
                    $help = (isset($input->description) != '' ? '<span class="tooltip-element float-right" title="' . $input->description . '">?</span>' : '');
                    echo '<div class="form-group">';
                    echo form_label($input->label . $required . $help, $input->name, array('class' => 'control-label full-width'));
                    echo form_error($input->name, '<div class="alert alert-danger" role="alert">', '</div>');
                    echo form_textarea(array(
                        'name' => $input->name,
                        'class' => $input->className,
                        'placeholder' => (isset($input->placeholder) ? $input->placeholder : ''),
                        'rows' => (isset($input->rows) ? $input->rows : '')
                    ), $CI->input->post($input->name));
                    echo '</div>';
                }
            }
            echo form_close();
        } else {
            echo '<div class="alert alert-warning" role="alert">
                <i class="fa fa-warning"></i> Woops! This form has no fields assigned. Contact the webmaster.
            </div>';
        }
    }

    $output_string = ob_get_contents();
    ob_end_clean();

    return $output_string;
}
Shortcode::add('bpForms', 'bpForms');