<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php if (!empty($submissions)) { ?>
        <div class="panel">
            <div class="panel-body">
                <table class="table table-striped" id="submissions-table" width="100%">
                    <thead>
                        <tr>
                            <th scope="col" width="5%">ID&nbsp;</th>
                            <th scope="col" width="5%">Form&nbsp;ID</th>
                            <th scope="col">Form Title</th>
                            <th scope="col" class="text-right">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($submissions as $submission) {
                        ?>
                        <tr>
                            <td><?php echo $submission['id']; ?></td>
                            <td><?php echo $submission['form_id']; ?></td>
                            <td><?php echo $this->Contents_model->getForm($submission['form_id'])->title; ?></td>
                            <td class="text-right">
                                <a href="/admin/contents/viewSubmission/<?php echo $submission['id']; ?>">
                                    <span class="label label-primary">View</span></a>
                                <a href="javascript:;" onclick="confirmDeleteSubmission(<?php echo $submission['id']; ?>);">
                                    <span class="label label-danger">Delete</span></a>
                                </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <script>
            function confirmDeleteSubmission(id) {
                swal({
                    title: "Delete Submission",
                    text: "Are you sure you wish to delete this Submission?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Delete",
                    closeOnConfirm: false
                },
                function() {
                    $.ajax({
                        url: '/admin/deleteSubmission/' + id,
                        type: 'GET',
                        success: function(response) {
                            var data = JSON.parse(response);
                            if (data.status == 'success') {
                                $.fn.loadForms();
                                swal({
                                    title: "Success",
                                    text: "The Submission was successfully deleted.",
                                    type: "success",
                                    confirmButtonClass: "btn-success",
                                    confirmButtonText: "OK",
                                    showCancelButton: false
                                });
                            } else {
                                swal({
                                    title: "Error",
                                    text: "The was an error deleting the Submission.",
                                    type: "warning",
                                    confirmButtonClass: "btn-danger",
                                    confirmButtonText: "OK",
                                    showCancelButton: false
                                });
                            }
                        }
                    });
                });
            }
            jQuery(document).ready(function() {
                var $table = $('#submissions-table').DataTable({
                    "dom": 'Blftip',
                    "bSortCellsTop": true,
                    "pageLength": <?php echo $this->config->item('submissionsAdminPagination', 'settings'); ?>,
                    "lengthMenu": [
                        [<?php echo $this->config->item('submissionsAdminPagination', 'settings'); ?>, 30, 60, 100, -1],
                        [<?php echo $this->config->item('submissionsAdminPagination', 'settings'); ?>, 30, 60, 100, "All"]
                    ],
                    "order": [
                        [0, "asc"] // "desc" newest first, "asc" for oldest first
                    ],
                    responsive: true, // make table responsive
                    buttons: [ // relabel the export button
                        //'copy', 'excel'
                        {
                            extend: 'excel',
                            text: 'Export',
                            title: 'Submissions-Export',
                            className: 'btn btn-sm btn-primary shadow-sm',
                            exportOptions: {
                                columns: [0, 1]
                            },
                        }
                    ],
                    "initComplete": function(settings, json) { // do something immediately after the table is drawn
                        //applyContentsEmailFilter($table);
                    },
                    "oLanguage": { // adjust the text for the rows dropdown
                        "sLengthMenu": "_MENU_ Rows"
                    },
                    "aoColumns": [ // needed to keep Actions col from being sortable and searchable
                        /* id */ { "bSearchable": true, "bSortable": true },
                        /* title */ { "bSearchable": true, "bSortable": true },
                        /* form_id */ { "bSearchable": true, "bSortable": true },
                        /* actions */ { "bSearchable": false, "bSortable": false }
                    ]
                });
                $('.dt-buttons').css('float', 'right');
                $table.on('draw', function () {
                    // run a function or other action
                });
            });
        </script>
<?php } else { ?>
    None
<?php } ?>