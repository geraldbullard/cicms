<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
        <h3 class="page-title pull-left">Forms</h3>
        <div class="pull-right">
            <a href="/admin/contents/addForm/">
                <button class="btn btn-sm btn-primary" id="dt-add-row">
                    <i class="fa fa-plus"></i>&nbsp;&nbsp;Add Form
                </button>
            </a>
        </div>
        <?php if ($this->session->flashdata('msg')) { ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="alert alert-<?php echo $this->session->flashdata('msgtype'); ?> alert-dismissible action-alert" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">x</button>
                    <?php echo $this->session->flashdata('msg'); ?>
                </div>
            </div>
        </div>
        <?php } ?>
        <div class="row">
            <div class="col-lg-12">
                <div id="formsListingTable"></div><hr />
            </div>
        </div>
        <script>
            $(document).ready(function () {
                $("#clearFilter").on("click", function (evt) {
                    evt.preventDefault();
                    $('#forms_filter').trigger("reset");
                    $.fn.loadForms();
                    $("#clearFilter").blur();
                });
                $("#formsFilter").on("click", function (evt) {
                    evt.preventDefault();
                    $.fn.loadForms();
                    $("#formsFilter").blur();
                });
                $.fn.loadForms = function () {
                    $.blockUI({target: "#formsListingTable"});
                    $params = $("#forms_filter").serialize();
                    $.get('<?php echo base_url('/admin/contents/getForms'); ?>?' + $params, function (data) {
                        $("#formsListingTable").html(data);
                        $.unblockUI({target: "#formsListingTable"});
                    }).fail(function () {
                        alert("Error!", "Something went wrong loading the Forms!", "error");
                    });
                };
                $.fn.loadForms();
            });
        </script>
