<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
        <h3 class="page-title pull-left">Content</h3>
        <div class="pull-right">
            <a href="/admin/contents/add/">
                <button class="btn btn-sm btn-primary" id="dt-add-row">
                    <i class="fa fa-plus"></i>&nbsp;&nbsp;Add Content
                </button>
            </a>
        </div>
        <?php if ($this->session->flashdata('msg')) { ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="alert alert-<?php echo $this->session->flashdata('msgtype'); ?> alert-dismissible action-alert" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">x</button>
                    <?php echo $this->session->flashdata('msg'); ?>
                </div>
            </div>
        </div>
        <?php } ?>
        <div class="row">
            <div class="col-lg-12">
                <div id="contentsListingTable"></div><hr />
            </div>
        </div>
        <script>
            function setSiteIndex(e) {
                $.ajax({
                    url: '/admin/contents/setSiteIndex/' + e.id,
                    type: 'GET',
                    success: function(response) {
                        var data = JSON.parse(response);
                        if (data.status == 'success') {
                            window.location = window.location;
                        } else {
                            swal({
                                title: "Error",
                                text: "The was an error setting the Site Index.",
                                type: "warning",
                                confirmButtonClass: "btn-danger",
                                confirmButtonText: "OK",
                                showCancelButton: false
                            });
                        }
                    }
                });
            }
            $(document).ready(function () {
                $("#clearFilter").on("click", function (evt) {
                    evt.preventDefault();
                    $('#contents_filter').trigger("reset");
                    $.fn.loadContents();
                    $("#clearFilter").blur();
                });
                $("#contentsFilter").on("click", function (evt) {
                    evt.preventDefault();
                    $.fn.loadContents();
                    $("#contentsFilter").blur();
                });
                $.fn.loadContents = function () {
                    $.blockUI({target: "#contentsListingTable"});
                    $params = $("#contents_filter").serialize();
                    $.get('<?php echo base_url('/admin/contents/getContents'); ?>?' + $params, function (data) {
                        $("#contentsListingTable").html(data);
                        $.unblockUI({target: "#contentsListingTable"});
                    }).fail(function () {
                        alert("Error!", "Something went wrong loading the Contents!", "error");
                    });
                };
                $.fn.loadContents();
            });
        </script>
