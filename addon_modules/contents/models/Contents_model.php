<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Content Model
 */
class Contents_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	// Filter Contents
	public function filter($filter)
	{
		$this->db->distinct();

		/* Some examples of how wqe build the sql query based on filter data
		if (isset($filter->id) && $filter->id !== null) {
			$this->db->where("E.id", $filter->id);
		}
		if (isset($filter->first_name) && $filter->first_name !== null) {
			$this->db->where("first_name", $filter->first_name);
		}
		if (isset($filter->state[0]) && !empty($filter->state[0])) {
			$where = array();
			foreach ($filter->state as $state) {
				$where[] = "ELI.resident_state = '" . $state . "' OR ENRLI.non_resident_state = '" . $state . "'";
			}
			$where_str = implode(" OR ", $where);
			$this->db->where($where_str);
		}*/

		// $this->db->select('U.*');
		$contents = $this->db->get($this->db->dbprefix . "contents");
		// $this->data['contents'] = $contents->result_array();
		// $this->db->join('enrollers_experience EE', 'E.id = EE.id_enroller', 'left');
		// $this->db->group_by('E.id');
		/*echo '<pre>';
		print_r($this->db->last_query());
		die();*/

		if ($contents->num_rows() < 1) {
			return false;
		}
		return $contents->result_array();
	}

	public function getContent($id)
	{
		$this->db->where("id", $id);
		return $this->db->get($this->db->dbprefix . "contents")->first_row();
	}

	public function insert($data)
	{
        // these are for now, add into editable inputs later
        $data['parent'] = 0;
		$data['sort'] = 0;
		$data['status'] = 1;
		$data['siteIndex'] = 0;
		$data['type'] = 1;
		$data['created'] = date("Y-m-d H:i:s");
		$data['lastModified'] = date("Y-m-d H:i:s");
        // now do the db stuff
        if ($this->db->insert($this->db->dbprefix . "contents", $data)) return $this->db->insert_id();
		return false;
	}

	public function update($id, $data)
	{
		// these are for now, add into editable inputs later
		$data['parent'] = 0;
		$data['sort'] = 0;
		$data['status'] = 1;
		$data['type'] = 1;
		$data['lastModified'] = date("Y-m-d H:i:s");
		// now do the db stuff
		$this->db->where("id", $id);
		if ($this->db->update($this->db->dbprefix . "contents", $data)) return true;
		return false;
	}

	public function delete($id)
	{
		$this->db->where("id", $id);
		if ($this->db->delete($this->db->dbprefix . "contents")) return true;
		return false;
	}

	public function setSiteIndex($id)
	{
        // Set all siteIndex to 0 first
	    $data = array('siteIndex' => 0);
        $this->db->update($this->db->dbprefix . "contents", $data);

        // Now set siteIndex based on incoming content id
	    $data = array('siteIndex' => 1);
        $this->db->where("id", $id);
		if ($this->db->update($this->db->dbprefix . "contents", $data)) return true;
		return false;
	}

	public function getSiteIndex() {
		return $this->db->get_where($this->db->dbprefix . 'contents', array('siteIndex' => 1))->first_row();
	}

	public function getContentById($cid) {
		return $this->db->get_where($this->db->dbprefix . 'contents', array('id' => $cid))->result_array();
	}

	public function getContentBySlug($slug) { // page slug, seo url
		return $this->db->get_where($this->db->dbprefix . 'contents', array('slug' => $slug))->first_row();
	}

	public function getContentByStatus($status) { // Later, 0 = inactive, 1 = draft, 2 = active, 3 = private etc...
		return $this->db->get_where($this->db->dbprefix . 'contents', array('status' => $status))->result_array();
	}

	public function getContentByType($type) { // Later, Page = 1, Post = 2 etc...
		return $this->db->get_where($this->db->dbprefix . 'contents', array('type' => $type))->result_array();
	}

	public function getRoutes() {
		return $this->db->get_where($this->db->dbprefix . 'contents', array('status' => 1))->result_array();
	}

	/* Menus Methods */
	public function filterMenus($filter)
	{
		$this->db->distinct();

		$menus = $this->db->get($this->db->dbprefix . "menus");

		if ($menus->num_rows() < 1) {
			return false;
		}
		return $menus->result_array();
	}

	public function getMenu($id)
	{
		$this->db->where("id", $id);
		return $this->db->get($this->db->dbprefix . "menus")->first_row();
	}

	public function getMenuByDesignation($id)
	{
		$this->db->where("designation", $id);
		$result = $this->db->get($this->db->dbprefix . "menus");
        if ($result->num_rows() > 0)
            return $result->first_row();
        return false;
	}

	public function getMenuItemContentsData($id)
	{
		$this->db->where("id", $id);
        $data = $this->db->get($this->db->dbprefix . "contents");
        if ($data->num_rows() < 1) {
            return false;
        }
        return $data->first_row();
	}

	public function insertMenu($mData)
	{
		$data['menuData'] = serialize($mData);
		$data['created'] = date("Y-m-d H:i:s");
		$data['lastModified'] = date("Y-m-d H:i:s");
		if ($this->db->insert($this->db->dbprefix . "menus", $data)) return $this->db->insert_id();
		return false;
	}

	public function insertMenuMeta($mData)
	{
        $this->db->where("id", $mData['id']);
		$data['title'] = $mData['title'];
		$data['designation'] = $mData['designation'];
		if ($this->db->update($this->db->dbprefix . "menus", $data)) return true;
		return false;
	}

	public function updateMenu($id, $mData)
	{
        $data['menuData'] = serialize($mData);
		$data['lastModified'] = date("Y-m-d H:i:s");
		$this->db->where("id", $id);
		if ($this->db->update($this->db->dbprefix . "menus", $data)) return true;
		return false;
	}

	public function deleteMenu($id)
	{
		$this->db->where("id", $id);
		if ($this->db->delete($this->db->dbprefix . "menus")) return true;
		return false;
	}

	/* Forms Methods */
	public function filterForms($filter)
	{
		$this->db->distinct();

		$forms = $this->db->get($this->db->dbprefix . "forms");

		if ($forms->num_rows() < 1) {
			return false;
		}
		return $forms->result_array();
	}

	public function getForm($id)
	{
		$this->db->where("id", $id);
		return $this->db->get($this->db->dbprefix . "forms")->first_row();
	}

	public function getForms()
	{
		return $this->db->get($this->db->dbprefix . "forms")->result_array();
	}

	public function insertForm($data)
	{
		$data['created'] = date("Y-m-d H:i:s");
		$data['lastModified'] = date("Y-m-d H:i:s");
		if ($this->db->insert($this->db->dbprefix . "forms", $data)) return $this->db->insert_id();
		return false;
	}

	public function updateForm($id, $data)
	{
		$this->db->where("id", $id);
		if ($this->db->update($this->db->dbprefix . "forms", $data)) return true;
		return false;
	}

	public function deleteForm($id)
	{
		$this->db->where("id", $id);
		if ($this->db->delete($this->db->dbprefix . "forms")) return true;
		return false;
	}

	/* Submissions Methods */
	public function filterSubmissions($filter)
	{
		$this->db->distinct();

		if (isset($filter->filterformid) && $filter->filterformid !== null) {
			$this->db->where("form_id", $filter->filterformid);
		}

		$submissions = $this->db->get($this->db->dbprefix . "form_submissions");

		if ($submissions->num_rows() < 1) {
			return false;
		}
		return $submissions->result_array();
	}

	public function viewSubmission($id)
	{
		$this->db->where("id", $id);
		return $this->db->get($this->db->dbprefix . "form_submissions")->first_row();
	}

}