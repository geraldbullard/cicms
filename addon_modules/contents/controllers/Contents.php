<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contents extends MY_Controller
{

	function __construct()
	{
        parent::__construct();

		// Get site wide settings first
		$this->Settings_model->loadSitewideSettings();

		// Load This Model
		$this->load->model('Contents_model');

        include_once(APPPATH . 'helpers/shortcode_helper.php');
        // Custom Shortcodes Helper
        $this->load->helper('contents/shortcode');
        $this->load->helper('contents/functions');
    }

	public function index()
	{
	    // First thing let's set the template based on the site settings value
        if (file_exists(VIEWPATH . $this->config->item('siteTheme', 'settings') . '.php')) {
            $this->template->set_template($this->config->item('siteTheme', 'settings'));
        }
        
        // later check for slug, or load siteIndex content
        $cdata = $this->Contents_model->getContentBySlug($this->uri->segment(1));

        if (empty($cdata)) { // lets see if there is a siteIndex page before we 404
            $cdata = $this->Contents_model->getSiteIndex();
        }

        if (empty($cdata)) { // do 404 here after more research and checking to be sure
            $this->output->set_status_header('404');
            // Set the title
            $this->template->title = 'Page Not Found';

            // Add meta with and without siteWideSettings defines, later make CI_Meta class to possibly control this
            $this->template->meta->add("utf-8", 'charset');
            $this->template->meta->add("viewport", "width=device-width, initial-scale=1.0");
            $this->template->meta->add("description", $this->config->item('siteDescription', 'settings'));
            $this->template->meta->add("keywords", $this->config->item('siteKeywords', 'settings'));
            $this->template->meta->add("author", "brokenPIXEL");
            $this->template->meta->add("robots", "index,nofollow");

            // show 404 page
            if (file_exists(VIEWPATH . 'templates/' . $this->config->item('siteTheme', 'settings') . '/404.php')) {
                $this->template->content->view('templates/' . $this->config->item('siteTheme', 'settings') . '/404');
            } else {
                $this->template->content->view('welcome/404');
            }
        } else {
            // Set the title
            $this->template->title = $cdata->title;

            // Add meta with and without siteWideSettings defines, later make CI_Meta class to possibly control this
            $this->template->meta->add("utf-8", 'charset');
            $this->template->meta->add("viewport", "width=device-width, initial-scale=1.0");
            $this->template->meta->add("description", $this->config->item('siteDescription', 'settings'));
            $this->template->meta->add("keywords", $this->config->item('siteKeywords', 'settings'));
            $this->template->meta->add("author", "brokenPIXEL");
            $this->template->meta->add("robots", "index,follow");

            // Dynamically add a css stylesheet, maybe specifically for this page etc
            //$this->template->stylesheet->add('REPLACE_ME_WITH_URL_OR_PATH');

            // Parse the content for shortcodes before sending to the view
            $cdata->content = Shortcode::parse($cdata->content);
            $this->data['pagedata'] = $cdata;

            // get page data and show based on page template
            if (isset($cdata->template) && $cdata->template != '') {
                if (file_exists(VIEWPATH . 'templates/' . $this->config->item('siteTheme', 'settings') . '/' . $cdata->template . '.php')) {
                    $this->template->content->view('templates/' . $this->config->item('siteTheme', 'settings') . '/' . $cdata->template, $this->data);
                } else {
                    $this->template->content->view('templates/' . $this->config->item('siteTheme', 'settings') . '/default', $this->data);
                }
            } else {
                $this->template->content->view('templates/' . $this->config->item('siteTheme', 'settings') . '/default', $this->data);
            }
        }

		// Publish the template
		$this->template->publish();
	}

	public function admin_index()
	{
		// Make sure the user is logged in for only admin functionality
		if (!$this->session->userdata('user_id'))
		{
			// set the url they were trtying to go to in session
			$this->session->set_userdata('page_url', current_url());

			//Redirect to login
			redirect('user/login');
		}

		$this->data = array();
		$this->data['title'] = 'Content Index';

		// load the config from the current module
		$this->load->config('contents');
		$this->data['contents_config'] = $this->config->config['contents'];

		// Added for developer dropdown
		$contents = $this->db->get($this->db->dbprefix . "contents");
		$this->data['dev_data']['contents'] = $contents->result_array();

		$this->load->view('layout/admin/header.php', $this->data);
		$this->load->view('admin/index', $this->data);
		$this->load->view('layout/admin/footer.php', $this->data);
	}

    public function getContents() // add id later for single
    {
        // Make sure the user is logged in for only admin functionality
        if (!$this->session->userdata('user_id'))
        {
            // set the url they were trtying to go to in session
            $this->session->set_userdata('page_url', current_url());

            //Redirect to login
            redirect('user/login');
        }

        $filter = new stdClass();
        // Set filter data based on post input, none by default
        //$filter->first_name = ($this->input->get_post('filterfirstname') != '') ? $this->input->get_post('filterfirstname') : null;

        $data['contents'] = $this->Contents_model->filter($filter);

        return json_encode($this->load->view('admin/filter', $data));
    }

	public function add()
	{
        // Make sure the user is logged in for only admin functionality
        if (!$this->session->userdata('user_id'))
        {
            // set the url they were trtying to go to in session
            $this->session->set_userdata('page_url', current_url());

            //Redirect to login
            redirect('user/login');
        }

		$this->data = array();

		if ($this->input->post()) {
            $this->load->library('form_validation');
			$this->form_validation->set_rules('title', 'Name', 'trim|required');
			$this->form_validation->set_rules('slug', 'Slug', 'trim|required');
			if ($this->form_validation->run() !== FALSE) {
                $data = array(
                    'title' => $this->input->post('title'),
                    'slug' => $this->input->post('slug'),
                    'summary' => $this->input->post('summary'),
                    'metaTitle' => $this->input->post('metaTitle'),
                    'metaDescription' => $this->input->post('metaDescription'),
                    'metaKeywords' => $this->input->post('metaKeywords'),
                    'content' => $this->input->post('content'),
                    'template' => $this->input->post('template'),
                );
                $insertid = $this->Contents_model->insert($data);
				if ($insertid) {
                    $this->session->set_flashdata(array("msg" => "Content Created Successfully!", "msgtype" => "success"));
                    redirect("/admin/contents/edit/" . $insertid);
				} else {
					$this->data['msg'] = "There was a problem creating the Content.";
					$this->data['msgtype'] = "danger";
				}
			} else {
				$this->data['msg'] = "Your submission has the following Problems:<br>" . validation_errors();
				$this->data['msgtype'] = "danger";
			}
		}

        // Added for developer dropdown
        $this->data['dev_data'] = '';

		$this->data['title'] = 'Add';

		$this->load->view('layout/admin/header.php', $this->data);
		$this->load->view('admin/add', $this->data);
		$this->load->view('layout/admin/footer.php', $this->data);
	}

	public function edit()
	{
        // Make sure the user is logged in for only admin functionality
        if (!$this->session->userdata('user_id'))
        {
            // set the url they were trtying to go to in session
            $this->session->set_userdata('page_url', current_url());

            //Redirect to login
            redirect('user/login');
        }

        $contentid = $this->uri->segment(4);
		$this->data = array();

		$this->data['title'] = 'Content Edit';

		$this->data['content'] = $this->Contents_model->getContent($contentid);

		// Added for developer dropdown
		$this->data['dev_data']['content'] = $this->data['content'];

		$this->load->view('layout/admin/header.php', $this->data);
		$this->load->view('admin/edit', $this->data);
		$this->load->view('layout/admin/footer.php', $this->data);
	}

	public function update()
	{
        // Make sure the user is logged in for only admin functionality
        if (!$this->session->userdata('user_id'))
        {
            // set the url they were trtying to go to in session
            $this->session->set_userdata('page_url', current_url());

            //Redirect to login
            redirect('user/login');
        }

        $data = array(
            'title' => $this->input->post('title'),
            'slug' => $this->input->post('slug'),
            'summary' => $this->input->post('summary'),
            'content' => $this->input->post('content'),
            'metaTitle' => $this->input->post('metaTitle'),
            'metaDescription' => $this->input->post('metaDescription'),
            'metaKeywords' => $this->input->post('metaKeywords'),
            'template' => $this->input->post('template'),
            'siteIndex' => $this->input->post('siteIndex'),
        );
        $contentid = $this->uri->segment(4);
        if ($this->Contents_model->update($contentid, $data) !== false) {
            $response_array['status'] = 'success';
            echo json_encode($response_array);
        } else {
            $response_array['status'] = 'error';
            echo json_encode($response_array);
        }
    }

	public function delete()
	{
        // Make sure the user is logged in for only admin functionality
        if (!$this->session->userdata('user_id'))
        {
            // set the url they were trtying to go to in session
            $this->session->set_userdata('page_url', current_url());

            //Redirect to login
            redirect('user/login');
        }

        $contentid = $this->uri->segment(4);
        if ($this->Contents_model->delete($contentid) !== false) {
            $response_array['status'] = 'success';
            echo json_encode($response_array);
        } else {
            $response_array['status'] = 'error';
            echo json_encode($response_array);
        }
    }

	private function seoURL()
	{
        // Make sure the user is logged in for only admin functionality
        if (!$this->session->userdata('user_id'))
        {
            // set the url they were trtying to go to in session
            $this->session->set_userdata('page_url', current_url());

            //Redirect to login
            redirect('user/login');
        }

        //get from brokenPIXEL blue
	}

    /* Menus Methods */
    public function menus()
    {
        // Make sure the user is logged in for only admin functionality
        if (!$this->session->userdata('user_id'))
        {
            // set the url they were trtying to go to in session
            $this->session->set_userdata('page_url', current_url());

            //Redirect to login
            redirect('user/login');
        }

        $this->data = array();
        $this->data['title'] = 'Menus';

        // load the config from the current module
        $this->load->config('contents');
        $this->data['contents_config'] = $this->config->config['contents'];

        // Added for developer dropdown
        $menus = $this->db->get($this->db->dbprefix . "menus");
        $this->data['dev_data']['menus'] = $menus->result_array();

        $this->load->view('layout/admin/header.php', $this->data);
        $this->load->view('admin/menus', $this->data);
        $this->load->view('layout/admin/footer.php', $this->data);
    }

    public function getMenus() // add id later for single
    {
        $filter = new stdClass();
        // Set filter data based on post input, none by default
        //$filter->first_name = ($this->input->get_post('filterfirstname') != '') ? $this->input->get_post('filterfirstname') : null;

        $data['menus'] = $this->Contents_model->filterMenus($filter);

        return json_encode($this->load->view('admin/filterMenus', $data));
    }

    public function insertMenu() // add id later for single
    {
        // Make sure the user is logged in for only admin functionality
        if (!$this->session->userdata('user_id'))
        {
            // set the url they were trtying to go to in session
            $this->session->set_userdata('page_url', current_url());

            //Redirect to login
            redirect('user/login');
        }
        
        if (!empty($this->input->post()) && $insertid = $this->Contents_model->insertMenu($this->input->post())) {
            $response_array['status'] = 'success';
            $response_array['insertid'] = $insertid;
            echo json_encode($response_array);
        } else {
            $response_array['status'] = 'error';
            echo json_encode($response_array);
        }
    }

    public function insertMenuMeta() // add id later for single
    {
        // Make sure the user is logged in for only admin functionality
        if (!$this->session->userdata('user_id'))
        {
            // set the url they were trtying to go to in session
            $this->session->set_userdata('page_url', current_url());

            //Redirect to login
            redirect('user/login');
        }
        
        if (!empty($this->input->post()) && $this->Contents_model->insertMenuMeta($this->input->post())) {
            $response_array['status'] = 'success';
            echo json_encode($response_array);
        } else {
            $response_array['status'] = 'error';
            echo json_encode($response_array);
        }
    }

    public function addMenu()
    {
        // Make sure the user is logged in for only admin functionality
        if (!$this->session->userdata('user_id'))
        {
            // set the url they were trtying to go to in session
            $this->session->set_userdata('page_url', current_url());

            //Redirect to login
            redirect('user/login');
        }

        $this->data = array();

        // Added for developer dropdown
        $this->data['dev_data'] = '';

        $this->data['title'] = 'Add Menu';

        $this->load->view('layout/admin/header.php', $this->data);
        $this->load->view('admin/addMenu', $this->data);
        $this->load->view('layout/admin/footer.php', $this->data);
    }

    public function editMenu()
    {
        // Make sure the user is logged in for only admin functionality
        if (!$this->session->userdata('user_id'))
        {
            // set the url they were trtying to go to in session
            $this->session->set_userdata('page_url', current_url());

            //Redirect to login
            redirect('user/login');
        }

        $cm = $this->load->model('contents/contents_model');

        $menuid = $this->uri->segment(4);
        $this->data = array();

        $this->data['menu'] = $this->Contents_model->getMenu($menuid);
        $this->data['pages'] = $this->Contents_model->getMenu($menuid);

        $this->data['title'] = $this->data['menu']->title;

        // Added for developer dropdown
        $this->data['dev_data']['menu'] = $this->data['menu'];

        $this->load->view('layout/admin/header.php', $this->data);
        $this->load->view('admin/editMenu', $this->data);
        $this->load->view('layout/admin/footer.php', $this->data);
    }

    public function updateMenu()
    {
        // Make sure the user is logged in for only admin functionality
        if (!$this->session->userdata('user_id'))
        {
            // set the url they were trtying to go to in session
            $this->session->set_userdata('page_url', current_url());

            //Redirect to login
            redirect('user/login');
        }

        $menuid = $this->uri->segment(4);

        if (!empty($this->input->post()) && $this->Contents_model->updateMenu($menuid, $this->input->post()) !== false) {
            $response_array['status'] = 'success';
            echo json_encode($response_array);
        } else {
            $response_array['status'] = 'error';
            echo json_encode($response_array);
        }
    }

    public function deleteMenu()
    {
        // Make sure the user is logged in for only admin functionality
        if (!$this->session->userdata('user_id'))
        {
            // set the url they were trtying to go to in session
            $this->session->set_userdata('page_url', current_url());

            //Redirect to login
            redirect('user/login');
        }

        $menuid = $this->uri->segment(4);
        if ($this->Contents_model->deleteMenu($menuid) !== false) {
            $response_array['status'] = 'success';
            echo json_encode($response_array);
        } else {
            $response_array['status'] = 'error';
            echo json_encode($response_array);
        }
    }

    /* Forms Methods */
    public function forms()
    {
        // Make sure the user is logged in for only admin functionality
        if (!$this->session->userdata('user_id'))
        {
            // set the url they were trtying to go to in session
            $this->session->set_userdata('page_url', current_url());

            //Redirect to login
            redirect('user/login');
        }

        $this->data = array();
        $this->data['title'] = 'Forms';

        // load the config from the current module
        $this->load->config('contents');
        $this->data['contents_config'] = $this->config->config['contents'];

        // Added for developer dropdown
        $forms = $this->db->get($this->db->dbprefix . "forms");
        $this->data['dev_data']['forms'] = $forms->result_array();

        $this->load->view('layout/admin/header.php', $this->data);
        $this->load->view('admin/forms', $this->data);
        $this->load->view('layout/admin/footer.php', $this->data);
    }

    public function getForms() // add id later for single
    {
        // Make sure the user is logged in for only admin functionality
        if (!$this->session->userdata('user_id'))
        {
            // set the url they were trtying to go to in session
            $this->session->set_userdata('page_url', current_url());

            //Redirect to login
            redirect('user/login');
        }

        $filter = new stdClass();
        // Set filter data based on post input, none by default
        //$filter->first_name = ($this->input->get_post('filterfirstname') != '') ? $this->input->get_post('filterfirstname') : null;

        $data['forms'] = $this->Contents_model->filterForms($filter);

        return json_encode($this->load->view('admin/filterForms', $data));
    }

    public function getForm($id) // add id later for single
    {
        if (!$id) $id = $this->uri->segment(4);

        // Make sure the user is logged in for only admin functionality
        if (!$this->session->userdata('user_id'))
        {
            // set the url they were trtying to go to in session
            $this->session->set_userdata('page_url', current_url());

            //Redirect to login
            redirect('user/login');
        }

        return $this->Contents_model->getForm($id)->result_array();
    }

    public function addForm()
    {
        // Make sure the user is logged in for only admin functionality
        if (!$this->session->userdata('user_id'))
        {
            // set the url they were trtying to go to in session
            $this->session->set_userdata('page_url', current_url());

            //Redirect to login
            redirect('user/login');
        }

        $this->data = array();

        if ($this->input->post()) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('title', 'Name', 'trim|required');
            if ($this->form_validation->run() !== FALSE) {
                $data = array(
                    'title' => $this->input->post('title'),
                    'data' => $this->input->post('data'),
                    'adminNotify' => $this->input->post('adminNotify'),
                    'notifyEmail' => $this->input->post('notifyEmail'),
                    'ccEmails' => $this->input->post('ccEmails'),
                    'redirect' => $this->input->post('redirect'),
                    'successMsg' => $this->input->post('successMsg'),
                    'showTitle' => $this->input->post('showTitle'), //
                    'storeSubmission' => $this->input->post('storeSubmission'),
                    'hideAfter' => $this->input->post('hideAfter'),
                    'recaptcha' => $this->input->post('recaptcha'),
                    'formId' => $this->input->post('formId'),
                    'formVerb' => $this->input->post('formVerb'),
                );
                $insertid = $this->Contents_model->insertForm($data);
                if ($insertid) {
                    $this->session->set_flashdata(array("msg" => "Content Created Successfully!", "msgtype" => "success"));
                    redirect("/admin/contents/editForm/" . $insertid);
                } else {
                    $this->data['msg'] = "There was a problem creating the Content.";
                    $this->data['msgtype'] = "danger";
                }
            } else {
                $this->data['msg'] = "Your submission has the following Problems:<br>" . validation_errors();
                $this->data['msgtype'] = "danger";
            }
        }

        // Added for developer dropdown
        $this->data['dev_data'] = '';

        $this->data['title'] = 'Add Form';

        $this->load->view('layout/admin/header.php', $this->data);
        $this->load->view('admin/addForm', $this->data);
        $this->load->view('layout/admin/footer.php', $this->data);
    }

    public function editForm()
    {
        // Make sure the user is logged in for only admin functionality
        if (!$this->session->userdata('user_id'))
        {
            // set the url they were trtying to go to in session
            $this->session->set_userdata('page_url', current_url());

            //Redirect to login
            redirect('user/login');
        }

        $formid = $this->uri->segment(4);
        $this->data = array();

        $this->data['form'] = $this->Contents_model->getForm($formid);

        $this->data['title'] = $this->data['form']->title;

        // Added for developer dropdown
        $this->data['dev_data']['form'] = $this->data['form'];

        $this->load->view('layout/admin/header.php', $this->data);
        $this->load->view('admin/editForm', $this->data);
        $this->load->view('layout/admin/footer.php', $this->data);
    }

    public function updateForm()
    {
        // Make sure the user is logged in for only admin functionality
        if (!$this->session->userdata('user_id'))
        {
            // set the url they were trtying to go to in session
            $this->session->set_userdata('page_url', current_url());

            //Redirect to login
            redirect('user/login');
        }

        $data = array(
            'title' => $this->input->post('title'),
            'data' => $this->input->post('data'),
            'adminNotify' => $this->input->post('adminNotify'),
            'notifyEmail' => $this->input->post('notifyEmail'),
            'ccEmails' => $this->input->post('ccEmails'),
            'redirect' => $this->input->post('redirect'),
            'successMsg' => $this->input->post('successMsg'),
            'showTitle' => $this->input->post('showTitle'), //
            'storeSubmission' => $this->input->post('storeSubmission'),
            'hideAfter' => $this->input->post('hideAfter'),
            'recaptcha' => $this->input->post('recaptcha'),
            'formId' => $this->input->post('formId'),
            'formVerb' => $this->input->post('formVerb'),
        );
        $formid = $this->uri->segment(4);
        if ($this->Contents_model->updateForm($formid, $data) !== false) {
            $this->session->set_flashdata(array("msg" => "Your Form was updated successfully", "msgtype" => "success"));
            redirect('/admin/contents/editForm/' . $formid, 'refresh');
        } else {
            $this->session->set_flashdata(array("msg" => "There was a problem updating your Form!", "msgtype" => "danger"));
            redirect('/admin/contents/editForm/' . $formid, 'refresh');
        }
    }

    public function deleteForm()
    {
        // Make sure the user is logged in for only admin functionality
        if (!$this->session->userdata('user_id'))
        {
            // set the url they were trtying to go to in session
            $this->session->set_userdata('page_url', current_url());

            //Redirect to login
            redirect('user/login');
        }

        $formid = $this->uri->segment(4);
        if ($this->Contents_model->deleteForm($formid) !== false) {
            $response_array['status'] = 'success';
            echo json_encode($response_array);
        } else {
            $response_array['status'] = 'error';
            echo json_encode($response_array);
        }
    }

    /* Submissions Methods */
    public function submissions()
    {
        // Make sure the user is logged in for only admin functionality
        if (!$this->session->userdata('user_id'))
        {
            // set the url they were trtying to go to in session
            $this->session->set_userdata('page_url', current_url());

            //Redirect to login
            redirect('user/login');
        }

        $this->data = array();
        $this->data['title'] = 'Submissions';
        $this->data['forms'] = $this->Contents_model->getForms();

        // load the config from the current module
        $this->load->config('contents');
        $this->data['contents_config'] = $this->config->config['contents'];

        // Added for developer dropdown
        //$submissions = $this->db->get($this->db->dbprefix . "form_submissions");
        //$this->data['dev_data']['submissions'] = $submissions->result_array();
        $this->data['dev_data']['submissions'] = array();

        $this->load->view('layout/admin/header.php', $this->data);
        $this->load->view('admin/submissions', $this->data);
        $this->load->view('layout/admin/footer.php', $this->data);
    }

    public function getSubmissions() // add id later for single
    {
        // Make sure the user is logged in for only admin functionality
        if (!$this->session->userdata('user_id'))
        {
            // set the url they were trtying to go to in session
            $this->session->set_userdata('page_url', current_url());

            //Redirect to login
            redirect('user/login');
        }

        $filter = new stdClass();
        // Set filter data based on post input, none by default
        $filter->filterformid = ($this->input->get_post('filterformid') != '') ? $this->input->get_post('filterformid') : null;

        $data['submissions'] = $this->Contents_model->filterSubmissions($filter);

        return json_encode($this->load->view('admin/filterSubmissions', $data));
    }

    public function viewSubmission()
    {
        // Make sure the user is logged in for only admin functionality
        if (!$this->session->userdata('user_id'))
        {
            // set the url they were trtying to go to in session
            $this->session->set_userdata('page_url', current_url());

            //Redirect to login
            redirect('user/login');
        }

        $subid = $this->uri->segment(4);
        $this->data = array();

        $this->data['title'] = 'View Submission';

        $this->data['submission'] = $this->Contents_model->viewSubmission($subid);

        // Added for developer dropdown
        $this->data['dev_data']['submission'] = $this->data['submission'];

        $this->load->view('layout/admin/header.php', $this->data);
        $this->load->view('admin/viewSubmission', $this->data);
        $this->load->view('layout/admin/footer.php', $this->data);
    }

    public function setSiteIndex()
    {
        // Make sure the user is logged in for only admin functionality
        if (!$this->session->userdata('user_id'))
        {
            // set the url they were trtying to go to in session
            $this->session->set_userdata('page_url', current_url());

            //Redirect to login
            redirect('user/login');
        }

        $contentid = $this->uri->segment(4);
        if ($this->Contents_model->setSiteIndex($contentid) !== false) {
            $response_array['status'] = 'success';
            echo json_encode($response_array);
        } else {
            $response_array['status'] = 'error';
            echo json_encode($response_array);
        }
    }

}