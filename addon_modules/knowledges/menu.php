<?php
/* Kanban module menu file */

$_menu = array(
    'url' => '/admin/knowledges/',
    'title' => 'Knowledge',
    'icon' => 'lnr lnr-book',
    'access' => 1,
    'sub_items' => array(
    	// Repeat main menu since it has subs
	    'knowledges' => array(
		    'url' => '/admin/knowledges/',
		    'title' => 'Knowledge',
		    'icon' => 'lnr lnr-leaf',
		    'access' => 1,
	    ),
	    'add' => array(
		    'url' => '/admin/knowledges/add/',
		    'title' => 'Add',
		    'icon' => 'lnr lnr-plus-circle',
		    'access' => 3,
	    ),
    ),
);