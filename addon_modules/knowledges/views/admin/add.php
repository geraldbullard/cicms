<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<h3 class="page-title">
    <a href="/admin/knowledges">Knowledge</a> &raquo; <?php echo $title; ?>
</h3>
<?php if (isset($msg)) { ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="alert alert-<?php echo $msgtype; ?> alert-dismissible action-alert" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                            aria-hidden="true">×</span></button>
                <?php echo $msg; ?>
            </div>
        </div>
    </div>
<?php } ?>
<div class="panel">
    <div class="panel-body">
        <form action="" method="post" id="knowledge_add">
            <div class="form-group">
                <div class="row">
                    <div class="col-lg-6">
                        <label>Category</label>
                        <select class="form-control" name="category-id" id="category-select">
                            <option id="select-default" value="" selected>Please Pick a category</option>
                            <?php foreach ($categories as $category): ?>
                                <option <?php if(isset($categoryId) && $categoryId != null ){if($categoryId === $category['id']) echo 'selected';} ?> value="<?php echo $category['id'] ?>"><?php echo $category['name'] ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="col-lg-6">
                        <button style="margin-top: 27px;" type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#categoryAddModal">
                            Add Category
                        </button>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label>Title</label>
                <input id="title" value="<?php if(isset($articleTitle) && $articleTitle != null){echo $articleTitle;} ?>" type="text" name="title" class="form-control"/>
            </div>
            <div class="form-group">
                <label>Body</label>
                <textarea class="form-control" name="body" id="knb-body" cols="200" rows="20"><?php if(isset($body) && $body != null) {echo $body;} ?></textarea>
            </div>
            <p class="text-right">
                <button id="article-form-submit" type="submit" class="btn btn-primary">Save</button>
            </p>
        </form>
    </div>
</div>

<div class="modal fade " id="categoryAddModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content category-modal">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Category</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="category-form">
                    <div class="form-group">
                        <label>Category Name</label>
                        <input type="text" class="form-control" id="category-name" name="category-name">
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button id="category-save-btn" type="submit" class="btn btn-primary">Save
                    changes
                </button>
            </div>
            </form>
        </div>
    </div>
</div>

<script>
    let editor = null;

    function handleSubmit(){
        $('#article-form-submit').on('click', function (e){
            e.preventDefault();
            let article = editor.codemirror.getValue();
            $('#knb-body').val(article);
            $('#knowledge_add').submit();
        })
    }

    function addListener() {
        $('#category-save-btn').on('click', function(e){

            e.preventDefault();

            let categoryName = $('category-name').val();

            $('#category-save-btn').prop("disabled", true);

            $.ajax({
                url: '<?php echo site_url("admin/knowledges/addcategory") ?>',
                type: 'POST',
                data: $('#category-form').serialize(),
                success: function (response) {

                    let r = JSON.parse(response);

                    $('#category-select').empty();
                    $('#category-select').append('<option id="selected-default" value="">Please Pick a Category</option>');

                    r.categories.map( x => {
                        let id = x.id;
                        let name = x.name;
                        $('#category-select').append(`<option value='${id}'>${name}</option>`);
                    });

                    $('#category-select').val(r.newId).prop('selected', true);

                    swal({
                        title: "Success",
                        text: "The Category was successfully added.",
                        type: "success",
                        confirmButtonClass: "btn-success",
                        confirmButtonText: "OK",
                        showCancelButton: false
                    }, function (){

                        $('#category-save-btn').prop('disabled', false);
                        $('#categoryAddModal').modal('hide');

                    });
                },
                error: function () {

                    swal({
                        title: "Uhoh!",
                        text: "Something went wrong!",
                        type: "warning",
                        confirmButtonClass: 'btn-danger',
                        confirmButtonText: "OK",
                        showCancelButton: false
                    }, function () {
                        $('#category-save-btn').prop('disabled', false);
                    });
                }
            });
        });
    }

    $(document).ready(function () {
        addListener();
        editor = new Editor();
        editor.render();
        handleSubmit();
    })
</script>