<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
        <h3 class="page-title pull-left">Knowledge</h3>
        <div class="pull-right" id="btn-div">
            <a href="/admin/knowledges/add/">
                <button class="btn btn-sm btn-primary" id="dt-add-row">
                    <i class="fa fa-plus"></i>&nbsp;&nbsp;Add Knowledge
                </button>
            </a>
        </div>
        <?php if ($this->session->flashdata('msg')) { ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="alert alert-<?php echo $this->session->flashdata('msgtype'); ?> alert-dismissible action-alert" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">x</button>
                    <?php echo $this->session->flashdata('msg'); ?>
                </div>
            </div>
        </div>
        <?php } ?>
        <div class="row">
            <div class="col-lg-12">
                <div id="knowledgesListingTable"></div><hr />
            </div>
        </div>
        <script>
            $(document).ready(function () {
                $("#clearFilter").on("click", function (evt) {
                    evt.preventDefault();
                    $('#knowledges_filter').trigger("reset");
                    $.fn.loadKnowledges();
                    $("#clearFilter").blur();
                });
                $("#knowledgesFilter").on("click", function (evt) {
                    evt.preventDefault();
                    $.fn.loadKnowledges();
                    $("#knowledgesFilter").blur();
                });
                $.fn.loadKnowledges = function () {
                    $.blockUI({target: "#knowledgesListingTable"});
                    $params = $("#knowledges_filter").serialize();
                    $.get('<?php echo base_url('/admin/knowledges/getknowledges'); ?>?' + $params, function (data) {
                        $("#knowledgesListingTable").html(data);
                        $.unblockUI({target: "#knowledgesListingTable"});
                    }).fail(function () {
                        alert("Error!", "Something went wrong loading the Knowledges!", "error");
                    });
                };
                $.fn.loadKnowledges();
            });
        </script>
