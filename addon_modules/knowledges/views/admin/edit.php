<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
        <h3 class="page-title">
            <a href="/admin/knowledges">Knowledge</a> &raquo; <?php echo $title; ?>
        </h3>
		<?php if (isset($msg)) { ?>
		<div class="alert alert-<?php echo $msgtype; ?> alert-dismissible action-alert" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			<?php echo $msg; ?>
		</div>
		<?php } ?>
		<div class="panel">
            <div class="panel-body">
                <form action="" method="post" id="knowledge_edit">
                    <p>
                        <label for="vname">Name</label>
                        <input type="text" class="form-control" name="vname" value="<?php echo $knowledge->vname; ?>">
                    </p>
                    <p class="text-right">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </p>
                </form>
            </div>
		</div>
