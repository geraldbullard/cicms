<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php if (!empty($knowledges)) { ?>
    <div class="">
        <div class="row">
            <div class="col-lg-4">
                <div class="panel">
                    <div class="panel-body">
                        <table class="table table-striped scroll-body" id="knowledges-table" width="100%">
                            <thead>
                            <tr>
                                <th scope="col" hidden>ID</th>
                                <th class="cat" scope="col">Category</th>
                                <th scope="col">Title</th>
                                <th scope="col" hidden>Full Title</th>
                                <th scope="col" hidden>Body</th>
                                <th scope="col" hidden>Creator</th>
                                <th scope="col" hidden>Timestamp</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($knowledges as $knowledge) {
                                ?>
                                <tr>
                                    <td hidden><?php echo $knowledge['id']; ?></td>
                                    <td><?php echo $knowledge['category'] ?></td>
                                    <td  id="titleid-<?php echo $knowledge['id'] ?>">
                                        <a onclick="loadArticle(<?php echo $knowledge['id'] ?>)" id="article-link" class="article" href="#article-panel">
                                        <?php echo strlen($knowledge['short_title']) >= 40 ? $knowledge['short_title'] . '...' : $knowledge['short_title']; ?>
                                        </a>
                                    </td>
                                    <td hidden id="full-titleid-<?php echo $knowledge['id'] ?>"><?php echo $knowledge['title'] ?></td>
                                    <td hidden id="bodyid-<?php echo $knowledge['id'] ?>"><?php echo htmlspecialchars($knowledge['body']) ?></td>
                                    <td hidden id="creatorid-<?php echo $knowledge['id'] ?>"><?php echo $knowledge['creator'] ?></td>
                                    <td hidden id="timeid-<?php echo $knowledge['id'] ?>"><?php echo $knowledge['timestamp'] ?></td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <form method="post" id="edit-form">
                    <div hidden id="article-id">
                    </div>
                    <input id="inpt-article-id" type="hidden" value="" name="article-id">
                    <div id="article-panel" class="panel panel-default">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-sm-4">
                                    Title
                                    <div id="kn-panel-title"></div>
                                </div>
                                <div class="col-sm-4">
                                    Created On
                                    <div id="kn-panel-le"></div>
                                </div>
                                <div class="col-sm-4">
                                    Controls
                                    <div id="kn-panel-ctrl">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body padding-10" id="kn-panel-bdy">
                            <div class="scroll-body" id="article-text">

                            </div>
                            <div id="edit-field">
                            </div>
                        </div>
                    </div>
                    <div class="float-right" id="save-cancel">
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>

        let table = null;
        let editor = null;

        //If on mobile, this will autoscroll you to the article you have just loaded.
        function scrollToArticle(){
            $('a').on('click', function(e){
                if(e.target.id === 'article-link'){
                    e.preventDefault();
                    if(window.screen.width < 1200){
                        $('html, body').animate({scrollTop:$(this.hash).offset().top}, 500);
                    }
                }
            });
        }

        function alignBtnForMobile(){
            if(window.screen.width < 1200){
                $('#btn-div').removeClass('pull-right');
                $('#btn-div').addClass('knowledge-btn-mobile');
            }
        }

        //Searches the in memory copy of the datable for an id and a column.
        function findInTable (id, column){
            let colnum = null;

            if(column === 'article'){
                colnum = 4;
            }
            if(column === 'title'){
                colnum = 3;
            }
            if(column === 'timestamp'){
                colnum = 6;
            }
            if(column === 'creator'){
                colnum = 5;
            }
            if(colnum){
                let tIndexes = table.column(0).data();
                let tArticles = table.column(colnum).data();
                let index = Array.from(tIndexes).indexOf(id.toString());
                return Array.from(tArticles)[index];
            }
        }

        //Loads the article onto the right panel, sets up all of the necessary event listeners.
        function loadArticle(id){

            $('#edit-field').hide();
            $('#edit-cancel').remove();
            $('#edit-save').remove();
            $('#body-text').remove();

            //find the article
            let article = findInTable(id, 'article');
            //if we found it, make sure to sanitize it of any Javascript
            if(article != null){
                article.replace(/<script/g,"&lt;script&gt;").replace(/<\/script/g,"&lt;/script&gt;");
                console.log(article);
                article = article.slice(1, -1);
            }else {//if we didn't find an article, display "Article not found!" and return from the script.
                article = '#Article Not Found!';
                let converter = new showdown.Converter();
                let    html      = converter.makeHtml(he.decode(article));
                $('#article-text').html(html).text();
                return;
            }
            //Update the fields on the article panel, update the hidden field for the form.
            let title = findInTable(id, 'title');
            let timestamp = findInTable(id, 'timestamp');
            let creator = findInTable(id, 'creator');
            $('input#inpt-article-id').val(id);
            $('#article-id').val(id).trigger('change');
            //Create the HTML for the control buttons.
            let ctrl = "<div style='cursor: pointer; margin-right: 5px;' class='btn btn-primary btn-sm' onclick='editKnowledge("+ id +")' ><i class='fa fa-pencil button-icon'></i>Edit</div>";
            ctrl += "<div style='cursor: pointer; margin-left: 5px;' class='btn btn-danger btn-sm' onclick='confirmDeleteKnowledge("+ id +")' ><i class='fa fa-trash button-icon'/>Delete</div>";
            ctrl += "<div id='share-article' class='btn btn-sm btn-info' style='cursor: pointer; margin-left: 10px;' onclick='copyArticle("+ id +")'><i class='fa fa-copy button-icon' />Copy Link</div>"
            //init the converter. Showdown is a library for converting markdown to HTML and viseversa.
            let converter = new showdown.Converter();
            //he is a library that let's us take the sanitized HTML input and convert it back to regular html. Otherwise we just see a display of html special chars.
            let    html      = converter.makeHtml(he.decode(article));
            if(html != '' || html != null){
                html = html.replace(/(?:\\[rn]|[\r\n]+)+/g, '').replace(/\\/g, '');
            }
            //Wire it all up.
            $('#article-text').html(html).text();
            $('#article-text').show();
            $('#kn-panel-le').html(timestamp + ' By: ' + creator);
            $('#kn-panel-title').html(title);
            $('#kn-panel-ctrl').html(ctrl);
            $('#article-id').text(id);
            //add the article id to the end of the query string in the url bar, and add event listeners to any links in the article so they will open in a new tab.
            changeUrl(id);
            openLinksInNewTab();
        }

        //Displays the editor and wires up the submit event listener.
        function editKnowledge(id) {
            let article = findInTable(id, 'article');
            let html = "<textarea name='body' id='body-text' class='form-control article-edit'>"+ article +"</textarea>";
            $('#article-text').hide();
            $('#edit-field').show();
            $('#edit-field').html(html);
            let buttons = "<button type='reset' style='margin-right: 5px;' onclick='cancelEdit("+ id +")' id='edit-cancel' class='btn btn-sm btn-grey'><i class='fa fa-times button-icon' />Cancel</button>" +
                            "<button id='edit-save' style='margin-left: 5px;' type='submit' class='btn btn-sm btn-primary'><i class='fa fa-' /><i class='fa fa-save button-icon' />Save</button>";

            $('#save-cancel').html(buttons);
            activateEditor();
            submitEditListner();
        }

        //Changes the URL to match what id article we have loaded.
        function changeUrl(id){
            let url = createUrl(id);
            history.replaceState({}, null, url);
        }

        //copys a link to the article into the users clipboard.
        function copyArticle(id){
            let url = createUrl(id);
            let dummy = document.createElement("textarea");
            document.body.appendChild(dummy);
            dummy.value = url;
            dummy.select();
            document.execCommand("copy");
            document.body.removeChild(dummy);
        }

        //Creates a linkable url for a specific article.
        function createUrl(id){
            let url = window.location.origin;
            url += "/admin/knowledges?id=" + id;
            return url;
        }

        //removes editor and reloads the same article.
        function cancelEdit(id){
            $('#edit-cancel').remove();
            $('#edit-save').remove();
            $('#body-text').remove();
            $('#edit-field').hide();
            loadArticle(id);
        }

        //Saves the changes to the datatable's internal copy of the table.
        function saveDTChanges(body, id){
            table.rows().every(function (){
                let d = this.data();
                if(d[0] === id.toString() ){
                    d[4] = body;
                    this.data(d).draw(false);
                }
            });
        }

        //event listener for submiting an edited article.
        function submitEditListner(){
            $('#edit-save').on('click', function(e){
               e.preventDefault();

               let el = $('.icon-preview.active');

               if(el.length){//If the preview is active on the article, we want to deactivate it before submitting as it blocks the confirmation modal.
                   el.trigger('click');
               }

               let id = $('#article-id').text();

               $.ajax({
                   url: '<?php echo site_url("admin/knowledges/edit") ?>',
                   type: 'POST',
                   data: $('#edit-form').serialize()+"&text="+editor.codemirror.getValue(),//this is how we get data from the markdown editor.
                   success: function (response){
                       let r = JSON.parse(response);
                       swal({
                           title: "Success",
                           text: "The Article was successfully updated.",
                           type: "success",
                           confirmButtonClass: "btn-success",
                           confirmButtonText: "OK",
                           showCancelButton: false
                       }, function (){
                           saveDTChanges(r.body, id);
                           cancelEdit(id);
                           loadArticle(id);
                       });
                   },
                   error: function(){
                       swal({
                           title: "Uhoh!",
                           text: "Something went wrong! We could not save your changes.",
                           type: "warning",
                           confirmButtonClass: 'btn-danger',
                           confirmButtonText: "OK",
                           showCancelButton: false
                       });
                   }
               });
            });
        }

        //activates the markdown editor.
        function activateEditor(){
            editor = new Editor();
            editor.render();
        }

        //This function iterates over all 'a' links in the loaded article, and adds an event listener that will make the links open in a new tab.
        function openLinksInNewTab(){
            $('#article-text').find('a').each(function (){
                $(this).on('click', function(e){
                    e.preventDefault();
                    let url = $(this).attr('href');
                    window.open(url, '_blank');
                });
            });
        }

        //If there are any querystring params, E.G., Article ids, in the URL, we capture them and load the article.
        function queryStringParams(name = 'id'){
            let results = new RegExp('[\?&]' + name + '=([^&#]*)')
                .exec(window.location.search);

            if(results !== null){
                loadArticle(results[1]);
            }
        }

        //Deletes are article via AJAX.
        function confirmDeleteKnowledge(id) {
            swal({
                    title: "Delete Knowledge",
                    text: "Are you sure you wish to delete this Knowledge?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Delete",
                    closeOnConfirm: false
                },
                function() {
                    $.ajax({
                        url: '/admin/knowledges/delete/' + id,
                        type: 'GET',
                        success: function(response) {
                            var data = JSON.parse(response);
                            if (data.status == 'success') {
                                $.fn.loadKnowledges();
                                swal({
                                    title: "Success",
                                    text: "The Knowledge was successfully deleted.",
                                    type: "success",
                                    confirmButtonClass: "btn-success",
                                    confirmButtonText: "OK",
                                    showCancelButton: false
                                });
                            } else {
                                swal({
                                    title: "Error",
                                    text: "The was an error deleting the Knowledge.",
                                    type: "warning",
                                    confirmButtonClass: "btn-danger",
                                    confirmButtonText: "OK",
                                    showCancelButton: false
                                });
                            }
                        }
                    });
                });
        }

        jQuery(document).ready(function() {
            scrollToArticle();
            alignBtnForMobile();
            table = $('#knowledges-table').DataTable({
                "dom": 'Blftip',
                "bSortCellsTop": true,
                "pageLength": <?php /* echo $this->config->item('knowledgesAdminPagination', 'settings');*/ ?>10,
                "lengthMenu": [
                    [<?php /*echo $this->config->item('knowledgesAdminPagination', 'settings');*/ ?> 10, 30, 60, 100, -1],
                    [<?php /*echo $this->config->item('knowledgesAdminPagination', 'settings'); */?>10, 30, 60, 100, "All"]
                ],
                "order": [
                    [0, "asc"] // "desc" newest first, "asc" for oldest first
                ],
                responsive: true, // make table responsive
                pagingType: "simple",
                info: false,
                lengthChange: false,
                buttons: [ // relabel the export button
                    //'copy', 'excel'
                    {
                        extend: 'excel',
                        text: 'Export',
                        title: 'Knowledges-Export',
                        className: 'btn btn-sm btn-primary shadow-sm',
                        exportOptions: {
                            columns: [0, 1]
                        },
                        enabled: false
                    }
                ],
                "initComplete": function(settings, json) { // do something immediately after the table is drawn
                    //This adds the category dropdown. I kinda just copied it off a SOF thread.
                    this.api().columns('.cat').every( function () {
                        var column = this;
                        var select = $('<select class="form-control category-select"><option value="">Select Category</option></select>')
                            .prependTo( $('#knowledges-table_filter') )
                            .on( 'change', function () {
                                var val = $.fn.dataTable.util.escapeRegex(
                                    $(this).val()
                                );

                                column
                                    .search( val ? '^'+val+'$' : '', true, false )
                                    .draw();
                            } );

                        column.data().unique().sort().each( function ( d, j ) {
                            select.append( '<option value="'+d+'">'+d+'</option>' )
                        } );
                    } );
                },
                "oLanguage": { // adjust the text for the rows dropdown
                    "sLengthMenu": "_MENU_ Rows"
                },
                "aoColumns": [ // needed to keep Actions col from being sortable and searchable
                    /* id */ { "bSearchable": true, "bSortable": true },
                    /* category name */ { "bSearchable": true, "bSortable": true },
                    /* short title */{ "bSearchable": true, "bSortable": true },
                    /* full title */{ "bSearchable": true, "bSortable": false },
                    /* body */{ "bSearchable": true, "bSortable": false },
                    /* creator */{ "bSearchable": false, "bSortable": false },
                    /* timestamp */{ "bSearchable": false, "bSortable": false }
                ]
            });
            $('.dt-buttons').remove();
            table.on('draw', function () {
                // run a function or other action
            });
            //needs to run AFTER Datables has initialized.
            queryStringParams();
        });
        openLinksInNewTab();
    </script>
<?php } else { ?>
    None
<?php } ?>