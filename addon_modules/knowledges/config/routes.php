<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* knowledges Routes */
$route['admin/knowledges'] = 'knowledges/admin_index';
$route['admin/knowledges/add'] = 'knowledges/add';
$route['admin/knowledges/getknowledges'] = 'knowledges/getKnowledges';
$route['admin/knowledges/edit'] = 'knowledges/edit';
$route['admin/knowledges/update/(:any)'] = 'knowledges/update/$1';
$route['admin/knowledges/delete/(:any)'] = 'knowledges/delete/$1';
$route['admin/knowledges/article/(:any)'] = 'knowledges/article/$1';
$route['admin/knowledges/categories/'] = 'knowledges/categories';
$route['admin/knowledges/addcategory'] = 'knowledges/addcategory';
