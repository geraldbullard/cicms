<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Knowledge Model
 */
class Knowledges_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();

        $this->_KNOWLEDGES_TABLE = $this->db->dbprefix . "knowledge_base";
        $this->_KNOWLEDGES_CATEGORIES = $this->db->dbprefix . "knowledge_base_categories";
        $this->_USERS_TABLE = $this->db->dbprefix . "users";

	}

	// Filter Knowledges
	public function filter($filter)
	{

        $tableSQL = "SELECT $this->_KNOWLEDGES_TABLE.id, name AS category, $this->_KNOWLEDGES_TABLE.timestamp,
        SUBSTRING(title, 1, 40) AS short_title, title, body, CONCAT(first_name, ' ', last_name) AS creator
        FROM $this->_KNOWLEDGES_TABLE
        JOIN $this->_KNOWLEDGES_CATEGORIES ON $this->_KNOWLEDGES_CATEGORIES.id = $this->_KNOWLEDGES_TABLE.knowledge_base_categories_id
        LEFT JOIN $this->_USERS_TABLE ON $this->_USERS_TABLE.id = $this->_KNOWLEDGES_TABLE.users_id
        ORDER BY title;";

        $query = $this->db->query($tableSQL);

        return $query->result_array();

	}


//	function getKnowledge($id)
//	{
//		$articleSQL="
//		SELECT $this->_KNOWLEDGES_TABLE.*, CONCAT(first_name, ' ', last_name) AS creator
//		FROM $this->_KNOWLEDGES_TABLE
//        LEFT JOIN $this->_USERS_TABLE ON $this->_USERS_TABLE.id = $this->_KNOWLEDGES_TABLE.users_id
//        WHERE $this->_KNOWLEDGES_TABLE.id = $id
//        LIMIT 1;
//		";
//
//		$article = $this->db->query($articleSQL)->row_array();
//
//		return $article;
//	}

    function updateArticle($id, $bodyChanges){
	    try{
            $data = array(
                'body' => $bodyChanges
            );

            $this->db->where('id', $id);
            $this->db->update($this->_KNOWLEDGES_TABLE, $data);

            return 1;
        }catch (Exception $ex){
            return 0;
        }
    }

    function getCategories(){

	    $catSQL = "
	    SELECT * FROM $this->_KNOWLEDGES_CATEGORIES
        ORDER BY name;
	    ";

	    $query = $this->db->query($catSQL);

	    return $query->result_array();
    }

    function addCategory($categoryName){

	    $data = array(
	        'name' => $categoryName
        );
	    $this->db->insert($this->_KNOWLEDGES_CATEGORIES, $data);

	    return $this->db->insert_id();
    }

	function add($knowledgeArr)
	{
	    if(!is_numeric($knowledgeArr['users_id']) || !is_numeric($knowledgeArr['knowledge_base_categories_id'])){
	        die('Invalid Argument');
        }
	    $knowledgeArr['title'] = $this->db->escape($knowledgeArr['title']);
	    //$knowledgeArr['body'] = $this->db->escape($knowledgeArr['body']);
	    //print_r($knowledgeArr);
	    //die();
		return $this->db->insert($this->_KNOWLEDGES_TABLE, $knowledgeArr);
	}

	function update()
	{
		return true;
	}

	function delete($id)
	{
		$this->db->where("id", $id);

		if ($this->db->delete($this->_KNOWLEDGES_TABLE)) return true;
		return false;
	}

}
