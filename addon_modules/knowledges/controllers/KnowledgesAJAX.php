<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KnowledgesAJAX extends MY_Controller
{

	function __construct()
	{
		parent::__construct();

		// First check if logged in
		if (!$this->session->userdata('user_id'))
		{
			// set the url they were trtying to go to in session
			$this->session->set_userdata('page_url', current_url());

			//Redirect to login
			redirect('user/login');
		}

		// Get site wide settings first
		$this->Settings_model->loadSitewideSettings();

		// Load This Model
		$this->load->model('Knowledges_model');
	}

	public function index(){
	    echo json_encode("sup");
    }

	public function article($id){
	    $resultArticle = $this->knowledges_model->getKnowledge($id);

	    if($resultArticle != null){
	        http_response_code(200);
	        echo json_encode($resultArticle);
	        return;
        }

	    http_response_code(500);
	    echo json_encode($resultArticle);
    }

}
