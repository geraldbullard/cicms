<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Knowledges extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        // First check if logged in
        if (!$this->session->userdata('user_id')) {
            // set the url they were trtying to go to in session
            $this->session->set_userdata('page_url', current_url());

            //Redirect to login
            redirect('user/login');
        }

        // Get site wide settings first
        $this->Settings_model->loadSitewideSettings();

        // Load This Model
        $this->load->model('Knowledges_model');
    }

    public function admin_index()
    {
        $this->data = array();
        $this->data['title'] = 'Knowledges Index';

        // load the config from the current module
        $this->load->config('knowledges');
        $this->data['knowledges_config'] = $this->config->config['knowledges'];

        // Added for developer dropdown
        $knowledges = $this->db->get($this->db->dbprefix . "knowledge_base");
        $this->data['dev_data']['knowledges'] = null;//$knowledges->result_array();

        $this->load->view('layout/admin/header.php', $this->data);
        $this->load->view('admin/index', $this->data);
        $this->load->view('layout/admin/footer.php', $this->data);
    }

    public function getKnowledges()
    {
        $filter = new stdClass();
        // Set filter data based on post input, none by default
        //$filter->first_name = ($this->input->get_post('filterfirstname') != '') ? $this->input->get_post('filterfirstname') : null;

        $data['knowledges'] = $this->Knowledges_model->filter($filter);

        return json_encode($this->load->view('admin/filter', $data));
    }

    public function add()
    {
        $this->data = array();

        if ($this->input->post()) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('category-id', 'Category Name', 'trim|required');
            $this->form_validation->set_rules('title', 'Title', 'trim|required');
            $this->form_validation->set_rules('body', 'Body', 'trim|required');

            if ($this->form_validation->run() !== FALSE) {
                $data = array(
                    'users_id' => $this->ion_auth->get_user_id(),
                    'knowledge_base_categories_id' => $this->input->post('category-id'),
                    'title' => $this->input->post('title'),
                    'body' => $this->input->post('body')
                );
                if ($this->Knowledges_model->add($data)) {
                    $this->data['msg'] = "The Knowledge was added successfully.";
                    $this->data['msgtype'] = "success";
                } else {
                    $this->data['msg'] = "There was a problem adding the Knowledge.";
                    $this->data['msgtype'] = "danger";
                }
//				if ($this->db->insert($this->db->dbprefix . "knowledges", $data)) {
//					$this->data['msg'] = "The Knowledge was added successfully.";
//					$this->data['msgtype'] = "success";
//				} else {
//					$this->data['msg'] = "There was a problem adding the Knowledge.";
//					$this->data['msgtype'] = "danger";
//				}
            } else {
                $this->data['msg'] = "Your submission has the following Problems:<br>" . validation_errors();
                $this->data['msgtype'] = "danger";
                $this->data['body'] = $this->input->post('body');
                $this->date['articleTitle'] = $this->input->post('title');
                $this->data['categoryId'] = $this->input->post('category-id');
            }
        }

        $this->data['categories'] = $this->Knowledges_model->getCategories();
        $this->data['title'] = 'Knowledge Add';

        $this->load->view('layout/admin/header.php', $this->data);
        $this->load->view('admin/add', $this->data);
        $this->load->view('layout/admin/footer.php', $this->data);
    }

    public function edit()
    {
        $articleChanges = $this->input->post('text');
        //print_r($this->input->post());
        $articleId = $this->input->post('article-id');

        $result = $this->Knowledges_model->updateArticle($articleId, $articleChanges) > 0;

        $respone_array = array();

        if($result){
            http_response_code(200);
            $respone_array['status'] = 'success';
            $respone_array['body'] = $articleChanges;
            echo json_encode($respone_array);
        }else {
            http_response_code(500);
            $respone_array['status'] = 'failure';
            echo json_encode($respone_array);
        }
    }


    public function delete()
    {
        $knowledgeid = $this->uri->segment(4);
        if ($this->Knowledges_model->delete($knowledgeid) !== false) {
            $response_array['status'] = 'success';
            echo json_encode($response_array);
        } else {
            $response_array['status'] = 'error';
            echo json_encode($response_array);
        }
    }

    public function categories()
    {
        $categories = $this->db->getCategories();
        echo json_encode($categories);
    }

    public function addcategory()
    {

        $categoryToAdd = $this->input->post('category-name');

        $newId = $this->Knowledges_model->addCategory($categoryToAdd);

        $response_array['newId'] = $newId;
        $response_array['status'] = 'success';
        $response_array['categories'] = $this->Knowledges_model->getCategories();

        http_response_code(200);
        echo json_encode($response_array);
    }

    public function article($id)
    {
        $resultArticle = $this->Knowledges_model->getKnowledge($id);

        if ($resultArticle != null) {
            http_response_code(200);
            echo json_encode($resultArticle);
            return;
        }

        http_response_code(500);
        echo json_encode($resultArticle);
    }
}
