<?php
/* Module functions file */

// If installing the module run this function to create db table(s)
if (!function_exists('bp_installModule')) {
    function bp_installModule() {
        $CI =& get_instance();

        // Update the module status
        $CI->db->set('status', 1);
        $CI->db->where("module", "priorities");
        $CI->db->update($CI->db->dbprefix . "modules");

        // Create table(s)
        $CI->db->query("
			CREATE TABLE IF NOT EXISTS `" . $CI->db->dbprefix . "priorities` ( 
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`projects_id` int(11) NOT NULL,
				`users_id` int(10) UNSIGNED NOT NULL,
				`task_id` int(11) NOT NULL,
				`sort_order` int(11) DEFAULT NULL,
				PRIMARY KEY (`id`),
				KEY `bp_content_pid_idx` (`projects_id`),
				KEY `bp_content_uid_idx` (`users_id`),
				KEY `bp_content_tid_idx` (`task_id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");

        return true;
    }
}

// If uninstalling the module run this function to remove db entries if selected
if (!function_exists('bp_uninstallModule')) {
    function bp_uninstallModule() {
        $CI =& get_instance();

        // Update the module status
        $CI->db->set('status', -1);
        $CI->db->where("module", "priorities");
        $CI->db->update($CI->db->dbprefix . "modules");

        // Drop table(s)
        $CI->db->query("
            DROP TABLE IF EXISTS `" . $CI->db->dbprefix . "priorities`;
        ");

        return true;
    }
}
