<?php
/* Priorities module menu file */

$_menu = array(
    'url' => '/admin/priorities/',
    'title' => 'Priorities',
    'icon' => 'lnr lnr-star',
    'access' => 1,
);