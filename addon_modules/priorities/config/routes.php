<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* Priorities Routing */
$route['admin/priorities'] = 'priorities/admin_index';
$route['admin/priorities/add'] = 'priorities/add';
$route['admin/priorities/edit/(:any)'] = 'priorities/edit/$1';
$route['admin/priorities/update/(:any)'] = 'priorities/update/$1';
$route['admin/priorities/delete/(:any)'] = 'priorities/delete/$1';

