<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Priorities extends MY_Controller
{

	function __construct()
	{
		parent::__construct();
		// Get site wide settings first
		$this->Settings_model->loadSitewideSettings();

		// Load This Model
		$this->load->model('Priorities_model');
	}

	public function admin_index()
	{
		if (!$this->session->userdata('user_id'))
		{
			// set the url they were trtying to go to in session
			$this->session->set_userdata('page_url', current_url());

			//Redirect to login
			redirect('user/login');
		} else {
			$this->data = array();
			$this->data['title'] = 'Priorities Index';

			// load the config from the current module
			$this->load->config('priorities');
			$this->data['priorities_config'] = $this->config->config['priorities'];
			// Added for developer dropdown
			$this->data['dev_data'] = '';

			$this->load->view('layout/admin/header.php', $this->data);
			$this->load->view('admin/index', $this->data);
			$this->load->view('layout/admin/footer.php', $this->data);
		}
	}

	public function add()
	{
		if (!$this->session->userdata('user_id'))
		{
			// set the url they were trtying to go to in session
			$this->session->set_userdata('page_url', current_url());

			//Redirect to login
			redirect('user/login');
		} else {
			$this->data = array();
			$this->data['title'] = 'Priorities Add';

			$this->load->view('layout/admin/header.php', $this->data);
			$this->load->view('admin/add', $this->data);
			$this->load->view('layout/admin/footer.php', $this->data);
		}
	}

	public function edit()
	{
		if (!$this->session->userdata('user_id'))
		{
			// set the url they were trtying to go to in session
			$this->session->set_userdata('page_url', current_url());

			//Redirect to login
			redirect('user/login');
		} else {
			$this->data = array();
			$this->data['title'] = 'Priorities Edit';

			$this->load->view('layout/admin/header.php', $this->data);
			$this->load->view('admin/edit', $this->data);
			$this->load->view('layout/admin/footer.php', $this->data);
		}
	}

}
