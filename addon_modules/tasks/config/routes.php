<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* Tasks Routing */
$route['admin/tasks'] = 'tasks/admin_index';
$route['admin/tasks/add'] = 'tasks/add';
$route['admin/tasks/getTasks'] = 'tasks/getTasks';
$route['admin/tasks/edit/(:any)'] = 'tasks/edit/$1';
$route['admin/tasks/update/(:any)'] = 'tasks/update/$1';
$route['admin/tasks/delete/(:any)'] = 'tasks/delete/$1';
$route['admin/tasks/activate/(:any)'] = 'tasks/activate/$1';
$route['admin/tasks/deactivate/(:any)'] = 'tasks/deactivate/$1';
$route['admin/tasks/archive/(:any)'] = 'tasks/archive/$1';

