<?php
/* Module functions file */

// If installing the module run this function to create db table(s)
if (!function_exists('bp_installModule')) {
    function bp_installModule() {
        $CI =& get_instance();

        // Update the module status
        $CI->db->set('status', 1);
        $CI->db->where("module", "tasks");
        $CI->db->update($CI->db->dbprefix . "modules");

        // Create table(s)
        $CI->db->query("
			CREATE TABLE IF NOT EXISTS `" . $CI->db->dbprefix . "tasks` (
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`projects_id` int(11) NOT NULL,
				`users_id` int(10) UNSIGNED NOT NULL,
				`task` text NOT NULL,
				`date_modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
				`is_completed` tinyint(1) NOT NULL DEFAULT '0',
				`is_priority` tinyint(1) NOT NULL,
				`archived` tinyint(1) NOT NULL,
				`deadline` date DEFAULT NULL,
				`calendar_date` date DEFAULT NULL,
				`out_of_scope` tinyint(1) DEFAULT '0',
				`users_id_updated_by` int(11) DEFAULT NULL,
				`users_id_created_by` int(11) DEFAULT NULL,
				`created_timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
				`time_estimate` decimal(4,2) DEFAULT NULL,
				`sort_order` int(11) DEFAULT NULL,
				`weighted` tinyint(1) DEFAULT '0',
				PRIMARY KEY (`id`),
				KEY `bp_tasks_pid_idx` (`projects_id`),
				KEY `bp_tasks_uid_idx` (`users_id`),
				KEY `bp_tasks_order_idx` (`is_completed`,`archived`,`date_modified`),
				KEY `bp_tasks_sort_order_idx` (`sort_order`),
				KEY `bp_tasks_uuid_idx` (`users_id_updated_by`),
				KEY `bp_tasks_ucid_idx` (`users_id_created_by`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
        ");

        return true;
    }
}

// If uninstalling the module run this function to remove db entries if selected
if (!function_exists('bp_uninstallModule')) {
    function bp_uninstallModule() {
        $CI =& get_instance();

        // Update the module status
        $CI->db->set('status', -1);
        $CI->db->where("module", "tasks");
        $CI->db->update($CI->db->dbprefix . "modules");

        // Drop table(s)
        $CI->db->query("
            DROP TABLE IF EXISTS `" . $CI->db->dbprefix . "tasks`;
        ");

        return true;
    }
}
