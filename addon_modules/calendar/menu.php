<?php
/* Calendar module menu file */

$_menu = array(
    'url' => '/admin/calendar/',
    'title' => 'Calendar',
    'icon' => 'lnr lnr-calendar-full',
    'access' => 1,
);