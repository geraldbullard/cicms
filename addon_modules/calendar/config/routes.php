<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* Calendar Routing */
$route['admin/calendar'] = 'calendar/admin_index';
$route['admin/calendar/add'] = 'calendar/add';
$route['admin/calendar/getCalendar'] = 'calendar/getCalendar';
$route['admin/calendar/edit/(:any)'] = 'calendar/edit/$1';
$route['admin/calendar/update/(:any)'] = 'calendar/update/$1';
$route['admin/calendar/delete/(:any)'] = 'calendar/delete/$1';

