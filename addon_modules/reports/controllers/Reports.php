<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends MY_Controller
{

	function __construct()
	{
		parent::__construct();

		// First check if logged in
		if (!$this->session->userdata('user_id'))
		{
			// set the url they were trtying to go to in session
			$this->session->set_userdata('page_url', current_url());

			//Redirect to login
			redirect('user/login');
		}

		// Get site wide settings first
		$this->Settings_model->loadSitewideSettings();

		// Load This Model
		$this->load->model('Reports_model');
	}

	public function admin_index()
	{
		$this->data = array();
		$this->data['title'] = 'Reports Index';

		// load the config from the current module
		$this->load->config('reports');
		$this->data['reports_config'] = $this->config->config['reports'];


		$this->load->view('layout/admin/header.php', $this->data);
		$this->load->view('admin/index', $this->data);
		$this->load->view('layout/admin/footer.php', $this->data);
	}

	public function searchClients(){
	     $res = $this->Reports_model->searchClients($this->input->post('search'));
	     echo json_encode($res);
    }

    public function searchProjects(){
	    $res = $this->Reports_model->searchProjects($this->input->post('search'));
	    echo json_encode($res);
    }

    public function clientDomains(){
	    $res = $this->Reports_model->clientDomains($this->input->post('clientId'));
	    echo json_encode($res);
    }

	public function getReports()
	{
		$filter = new stdClass();

		$data['reports'] = $this->Reports_model->filter($filter);
        $data["team"] = $this->Reports_model->getTeam();
        $data["categories"] = $this->Reports_model->getCategories();

		return json_encode($this->load->view('admin/filter', $data));
	}

	public function generateReport(){
        $path = $this->input->post('path');

        if($path == 'client'){
            echo json_encode($this->Reports_model->clientReport($this->input->post()));
        }else {
            echo json_encode($this->Reports_model->projectReport($this->input->post()));
        }
    }

    public function printReport(){
	    $records = json_decode($this->input->post('records'));
	    $tableType = $this->input->post('project-view-check');
	    $reportHours = json_decode($this->input->post('report-hours'));
	    $this->data['reportHours'] = $reportHours;

        if($tableType == 0){
            $this->data['records'] = $records;
            if(!isset($_POST['print-pdf'])){
                $this->load->view('admin/print-report', $this->data);
            }else {
                $this->load->view('admin/report-pdf', $this->data);
            }
        }else{
            $this->data['records'] = $records;
            $this->load->view('admin/print-project-report', $this->data);
        }
    }

    public function toggleBillable(){
	    $recordId = json_decode($this->input->post('recordId'));
	    if($this->Reports_model->toggleBillable($recordId) == true){
            $response_array['status'] = 'success';
            $response_array['status_code'] = "0";
            http_response_code(200);
            echo json_encode($response_array);
        }else{
            $response_array['status'] = 'error';
            $response_array['status_code'] = "907";
            http_response_code(500);
            echo json_encode($response_array);
        }
    }
}
