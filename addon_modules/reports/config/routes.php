<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* Reports Routes */
$route['admin/reports'] = 'reports/admin_index';
$route['admin/reports/add'] = 'reports/add';
$route['admin/reports/getReports'] = 'reports/getReports';
$route['admin/reports/edit/(:any)'] = 'reports/edit/$1';
$route['admin/reports/update/(:any)'] = 'reports/update/$1';
$route['admin/reports/delete/(:any)'] = 'reports/delete/$1';
$route['admin/reports/searchClients'] = 'reports/searchClients';
$route['admin/reports/searchProjects'] = 'reports/searchProjects';
$route['admin/reports/clientDomains'] = 'reports/clientDomains';
$route['admin/reports/generateReport'] = 'reports/generateReport';
$route['admin/reports/printReport'] = 'reports/printReport';

$route['admin/reports/toggleBillable'] = 'reports/toggleBillable';
