<?php
/* Kanban module menu file */

$_menu = array(
    'url' => '/admin/reports/',
    'title' => 'Reports',
    'icon' => 'lnr lnr-chart-bars',
    'access' => 1,
//    'sub_items' => array(
//    	// Repeat main menu since it has subs
//	    'reports' => array(
//		    'url' => '/admin/reports/',
//		    'title' => 'Reports',
//		    'icon' => 'lnr lnr-leaf',
//		    'access' => 1,
//	    ),
//	    'add' => array(
//		    'url' => '/admin/reports/add/',
//		    'title' => 'Add',
//		    'icon' => 'lnr lnr-plus-circle',
//		    'access' => 3,
//	    ),
//    ),
);