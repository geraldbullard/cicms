<?php
defined('BASEPATH') or exit('No direct script access allowed');

/*
 * Report Model
 */

class Reports_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();

        $this->_USERS_TABLE = $this->db->dbprefix . "users";
        $this->_CLIENTS_TABLE = $this->db->dbprefix . "clients";
        $this->_CATEGORIES_TABLE = $this->db->dbprefix . "categories";
        $this->_PROJECTS_TABLE = $this->db->dbprefix . "projects";
        $this->_DOMAINS_TABLE = $this->db->dbprefix . "domains";
        $this->_USERS_TO_PROJECTS_TABLE = $this->db->dbprefix . "users_to_projects";
        $this->_TIME_RECORDS_TABLE = $this->db->dbprefix . "time_records";
        $this->_USERS_TO_PROJECTS_TABLE = $this->db->dbprefix . "users_to_projects";
        $this->_TIME_RECORDS_TO_TASKS_TABLE = $this->db->dbprefix . "time_records_to_tasks";
        $this->_TASKS_TABLE = $this->db->dbprefix . "tasks";

        $this->_SQL = "SELECT 
       $this->_TIME_RECORDS_TABLE.id,
       $this->_TIME_RECORDS_TABLE.projects_id,
       $this->_TIME_RECORDS_TABLE.date,
       $this->_TIME_RECORDS_TABLE.start_time,
       $this->_TIME_RECORDS_TABLE.end_time,
       $this->_TIME_RECORDS_TABLE.notes,
       $this->_TIME_RECORDS_TABLE.billable,
       
       $this->_TIME_RECORDS_TO_TASKS_TABLE.tasks_id,
       $this->_TASKS_TABLE.task,
       
       $this->_CLIENTS_TABLE.client_name,
       $this->_CLIENTS_TABLE.id AS client_id,
       
       $this->_CATEGORIES_TABLE.id AS category_id,
       $this->_CATEGORIES_TABLE.name AS category_name,
       $this->_CATEGORIES_TABLE.standard AS category_standard,
        
       $this->_DOMAINS_TABLE.id AS domain_id,
       $this->_DOMAINS_TABLE.domain AS domain_name,
       
       $this->_PROJECTS_TABLE.id AS project_id,
       $this->_PROJECTS_TABLE.time_estimate AS project_time_estimate,
       $this->_PROJECTS_TABLE.clients_id AS project_client_id,
       $this->_PROJECTS_TABLE.categories_id AS project_categories_id,
       T.project_category_name,
       
       $this->_USERS_TABLE.id AS user_id,
        CONCAT($this->_USERS_TABLE.last_name, ', ', $this->_USERS_TABLE.first_name) AS user_name

        FROM $this->_TIME_RECORDS_TABLE
        LEFT JOIN $this->_CLIENTS_TABLE ON $this->_TIME_RECORDS_TABLE.clients_id = $this->_CLIENTS_TABLE.id
        LEFT JOIN $this->_PROJECTS_TABLE ON $this->_TIME_RECORDS_TABLE.projects_id = $this->_PROJECTS_TABLE.id
        LEFT JOIN $this->_CATEGORIES_TABLE ON $this->_TIME_RECORDS_TABLE.categories_id = $this->_CATEGORIES_TABLE.id
        LEFT JOIN $this->_DOMAINS_TABLE ON $this->_TIME_RECORDS_TABLE.domains_id = $this->_DOMAINS_TABLE.id
        LEFT JOIN $this->_USERS_TABLE ON $this->_TIME_RECORDS_TABLE.users_id = $this->_USERS_TABLE.id 
        LEFT JOIN $this->_TIME_RECORDS_TO_TASKS_TABLE ON $this->_TIME_RECORDS_TABLE.id = $this->_TIME_RECORDS_TO_TASKS_TABLE.time_records_id
        LEFT JOIN $this->_TASKS_TABLE on $this->_TIME_RECORDS_TO_TASKS_TABLE.tasks_id = $this->_TASKS_TABLE.id
        LEFT JOIN ( 
	                SELECT $this->_PROJECTS_TABLE.id, $this->_CATEGORIES_TABLE.name AS project_category_name 
	                FROM $this->_PROJECTS_TABLE
	                LEFT JOIN $this->_CATEGORIES_TABLE ON $this->_PROJECTS_TABLE.categories_id = $this->_CATEGORIES_TABLE.id 
	              ) AS T ON T.id = $this->_PROJECTS_TABLE.id ";
    }

    // Filter Reports
    public function filter($filter)
    {
        return true;
    }

    public function getTeam()
    {
        $SQL = "SELECT id, first_name, last_name FROM $this->_USERS_TABLE WHERE active = 1
                ORDER BY last_name, first_name";
        $query = $this->db->query($SQL);
        $r = $query->result_array();

        return $r;
    }

    public function getCategories()
    {
        $SQL = "SELECT * FROM $this->_CATEGORIES_TABLE
	    ORDER BY name";
        $query = $this->db->query($SQL);
        return $query->result_array();
    }

    function searchClients($clientNameSearchString)
    {
        $clientNameSearchString = $this->db->escape_like_str($clientNameSearchString);

        $SQL = "SELECT id, client_name FROM $this->_CLIENTS_TABLE
        WHERE client_name LIKE '%$clientNameSearchString%'";
        $query = $this->db->query($SQL);
        $r = $query->result_array();

        return $r;
    }

    function clientDomains($clientId)
    {
        $clientId = $this->db->escape($clientId);
        $SQL = "SELECT id, domain FROM $this->_DOMAINS_TABLE WHERE clients_id = $clientId
	    ORDER BY domain";
        $query = $this->db->query($SQL);
        return $query->result_array();
    }

    function searchProjects($projectSearchString)
    {
        $user_id = $this->session->userdata('user_id');
        $projectSearchString = $this->db->escape_like_str($projectSearchString);

        $SQL = "SELECT $this->_CATEGORIES_TABLE.name AS cat_name, $this->_CATEGORIES_TABLE.id AS cat_id, $this->_DOMAINS_TABLE.domain, $this->_PROJECTS_TABLE.*, $this->_CLIENTS_TABLE.client_name, $this->_CLIENTS_TABLE.id AS client_table_id
                FROM $this->_PROJECTS_TABLE
                JOIN $this->_USERS_TO_PROJECTS_TABLE ON $this->_USERS_TO_PROJECTS_TABLE.projects_id = $this->_PROJECTS_TABLE.id
                LEFT JOIN $this->_CLIENTS_TABLE ON $this->_CLIENTS_TABLE.id = $this->_PROJECTS_TABLE.clients_id
                LEFT JOIN $this->_DOMAINS_TABLE ON $this->_DOMAINS_TABLE.id = $this->_PROJECTS_TABLE.domains_id
                LEFT JOIN $this->_CATEGORIES_TABLE ON $this->_CATEGORIES_TABLE.id = $this->_PROJECTS_TABLE.categories_id
                WHERE $this->_USERS_TO_PROJECTS_TABLE.users_id = $user_id AND (
                ($this->_DOMAINS_TABLE.domain LIKE '%$projectSearchString%') 
                OR ($this->_CLIENTS_TABLE.client_name LIKE '%$projectSearchString%') 
                OR ($this->_CATEGORIES_TABLE.name LIKE '%$projectSearchString%' ESCAPE '!')) ";

        $query = $this->db->query($SQL);

        return $query->result_array();
    }

    function projectReport($request)
    {
        $memberId = $request['memberId'];
        $projectId = $request['searchId'];
        $start = $this->db->escape($request['startDate']);
        $end = $this->db->escape($request['endDate']);

        $SQL = $this->_SQL . "WHERE date >= $start AND date <= $end ";

        if ($memberId != '' and $memberId != 0) {
            $memberId = $this->db->escape($memberId);
            $SQL = $SQL . "AND ($this->_TIME_RECORDS_TABLE.users_id = $memberId) ";
        }

        if ($projectId != '' and $projectId != 0) {
            $projectId = $this->db->escape($projectId);
            $SQL = $SQL . "AND ($this->_TIME_RECORDS_TABLE.projects_id = $projectId)";
        }

        $query = $this->db->query($SQL);

        return $query->result_array();
    }

    function clientReport($request)
    {
        $clientId = $request['searchId'];
        $domainId = $request['domainId'];
        $categoryId = $request['categoryId'];
        $memberId = $request['memberId'];
        $start = $this->db->escape($request['startDate']);
        $end = $this->db->escape($request['endDate']);

        $SQL =  $this->_SQL." WHERE date >= $start AND date <= $end ";

        if ($clientId != '') {
            $clientId = $this->db->escape($clientId);
            $SQL = $SQL . "AND ( $this->_TIME_RECORDS_TABLE.clients_id = $clientId) ";
        }
        if ($domainId != '' and $domainId != 0) {
            $domainId = $this->db->escape($domainId);
            $SQL = $SQL . "AND ($this->_TIME_RECORDS_TABLE.domains_id = $domainId) ";
        }
        if ($categoryId != '' and $categoryId != 0) {
            $categoryId = $this->db->escape($categoryId);
            $SQL = $SQL . "AND ($this->_TIME_RECORDS_TABLE.categories_id = $categoryId) ";
        }
        if ($memberId != '' and $memberId != 0) {
            $memberId = $this->db->escape($memberId);
            $SQL = $SQL . "AND ($this->_TIME_RECORDS_TABLE.users_id = $memberId)";
        }

        $SQL = $SQL . " ORDER BY date";

        $query = $this->db->query($SQL);

        return $query->result_array();
    }

    function toggleBillable($id){
        $sql = "UPDATE $this->_TIME_RECORDS_TABLE
        SET billable = 
            CASE 
                WHEN billable = 1 THEN 0 
                WHEN billable = 0 THEN 1
                END 
        WHERE id = $id";

        $query = $this->db->query($sql);

        return true;
    }

}
