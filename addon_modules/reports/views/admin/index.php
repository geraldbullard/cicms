<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
        <h3 class="page-title pull-left">Reports</h3>

        <?php if ($this->session->flashdata('msg')) { ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="alert alert-<?php echo $this->session->flashdata('msgtype'); ?> alert-dismissible action-alert" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">x</button>
                    <?php echo $this->session->flashdata('msg'); ?>
                </div>
            </div>
        </div>
        <?php } ?>
        <div class="row">
            <div class="col-lg-12">
                <div id="reportsListingTable"></div><hr />
            </div>
        </div>

        <script>
            $(document).ready(function () {
                $("#clearFilter").on("click", function (evt) {
                    evt.preventDefault();
                    $('#reports_filter').trigger("reset");
                    $.fn.loadReports();
                    $("#clearFilter").blur();
                });
                $("#reportsFilter").on("click", function (evt) {
                    evt.preventDefault();
                    $.fn.loadReports();
                    $("#reportsFilter").blur();
                });
                $.fn.loadReports = function () {
                    $.blockUI({target: "#reportsListingTable"});
                    $params = $("#reports_filter").serialize();
                    $.get('<?php echo base_url('/admin/reports/getReports'); ?>?' + $params, function (data) {
                        $("#reportsListingTable").html(data);
                        $.unblockUI({target: "#reportsListingTable"});
                    }).fail(function () {
                        alert("Error!", "Something went wrong loading the Reports!", "error");
                    });
                };
                $.fn.loadReports();
            });
        </script>
