<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php if (!empty($referrals)) {  ?>

  <div class="panel">
    <div class="panel-body">
      <h1>Referrals</h1>
      <table class="table table-striped" id="reports-table" width="100%">
        <thead>
        <tr>
          <th scope="col">Giving Member</th>
          <th scope="col">Receiving Member</th>
          <th scope="col">Contract Info</th>
          <th scope="col">Notes</th>
          <th scope="col">Date Given</th>
          <th scope="col" class="text-right">Actions</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($referrals as $referral) {
          ?>
          <tr>
            <td><?php echo ucwords($referral['gu_first_name'] ." ". $referral['gu_last_name']); ?></td>
            <td><?php echo ucwords($referral['ru_first_name'] ." ". $referral['ru_last_name']); ?></td>
            <td><?php echo $referral['referral_contract_info']; ?></td>
            <td><?php echo $referral['notes']; ?></td>
            <td><?php echo $referral['date_given']; ?></td>
            <td class="text-right">
              <?php if($referral['closure_id'] != NULL): ?>
                <a href="javascript:" onclick="getClosure(<?php echo $referral['closure_id']; ?>)">
                  <span class="label label-info">View Closure</span>
                </a>

<!--                <input id="closure-id" name="closureId" value="--><?php //echo $referral['closure_id'] ?><!--">-->
<!--                <button id="closure-form-submit" class="btn btn-blue" type="submit">View Closure</button>-->
<!--              </form>-->
<!--                <a id="--><?php //echo $referral['closure_id'] ?><!--">-->
<!--                  <span class="label label-info">View Closure</span>-->
<!--                </a>-->
              <?php endif; ?>
            </td>
          </tr>
        <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
  <script>

      function getClosure(id){
          $.ajax({
              type: "GET",
              url: '<?php echo site_url("admin/reports/getClosure/") ?>' + id,
              success: function (response) {
                  var data = JSON.parse(response);
                  console.log(data);
                  $('#cm-closing-member').text(data.member_name);
                  $("#cm-dollar-amount").text(data.dollar_amount);
                  $("#cm-payment-frequency").text(data.payment_freq);
                  $("#cm-type-of-service").text(data.type_service_referred);
                  $("#referral-modal").modal("show");
              },
              error: function (){
                  alert('Error');
              }
          });
      };

      function confirmDeleteReport(id) {
          swal({
                  title: "Delete Report",
                  text: "Are you sure you wish to delete this Report?",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Delete",
                  closeOnConfirm: false
              },
              function() {
                  $.ajax({
                      url: '/admin/reports/delete/' + id,
                      type: 'GET',
                      success: function(response) {
                          var data = JSON.parse(response);
                          if (data.status == 'success') {
                              $.fn.loadReports();
                              swal({
                                  title: "Success",
                                  text: "The Report was successfully deleted.",
                                  type: "success",
                                  confirmButtonClass: "btn-success",
                                  confirmButtonText: "OK",
                                  showCancelButton: false
                              });
                          } else {
                              swal({
                                  title: "Error",
                                  text: "The was an error deleting the Report.",
                                  type: "warning",
                                  confirmButtonClass: "btn-danger",
                                  confirmButtonText: "OK",
                                  showCancelButton: false
                              });
                          }
                      }
                  });
              });
      }
      jQuery(document).ready(function() {
          var $table = $('#reports-table').DataTable({
              "dom": 'Blftip',
              "bSortCellsTop": true,
              "pageLength":  <?php echo $this->config->item('reportsAdminPagination', 'settings'); ?>,
              "lengthMenu": [
                  [<?php echo $this->config->item('reportsAdminPagination', 'settings'); ?>, 30, 60, 100, -1],
                  [<?php echo $this->config->item('reportsAdminPagination', 'settings'); ?>, 30, 60, 100, "All"]
              ],
              "order": [
                  [0, "asc"] // "desc" newest first, "asc" for oldest first
              ],
              responsive: true, // make table responsive
              buttons: [ // relabel the export button
                  //'copy', 'excel'
                  {
                      extend: 'excel',
                      text: 'Export',
                      title: 'Reports-Export',
                      className: 'btn btn-sm btn-primary shadow-sm',
                      exportOptions: {
                          columns: [0, 1]
                      },
                  }
              ],
              "initComplete": function(settings, json) { // do something immediately after the table is drawn
                  //applyReportsEmailFilter($table);
              },
              "oLanguage": { // adjust the text for the rows dropdown
                  "sLengthMenu": "_MENU_ Rows"
              },
              "aoColumns": [ // needed to keep Actions col from being sortable and searchable
                  /* giving member */ { "bSearchable": true, "bSortable": true },
                  /* receiving member */ { "bSearchable": true, "bSortable": true },
                  /* contract info */ { "bSearchable": true, "bSortable": true },
                  /* notes */ { "bSearchable": true, "bSortable": true },
                  /* date */ { "bSearchable": true, "bSortable": true },
                  /* actions */ { "bSearchable": false, "bSortable": false }
              ]
          });
          $('.dt-buttons').css('float', 'right');
          $table.on('draw', function () {
              // run a function or other action
          });
      });
  </script>
<?php } else { ?>
  None
<?php } ?>