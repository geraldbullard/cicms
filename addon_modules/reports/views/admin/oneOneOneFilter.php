<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php if (!empty($oneOnOneMeetings)) {  ?>

  <div class="panel">
    <div class="panel-body">
      <h1>Hello</h1>
      <table class="table table-striped" id="reports-table" width="100%">
        <thead>
        <tr>
          <th scope="col">Host Member</th>
          <th scope="col">Guest Member</th>
          <th scope="col">Meeting Notes</th>
          <th scope="col">Date Of Meeting</th>
          <th scope="col" class="text-right">Actions</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($oneOnOneMeetings as $meeting) {
          ?>
          <tr>
            <td><?php echo ucwords($meeting['host_first_name'] ." ". $meeting['host_last_name']); ?></td>
            <td><?php echo ucwords($meeting['guest_first_name'] ." ". $meeting['guest_last_name']); ?></td>
            <td><?php echo $meeting['notes']; ?></td>
            <td><?php echo $meeting['date']; ?></td>
            <td class="text-right">
              <a href="/admin/reorts/edit/<?php echo $meeting['id']; ?>">
                <span class="label label-primary">Edit</span></a>
              <a href="/admin/reorts/view/<?php echo $meeting['id'] ?>">
                <span class="label label-info">View</span>
              </a>
              <a href="javascript:;" onclick="confirmDeleteReport(<?php echo $meeting['id']; ?>//);">
                <span class="label label-danger">Delete</span></a>
            </td>
          </tr>
        <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
  <script>
      function confirmDeleteReport(id) {
          swal({
                  title: "Delete Report",
                  text: "Are you sure you wish to delete this Report?",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Delete",
                  closeOnConfirm: false
              },
              function() {
                  $.ajax({
                      url: '/admin/reports/delete/' + id,
                      type: 'GET',
                      success: function(response) {
                          var data = JSON.parse(response);
                          if (data.status == 'success') {
                              $.fn.loadReports();
                              swal({
                                  title: "Success",
                                  text: "The Report was successfully deleted.",
                                  type: "success",
                                  confirmButtonClass: "btn-success",
                                  confirmButtonText: "OK",
                                  showCancelButton: false
                              });
                          } else {
                              swal({
                                  title: "Error",
                                  text: "The was an error deleting the Report.",
                                  type: "warning",
                                  confirmButtonClass: "btn-danger",
                                  confirmButtonText: "OK",
                                  showCancelButton: false
                              });
                          }
                      }
                  });
              });
      }
      jQuery(document).ready(function() {
          var $table = $('#reports-table').DataTable({
              "dom": 'Blftip',
              "bSortCellsTop": true,
              "pageLength":  <?php echo $this->config->item('reportsAdminPagination', 'settings'); ?>,
              "lengthMenu": [
                  [<?php echo $this->config->item('reportsAdminPagination', 'settings'); ?>, 30, 60, 100, -1],
                  [<?php echo $this->config->item('reportsAdminPagination', 'settings'); ?>, 30, 60, 100, "All"]
              ],
              "order": [
                  [0, "asc"] // "desc" newest first, "asc" for oldest first
              ],
              responsive: true, // make table responsive
              buttons: [ // relabel the export button
                  //'copy', 'excel'
                  {
                      extend: 'excel',
                      text: 'Export',
                      title: 'Reports-Export',
                      className: 'btn btn-sm btn-primary shadow-sm',
                      exportOptions: {
                          columns: [0, 1]
                      },
                  }
              ],
              "initComplete": function(settings, json) { // do something immediately after the table is drawn
                  //applyReportsEmailFilter($table);
              },
              "oLanguage": { // adjust the text for the rows dropdown
                  "sLengthMenu": "_MENU_ Rows"
              },
              "aoColumns": [ // needed to keep Actions col from being sortable and searchable
                  /* host member */ { "bSearchable": true, "bSortable": true },
                  /* guest member */ { "bSearchable": true, "bSortable": true },
                  /* notes */ { "bSearchable": true, "bSortable": true },
                  /* date */ { "bSearchable": true, "bSortable": true },
                  /* actions */ { "bSearchable": false, "bSortable": false }
              ]
          });
          $('.dt-buttons').css('float', 'right');
          $table.on('draw', function () {
              // run a function or other action
          });
      });
  </script>
<?php } else { ?>
  None
<?php } ?>