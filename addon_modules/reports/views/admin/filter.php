<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<?php if (!empty($reports)) {
    ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel">
                <div class="panel-body" style="padding-top: 15px;">
                    <div autocomplete="off" onsubmit="return false">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-9" style="position: relative">
                                    <input autocomplete="off" id="client-input" type="text" class="form-control"
                                           placeholder="Client - start typing to see suggestions">
                                    <div id="client-list" class="panel suggestion-list">

                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <select id="cat-id" class="form-control">
                                        <option selected value="0">Select a category</option>
                                        <?php foreach ($categories as $category): ?>
                                            <option value="<?php echo $category['id'] ?>"><?php echo $category['name'] ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3" id="domain-select-div">
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-lg-12" style="position: relative">
                                    <input autocomplete="off" id="project-input" type="text" class="form-control"
                                           placeholder="Project - start typing to see suggestions">
                                    <div id="project-list" class="panel suggestion-list">

                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-lg-3">
                                    <label id="start-date-label">Start Date</label><span style="display: none"
                                                                                         class="error-text"></span>
                                    <input id="start-date" class="form-control" type="text">
                                </div>
                                <div class="col-lg-3">
                                    <label id="end-date-label">End Date</label><span style="display: none"
                                                                                     class="error-text"></span>
                                    <input id="end-date" class="form-control" type="text"><span></span>
                                </div>
                                <div class="col-lg-2">
                                    <label>Team</label>
                                    <select id="team-member-id" class="form-control">
                                        <option selected value="0">All Members</option>
                                        <?php foreach ($team as $member): ?>
                                            <option value="<?php echo $member['id'] ?>"><?php echo $member['last_name'] . ', ' . $member['first_name'] ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                                <div class="col-lg-2">
                                    <button style="margin-top: 22px;" id="generate-report" class="btn btn-primary">Create Report</button>
                                </div>
                                <div class="col-lg-2">
                                    <button style="margin-top: 22px;" id="reset-changes" class="btn btn-danger">Reset Changes</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div id="record-table-panel" class="panel" style="display: none">
            </div>

        </div>
    </div>
    <script src="<?php echo base_url('assets/klorofil/scripts/time-reports.js'); ?>"></script>
    <script src="<?php echo base_url('assets/klorofil/scripts/reports-main.js')?>"></script>
<?php } else { ?>
    None
<?php } ?>