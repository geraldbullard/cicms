<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php if (!empty($attendances)) { ?>
  <div class="panel">
    <div class="panel-body">
      <h1>Member Attendance</h1>
      <table class="table table-striped" id="reports-table" width="100%">
        <thead>
        <tr>
          <th scope="col" style="width: 100px; text-align: left">Member Name</th>
          <th scope="col">Company</th>
          <th scope="col">Category</th>
          <th scope="col">Phone</th>
          <th scope="col">Email</th>
          <?php foreach ($attendances['meeting_dates'] as $meetingDate):?>
          <th scope="col"><?php echo $meetingDate['date'] ?></th>
          <? endforeach;?>
          <?php if($this->session->userdata('system_access')['reports'] > 1) :?>
            <th scope="col" class="text-right">Actions</th>
          <?php endif;?>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($attendances['users'] as $user) {
          ?>
          <tr <?php if($user['misses_in_row'] == 2){echo 'class="warning"';} if($user['misses_in_row'] > 2){echo 'class="danger"';}  ?>>
            <td><?php echo ucwords($user['first_name'] ." ". $user['last_name']); ?></td>
            <td><?php echo $user['company']; ?></td>
            <td><?php echo $user['category'] ?></td>
            <td><?php echo $user['phone'] ?></td>
            <td><?php echo $user['email']; ?></td>
            <?php foreach ($attendances['meeting_dates'] as $meetingDate):?>
              <?php
                $matchedAttd = null;
                foreach ($attendances['attendances'] as $attd){
                  if($attd['meeting_id'] == $meetingDate['id'] AND $attd['user_id'] == $user['id'] ){
                    $matchedAttd = $attd;
                  }
                }
                if($matchedAttd !== null){
                  if($matchedAttd['substitute'] == 1){
                    ?>
                    <th><i class="fas fa-user-friends blue"><div style="display: none">substitute</div></i></th>
                    <?php
                  }
                  else{
                      ?>
                      <th><i class="fa fa-check green"><div style="display: none">present</div></i></th>
                      <?php
                    }
                }
                else{
                  ?>
                  <th><i class="fas fa-times red"><div style="display: none">absent</div></i></th>
                  <?php
                }
              ?>
            <?php endforeach;?>
            <?php if($this->session->userdata('system_access')['reports'] > 1) :?>
              <td class="text-right">
                <a href="/admin/reorts/edit/<?php echo $user['id']; ?>">
                  <span class="label label-primary">Edit</span></a>
                <a href="/admin/reorts/view/<?php echo $user['id'] ?>">
                  <span class="label label-info">View</span>
                </a>
                <a href="javascript:;" onclick="confirmDeleteReport(<?php echo $user['id']; ?>//);">
                  <span class="label label-danger">Delete</span></a>
              </td>
            <?php endif;?>
          </tr>
        <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
<!--  <pre>  --><?php //print_r($attendances); ?><!--</pre>-->

  <script>
      function confirmDeleteReport(id) {
          swal({
                  title: "Delete Report",
                  text: "Are you sure you wish to delete this Report?",
                  type: "warning",
                  showCancelButton: true,
                  confirmButtonClass: "btn-danger",
                  confirmButtonText: "Delete",
                  closeOnConfirm: false
              },
              function() {
                  $.ajax({
                      url: '/admin/reports/delete/' + id,
                      type: 'GET',
                      success: function(response) {
                          var data = JSON.parse(response);
                          if (data.status == 'success') {
                              $.fn.loadReports();
                              swal({
                                  title: "Success",
                                  text: "The Report was successfully deleted.",
                                  type: "success",
                                  confirmButtonClass: "btn-success",
                                  confirmButtonText: "OK",
                                  showCancelButton: false
                              });
                          } else {
                              swal({
                                  title: "Error",
                                  text: "The was an error deleting the Report.",
                                  type: "warning",
                                  confirmButtonClass: "btn-danger",
                                  confirmButtonText: "OK",
                                  showCancelButton: false
                              });
                          }
                      }
                  });
              });
      }
      jQuery(document).ready(function() {
          var $table = $('#reports-table').DataTable({
              "dom": 'Blftip',
              "bSortCellsTop": true,
              "pageLength":  <?php echo $this->config->item('reportsAdminPagination', 'settings'); ?>,
              "lengthMenu": [
                  [<?php echo $this->config->item('reportsAdminPagination', 'settings'); ?>, 30, 60, 100, -1],
                  [<?php echo $this->config->item('reportsAdminPagination', 'settings'); ?>, 30, 60, 100, "All"]
              ],
              "order": [
                  [0, "asc"] // "desc" newest first, "asc" for oldest first
              ],
              responsive: true, // make table responsive
              buttons: [ // relabel the export button
                  //'copy', 'excel'
                  {
                      extend: 'excel',
                      text: 'Export',
                      title: 'Reports-Export',
                      className: 'btn btn-sm btn-primary shadow-sm',
                      exportOptions: {
                          columns: [0, 1]
                      },
                  }
              ],
              "initComplete": function(settings, json) { // do something immediately after the table is drawn
                  //applyReportsEmailFilter($table);
              },
              "oLanguage": { // adjust the text for the rows dropdown
                  "sLengthMenu": "_MENU_ Rows"
              },
              "aoColumns": [ // needed to keep Actions col from being sortable and searchable
                  /* member name */ { "bSearchable": true, "bSortable": true },
                  /* company */ { "bSearchable": true, "bSortable": true },
                  /* category */ { "bSearchable": true, "bSortable": true },
                  /* phone */ { "bSearchable": true, "bSortable": true },
                  /* email */ { "bSearchable": true, "bSortable": true },
                  // /* date */ { "bSearchable": true, "bSortable": true },
                  // /* substitute */ { "bSearchable": true, "bSortable": true },
                  <?php foreach ($attendances['meeting_dates'] as $dates):?>
                  /* meeting date */ {"bSearchable": true, "bSortable": true},
                  <?php endforeach;?>
                <?php if($this->session->userdata('system_access')['reports'] > 1) :?>
                  /* actions */ { "bSearchable": false, "bSortable": false }
                <?php endif;?>
              ]
          });
          $('.dt-buttons').css('float', 'right');
          $table.on('draw', function () {
              // run a function or other action
          });
      });
  </script>
<?php } else { ?>
  None
<?php } ?>

<?php if($this->session->userdata('system_access')['reports'] > 1) :?>

<?php endif;?>