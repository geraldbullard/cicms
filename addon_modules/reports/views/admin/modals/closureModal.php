<div class="modal fade" id="referral-modal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 style="text-align: center" class="modal-title">Closure Details</h4>
      </div>
      <div class="modal-body" style="text-align: center">
        <table class="table">
          <thead>
            <tr>
              <th scope="col">Closing Member</th>
              <th scope="col">Dollar Amount</th>
              <th scope="col">Payment Frequency</th>
              <th scope="col">Type Of Service</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td id="cm-closing-member"></td>
              <td id="cm-dollar-amount"></td>
              <td id="cm-payment-frequency"></td>
              <td id="cm-type-of-service"></td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="modal-footer" style="text-align: center">
        <button type="submit" style="text-align: center" class="btn btn-success" data-dismiss="modal">Dismiss</button>
      </div>
      </form>
    </div>
  </div>

</div>
</div>

<script>
  $("#referral-modal").on('hidden.bs.modal', function (e){
      $('#cm-closing-member').val('');
      $("#cm-dollar-amount").val('');
      $("#cm-payment-frequency").val('');
      $("#cm-type-of-service").val('');
  });
</script>