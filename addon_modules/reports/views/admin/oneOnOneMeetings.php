<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<h3 class="page-title pull-left">One On One Meetings</h3>
<div class="pull-right">
  <!--  <a href="/admin/knowledges/add/">-->
  <!--    <button class="btn btn-sm btn-primary" id="dt-add-row">-->
  <!--      <i class="fa fa-plus"></i>&nbsp;&nbsp;Add Knowledge-->
  <!--    </button>-->
  <!--  </a>-->
</div>
<?php if ($this->session->flashdata('msg')) { ?>
  <div class="row">
    <div class="col-lg-12">
      <div class="alert alert-<?php echo $this->session->flashdata('msgtype'); ?> alert-dismissible action-alert" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">x</button>
        <?php echo $this->session->flashdata('msg'); ?>
      </div>
    </div>
  </div>
<?php } ?>
<!--<div class="row">-->
<!--  <div class="col-lg-12">-->
<!--    <form id="reports_filter" action="" method="post">-->
<!--      <label for="filterstart" >Start Date</label>-->
<!--      <input type="date" name="filterstart" value="" />-->
<!--      <label for="filterend">End Date</label>-->
<!--      <input type="date" name="filterend" value="" />-->
<!--      <button type="submit" value="go" class="form-control" id="reportsFilter">Go</button><br>-->
<!--    </form>-->
<!--  </div>-->
<!--</div>-->
<div class="row">
  <div class="col-lg-12">
    <div id="oneOnOneListingTable"></div><hr />
  </div>
</div>
<script>
    $(document).ready(function () {
        $("#clearFilter").on("click", function(evt) {
            evt.preventDefault();
            $('#reports_filter').trigger("reset");
            $.fn.loadReports();
            $("#clearFilter").blur();
        });
        $("#reportsFilter").on("click", function(evt) {
            evt.preventDefault();
            $.fn.loadReports();
            $("#reportsFilter").blur();
        });
        $.fn.loadReports = function() {
            $.blockUI({target: "#oneOnOneListingTable"});
            $params = $("#reports_filter").serialize();
            $.get('<?php echo base_url('/admin/reports/getOneOnOneMeetings'); ?>?' + $params, function (data) {
                $("#oneOnOneListingTable").html(data);
                $.unblockUI({target: "#oneOnOneListingTable"});
            }).fail(function () {
                alert("Error!", "Something went wrong loading the Knowledges!", "error");
            });
        };
        $.fn.loadReports();
    });
</script>
