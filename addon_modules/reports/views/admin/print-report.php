<?php
?>
<!DOCTYPE html>
<html>
<head>
    <title>Print report record view</title>
<!--    <link href="--><?php //echo base_url('assets/klorofil/css/bootstrap.min.css'); ?><!--" rel="stylesheet" />-->
    <link href="<?php echo base_url('assets/klorofil/css/bp_custom.css'); ?>" rel="stylesheet" />
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.debug.js"></script>


    <!--    <link href="--><?php //echo base_url('assets/klorofil/css/main.css'); ?><!--" rel="stylesheet" />-->
    <style>
        table {
            color: #333;
            background: white;
            border: 1px solid grey;
            font-size: 12pt;
            border-collapse: collapse;
            width: 100%;
        }
        table thead th,
        table tfoot th {
            color: #777;
            background: rgba(0,0,0,.1);
        }
        table caption {
            padding:.5em;
        }
        table th,
        table td {
            padding: .5em;
            border: 1px solid lightgrey;
        }
    </style>
</head>
<body>
<!--<a href="javascript:createPDF()">Create PDF</a>-->
<div id="report-container">
    <table id="report-table">
        <thead>
        <tr>
            <th>Date</th>
            <th>Project or Client</th>
            <th>Domain</th>
            <th>Time</th>
            <th>Team Member</th>
            <th>Hours</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($records as $record): ?>
            <tr class="<?php echo $record->billable == 1 ? '' : 'un-billable'?>" style="font-size: 12px" >
                <td><?php echo $record->date ?></td>
                <td>
                    <?php echo $record->client_name?>
                    <br>
                    <?php echo $record->projects_id == 0 ? $record->category_name : $record->project_category_name ?>
                </td>
                <td><?php echo $record->domain_name?></td>
                <td><?php echo date("g:i a", strtotime($record->start_time)) . ' ' . date("g:i a", strtotime($record->end_time))?></td>
                <td><?php echo $record->user_name ?></td>
                <td><?php echo $record->billable == 1 ? $record->timeSpentInHours : '<s>' . $record->timeSpentInHours . '</s> <span> NOT BILLED</span>' ?></td>
            <tr class="<?php echo $record->billable == 1 ? '' : 'un-billable'?>">
                <td><strong>Note:</strong></td>
                <td colspan="6"><?php echo $record->notes ?></td>
            </tr>
            </tr>

        <?php endforeach; ?>
        <tr class="report-client-footer">
            <td colspan="6" style="text-align: right; font-weight: bolder; font-size: 20px">
                <strong>
                    <span>Total -- </span>

                <?php if($reportHours->billed != 0):?>
                    <span>Billed: <?php echo number_format((float)$reportHours->billed, 2, '.', '') ?> </span>
                <?php endif;?>
                <?php if($reportHours->unbilled != 0): ?>
                    <span>Unbilled: <?php echo number_format((float)$reportHours->unbilled, 2, '.', '') ?> </span>
                <?php endif; ?>
                <?php if($reportHours->breaks != 0): ?>
                    <span>Breaks: <?php echo number_format((float)$reportHours->breaks, 2, '.' ,'') ?> </span>
                <?php endif; ?>
                <?php if($reportHours->total != 0): ?>
                    <span>Total: <?php echo number_format((float)$reportHours->total, 2, '.', '') ?> </span>
                <?php endif;?>
                </strong>
            </td>
        </tr>
        </tbody>
    </table>
</div>
</body>
</html>