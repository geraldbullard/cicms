<?php
$clientArray = array();
/**
 * Creates an Array of key value pairs, where the key is the Client ID, and value is an array containing
 * all time records that belong to that client. (think bucket sort).
 */
foreach ($records as $record){
    if(!array_key_exists($record->client_id, $clientArray)){
        $var = array();
        $var[] = $record;
        $clientArray[$record->client_id] = $var;
    }else{
        $clientArray[$record->client_id][] = $record;
    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <title>Print report project view</title>
    <link href="<?php echo base_url('assets/klorofil/css/bp_custom.css'); ?>" rel="stylesheet" />
    <style>
        table {
            color: #333;
            background: white;
            border: 1px solid grey;
            font-size: 12pt;
            border-collapse: collapse;
            width: 100%;
        }
        table thead th,
        table tfoot th {
            color: #777;
            background: rgba(0,0,0,.1);
        }
        table caption {
            padding:.5em;
        }
        table th,
        table td {
            padding: .5em;
            border: 1px solid lightgrey;
        }
    </style>
</head>
<body>

<table>
    <thead>
    <tr>
        <th>Date</th>
        <th>Project or Client</th>
        <th>Domain</th>
        <th>Time</th>
        <th>Team Member</th>
        <th>Hours</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($clientArray as $client):/* client loop*/ ?>
    <?php

    $clientBilled = 0;
    $clientUnbilled = 0;
    $clientBreaks = 0;

    $projectArr = array();
    /**
     * Creates an Array of key value pairs where the key is the Project ID and the value is an array of all time records
     * that belong to that project. It's important to note that we are only using the records that belong to the client
     * selected by whatever iteration of the client loop we are on.
     */
    foreach($client as $record){
        if(!array_key_exists($record->projects_id, $projectArr)){
            $recordArr = array();
            $recordArr[] = $record;
            $projectArr[$record->projects_id] = $recordArr;
        }else{
            $projectArr[$record->projects_id][] = $record;
        }
    }
        /**
         * ksort the projectArr so that records that meet the condition project_id = 0 will go first. These are our
         * "non-project" records.
         */
    ksort($projectArr);
    ?>

    <?php foreach ($projectArr as $project): /* project collection loop */?>
            <?php
            $projectBilled = 0;
            $projectUnbilled = 0;
            $projectBreaks = 0;
            ?>
        <?php $projectName = $project[0]->projects_id == 0 ? 'Non-project-time' : $project[0]->domain_name; ?>
        <?php foreach ($project as $record): /* record loop */ ?>
            <tr <?php if($record->category_id == 992 OR $record->billable == 0) echo 'class="un-billable"'; ?> style="font-size: 12px">
                <td><?php echo $record->date ?></td>
                <td>
                    <?php echo $record->client_name ?>
                    <br>
                    <?php echo $record->projects_id == 0 ? $record->category_name : $record->project_category_name ?>
                </td>
                <td><?php echo $record->domain_name === '' ? 'N/A' : $record->domain_name  ?></td>
                <td><?php echo date("g:i a", strtotime($record->start_time)) . '  ' . date("g:i a", strtotime($record->end_time)) ?></td>
                <td><?php echo $record->user_name ?></td>
                <td>
                    <?php if($record->billable == 0 OR $record->category_id == 992)
                    {
                        echo"<s>" . $record->timeSpentInHours . "</s> <span> NOT BILLED</span>";
                    } else{
                        echo $record->timeSpentInHours;
                    } ?>
                </td>
            </tr>
            <tr class="<?php echo $record->billable == 1 ? '' : 'un-billable'?>" style="font-size: 12px">
                <td><strong>Note:</strong></td>
                <td colspan="6"><?php echo $record->notes ?></td>
            </tr>
            <?php
                if($record->category_id == 992){
                    $projectBreaks += $record->timeSpentInHours;
                    $clientBreaks += $record->timeSpentInHours;
                }elseif($record->billable == 1){
                    $projectBilled += $record->timeSpentInHours;
                    $clientBilled += $record->timeSpentInHours;
                }else{
                    $projectUnbilled += $record->timeSpentInHours;
                    $clientUnbilled += $record->timeSpentInHours;
                }
            ?>
        <?php endforeach; /* end record loop */ ?>
        <tr style="background-color: #EEEEEE">
            <td colspan="6" style="text-align: right; font-weight: normal;">
                <span><?php echo $projectName . ' -- ' ?></span>
                <?php if($projectBilled != 0): ?>
                <span> Billed: <?php echo number_format((float)$projectBilled, 2, '.', '') ?></span>
                <?php endif; ?>
                <?php if($projectUnbilled != 0): ?>
                <span> Unbilled: <?php echo number_format((float)$projectUnbilled, 2, '.', '') ?></span>
                <?php endif; ?>
                <?php if($projectBreaks != 0): ?>
                <span> Breaks: <?php echo number_format((float)$projectBreaks, 2, '.', '') ?></span>
                <?php endif; ?>
                <span> Total: <?php echo number_format((float)($projectBilled + $projectBreaks + $projectUnbilled), 2, '.', '') ?></span>
            </td>
        </tr>
    <?php endforeach; /* end project loop */ ?>
        <tr class="report-client-footer">
            <td colspan="6" style="text-align: right; font-weight: bold;">
                <span><?php echo $client[0]->client_name ?> -- </span>
                <?php if($clientBilled != 0): ?>
                    <span> Billed: <?php echo number_format((float)$clientBilled, 2, '.', '') ?></span>
                <?php endif; ?>
                <?php if($clientUnbilled != 0): ?>
                    <span> Unbilled: <?php echo number_format((float)$clientUnbilled, 2, '.', '') ?></span>
                <?php endif; ?>
                <?php if($clientBreaks != 0): ?>
                    <span> Breaks: <?php echo number_format((float)$clientBreaks, 2, '.', '') ?></span>
                <?php endif; ?>
                <span> Total: <?php echo number_format((float)($clientBilled + $clientBreaks + $clientUnbilled), 2, '.', '') ?></span>
            </td>
        </tr>
    <?php endforeach;/* end client loop */ ?>
    <tr style="background-color: grey">
        <td colspan="6" style="text-align: right; font-weight: bolder; font-size: 20px">
            <span>Total -- </span>
            <?php if($reportHours->billed != 0):?>
            <span>Billed: <?php echo number_format((float)$reportHours->billed, 2, '.', '') ?> </span>
            <?php endif;?>
            <?php if($reportHours->unbilled != 0): ?>
            <span>Unbilled: <?php echo number_format((float)$reportHours->unbilled, 2, '.', '') ?> </span>
            <?php endif; ?>
            <?php if($reportHours->breaks != 0): ?>
            <span>Breaks: <?php echo number_format((float)$reportHours->breaks, 2, '.' ,'') ?> </span>
            <?php endif; ?>
            <?php if($reportHours->total != 0): ?>
            <span>Total: <?php echo number_format((float)$reportHours->total, 2, '.', '') ?> </span>
            <?php endif;?>
        </td>
    </tr>
    </tbody>
</table>
</body>
</html>