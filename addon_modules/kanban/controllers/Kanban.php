<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kanban extends MY_Controller
{

	function __construct()
	{
		parent::__construct();
		// Get site wide settings first
		$this->Settings_model->loadSitewideSettings();

		// Load This Model
		$this->load->model( 'Kanban_model' );
	}

	public function admin_index()
	{
		if (!$this->session->userdata('user_id'))
		{
			// set the url they were trtying to go to in session
			$this->session->set_userdata('page_url', current_url());

			//Redirect to login
			redirect('user/login');
		} else {
			$this->data = array();
			$this->data['title'] = 'Kanban Index';

			// load the config from the current module
			$this->load->config('kanban');
			$this->data['kanban_config'] = $this->config->config['kanban'];
			// Added for developer dropdown
			$this->data['dev_data'] = '';

			$this->load->view('layout/admin/header.php', $this->data);
			$this->load->view('admin/index', $this->data);
			$this->load->view('layout/admin/footer.php', $this->data);
		}
	}

	public function add()
	{
		if (!$this->session->userdata('user_id'))
		{
			// set the url they were trtying to go to in session
			$this->session->set_userdata('page_url', current_url());

			//Redirect to login
			redirect('user/login');
		} else {
			$this->data = array();
			$this->data['title'] = 'Kanban Add';

			$this->load->view('layout/admin/header.php', $this->data);
			$this->load->view('admin/add', $this->data);
			$this->load->view('layout/admin/footer.php', $this->data);
		}
	}

	public function edit()
	{
		if (!$this->session->userdata('user_id'))
		{
			// set the url they were trtying to go to in session
			$this->session->set_userdata('page_url', current_url());

			//Redirect to login
			redirect('user/login');
		} else {
			$this->data = array();
			$this->data['title'] = 'Kanban Edit';

			$this->load->view('layout/admin/header.php', $this->data);
			$this->load->view('admin/edit', $this->data);
			$this->load->view('layout/admin/footer.php', $this->data);
		}
	}

	public function updateUserKanban()
	{
		if (!$this->session->userdata('user_id'))
		{
			// set the url they were trtying to go to in session
			$this->session->set_userdata('page_url', current_url());

			//Redirect to login
			redirect('user/login');
		} else {
			$tid = ltrim($this->uri->segment(3),"t");
			$col = $this->uri->segment(4);
			if ($this->Kanban_model->updateUserKanban($tid, $col) === true) {
				return json_encode(array('success' => 'true'));
			}
			return json_encode(array('success' => 'false'));
		}
	}

}
