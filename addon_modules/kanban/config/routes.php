<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* Kanban Routing */
$route['admin/kanban'] = 'kanban/admin_index';
$route['admin/kanban/add'] = 'kanban/add';
$route['admin/kanban/edit/(:any)'] = 'kanban/edit/$1';
$route['admin/kanban/update/(:any)'] = 'kanban/update/$1';
$route['admin/kanban/delete/(:any)'] = 'kanban/delete/$1';

