<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
			<h3 class="page-title">Kanban Index</h3>
			<div class="row margin-bottom-30">
				<div class="col-lg-12" id="cardtasks"></div>
				<div class="col-lg-12">
                    <style>
                        .pt-3, .py-3 {
                            padding-top: 1rem!important;
                        }
                        .container-fluid {
                            width: 100%;
                            padding-right: 15px;
                            padding-left: 15px;
                            margin-right: auto;
                            margin-left: auto;
                        }
                        .bg-light {
                            background-color: #f8f9fa!important;
                        }
                        .card {
                            position: relative;
                            display: -ms-flexbox;
                            display: flex;
                            -ms-flex-direction: column;
                            flex-direction: column;
                            min-width: 0;
                            word-wrap: break-word;
                            background-color: #fff;
                            background-clip: border-box;
                            border: 1px solid rgba(0,0,0,.125);
                            border-radius: .25rem;
                        }
                        .card-body {
                            -ms-flex: 1 1 auto;
                            flex: 1 1 auto;
                            padding: 1.25rem;
                        }
                        .card.draggable {
                            margin-bottom: 1rem;
                            cursor: grab;
                        }
                        .card-title {
                            margin-bottom: .5rem;
                        }
                        .float-right {
                            float: right!important;
                        }
                        .rounded-circle {
                            border-radius: 50%!important;
                            background-color: #00AAFF;
                            width: 30px;
                            height: 30px;
                            text-align: center;
                            color: #fff;
                        }
                        img {
                            vertical-align: middle;
                            border-style: none;
                        }
                        .droppable {
                            background-color: green;
                            min-height: 120px;
                            margin-bottom: 1rem;
                        }
                        .card.draggable {
                            margin-bottom: 1rem;
                            cursor: grab;
                        }
                    </style>
                    <script>
                        const drag = (event) => {
                            event.dataTransfer.setData("text/plain", event.target.id);
                        };
                        const allowDrop = (ev) => {
                            ev.preventDefault();
                            if (hasClass(ev.target, "dropzone")) {
                                addClass(ev.target, "droppable");
                            }
                        };
                        const clearDrop = (ev) => {
                            removeClass(ev.target, "droppable");
                        };
                        const drop = (event) => {
                            event.preventDefault();
                            const data = event.dataTransfer.getData("text/plain");
                            const element = document.querySelector(`#${data}`);
                            try {
                                // remove the spacer content from dropzone
                                event.target.removeChild(event.target.firstChild);
                                // add the draggable content
                                event.target.appendChild(element);
                                // remove the dropzone parent
                                unwrap(event.target);
                            } catch (error) {
                                console.warn("can't move the item to the same place")
                            }
                            updateDropzones();
                        };
                        const updateDropzones = () => {
                            /* after dropping, refresh the drop target areas
							  so there is a dropzone after each item
							  using jQuery here for simplicity */

                            var dz = $('<div class="dropzone rounded" ondrop="drop(event)" ondragover="allowDrop(event)" ondragleave="clearDrop(event)"> &nbsp; </div>');

                            // delete old dropzones
                            $('.dropzone').remove();

                            // insert new dropdzone after each item
                            dz.insertAfter('.card.draggable');

                            // insert new dropzone in any empty swimlanes
                            $(".items:not(:has(.card.draggable))").append(dz);
                            // START - Here is where we will put the ajax to update the db after every drop ////////////
                            setTimeout(function(){
                                var cardTasks = [];
                                $('.card-column').each(function(){
                                    var colId = $(this).attr('id');
                                    $(this).find('.draggable').each(function() {
                                        cardTasks.push({
                                            col : colId,
                                            task : $(this).attr('id')
                                        });
                                    });
                                });
                                $('#cardtasks').html("");
                                for (i = 0; i < cardTasks.length; i++) {
                                    $('#cardtasks').append(jQuery.param(cardTasks[i]) + '<br>');
                                    var tid = cardTasks[i].task;
                                    var col = cardTasks[i].col;
                                    var url = "<?php echo base_url(); ?>kanban/updateUserKanban/" + tid + "/" + col;
                                    $.post(url).done(function(data) {
                                        //alert(url);
                                    });
                                }
                            }, 100);
                            // END - Here is where we will put the ajax to update the db after every drop //////////////
                        };
                        // helpers
                        function hasClass(target, className) {
                            return new RegExp('(\\s|^)' + className + '(\\s|$)').test(target.className);
                        }
                        function addClass(ele, cls) {
                            if (!hasClass(ele, cls)) ele.className += " " + cls;
                        }
                        function removeClass(ele, cls) {
                            if (hasClass(ele, cls)) {
                                var reg = new RegExp('(\\s|^)' + cls + '(\\s|$)');
                                ele.className = ele.className.replace(reg, ' ');
                            }
                        }
                        function unwrap(node) {
                            node.replaceWith(...node.childNodes);
                        }
                    </script>
                    <div class="container-fluid pt-3">
                        <div class="row flex-row flex-sm-nowrap py-3">
                            <div class="col-xs-6 col-sm-3">
                                <div class="card bg-light margin-bottom-30">
                                    <div class="card-body card-column" id="backlog">
                                        <h4 class="card-title text-uppercase text-truncate py-2">Backlog</h4>
                                        <div class="items border border-light">
                                            <div class="card draggable shadow-sm" id="t152" draggable="true" ondragstart="drag(event)">
                                                <div class="card-body p-2">
                                                    <div class="card-title">
                                                        <span class="float-right">
                                                            <img
                                                                class="rounded-circle"
                                                                src="/assets/klorofil/img/user2.png"
                                                                title="Kyle Lemire"
                                                                width="30"
                                                                height="30"
                                                            />
                                                        </span>
                                                        <a href="" class="lead font-weight-light">T-152</a>
                                                    </div>
                                                    <p>
                                                        This is a task.
                                                    </p>
                                                    <button class="btn btn-primary btn-sm btn-xxs">View</button>
                                                </div>
                                            </div>
                                            <div class="dropzone rounded" ondrop="drop(event)" ondragover="allowDrop(event)" ondragleave="clearDrop(event)"> &nbsp; </div>
                                            <div class="card draggable shadow-sm" id="t153" draggable="true" ondragstart="drag(event)">
                                                <div class="card-body p-2">
                                                    <div class="card-title">
                                                        <span class="float-right">
                                                            <img
                                                                class="rounded-circle"
                                                                src="/assets/klorofil/img/user5.png"
                                                                title="Daniel Bage"
                                                                width="30"
                                                                height="30"
                                                            />
                                                        </span>
                                                        <a href="" class="lead font-weight-light">T-153</a>
                                                    </div>
                                                    <p>
                                                        Another task here.
                                                    </p>
                                                    <button class="btn btn-primary btn-sm btn-xxs">View</button>
                                                </div>
                                            </div>
                                            <div class="dropzone rounded" ondrop="drop(event)" ondragover="allowDrop(event)" ondragleave="clearDrop(event)"> &nbsp; </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-3">
                                <div class="card bg-light margin-bottom-30">
                                    <div class="card-body card-column" id="priority">
                                        <h4 class="card-title text-uppercase text-truncate py-2">Priority</h4>
                                        <div class="items border border-light">
                                            <div class="card draggable shadow-sm" id="t154" draggable="true" ondragstart="drag(event)">
                                                <div class="card-body p-2">
                                                    <div class="card-title">
                                                        <span class="float-right">
                                                            <img
                                                                class="rounded-circle"
                                                                src="/assets/klorofil/img/user-medium.png"
                                                                title="Gerald Bullard"
                                                                width="30"
                                                                height="30"
                                                            />
                                                        </span>
                                                        <a href="" class="lead font-weight-light">T-154</a>
                                                    </div>
                                                    <p>
                                                        This is a description.
                                                    </p>
                                                    <button class="btn btn-primary btn-sm btn-xxs">View</button>
                                                </div>
                                            </div>
                                            <div class="dropzone rounded" ondrop="drop(event)" ondragover="allowDrop(event)" ondragleave="clearDrop(event)"> &nbsp; </div>
                                            <div class="card draggable shadow-sm" id="t156" draggable="true" ondragstart="drag(event)">
                                                <div class="card-body p-2">
                                                    <div class="card-title">
                                                        <span class="icon float-right rounded-circle">
                                                            <i class="fa fa-user" style="padding-top:7px;" title="Joe Ruiz"></i>
                                                        </span>
                                                        <a href="" class="lead font-weight-light">T-156</a>
                                                    </div>
                                                    <p>
                                                        A description that explains the item.
                                                    </p>
                                                    <button class="btn btn-primary btn-sm btn-xxs">View</button>
                                                </div>
                                            </div>
                                            <div class="dropzone rounded" ondrop="drop(event)" ondragover="allowDrop(event)" ondragleave="clearDrop(event)"> &nbsp; </div>
                                            <div class="card draggable shadow-sm" id="t157" draggable="true" ondragstart="drag(event)">
                                                <div class="card-body p-2">
                                                    <div class="card-title">
                                                        <span class="float-right">
                                                            <img
                                                                class="rounded-circle"
                                                                src="/assets/klorofil/img/user2.png"
                                                                title="Daniel Bage"
                                                                width="30"
                                                                height="30"
                                                            />
                                                        </span>
                                                        <a href="" class="lead font-weight-light">T-157</a>
                                                    </div>
                                                    <p>
                                                        This is an item on the board.
                                                    </p>
                                                    <button class="btn btn-primary btn-sm btn-xxs">View</button>
                                                </div>
                                            </div>
                                            <div class="dropzone rounded" ondrop="drop(event)" ondragover="allowDrop(event)" ondragleave="clearDrop(event)"> &nbsp; </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-3">
                                <div class="card bg-light margin-bottom-30">
                                    <div class="card-body card-column" id="inprogress">
                                        <h4 class="card-title text-uppercase text-truncate py-2">In Progress</h4>
                                        <div class="items border border-light">
                                            <div class="card draggable shadow-sm" id="t155" draggable="true" ondragstart="drag(event)">
                                                <div class="card-body p-2">
                                                    <div class="card-title">
                                                        <span class="float-right">
                                                            <img
                                                                class="rounded-circle"
                                                                src="/assets/klorofil/img/user5.png"
                                                                title="Gerald Bullard"
                                                                width="30"
                                                                height="30"
                                                            />
                                                        </span>
                                                        <a href="" class="lead font-weight-light">T-155</a>
                                                    </div>
                                                    <p>
                                                        This is a description of the item.
                                                    </p>
                                                    <button class="btn btn-primary btn-sm btn-xxs">View</button>
                                                </div>
                                            </div>
                                            <div class="dropzone rounded" ondrop="drop(event)" ondragover="allowDrop(event)" ondragleave="clearDrop(event)"> &nbsp; </div>
                                            <div class="card draggable shadow-sm" id="t158" draggable="true" ondragstart="drag(event)">
                                                <div class="card-body p-2">
                                                    <div class="card-title">
                                                        <span class="float-right">
                                                            <img
                                                                class="rounded-circle"
                                                                src="/assets/klorofil/img/user-medium.png"
                                                                title="Kyle Lemire"
                                                                width="30"
                                                                height="30"
                                                            />
                                                        </span>
                                                        <a href="" class="lead font-weight-light">T-158</a>
                                                    </div>
                                                    <p>
                                                        This is some descriptive text.
                                                    </p>
                                                    <button class="btn btn-primary btn-sm btn-xxs">View</button>
                                                </div>
                                            </div>
                                            <div class="dropzone rounded" ondrop="drop(event)" ondragover="allowDrop(event)" ondragleave="clearDrop(event)"> &nbsp; </div>
                                            <div class="card draggable shadow-sm" id="t159" draggable="true" ondragstart="drag(event)">
                                                <div class="card-body p-2">
                                                    <div class="card-title">
                                                        <span class="icon float-right rounded-circle">
                                                            <i class="fa fa-user" style="padding-top:7px;" title="Joe Ruiz"></i>
                                                        </span>
                                                        <a href="" class="lead font-weight-light">T-159</a>
                                                    </div>
                                                    <p>
                                                        This is an item here.
                                                    </p>
                                                    <button class="btn btn-primary btn-sm btn-xxs">View</button>
                                                </div>
                                            </div>
                                            <div class="dropzone rounded" ondrop="drop(event)" ondragover="allowDrop(event)" ondragleave="clearDrop(event)"> &nbsp; </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-3">
                                <div class="card">
                                    <div class="card-body card-column" id="review">
                                        <h4 class="card-title text-uppercase text-truncate py-2">Review</h4>
                                        <div class="items border border-light">
                                            <div class="card draggable shadow-sm" id="t144" draggable="true" ondragstart="drag(event)">
                                                <div class="card-body p-2">
                                                    <div class="card-title">
                                                        <span class="float-right">
                                                            <img
                                                                class="rounded-circle"
                                                                src="/assets/klorofil/img/user2.png"
                                                                title="Gerald Bullard"
                                                                width="30"
                                                                height="30"
                                                            />
                                                        </span>
                                                        <a href="" class="lead font-weight-light">T-144</a>
                                                    </div>
                                                    <p>
                                                        This is a description of an item.
                                                    </p>
                                                    <button class="btn btn-primary btn-sm btn-xxs">View</button>
                                                </div>
                                            </div>
                                            <div class="dropzone rounded" ondrop="drop(event)" ondragover="allowDrop(event)" ondragleave="clearDrop(event)"> &nbsp; </div>
                                            <div class="card draggable shadow-sm" id="t146" draggable="true" ondragstart="drag(event)">
                                                <div class="card-body p-2">
                                                    <div class="card-title">
                                                        <span class="float-right">
                                                            <img
                                                                class="rounded-circle"
                                                                src="/assets/klorofil/img/user5.png"
                                                                title="Kyle Lemire"
                                                                width="30"
                                                                height="30"
                                                            />
                                                        </span>
                                                        <a href="" class="lead font-weight-light">T-146</a>
                                                    </div>
                                                    <p>
                                                        This is a description of a task item.
                                                    </p>
                                                    <button class="btn btn-primary btn-sm btn-xxs">View</button>
                                                </div>
                                            </div>
                                            <div class="dropzone rounded" ondrop="drop(event)" ondragover="allowDrop(event)" ondragleave="clearDrop(event)"> &nbsp; </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
			</div>
