<?php
/* Module functions file */

// If installing the module run this function to create db table(s)
if (!function_exists('bp_installModule')) {
    function bp_installModule() {
        $CI =& get_instance();

        // Update the module status
        $CI->db->set('status', 1);
        $CI->db->where("module", "kanban");
        $CI->db->update($CI->db->dbprefix . "modules");

        // Create table(s)
	    $CI->db->query("
			CREATE TABLE IF NOT EXISTS `" . $CI->db->dbprefix . "kanban` (
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`task_id` int(11) NOT NULL,
				`col` varchar(64) NOT NULL,
				PRIMARY KEY (`id`),
				KEY `bp_task_id` (`task_id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	    ");

        return true;
    }
}

// If uninstalling the module run this function to remove db entries if selected
if (!function_exists('bp_uninstallModule')) {
    function bp_uninstallModule() {
        $CI =& get_instance();

        // Update the module status
        $CI->db->set('status', -1);
        $CI->db->where("module", "kanban");
        $CI->db->update($CI->db->dbprefix . "modules");

        // Drop table(s)
        $CI->db->query("
            DROP TABLE IF EXISTS `" . $CI->db->dbprefix . "kanban`;
        ");

        return true;
    }
}
