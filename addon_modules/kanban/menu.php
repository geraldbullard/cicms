<?php
/* Kanban module menu file */

$_menu = array(
    'url' => '/admin/kanban/',
    'title' => 'Kanban',
    'icon' => 'lnr lnr-rocket',
    'access' => 1,
);