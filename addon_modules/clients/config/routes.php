<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* Clients Routing */
$route['admin/clients'] = 'clients/admin_index';
$route['admin/clients/add'] = 'clients/add';
$route['admin/clients/inactive'] = 'clients/inactive';
$route['admin/clients/getClients'] = 'clients/getClients';
$route['admin/clients/edit/(:any)'] = 'clients/edit/$1';
$route['admin/clients/update/(:any)'] = 'clients/update/$1';
$route['admin/clients/delete/(:any)'] = 'clients/delete/$1';
$route['admin/clients/activate/(:any)'] = 'clients/activate/$1';
$route['admin/clients/deactivate/(:any)'] = 'clients/deactivate/$1';
$route['admin/clients/migrate'] = 'clients/migrate';
$route['admin/clients/migrateClient/(:any)'] = 'clients/migrateClient/$1';
$route['admin/clients/unlock/(:any)'] = 'clients/unlock/$1';
