<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Clients Model
 */
class Clients_model extends CI_Model
{

	function __construct()
	{
		parent::__construct();
	}

	public function getClientsRow($id) {
		$this->db->where("id", $id);
		return $this->db->get($this->db->dbprefix . "clients")->result_array();
	}

	public function getClientDomains($cid) {
		$this->db->where("clients_id", $cid);
		return $this->db->get($this->db->dbprefix . "domains")->result_array();
	}

	public function delete($id) {
		$domains = false;
		$this->db->where("clients_id", $id);
		if ($this->db->delete($this->db->dbprefix . "domains")){
			$domains = true;
		}
		if ($domains) {
			$this->db->where("id", $id);
			if ($this->db->delete($this->db->dbprefix . "clients")) {
				return true;
			}
		}
		return false;
	}

	public function deleteDomain($id) {
		$this->db->where("id", $id);
		if ($this->db->delete($this->db->dbprefix . "domains")) {
			return true;
		}
		return false;
	}

	public function deleteClientDomains($cid) {
		$this->db->where("clients_id", $cid);
		if ($this->db->delete($this->db->dbprefix . "domains")) {
			return true;
		}
		return false;
	}

	public function deleteService($id) {
		$this->db->where("id", $id);
		if ($this->db->delete($this->db->dbprefix . "services")) {
			return true;
		}
		return false;
	}

	public function deleteClientServices($cid) {
		$this->db->where("clients_id", $cid);
		if ($this->db->delete($this->db->dbprefix . "services")) {
			return true;
		}
		return false;
	}

	public function deleteProject($id) {
		$this->db->where("projects_id", $id);
		$this->db->delete($this->db->dbprefix . "users_to_projects");
		$this->db->where("id", $id);
		if ($this->db->delete($this->db->dbprefix . "projects")) {
			return true;
		}
		return false;
	}

	public function deleteClientProjects($cid) {
		$this->db->where("clients_id", $cid);
		if ($this->db->delete($this->db->dbprefix . "projects")) {
			return true;
		}
		return false;
	}

	public function addNote($post) {
		$data = array(
			'clients_id' => $post['clientid'],
			'note_name' => $post['n_title'],
			'url' => $post['n_link'],
			'username' => $post['n_username'],
			'password' => $post['n_password'],
			'notes' => $post['n_notes'],
			'author' => $this->session->userdata('user_id'),
			'date_updated' => date("Y-m-d H:i:s"),
		);
		if ($this->db->insert($this->db->dbprefix . "client_notes", $data)) {
			return true;
		}
		return false;
	}

	public function editNote($post) {
		$data = array(
			'note_name' => $post['n_title'],
			'url' => $post['n_link'],
			'username' => $post['n_username'],
			'password' => $post['n_password'],
			'notes' => $post['n_notes'],
			'author' => $this->session->userdata('user_id'),
			'date_updated' => date("Y-m-d H:i:s"),
		);
		$this->db->where("id", $post['noteid']);
		if ($this->db->update($this->db->dbprefix . "client_notes", $data)) {
			return true;
		}
		return false;
	}

	public function deleteNote($id) {
		$this->db->where("id", $id);
		if ($this->db->delete($this->db->dbprefix . "client_notes")) {
			return true;
		}
		return false;
	}

	public function deleteClientNotes($cid) {
		$this->db->where("clients_id", $cid);
		if ($this->db->delete($this->db->dbprefix . "client_notes")) {
			return true;
		}
		return false;
	}

	public function addContact($post) {
		// Setup emails ["mwarhola@medcertinc.com","mickeywmedcert@outlook.com"]
		$emailstr = '[';
		foreach ($post['c_emails'] as $e => $email) {
			$emailstr .= '"' . $email . '",';
		}
		$emailstr = rtrim($emailstr, ',');
		$emailstr .= ']';
		// Setup Phones [{"number":"9847362709","label":"office"},{"number":"3058493925","label":"mobile"}]
		$phones = array();
		foreach ($post['c_phones'] as $p => $phone) {
			$phones[$p]['number'] = $phone;
		}
		foreach ($post['c_phonelabels'] as $l => $label) {
			$phones[$l]['label'] = $label;
		}
		$phonestr = '[';
		foreach ($phones as $ph => $phone) {
			$phonestr .= '{"number":"' . $phone['number'] . '","label":"' . $phone['label'] . '"},';
		}
		$phonestr = rtrim($phonestr, ',');
		$phonestr .= ']';
		$data = array(
			'clients_id' => ($post['clientid'] != '' ? $post['clientid'] : ''),
			'first_name' => ($post['c_firstname'] != '' ? $post['c_firstname'] : ''),
			'last_name' => ($post['c_lastname'] != '' ? $post['c_lastname'] : ''),
			'title' => ($post['c_title'] != '' ? $post['c_title'] : ''),
			'address' => ($post['c_address'] != '' ? $post['c_address'] : ''),
			'city' => ($post['c_city'] != '' ? $post['c_city'] : ''),
			'state' => ($post['c_state'] != '' ? $post['c_state'] : ''),
			'zip' => ($post['c_zip'] != '' ? $post['c_zip'] : ''),
			'emails' => ($emailstr != '' ? $emailstr : ''),
			'phones' => ($phonestr != '' ? $phonestr : ''),
		);
		if ($this->db->insert($this->db->dbprefix . "contacts", $data)) {
			return true;
		}
		return false;
	}

	public function editContact($post) {
		/*
		Array(
		    [contactid] =>
		    [ce_firstname] => Gerald
		    [ce_lastname] => Bullard
		    [ce_title] => Developer
		    [ce_emails] => Array(
	            [0] => one@mail.com
	            [1] => two@mail.com
		    [ce_address] => 111 Any St
		    [ce_city] => Jacksonville
		    [ce_state] => FL
		    [ce_zip] => 32218
		    [ce_phonelabels] => Array(
	            [0] => Cell
	            [1] => Work
		    [ce_phones] => Array(
	            [0] => 9049450456
	            [1] => 9049981935
		*/
		// Setup emails and phones array
		// Setup emails ["mwarhola@medcertinc.com","mickeywmedcert@outlook.com"]
		$emailstr = '[';
		foreach ($post['ce_emails'] as $e => $email) {
			$emailstr .= '"' . $email . '",';
		}
		$emailstr = rtrim($emailstr, ',');
		$emailstr .= ']';
		// Setup Phones [{"number":"9847362709","label":"office"},{"number":"3058493925","label":"mobile"}]
		$phones = array();
		foreach ($post['ce_phones'] as $p => $phone) {
			$phones[$p]['number'] = $phone;
		}
		foreach ($post['ce_phonelabels'] as $l => $label) {
			$phones[$l]['label'] = $label;
		}
		$phonestr = '[';
		foreach ($phones as $ph => $phone) {
			$phonestr .= '{"number":"' . $phone['number'] . '","label":"' . $phone['label'] . '"},';
		}
		$phonestr = rtrim($phonestr, ',');
		$phonestr .= ']';
		$data = array(
			'clients_id' => ($post['clientid'] != '' ? $post['clientid'] : ''),
			'first_name' => ($post['ce_firstname'] != '' ? $post['ce_firstname'] : ''),
			'last_name' => ($post['ce_lastname'] != '' ? $post['ce_lastname'] : ''),
			'title' => ($post['ce_title'] != '' ? $post['ce_title'] : ''),
			'address' => ($post['ce_address'] != '' ? $post['ce_address'] : ''),
			'city' => ($post['ce_city'] != '' ? $post['ce_city'] : ''),
			'state' => ($post['ce_state'] != '' ? $post['ce_state'] : ''),
			'zip' => ($post['ce_zip'] != '' ? $post['ce_zip'] : ''),
			'emails' => ($emailstr != '' ? $emailstr : ''),
			'phones' => ($phonestr != '' ? $phonestr : ''),
		);
		$this->db->where("id", $post['contactid']);
		if ($this->db->update($this->db->dbprefix . "contacts", $data)) {
			return true;
		}
		return false;
	}

	public function deleteContact($id) {
		$this->db->where("id", $id);
		if ($this->db->delete($this->db->dbprefix . "contacts")) {
			return true;
		}
		return false;
	}

	public function deleteClientContacts($cid) {
		$this->db->where("clients_id", $cid);
		if ($this->db->delete($this->db->dbprefix . "contacts")) {
			return true;
		}
		return false;
	}

	public function deleteClientLogins($cid) {
		$this->db->where("clients_id", $cid);
		if ($this->db->delete($this->db->dbprefix . "client_logins")) {
			return true;
		}
		return false;
	}

	public function deleteClientAuditTrail($cid) {
		$this->db->where("url LIKE '%" . $cid . "%'");
		$this->db->delete($this->db->dbprefix . "audit");

		$this->db->where("related_id", $cid);
		if ($this->db->delete($this->db->dbprefix . "audit")) {
			return true;
		}
		return false;
	}

	public function deleteClientTimeRecords($cid) {
		$this->db->where("clients_id", $cid);
		if ($this->db->delete($this->db->dbprefix . "time_records")) {
			return true;
		}
		return false;
	}

	public function activateClient($id) {
		$clientinfo = array(
			'active' => 1
		);
		$this->db->where("id", $id);
		if ($this->db->update($this->db->dbprefix . "clients", $clientinfo)) {
			return true;
		}
		return false;
	}

	public function deactivateClient($id) {
		$clientinfo = array(
			'active' => 0
		);
		$this->db->where("id", $id);
		if ($this->db->update($this->db->dbprefix . "clients", $clientinfo)) {
			return true;
		}
		return false;
	}

	public function migrateClient($post) {
		// connect to secondary database
		$bf3 = $this->load->database('bf3', TRUE);

		// get client data
		$bf3->where("id", $post['clientid']);
		$client = $bf3->get("clients")->result_array();
		$clientdata = array(
			'id' => $client[0]['id'],
			'active' => $client[0]['active'],
			'client_name' => $client[0]['name'],
			'time_remaining' => $client[0]['time_remaining'],
			'is_time_blocked' => $client[0]['is_time_blocked'],
			'time_block_change_date' => $client[0]['time_block_change_date'],
			'username' => $client[0]['username'],
			'password' => $client[0]['password'],
			'salt' => $client[0]['salt'],
		);
		// Insert into BF4
		$this->db->insert($this->db->dbprefix . "clients", $clientdata);

		// get all notes data
		if ($post['notes'] == 'yes') {
			$bf3->where("clients_id", $post['clientid']);
			$notes = $bf3->get("client_notes")->result_array();
			// for each individual note
			foreach ($notes as $n => $note) {
				$notedata = array(
					'id' => $note['id'],
					'clients_id' => $note['clients_id'],
					'note_name' => $note['name'],
					'url' => $note['url'],
					'username' => $note['username'],
					'password' => $note['password'],
					'notes' => $note['notes'],
					'author' => $note['author'],
					'date_updated' => $note['date_updated'],
				);
				// Insert into BF4
				$this->db->insert($this->db->dbprefix . "client_notes", $notedata);
			}
		}

		// get contacts data
		if ($post['contacts'] == 'yes') {
			$bf3->where("clients_id", $post['clientid']);
			$contacts = $bf3->get("contacts")->result_array();
			// for each individual contact
			foreach ($contacts as $c => $contact) {
				$contactdata = array(
					'id' => $contact['id'],
					'clients_id' => $contact['clients_id'],
					'first_name' => $contact['first_name'],
					'last_name' => $contact['last_name'],
					'title' => $contact['title'],
					'emails' => $contact['emails'],
					'address' => $contact['address'],
					'city' => $contact['city'],
					'state' => $contact['state'],
					'zip' => $contact['zip'],
					'phones' => $contact['phones'],
				);
				// Insert into BF4
				$this->db->insert($this->db->dbprefix . "contacts", $contactdata);
			}
		}

		// get domains data
		if ($post['domains'] == 'yes') {
			$bf3->where("clients_id", $post['clientid']);
			$domains = $bf3->get("domains")->result_array();
			// for each individual domain
			foreach ($domains as $d => $domain) {
				$domaindata = array(
					'id' => $domain['id'],
					'clients_id' => $domain['clients_id'],
					'domain' => $domain['domain'],
					'ada' => $domain['ada'],
				);
				// Insert into BF4
				$this->db->insert($this->db->dbprefix . "domains", $domaindata);
			}
		}

		return true;
	}

}
