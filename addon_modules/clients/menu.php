<?php
/* Clients module initialization file */

$_menu = array(
    'url' => '/admin/clients/',
    'title' => 'Clients',
    'icon' => 'lnr lnr-mustache',
    'access' => 1,
    'sub_items' => array(
        'clients' => array(
            'url' => '/admin/clients/',
            'title' => 'Clients',
            'icon' => 'lnr lnr-mustache',
            'access' => 1,
        ),
        'inactive' => array(
            'url' => '/admin/clients/inactive/',
            'title' => 'Inactive',
            'icon' => 'lnr lnr-sad',
            'access' => 4,
        ),
        'add' => array(
            'url' => '/admin/clients/add/',
            'title' => 'Add',
            'icon' => 'lnr lnr-plus-circle',
            'access' => 3,
        ),
        'migrate' => array(
            'url' => '/admin/clients/migrate/',
            'title' => 'Migrate',
            'icon' => 'lnr lnr-file-import',
            'access' => 4,
        ),
    ),
);