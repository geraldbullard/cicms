<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clients extends MY_Controller
{

	function __construct()
	{
		parent::__construct();
		// Get site wide settings first
		$this->Settings_model->loadSitewideSettings();

		// Load This Model
		$this->load->model('Clients_model');

		// Make sure the user is logged in for this module
		if (!$this->session->userdata('user_id'))
		{
			// set the url they were trtying to go to in session
			$this->session->set_userdata('page_url', current_url());

			//Redirect to login
			redirect('user/login');
		}
	}

	public function admin_index()
	{
		$this->data = array();
		$this->data['title'] = 'Clients Index';

		// load the config from the current module
		$this->load->config('clients');
		$this->data['clients_config'] = $this->config->config['clients'];

        // get all active clients data
		$this->db->where("active", 1);
        $this->db->order_by("client_name", "ASC");
        $clients = $this->db->get($this->db->dbprefix . "clients");
		$this->data['clients'] = $clients->result_array();

		// Added for developer dropdown
		$this->data['dev_data'] = $clients->result_array();

		$this->load->view('layout/admin/header.php', $this->data);
		$this->load->view('admin/index', $this->data);
		$this->load->view('layout/admin/footer.php', $this->data);
	}

	public function inactive()
	{
		$this->data = array();
		$this->data['title'] = 'Inactive Clients';

		// load the config from the current module
		$this->load->config('clients');
		$this->data['clients_config'] = $this->config->config['clients'];

        // get all inactive clients data
		$this->db->where("active", 0);
        $this->db->order_by("client_name", "ASC");
        $clients = $this->db->get($this->db->dbprefix . "clients");
		$this->data['clients'] = $clients->result_array();

		// Added for developer dropdown
		$this->data['dev_data'] = $clients->result_array();

		$this->load->view('layout/admin/header.php', $this->data);
		$this->load->view('admin/inactive', $this->data);
		$this->load->view('layout/admin/footer.php', $this->data);
	}

	public function add()
	{
        $this->data = array();
		$this->data['title'] = 'Clients Add';

		if ($this->input->post()) {
			/*echo '<pre>';
			print_r($this->input->post());
			die();*/
            $ci_success = false;
            $dn_success = false;
			// we need to validate the form first thing
			$this->load->library('form_validation');
			$this->form_validation->set_rules('client_name', 'Client Name', 'trim|required');
			$this->form_validation->set_rules('username', 'User Name', 'trim|required|is_unique[ci_clients.username]');
			$this->form_validation->set_message('is_unique', '%s already exists.');
			$this->form_validation->set_rules(
				'password',
				'Password',
				'trim|required|matches[passconf]' //|callback_is_password_strong|callback_check_password
			);
			$this->form_validation->set_rules('passconf', 'Confirm Password', 'trim|required');
			/* Turn back on later???
			$this->form_validation->set_message(
				'is_password_strong',
				'<strong>Your password must have the following:</strong>
                <ul>
	                <li>Must contain one Uppercase Letter</li>
	                <li>Must contain one Lowercase Letter</li>
	                <li>Must contain a Number</li>
	                <li>Must contain a Special Character</li>
                </ul>'
			);*/
			//$this->form_validation->set_message('check_password', 'Passwords do not match!');
			if ($this->form_validation->run() !== FALSE)
			{
				/* Array(
					[client_name] => ACME Test
					[username] => acmetest
					[password] => pass1234
					[passconf] => pass1234
					[is_time_blocked] => on
					[domains] => Array(
						[0] => www.domain1.ext
						[1] => www.domain2.ext
						etc...
				*/
				// Set the insert info from $_POST values
				$clientinfo = array(
					'active' => 1,
					'client_name' => $this->input->post('client_name'),
					'time_remaining' => $this->input->post('time_remaining'),
					'is_time_blocked' => $this->input->post('is_time_blocked'),
					'time_block_change_date' => date('Y-m-d H:i:s'),
					'username' => $this->input->post('username'),
					'password' => NULL,
					'salt' => NULL
				);

				// Insert the Client information
				$this->db->insert($this->db->dbprefix . "clients", $clientinfo);
                $clientid = $this->db->insert_id();

				// If it doesn't update:
				if (!$clientid)
				{
					// Set the error to the data array:
					$this->data['msg'] = "Error Creating the Client!";
					$this->data['msgtype'] = "danger";
				}
				// If we get here, it means the process was successful.
				else
				{
                    $ci_success = true;
				}

				// Now add services
				// First, remove all from Services table
				$this->db->where("clients_id", $clientid);
				$this->db->delete($this->db->dbprefix . "services");
				if (!empty($this->input->post('services'))) {
					// Now set the array and import post data
					foreach ($this->input->post('services') as $i => $service) {
						if (!empty($service[0])) {
							$service = array(
								'clients_id' => $clientid,
								'service_name' => $service[0],
								'service_code' => $service[2],
								'service_note' => $service[1],
								'last_updated_by' => $this->session->userdata('user_id'),
								'last_updated' => date("Y-m-d"),
							);
							$this->db->insert($this->db->dbprefix . "services", $service);
						}
					}
				}

                // Now add domains
                if ($ci_success === true) {
                    if (count($this->input->post('domains')) > 0) {
                        $domains = array();
                        foreach ($this->input->post('domains') as $i => $domain_name) {
                            $domains[] = array(
                                'clients_id' => $clientid,
                                'domain' => $this->input->post('domains')[$i],
                                'ada' => $this->input->post('domains_ada')[$i]
                            );
                        }
                    }
                    foreach ($domains as $i => $domain) {
                        $cleandomain = preg_replace('/[^a-zA-Z0-9.]/', '', strtolower($domain['domain']));
                        $domaininfo = array(
                            'clients_id' => $domain['clients_id'],
                            'domain' => $cleandomain,
                            'ada' => $domain['ada'],
                        );
                        if (!$this->db->insert($this->db->dbprefix . "domains", $domaininfo)) {
                            $dn_success = false;
                            break;
                        } else {
	                        $dn_success = true;
                        }
                    }
                }
			} else {
				$this->data['msg'] = "Your submission has the following Problems:<br>" .
                     validation_errors('<div>','</div>');
				$this->data['msgtype'] = "danger";
			}

            // If domains added successfully, all is golden...
            if ($dn_success === true) {
                // Add Audit Trail
                $this->load->model('Audit_model');
                $this->Audit_model->addAuditLog(
                    $this->config->config['settings']['auditTypeRecordCreated'],
                    '/clients/add',
                    'New Client Created',
                    'clients',
                    $clientid
                );

                // Set the success message:
                $this->session->set_flashdata(array("msg" => "Client Successfully Created!", "msgtype" => "success"));

                // Redirect to Clients Page
                redirect('/admin/clients/', 'refresh');
            } else {
                // Set the success message:
                $this->session->set_flashdata(array("msg" => "Error Creating Client!", "msgtype" => "danger"));

                // Redirect to Clients Page
                redirect('/admin/clients/add', 'refresh');
            }
		}

		$this->load->view('layout/admin/header.php', $this->data);
		$this->load->view('admin/add', $this->data);
		$this->load->view('layout/admin/footer.php', $this->data);
	}

	public function edit()
	{
		$clientid = $this->uri->segment(4);

        $this->data = array();
        $this->data['title'] = 'Client Edit';

        if ($this->input->post()) {
            $ci_success = false;
            // we need to validate the form first thing
            $this->load->library('form_validation');
            $this->form_validation->set_rules('client_name', 'Client Name', 'trim|required');
            //$this->form_validation->set_rules('username', 'User Name', 'trim|required|is_unique[ci_clients.username]');
            //$this->form_validation->set_message('is_unique', '%s already exists.');
            /*$this->form_validation->set_rules(
                'password',
                'Password',
                'trim|required|matches[passconf]' //|callback_is_password_strong|callback_check_password
            );
            $this->form_validation->set_rules('passconf', 'Confirm Password', 'trim|required');*/
            /* Turn back on later???
            $this->form_validation->set_message(
                'is_password_strong',
                '<strong>Your password must have the following:</strong>
                <ul>
                    <li>Must contain one Uppercase Letter</li>
                    <li>Must contain one Lowercase Letter</li>
                    <li>Must contain a Number</li>
                    <li>Must contain a Special Character</li>
                </ul>'
            );*/
            //$this->form_validation->set_message('check_password', 'Passwords do not match!');
            if ($this->form_validation->run() !== FALSE)
            {
                // add a check here if the time remaining has changed //////////////////////////////////////////////////
                $this->db->where("id", $clientid);
                $existing_data = $this->db->get($this->db->dbprefix . "clients")->row_array();

                // set the data array to be updated
                $clientinfo = array(
                    'active' => 1,
                    'client_name' => $this->input->post('client_name'),
                    'time_remaining' => $this->input->post('time_remaining'),
                    'is_time_blocked' => ($this->input->post('is_time_blocked') == 'on' ? 1 : 0),
                    'time_block_change_date' => (
                        $this->input->post('time_remaining') !== $existing_data['time_remaining']
                    ) ? date('Y-m-d H:i:s') : $existing_data['time_block_change_date'],
                    'username' => $this->input->post('username'),
                    //'password' => NULL,
                    //'salt' => NULL
                );

                // Insert the Client information
                $this->db->where("id", $clientid);
                if ($this->db->update($this->db->dbprefix . "clients", $clientinfo))
                    $ci_success = true;

                // Now add services
	            // First, remove all from Services table
	            $this->db->where("clients_id", $clientid);
	            $this->db->delete($this->db->dbprefix . "services");
	            if (!empty($this->input->post('services'))) {
		            // Now set the array and import post data
		            foreach ($this->input->post('services') as $i => $service) {
		            	if (isset($service[1])) {
				            $data = array(
					            'clients_id' => $clientid,
					            'active' => 1,
					            'service_name' => ($service[1] ? $service[1] : NULL),
					            'service_code' => ($service[0] ? $service[0] : NULL),
					            'service_note' => ($service[2] ? $service[2] : NULL),
					            'last_updated_by' => $this->session->userdata('user_id'),
					            'last_updated' => date("Y-m-d"),
				            );
		            	} else {
				            $data = array(
					            'clients_id' => $clientid,
					            'active' => 0,
					            'service_name' => NULL,
					            'service_code' => ($service[0] ? $service[0] : NULL),
					            'service_note' => ($service[2] ? $service[2] : NULL),
					            'last_updated_by' => ($service[2] != '' ? $this->session->userdata('user_id') : NULL),
					            'last_updated' => ($service[2] != '' ? date("Y-m-d") : NULL),
				            );
			            }
			            $this->db->insert($this->db->dbprefix . "services", $data);
		            }
	            }

                // Now add domains
                if (!empty($this->input->post('domains'))) {
                    $domains = array();
                    foreach ($this->input->post('domains') as $i => $domain_name) {
                        $domains[] = array(
                            'id' => $this->input->post('domains_id')[$i],
                            'clients_id' => $clientid,
                            'domain' => $this->input->post('domains')[$i],
                            'ada' => $this->input->post('domains_ada')[$i]
                        );
                    }
	                foreach ($domains as $i => $domain) {
	                    $cleandomain = preg_replace('/[^a-zA-Z0-9.]/', '', strtolower($domain['domain']));
	                    if ($domain['id'] != '') {
		                    $domainupdate = array(
		                        'clients_id' => $domain['clients_id'],
		                        'domain' => $cleandomain,
		                        'ada' => $domain['ada'],
		                    );
		                    $this->db->where("id", $domain['id']);
		                    $this->db->update($this->db->dbprefix . "domains", $domainupdate);
	                    } else {
		                    $domaininsert = array(
			                    'clients_id' => $domain['clients_id'],
			                    'domain' => $cleandomain,
			                    'ada' => $domain['ada'],
		                    );
		                    $this->db->insert($this->db->dbprefix . "domains", $domaininsert);
	                    }
	                }
                }
            }

            // If domains added successfully, all is golden...
            if ($ci_success == true) {
                // Add Audit Trail
                $this->load->model('Audit_model');
                $this->Audit_model->addAuditLog(
                    $this->config->config['settings']['auditTypeRecordUpdated'],
                    '/clients/edit/' . $clientid,
                    'Client Updated',
                    'clients',
                    $clientid
                );

                // Set the success message:
                $this->session->set_flashdata(array("msg" => "Client Successfully Updated!", "msgtype" => "success"));

                // Redirect to Clients Page
                redirect('/admin/clients/edit/' . $clientid, 'refresh');
            } else {
                // Set the success message:
                $this->session->set_flashdata(array("msg" => "Error Updating Client!", "msgtype" => "danger"));

                // Redirect to Clients Page
                redirect('/admin/clients/edit/' . $clientid, 'refresh');
            }
        }

		// load the config from the current module
		$this->load->config('clients');
		$this->data['clients_config'] = $this->config->config['clients'];

		// Start Benchmark for Query Time
		$this->benchmark->mark('query_start');

		// get client data
		$this->db->where("id", $clientid);
		$this->db->limit(1);
		$client = $this->db->get($this->db->dbprefix . "clients");
		$this->data['client'] = $client->result_array()[0];

		// get services data
		$this->db->where("clients_id", $clientid);
		$this->db->order_by("id", "ASC");
		$services = $this->db->get($this->db->dbprefix . "services");
		$this->data['client']['services'] = $services->result_array();

		// get domains data
		$this->db->where("clients_id", $clientid);
		$this->db->order_by("id", "ASC");
		$domains = $this->db->get($this->db->dbprefix . "domains");
		$this->data['client']['domains'] = $domains->result_array();

		// Added for developer dropdown
		$this->data['dev_data']['client'] = $this->data['client'];

		// End Benchmark for Query Time
		$this->benchmark->mark('query_end');
		$this->data['dev_data']['query_time'] = $this->benchmark->elapsed_time('query_start', 'query_end');

		$this->load->view('layout/admin/header.php', $this->data);
		$this->load->view('admin/edit', $this->data);
		$this->load->view('layout/admin/footer.php', $this->data);
	}

	public function delete() // ONLY TOP DEVELOPER, DANGEROUS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	{
		$clientid = $this->uri->segment(4);
		if (!$clientid) $clientid = $this->uri->segment(3);

		// first delete all the related data
		$this->Clients_model->deleteClientAuditTrail($clientid);
		$this->Clients_model->deleteClientLogins($clientid);
		$this->Clients_model->deleteClientNotes($clientid);
		$this->Clients_model->deleteClientContacts($clientid);
		$this->Clients_model->deleteClientDomains($clientid);
		// priorities
		// project links
		// project notes
		// project tasks
		// users to projects
		$this->Clients_model->deleteClientProjects($clientid);
		$this->Clients_model->deleteClientServices($clientid);
		// time records to tasks
		$this->Clients_model->deleteClientTimeRecords($clientid);
		// users
		// users files

		if (!$this->Clients_model->delete($clientid)) {
			// Set the error to the flashdata array:
			$this->session->set_flashdata(array("msg" => "Error Deleting the Client!", "msgtype" => "danger"));
		} else {
			// Add Audit Trail
			$this->load->model('Audit_model');
			$this->Audit_model->addAuditLog(
				$this->config->config['settings']['auditTypeUserLogout'],
				'/client/delete/' . $clientid,
				'Client Deleted',
				'clients',
				$this->session->userdata('user_id')
			);
			// Set the success message to the flashdata array:
			$this->session->set_flashdata(array("msg" => "Client Successfully Deleted!", "msgtype" => "success"));
		}
		// Redirect to Clients Page
		redirect('/admin/clients/','refresh');
	} // ONLY TOP DEVELOPER, DANGEROUS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	public function deleteDomain()
	{
		$did = $this->uri->segment(3);

		if (!$this->Clients_model->deleteDomain($did)) {
			echo json_encode(array("msg" => "failure"));
		} else {
			// Add Audit Trail
			$this->load->model('Audit_model');
			$this->Audit_model->addAuditLog(
				$this->config->config['settings']['auditTypeRecordDeleted'],
				'/client/deleteDomain/' . $did,
				'Client Domain Deleted',
				'clients',
				$this->session->userdata('user_id')
			);
			echo json_encode(array("msg" => "success"));
		}
	}

	public function loadClientData()
	{
        $userid = $this->uri->segment(3);

        $this->db->where("id", $userid);
        $this->db->where("active", 1);
        $this->db->limit(1);
        $client = $this->db->get($this->db->dbprefix . "clients")->result_array();
        $data = array(
            'id' => $client[0]['id'],
            'client_name' => ($client[0]['client_name'] != '' ? $client[0]['client_name'] : 'undefined'),
            'active' => ($client[0]['active'] != '' ? $client[0]['active'] : 'undefined'),
            'time_remaining' => ($client[0]['time_remaining'] != '' ? $client[0]['time_remaining'] : 'undefined'),
            'is_time_blocked' => ($client[0]['is_time_blocked'] != '' ? $client[0]['is_time_blocked'] : 'undefined'),
            'time_block_change_date' => ($client[0]['time_block_change_date'] != '' ? $client[0]['time_block_change_date'] : 'undefined'),
            'username' => ($client[0]['username'] != '' ? $client[0]['username'] : 'undefined'),
            'n_cnt' => 0,
        );

        // projects count
        $this->db->where("clients_id", $userid);
        $this->db->where("project_statuses_id != ", 5);
        $projects = $this->db->get($this->db->dbprefix . "projects");
        $data['p_cnt'] = $projects->num_rows();

        // notes count
        $this->db->where("clients_id", $userid);
        $notes = $this->db->get($this->db->dbprefix . "client_notes");
        $data['n_cnt'] = $notes->num_rows();

        // contacts count
        $this->db->where("clients_id", $userid);
        $contacts = $this->db->get($this->db->dbprefix . "contacts");
        $data['c_cnt'] = $contacts->num_rows();

        // domains count
        $this->db->where("clients_id", $userid);
        $domains = $this->db->get($this->db->dbprefix . "domains");
        $data['d_cnt'] = $domains->num_rows();

        // services count
        $this->db->where("clients_id", $userid);
        $this->db->where("active", 1);
        $services = $this->db->get($this->db->dbprefix . "services");
        $data['s_cnt'] = $services->num_rows();

        // tasks count (Needs left join from projects)
        /*$this->db->where("clients_id", $userid);
        $tasks = $this->db->get($this->db->dbprefix . "tasks");
        $data['t_cnt'] = $tasks->num_rows();*/

        echo json_encode($data);
	}

    public function getClientDomains()
    {
        $cid = $this->uri->segment(3);
		$data = $this->Clients_model->getClientDomains($cid);
	    echo json_encode($data);
    }

    public function loadDomainsData()
    {
        $userid = $this->uri->segment(3);

        $this->db->where("clients_id", $userid);
        $this->db->order_by("id", "ASC");
        $domains = $this->db->get($this->db->dbprefix . "domains")->result_array();
        $data = array();
        foreach ($domains as $d => $domain) {
            $data[] = array(
                'id' => $domain['id'],
                'clients_id' => ($domain['clients_id'] != '' ? $domain['clients_id'] : 'undefined'),
                'domain' => ($domain['domain'] != '' ? $domain['domain'] : 'undefined'),
                'ada' => ($domain['ada'] != '' ? $domain['ada'] : 'undefined'),
            );
        }

        echo json_encode($data);
    }

	public function loadProjectsData()
	{
        $userid = $this->uri->segment(3);

        $this->db->where("clients_id", $userid);
		$this->db->where("project_statuses_id != ", 5);
        $this->db->order_by("id", "ASC");
        $projects = $this->db->get($this->db->dbprefix . "projects")->result_array();
        $data = array();
        foreach ($projects as $p => $project) {
            $data[] = array(
                'id' => $project['id'],
                'clients_id' => ($project['clients_id'] != '' ? $project['clients_id'] : 'undefined'),
                'project_statuses_id' => ($project['project_statuses_id'] != '' ? $project['project_statuses_id'] : 'undefined'),
                'categories_id' => ($project['categories_id'] != '' ? $project['categories_id'] : 'undefined'),
                'domains_id' => ($project['domains_id'] != '' ? $project['domains_id'] : 'undefined'),
                'contract_info' => ($project['contract_info'] != '' ? $project['contract_info'] : 'undefined'),
                'time_estimate' => ($project['time_estimate'] != '' ? $project['time_estimate'] : 'undefined'),
            );
        }

        echo json_encode($data);
	}

	public function loadProjectStatus()
	{
        $statusid = $this->uri->segment(3);

        $this->db->where("id", $statusid);
        $this->db->limit(1);
        $status = $this->db->get($this->db->dbprefix . "project_statuses");

        echo json_encode($status->row());
	}

	public function loadProjectCategory()
	{
        $catid = $this->uri->segment(3);

        $this->db->where("id", $catid);
        $this->db->limit(1);
        $category = $this->db->get($this->db->dbprefix . "categories");

        echo json_encode($category->row());
	}

	public function loadProjectDomain()
	{
        $domid = $this->uri->segment(3);

        $this->db->where("id", $domid);
        $this->db->limit(1);
        $domain = $this->db->get($this->db->dbprefix . "domains");

        echo json_encode($domain->row());
	}

	public function loadDomainIp()
	{
        $domain = $this->uri->segment(3);

        echo gethostbyname($domain);
	}

	public function loadDomainIpCheck()
	{
        $domain = $this->uri->segment(3);

        $true = false;
        foreach ($this->db->get($this->db->dbprefix . "server_ips")->result_array() as $i => $ip) {
            if (gethostbyname($domain) == $ip['ip_address']) {
                $true = true;
                break;
            }
        }
        if ($true == true) {
            echo 'true';
        } else {
            echo 'false';
        }
	}

	public function loadNotesData()
	{
        $userid = $this->uri->segment(3);

        $this->db->where("clients_id", $userid);
        $notes = $this->db->get($this->db->dbprefix . "client_notes")->result_array();
        $data = array();
        foreach ($notes as $n => $note) {
	        $aname = '';
	        if (is_numeric($note['author'])) {
	        	$author = $this->Users_model->get_user_info($note['author']);
	        	$aname = $author['first_name'] . ' ' . $author['last_name'];
	        } else {
	        	$aname = $note['author'];
	        }
	        $data[] = array(
                'id' => $note['id'],
                'clients_id' => ($note['clients_id'] != '' ? $note['clients_id'] : 'undefined'),
                'note_name' => ($note['note_name'] != '' ? $note['note_name'] : 'undefined'),
                'url' => ($note['url'] != '' ? $note['url'] : 'undefined'),
                'username' => ($note['username'] != '' ? $note['username'] : 'undefined'),
                'password' => ($note['password'] != '' ? $note['password'] : 'undefined'),
                'notes' => ($note['notes'] != '' ? $note['notes'] : 'undefined'),
                'author' => ($aname != '' ? $aname : 'undefined'),
                'date_updated' => ($note['date_updated'] != '' ? date("m-d-Y", strtotime($note['date_updated'])) : 'undefined'),
            );
        }
        echo json_encode($data);
	}

	public function loadSingleNoteData()
	{
		$id = $this->uri->segment(3);

        $this->db->where("id", $id);
        $note = $this->db->get($this->db->dbprefix . "client_notes")->result_array();
        $data = array();
        $aname = '';
        if (is_numeric($note[0]['author'])) {
            $author = $this->Users_model->get_user_info($note[0]['author']);
            $aname = $author['first_name'] . ' ' . $author['last_name'];
        } else {
            $aname = $note[0]['author'];
        }
        $data = array(
            'id' => $note[0]['id'],
            'clients_id' => ($note[0]['clients_id'] != '' ? $note[0]['clients_id'] : 'undefined'),
            'note_name' => ($note[0]['note_name'] != '' ? $note[0]['note_name'] : 'undefined'),
            'url' => ($note[0]['url'] != '' ? $note[0]['url'] : 'undefined'),
            'username' => ($note[0]['username'] != '' ? $note[0]['username'] : 'undefined'),
            'password' => ($note[0]['password'] != '' ? $note[0]['password'] : 'undefined'),
            'notes' => ($note[0]['notes'] != '' ? $note[0]['notes'] : 'undefined'),
            'author' => ($aname != '' ? $aname : 'undefined'),
            'date_updated' => ($note[0]['date_updated'] != '' ? date("m-d-Y", strtotime($note[0]['date_updated'])) : 'undefined'),
        );
        echo json_encode($data);
	}

	public function loadSingleContactData()
	{
		$id = $this->uri->segment(3);

        $this->db->where("id", $id);
        $contact = $this->db->get($this->db->dbprefix . "contacts")->result_array();
        $data = array(
            'id' => $contact[0]['id'],
            'clients_id' => ($contact[0]['clients_id'] != '' ? $contact[0]['clients_id'] : 'undefined'),
            'first_name' => ($contact[0]['first_name'] != '' ? $contact[0]['first_name'] : 'undefined'),
            'last_name' => ($contact[0]['last_name'] != '' ? $contact[0]['last_name'] : 'undefined'),
            'title' => ($contact[0]['title'] != '' ? $contact[0]['title'] : 'undefined'),
            'emails' => ($contact[0]['emails'] != '' ? $contact[0]['emails'] : 'undefined'),
            'address' => ($contact[0]['address'] != '' ? $contact[0]['address'] : 'undefined'),
            'city' => ($contact[0]['city'] != '' ? $contact[0]['city'] : 'undefined'),
            'state' => ($contact[0]['state'] != '' ? $contact[0]['state'] : 'undefined'),
            'zip' => ($contact[0]['zip'] != '' ? $contact[0]['zip'] : 'undefined'),
            'phones' => ($contact[0]['phones'] != '' ? $contact[0]['phones'] : 'undefined'),
        );
        echo json_encode($data);
	}

	public function loadContactsData()
	{
        $userid = $this->uri->segment(3);

        $this->db->where("clients_id", $userid);
        $contacts = $this->db->get($this->db->dbprefix . "contacts")->result_array();
        $data = array();
        foreach ($contacts as $c => $contact) {
            $data[] = array(
                'id' => $contact['id'],
                'clients_id' => ($contact['clients_id'] != '' ? $contact['clients_id'] : 'undefined'),
                'first_name' => ($contact['first_name'] != '' ? $contact['first_name'] : 'undefined'),
                'last_name' => ($contact['last_name'] != '' ? $contact['last_name'] : 'undefined'),
                'title' => ($contact['title'] != '' ? $contact['title'] : 'undefined'),
                'emails' => ($contact['emails'] != '' ? $contact['emails'] : 'undefined'),
                'address' => ($contact['address'] != '' ? $contact['address'] : 'undefined'),
                'city' => ($contact['city'] != '' ? $contact['city'] : 'undefined'),
                'state' => ($contact['state'] != '' ? $contact['state'] : 'undefined'),
                'zip' => ($contact['zip'] != '' ? $contact['zip'] : 'undefined'),
                'phones' => ($contact['phones'] != '' ? $contact['phones'] : 'undefined'),
            );
        }

        echo json_encode($data);
	}

    public function loadServicesData()
    {
        $userid = $this->uri->segment(3);
        $active = $this->uri->segment(4);

        $this->db->where("clients_id", $userid);
	    if ($active == 1) $this->db->where("active", 1);
        $services = $this->db->get($this->db->dbprefix . "services")->result_array();
        $data = array();
        foreach ($services as $s => $service) {
            $data[] = array(
                'id' => $service['id'],
                'clients_id' => ($service['clients_id'] != '' ? $service['clients_id'] : 'undefined'),
                'service_name' => ($service['service_name'] != '' ? $service['service_name'] : 'undefined'),
                'service_category' => ($service['service_category'] != '' ? $service['service_category'] : 'undefined'),
                'service_length' => ($service['service_length'] != '' ? $service['service_length'] : 'undefined'),
                'service_frequency' => ($service['service_frequency'] != '' ? $service['service_frequency'] : 'undefined'),
                'service_note' => ($service['service_note'] != '' ? $service['service_note'] : 'undefined'),
                'last_updated_by' => ($service['last_updated_by'] != '' ? $service['last_updated_by'] : 'undefined'),
                'last_updated' => ($service['last_updated'] != '' ? $service['last_updated'] : 'undefined'),
            );
        }

        echo json_encode($data);
    }

    /*public function loadTasksData()
    {
        $userid = $this->uri->segment(3);

        $this->db->where("clients_id", $userid);
        $notes = $this->db->get($this->db->dbprefix . "client_notes")->result_array();
        $data = array();
        foreach ($notes as $n => $note) {
            $data[] = array(
                'id' => $note['id'],
                'clients_id' => ($note['clients_id'] != '' ? $note['clients_id'] : 'undefined'),
                'note_name' => ($note['note_name'] != '' ? $note['note_name'] : 'undefined'),
                'url' => ($note['url'] != '' ? $note['url'] : 'undefined'),
                'username' => ($note['username'] != '' ? $note['username'] : 'undefined'),
                'password' => ($note['password'] != '' ? $note['password'] : 'undefined'),
                'notes' => ($note['notes'] != '' ? $note['notes'] : 'undefined'),
                'author' => ($note['author'] != '' ? $note['author'] : 'undefined'),
                'date_updated' => ($note['date_updated'] != '' ? date("m-d-Y", strtotime($note['date_updated'])) : 'undefined'),
            );
        }

        echo json_encode($data);
    }*/

	public function loadLoginsData()
	{
        $userid = $this->uri->segment(3);

        $this->db->where("clients_id", $userid);
        $this->db->order_by("timestamp", "DESC");
        $logins = $this->db->get($this->db->dbprefix . "client_logins")->result_array();
        $data = array();
        foreach ($logins as $l => $login) {
            $data[] = array(
                'id' => $login['id'],
                'clients_id' => ($login['clients_id'] != '' ? $login['clients_id'] : 'undefined'),
                'timestamp' => ($login['timestamp'] != '' ? date("F j, Y - g:i a", strtotime($login['timestamp'])) : 'undefined'),
                'ip_address' => ($login['ip_address'] != '' ? $login['ip_address'] : 'undefined'),
            );
        }

        echo json_encode($data);
	}

	public function deleteService()
	{
		$id = $this->uri->segment(3);

		if (!$this->Clients_model->deleteService($id)) {
			echo json_encode(array("msg" => "failure"));
		} else {
			// Add Audit Trail
			$this->load->model('Audit_model');
			$this->Audit_model->addAuditLog(
				$this->config->config['settings']['auditTypeRecordDeleted'],
				'/client/deleteService/' . $id,
				'Client Service Deleted',
				'clients',
				$this->session->userdata('user_id')
			);
			echo json_encode(array("msg" => "success"));
		}
	}

	public function deleteProject()
	{
		$id = $this->uri->segment(3);

		if (!$this->Clients_model->deleteProject($id)) {
			echo json_encode(array("msg" => "failure"));
		} else {
			// Add Audit Trail
			$this->load->model('Audit_model');
			$this->Audit_model->addAuditLog(
				$this->config->config['settings']['auditTypeRecordDeleted'],
				'/client/deleteProject/' . $id,
				'Client Project Deleted',
				'clients',
				$this->session->userdata('user_id')
			);
			echo json_encode(array("msg" => "success"));
		}
	}

	public function addNote()
	{
		if (!$this->Clients_model->addNote($this->input->post())) {
			return false;
		} else {
			// Add Audit Trail
			$this->load->model('Audit_model');
			$this->Audit_model->addAuditLog(
				$this->config->config['settings']['auditTypeRecordCreated'],
				'/client/addNote/',
				'Client Note Added',
				'clients',
				$this->session->userdata('user_id')
			);
			return true;
		}
	}

	public function editNote()
	{
		if (!$this->Clients_model->editNote($this->input->post())) {
			return false;
		} else {
			// Add Audit Trail
			$this->load->model('Audit_model');
			$this->Audit_model->addAuditLog(
				$this->config->config['settings']['auditTypeRecordUpdated'],
				'/client/editNote/' . $this->uri->segment(3),
				'Client Note Updated',
				'clients',
				$this->session->userdata('user_id')
			);
			return true;
		}
	}

	public function deleteNote()
	{
		$id = $this->uri->segment(3);

		if (!$this->Clients_model->deleteNote($id)) {
			echo json_encode(array("msg" => "failure"));
		} else {
			// Add Audit Trail
			$this->load->model('Audit_model');
			$this->Audit_model->addAuditLog(
				$this->config->config['settings']['auditTypeRecordDeleted'],
				'/client/deleteNote/' . $id,
				'Client Note Deleted',
				'clients',
				$this->session->userdata('user_id')
			);
			echo json_encode(array("msg" => "success"));
		}
	}

	public function addContact()
	{
		if (!$this->Clients_model->addContact($this->input->post())) {
			return false;
		} else {
			// Add Audit Trail
			$this->load->model('Audit_model');
			$this->Audit_model->addAuditLog(
				$this->config->config['settings']['auditTypeRecordCreated'],
				'/client/addContact/',
				'Client Contact Added',
				'clients',
				$this->session->userdata('user_id')
			);
			return true;
		}
	}

	public function editContact()
	{
		if (!$this->Clients_model->editContact($this->input->post())) {
			return false;
		} else {
			// Add Audit Trail
			$this->load->model('Audit_model');
			$this->Audit_model->addAuditLog(
				$this->config->config['settings']['auditTypeRecordUpdated'],
				'/client/editContact/' . $this->uri->segment(3),
				'Client Contact Updated',
				'clients',
				$this->session->userdata('user_id')
			);
			return true;
		}
	}

	public function deleteContact()
	{
		$id = $this->uri->segment(3);

		if (!$this->Clients_model->deleteContact($id)) {
			echo json_encode(array("msg" => "failure"));
		} else {
			// Add Audit Trail
			$this->load->model('Audit_model');
			$this->Audit_model->addAuditLog(
				$this->config->config['settings']['auditTypeRecordDeleted'],
				'/client/deleteContact/' . $id,
				'Client Contact Deleted',
				'clients',
				$this->session->userdata('user_id')
			);
			echo json_encode(array("msg" => "success"));
		}
	}

	public function activate()
	{
		$id = $this->uri->segment(3);

		if (!$this->Clients_model->activateClient($id)) {
			echo json_encode(array("msg" => "failure"));
		} else {
			// Add Audit Trail
			$this->load->model('Audit_model');
			$this->Audit_model->addAuditLog(
				$this->config->config['settings']['auditTypeRecordUpdated'],
				'/client/activate/' . $id,
				'Client Activated',
				'clients',
				$this->session->userdata('user_id')
			);
			// Set the success message:
			$this->session->set_flashdata(
                array("msg" => $this->Clients_model->getClientsRow($id)[0]['client_name'] . " Successfully Activated!", "msgtype" => "success")
            );
			// Redirect to Inactive Clients Page
			redirect('/admin/clients/inactive', 'refresh');
		}
	}

	public function deactivate()
	{
		$id = $this->uri->segment(4);

		if (!$this->Clients_model->deactivateClient($id)) {
			echo json_encode(array("msg" => "failure"));
		} else {
			// Add Audit Trail
			$this->load->model('Audit_model');
			$this->Audit_model->addAuditLog(
				$this->config->config['settings']['auditTypeRecordUpdated'],
				'/client/deactivate/' . $id,
				'Client Deactivated',
				'clients',
				$this->session->userdata('user_id')
			);
			// Set the success message:
			$this->session->set_flashdata(array("msg" => "Client Successfully Deactivated!", "msgtype" => "success"));
			// Redirect to Clients Page
			redirect('/admin/clients/','refresh');
		}
	}

	public function migrate()
	{
		$this->data = array();
		$this->data['title'] = 'Migrate Clients';

		// load the config from the current module
		$this->load->config('clients');
		$this->data['clients_config'] = $this->config->config['clients'];

		// get id array of all existing BF4 clients
		$this->data['bf4_clients'] = array();
		$this->db->select('id');
		$bf4_clients = $this->db->get($this->db->dbprefix . "clients");
		foreach ($bf4_clients->result_array() as $i => $client) {
			$this->data['bf4_clients'][] = $client['id'];
		}

		// connect to secondary database
		$bf3 = $this->load->database('bf3', TRUE);
		// get all BF3 clients data
		$bf3->order_by("name", "ASC");
		$clients = $bf3->get("clients");
		$this->data['bf3_clients'] = $clients->result_array();

		// Added for developer dropdown
		$this->data['dev_data'] = $clients->result_array();

		$this->load->view('layout/admin/header.php', $this->data);
		$this->load->view('admin/migrate', $this->data);
		$this->load->view('layout/admin/footer.php', $this->data);

	}

	public function migrateClient()
	{
		$cid = $this->input->post('clientid');

		// Load BF3 database
		$bf3 = $this->load->database('bf3', TRUE);
		// get BF3 client data
		$bf3->where("id", $cid);
		$client = $bf3->get("clients");
		$client = $client->result_array();

		if (!$this->Clients_model->migrateClient($this->input->post())) {
			echo json_encode(array("msg" => "failure"));
		} else {
			// Add Audit Trail
			$this->load->model('Audit_model');
			$this->Audit_model->addAuditLog(
				$this->config->config['settings']['auditTypeRecordCreated'],
				'/client/migrateClient/' . $this->input->post('clientid'),
				'Client Migrated',
				'clients',
				$this->session->userdata('user_id')
			);
			// Set the success message:
			$this->session->set_flashdata(
				array("msg" => "Client Successfully Migrated!", "msgtype" => "success")
			);
		}
	}

}
