<?php
/* Module functions file */

// If installing the module run this function to create db table(s)
if (!function_exists('bp_installModule')) {
    function bp_installModule() {
        $CI =& get_instance();

        // Update the module status
        $CI->db->set('status', 1);
        $CI->db->where("module", "clients");
        $CI->db->update($CI->db->dbprefix . "modules");

        // Create table(s)
        $CI->db->query("
            CREATE TABLE IF NOT EXISTS `" . $CI->db->dbprefix . "clients` (
				`id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
				`active` tinyint(1) NOT NULL DEFAULT '1',
				`client_name` varchar(63) NOT NULL,
				`time_remaining` decimal(7,2) NOT NULL,
				`is_time_blocked` tinyint(1) NOT NULL,
				`time_block_change_date` datetime DEFAULT NULL,
				`username` varchar(255) DEFAULT NULL,
				`password` char(40) DEFAULT NULL,
				`salt` char(10) DEFAULT NULL,
            	PRIMARY KEY (`id`),
            	KEY `" . $CI->db->dbprefix . "clients_id_idx` (`id`),
            	KEY `" . $CI->db->dbprefix . "clients_active_idx` (`active`),
            	KEY `" . $CI->db->dbprefix . "clients_username_idx` (`username`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");
	    $CI->db->query("
			CREATE TABLE IF NOT EXISTS `" . $CI->db->dbprefix . "client_logins` (
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`clients_id` int(11) DEFAULT NULL,
				`timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
				`ip_address` varchar(15) DEFAULT NULL,
            	PRIMARY KEY (`id`),
            	KEY `" . $CI->db->dbprefix . "client_logins_id_idx` (`id`),
            	KEY `" . $CI->db->dbprefix . "client_logins_cid_idx` (`clients_id`),
            	KEY `" . $CI->db->dbprefix . "client_logins_ip_idx` (`ip_address`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	    ");
	    $CI->db->query("
			CREATE TABLE IF NOT EXISTS `" . $CI->db->dbprefix . "client_notes` (
				`id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
				`clients_id` int(10) UNSIGNED NOT NULL,
				`note_name` varchar(63) NOT NULL,
				`url` varchar(63) NOT NULL,
				`username` varchar(63) NOT NULL,
				`password` varchar(63) NOT NULL,
				`notes` text NOT NULL,
				`author` varchar(60) DEFAULT NULL,
				`date_updated` datetime DEFAULT NULL,
            	PRIMARY KEY (`id`),
            	KEY `" . $CI->db->dbprefix . "client_notes_id_idx` (`id`),
            	KEY `" . $CI->db->dbprefix . "client_notes_cid_idx` (`clients_id`),
            	KEY `" . $CI->db->dbprefix . "client_notes_author_idx` (`author`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	    ");
	    $CI->db->query("
			CREATE TABLE IF NOT EXISTS `" . $CI->db->dbprefix . "client_note_categories` (
				`id` int(3) NOT NULL AUTO_INCREMENT,
				`client_id` int(10) NOT NULL,
				`note_id` int(10) NOT NULL,
				`category_name` varchar(128) NOT NULL,				
				PRIMARY KEY (`id`),
				KEY `" . $CI->db->dbprefix . "client_note_categories_id_idx` (`id`),
				KEY `" . $CI->db->dbprefix . "client_note_categories_cid_idx` (`client_id`),
				KEY `" . $CI->db->dbprefix . "client_note_categories_nid_idx` (`note_id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	    ");
	    $CI->db->query("
			CREATE TABLE IF NOT EXISTS `" . $CI->db->dbprefix . "client_contacts` (
				`id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
				`clients_id` int(10) UNSIGNED NOT NULL,
				`first_name` varchar(31) NOT NULL,
				`last_name` varchar(31) NOT NULL,
				`title` varchar(63) NOT NULL,
				`emails` text NOT NULL,
				`address` varchar(63) NOT NULL,
				`city` varchar(31) NOT NULL,
				`state` char(2) NOT NULL,
				`zip` varchar(15) NOT NULL,
				`phones` text NOT NULL,
				PRIMARY KEY (`id`),
				KEY `" . $CI->db->dbprefix . "client_contacts_cid_idx` (`clients_id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	    ");
	    $CI->db->query("
			CREATE TABLE IF NOT EXISTS `" . $CI->db->dbprefix . "client_domains` (
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`clients_id` int(11) NOT NULL,
				`domain` varchar(64) NOT NULL,
				`ada` varchar(10) NOT NULL,
				PRIMARY KEY (`id`),
				KEY `" . $CI->db->dbprefix . "client_domains_cid_idx` (`clients_id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	    ");

        return true;
    }
}

// If uninstalling the module run this function to remove db entries if selected
if (!function_exists('bp_uninstallModule')) {
    function bp_uninstallModule() {
        $CI =& get_instance();

        // Update the module status
        $CI->db->set('status', -1);
        $CI->db->where("module", "clients");
        $CI->db->update($CI->db->dbprefix . "modules");

        // Drop table(s)
        $CI->db->query("
            DROP TABLE IF EXISTS `" . $CI->db->dbprefix . "clients`;
        ");
        $CI->db->query("
	    	DROP TABLE IF EXISTS `" . $CI->db->dbprefix . "client_logins`;
        ");
        $CI->db->query("
            DROP TABLE IF EXISTS `" . $CI->db->dbprefix . "client_notes`;
        ");
        $CI->db->query("
            DROP TABLE IF EXISTS `" . $CI->db->dbprefix . "client_note_categories`;
        ");
        $CI->db->query("
            DROP TABLE IF EXISTS `" . $CI->db->dbprefix . "contacts`;
        ");
        $CI->db->query("
            DROP TABLE IF EXISTS `" . $CI->db->dbprefix . "domains`;
        ");

        return true;
    }
}
