<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
			<h3 class="page-title">
                Clients
                <div class="pull-right">
                    <?php if (isset($_POST['pid']) && $_POST['pid'] != '') { ?>
                    <a class="margin-left-10" href="javascript:void(0);" onclick="returnToProject(<?php echo $_POST['pid']; ?>);">
                        <button class="btn btn-sm btn-default" id="project_from">
                            <i class="fa fa-arrow-left"></i><span class="hide-below-480">&nbsp;&nbsp;Back to<span class="hide-below-768"> Project</span></span>
                        </button>
                    </a>
                    <?php } ?>
                    <a class="margin-left-10" href="/admin/clients/add/">
                        <button class="btn btn-sm btn-primary" id="client-add">
                            <i class="fa fa-plus"></i><span class="hide-below-480">&nbsp;&nbsp;Add<span class="hide-below-768"> Client</span></span>
                        </button>
                    </a>
                    <a class="hide margin-left-10" href="javascript:;" id="client-edit">
                        <button class="btn btn-sm btn-default">
                            <i class="fa fa-pencil"></i><span class="hide-below-480">&nbsp;&nbsp;Edit<span class="hide-below-768"> Client</span></span>
                        </button>
                    </a>
	                <?php if ($this->session->userdata['system_access']['clients'] > 3) { ?>
                    <a class="hide margin-left-10" href="javascript:;" id="client-deactivate" onclick="return confirm('Are you sure?');">
                        <button class="btn btn-sm btn-warning">
                            <i class="fa fa-power-off"></i><span class="hide-below-480">&nbsp;&nbsp;Deactivate<span class="hide-below-768"> Client</span></span>
                        </button>
                    </a>
	                <?php } ?>
	                <?php if ($this->session->userdata['system_access']['clients'] > 4) { ?>
                    <a class="hide margin-left-10" href="javascript:;" id="client-delete" onclick="deleteClient();">
                        <button class="btn btn-sm btn-danger">
                            <i class="fa fa-trash"></i><span class="hide-below-480">&nbsp;&nbsp;Delete<span class="hide-below-768"> Client</span></span>
                        </button>
                    </a>
                    <?php } ?>
                </div>
            </h3>
            <?php if ($this->session->flashdata('msg')) { ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert alert-<?php echo $this->session->flashdata('msgtype'); ?> alert-dismissible action-alert" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                        <?php echo $this->session->flashdata('msg'); ?>
                    </div>
                </div>
            </div>
            <?php } ?>
			<div class="row clients-main-page">
				<div class="col-sm-4 col-lg-3">
                    <div class="panel panel-headline" style="height:65vh;">
                        <div class="panel-heading padding-10">
                            <span class="clearable">
                                <input
                                    type="text"
                                    placeholder="Find Client/Contact Name"
                                    class="form-control form-input-small"
                                    id="filterClients"
                                    value="<?php echo (
                                        isset($_GET['s']) && $_GET['s'] != '' ? $_GET['s'] : null
                                    ); ?>"
                                >
                                <i class="clearable__clear">&times;</i>
                            </span>
                        </div>
                        <div class="panel-body margin-bottom-15" style="padding-left:10px;padding-bottom:15px;border-left:1px solid #eee;height:87%;overflow-y:scroll;margin-left:10px;margin-right:10px;-moz-box-shadow: inset 0 -10px 10px -10px #ddd;-webkit-box-shadow: inset 0 -10px 10px -10px #ddd;box-shadow: inset 0 -10px 10px -10px #ddd;">
                            <?php
                                foreach ($clients as $c => $client) {
                                    echo '<div>';
                                    echo '<a href="javascript:;" class="load-clients-data" data-id="';
                                    echo $client['id'];
                                    echo '">';
                                    echo $client['client_name'] . '</a>';
                                    echo '</div>';
                                }
                            ?>
                        </div>
                    </div>
				</div>
                <div class="col-sm-8 col-lg-9 margin-bottom-30" id="clientdata">
                    <div class="panel panel-headline">
                        <div class="panel-heading padding-bottom-15">
                            <div class="row">
                                <div class="col-xs-4">
                                    <h3 class="panel-title client-panel-title">Select a Client</h3>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <p class="client-panel-username"></p>
                                </div>
                                <div class="col-xs-4 text-right">
                                    <p class="client-panel-timeblock"></p>
                                </div>
                            </div>
                        </div>
                        <!--style="height:90%;overflow-y:scroll;margin-left:10px;margin-right:10px;-moz-box-shadow: inset 0 -10px 10px -10px #ddd;-webkit-box-shadow: inset 0 -10px 10px -10px #ddd;box-shadow: inset 0 -10px 10px -10px #ddd;"-->
                        <div class="panel-body client-panel-body margin-bottom-15" style="display:none;">
                            <div class="row">
                                <div class="col-sm-6 col-lg-4 col-xl-2">
                                    <a href="javascript:;" id="load-services-data" data-id="">
                                        <div class="metric">
                                            <span class="icon">
                                                <i class="fa fa-shopping-cart"></i>
                                            </span>
                                            <p>
                                                <span class="number">&nbsp;</span>
                                                <span class="title">Services</span>
                                            </p>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-sm-6 col-lg-4 col-xl-2">
                                    <a href="javascript:;" id="load-domains-data" data-id="">
                                        <div class="metric">
                                            <span class="icon">
                                                <i class="fa fa-globe"></i>
                                            </span>
                                            <p>
                                                <span class="number">&nbsp;</span>
                                                <span class="title">Domains</span>
                                            </p>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-sm-6 col-lg-4 col-xl-2">
                                    <a href="javascript:;" id="load-projects-data" data-id="">
                                        <div class="metric">
                                            <span class="icon">
                                                <i class="fa fa-paperclip"></i>
                                            </span>
                                            <p>
                                                <span class="number">&nbsp;</span>
                                                <span class="title">Projects</span>
                                            </p>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-sm-6 col-lg-4 col-xl-2">
                                    <a href="javascript:;" id="load-notes-data" data-id="">
                                        <div class="metric">
                                            <span class="icon">
                                                <i class="fa fa-sticky-note"></i>
                                            </span>
                                            <p>
                                                <span class="number">&nbsp;</span>
                                                <span class="title">Notes</span>
                                            </p>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-sm-6 col-lg-4 col-xl-2">
                                    <a href="javascript:;" id="load-contacts-data" data-id="">
                                        <div class="metric">
                                            <span class="icon">
                                                <i class="fa fa-address-card"></i>
                                            </span>
                                            <p>
                                                <span class="number">&nbsp;</span>
                                                <span class="title">Contacts</span>
                                            </p>
                                        </div>
                                    </a>
                                </div>
                                <div class="col-sm-6 col-lg-4 col-xl-2">
                                    <a href="javascript:;" id="load-developer-data" data-id="" onclick="swal('Coming Soon!');">
                                        <div class="metric">
                                            <span class="icon">
                                                <i class="fa fa-database"></i>
                                            </span>
                                            <p>
                                                <span class="number">&nbsp;</span>
                                                <span class="title">Developers</span>
                                            </p>
                                        </div>
                                    </a>
                                </div>
                                <!--<div class="col-sm-4 col-lg-2">
                                    <a href="javascript:;" id="load-invoices-data" data-id="">
                                        <div class="metric">
                                            <span class="icon">
                                                <i class="fa fa-dollar"></i>
                                            </span>
                                            <p>
                                                <span class="number">25</span>
                                                <span class="title">Invoices</span>
                                            </p>
                                        </div>
                                    </a>
                                </div>-->
                            </div><hr style="margin:0 0 10px;">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div id="clientcontent"></div>
                                </div>
                            </div><hr>
                            <div class="row">
                                <div class="col-lg-12 text-right">
                                    <div id="clientcontentadd"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
            <!-- New Project Modal -->
            <div class="modal fade" id="addProject" tabindex="-1" role="dialog"
                 aria-labelledby="addProjectLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="addProjectLabel">
                                Add Project
                            </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form id="addProjectForm">
                            <input type="hidden" name="clientid" id="p_cid" value="">
                            <div class="modal-body">
                                <p class="form-group">
                                    <label for="p_domain">Domain</label>
                                    <select name="p_domain" id="p_domain" class="form-control p-selection">
                                        <option value="">Select Domain</option>
                                    </select>
                                </p>
                                <p class="form-group">
                                    <label for="p_category">Category</label>
                                    <select name="p_category" id="p_category" class="form-control p-selection">
                                        <option value="">Select Category</option>
                                        <option value="1">Research/Estimation</option>
                                        <option value="2">Website Design/Build</option>
                                        <option value="3">Website Maintenance/Updates</option>
                                        <option value="4">Email Marketing</option>
                                        <option value="5">Search Engine Optimization</option>
                                        <option value="6">Pay-Per-Click Marketing</option>
                                        <option value="7">Meeting</option>
                                        <option value="8">Misc. Administrative</option>
                                        <option value="9">Quality Control</option>
                                        <option value="10">Lunch/Break</option>
                                        <option value="11">WordFence Project</option>
                                        <option value="12">NTS Deployment</option>
                                        <option value="13">ADA Audit</option>
                                        <option value="14">ADA Compliance</option>
                                        <option value="15">Verify Tracked Time</option>
                                        <option value="16">LW PHP Upgrade</option>
                                        <option value="-1">Other</option>
                                    </select>
                                </p>
                                <p class="form-group hidden" id="p-custom-category">
                                    <label for="p_custom_category">Custom Category (Other)</label>
                                    <input type="text" name="p_custom_category" id="p_custom_category" class="form-control">
                                </p>
                                <p class="form-group">
                                    <label for="p_status">Select Status</label>
                                    <select name="p_status" id="p_status" class="form-control p-selection">
                                        <option value="1">New</option>
                                        <option value="2">Urgent</option>
                                        <option value="3">In Progress</option>
                                        <option value="4">Complete</option>
                                        <option value="6">Pending</option>
                                        <option value="5">Archive</option>
                                    </select>
                                </p>
                                <p class="form-group">
                                    <label for="p_custom_category">Time Estimate</label>
                                    <input type="text" name="p_estimate" id="p_estimate" class="form-control">
                                </p>
                                <p class="form-group">
                                    <label for="p_members">Select Team Members <small>(Multi-Select)</small></label>
                                    <select
                                        name="p_members[]"
                                        id="p_members"
                                        class="form-control p-selection"
                                        multiple>
                                    </select>
                                </p>
                                <p class="form-group">
                                    <label for="p_contract_info">Contract Info</label>
                                    <textarea name="p_contract_info" id="p_contract_info" class="form-control" rows="5"></textarea>
                                </p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" id="addProjectClose" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" id="addProjectSubmit" class="btn btn-primary disabled" disabled>Create Project</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- New Note Modal -->
            <div class="modal fade" id="addNote" tabindex="-1" role="dialog"
                 aria-labelledby="addNoteLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="addNoteLabel">
                                Add Note
                            </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form id="addNoteForm">
                            <input type="hidden" name="clientid" id="n_cid" value="">
                            <div class="modal-body">
                                <p class="form-group">
                                    <label for="n_title">Title <small>(Required. Min 4 chars)</small></label>
                                    <input type="text" name="n_title" id="n_title" class="form-control n-input">
                                </p>
                                <p class="form-group">
                                    <label for="n_link">Link</label>
                                    <input type="text" name="n_link" id="n_link" class="form-control n-input">
                                </p>
                                <p class="form-group">
                                    <label for="n_username">Username</label>
                                    <input type="text" name="n_username" id="n_username" class="form-control n-input">
                                </p>
                                <p class="form-group">
                                    <label for="n_password">Password</label>
                                    <input type="text" name="n_password" id="n_password" class="form-control n-input">
                                </p>
                                <p class="form-group">
                                    <label for="n_notes">Notes</label>
                                    <textarea name="n_notes" id="n_notes" class="form-control" rows="5"></textarea>
                                </p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" id="addNoteClose" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" id="addNoteSubmit" class="btn btn-primary disabled" disabled>Create Note</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Edit Note Modal -->
            <div class="modal fade" id="editNote" tabindex="-1" role="dialog"
                 aria-labelledby="editNoteLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="editNoteLabel">
                                Edit Note
                            </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form id="editNoteForm">
                            <input type="hidden" name="noteid" id="n_id" value="">
                            <div class="modal-body">
                                <p class="form-group">
                                    <label for="en_title">Title <small>(Required. Min 4 chars)</small></label>
                                    <input type="text" name="n_title" id="en_title" class="form-control en-input">
                                </p>
                                <p class="form-group">
                                    <label for="en_link">Link</label>
                                    <input type="text" name="n_link" id="en_link" class="form-control en-input">
                                </p>
                                <p class="form-group">
                                    <label for="en_username">Username</label>
                                    <input type="text" name="n_username" id="en_username" class="form-control en-input">
                                </p>
                                <p class="form-group">
                                    <label for="en_password">Password</label>
                                    <input type="text" name="n_password" id="en_password" class="form-control en-input">
                                </p>
                                <p class="form-group">
                                    <label for="en_notes">Notes</label>
                                    <textarea name="n_notes" id="en_notes" class="form-control" rows="5"></textarea>
                                </p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" id="editNoteClose" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" id="editNoteSubmit" class="btn btn-primary">Save Note</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- New Contact Modal -->
            <div class="modal fade" id="addContact" tabindex="-1" role="dialog"
                 aria-labelledby="addContactLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="addContactLabel">
                                Add Contact
                            </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form id="addContactForm">
                            <input type="hidden" name="clientid" id="c_cid" value="">
                            <div class="modal-body">
                                <p class="form-group">
                                    <label for="c_firstname">First Name</label>
                                    <input
                                        type="text"
                                        name="c_firstname"
                                        id="c_firstname"
                                        class="form-control c-input"
                                        onchange="enableAddContactSubmit();"
                                    >
                                </p>
                                <p class="form-group">
                                    <label for="c_lastname">Last Name</label>
                                    <input type="text" name="c_lastname" id="c_lastname" class="form-control c-input">
                                </p>
                                <p class="form-group">
                                    <label for="c_title">Title</label>
                                    <input type="text" name="c_title" id="c_title" class="form-control c-input">
                                </p>
                                <h5 class="full-width">
                                    <strong>Email(s)</strong>
                                    <div class="pull-right" id="editAddEmail">
                                        <span class="label label-info">
                                            <i class="fa fa-plus-square margin-right-5"></i> Add Email
                                        </span>
                                    </div>
                                    <hr>
                                </h5>
                                <div id="addEmailsGroup">
                                    <p class="form-group input-group relative">
                                        <span class="input-group-addon">
                                            <i class="fas fa-envelope" title="Label"></i>
                                        </span>
                                        <input type="text" name="c_emails[]" class="form-control c-input ce-email" />
                                        <span class="input-group-btn">
                                            <button class="btn btn-danger modal-email-delete" type="button">Delete</button>
                                        </span>
                                    </p>
                                </div>
                                <p class="form-group">
                                    <label for="c_address">Address</label>
                                    <input type="text" name="c_address" id="c_address" class="form-control c-input">
                                </p>
                                <p class="form-group">
                                    <label for="c_city">City</label>
                                    <input type="text" name="c_city" id="c_city" class="form-control c-input">
                                </p>
                                <p class="form-group">
                                    <label for="c_state">State</label>
                                    <input type="text" name="c_state" id="c_state" class="form-control c-input">
                                </p>
                                <p class="form-group">
                                    <label for="c_zip">Zip</label>
                                    <input type="text" name="c_zip" id="c_zip" class="form-control c-input">
                                </p>
                                <h5 class="full-width">
                                    <strong>Phone(s)</strong>
                                    <div class="pull-right" id="editAddPhone">
                                        <span class="label label-info">
                                            <i class="fa fa-plus-square margin-right-5"></i> Add Phone
                                        </span>
                                    </div>
                                    <hr>
                                </h5>
                                <div id="addPhonesGroup">
                                    <p class="form-group input-group relative">
                                        <span class="input-group-addon"><i class="fas fa-address-book" title="Label"></i></span>
                                        <input type="text" name="c_phonelabels[]" class="form-control c-input ce-phonelabel">
                                        <span class="input-group-btn">
                                            <button class="btn btn-danger modal-phone-delete" type="button">Delete</button>
                                        </span>
                                    </p>
                                    <p class="form-group input-group relative">
                                        <span class="input-group-addon"><i class="fas fa-phone-square" title="Number"></i></span>
                                        <input type="text" name="c_phones[]" class="form-control c-input ce-phone">
                                    </p>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" id="addContactClose" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" id="addContactSubmit" class="btn btn-primary disabled" disabled>Create Contact</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Edit Contact Modal -->
            <div class="modal fade" id="editContact" tabindex="-1" role="dialog"
                 aria-labelledby="editContactLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="editContactLabel">
                                Edit Contact
                            </h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <form id="editContactForm">
                            <input type="hidden" name="clientid" id="ce_cid" value="">
                            <input type="hidden" name="contactid" id="ce_id" value="">
                            <div class="modal-body">
                                <p class="form-group">
                                    <label for="ce_firstname">First Name</label>
                                    <input type="text" name="ce_firstname" id="ce_firstname" class="form-control c-input">
                                </p>
                                <p class="form-group">
                                    <label for="ce_lastname">Last Name</label>
                                    <input type="text" name="ce_lastname" id="ce_lastname" class="form-control c-input">
                                </p>
                                <p class="form-group">
                                    <label for="ce_title">Title</label>
                                    <input type="text" name="ce_title" id="ce_title" class="form-control c-input">
                                </p>
                                <h5 class="full-width">
                                    <strong>Email(s)</strong>
                                    <div class="pull-right" id="contactAddEmail">
                                        <span class="label label-info">
                                            <i class="fa fa-plus-square margin-right-5"></i> Add Email
                                        </span>
                                    </div>
                                    <hr>
                                </h5>
                                <div id="editEmailsGroup"></div>
                                <hr>
                                <p class="form-group">
                                    <label for="ce_address">Address</label>
                                    <input type="text" name="ce_address" id="ce_address" class="form-control c-input">
                                </p>
                                <p class="form-group">
                                    <label for="ce_city">City</label>
                                    <input type="text" name="ce_city" id="ce_city" class="form-control c-input">
                                </p>
                                <p class="form-group">
                                    <label for="ce_state">State</label>
                                    <input type="text" name="ce_state" id="ce_state" class="form-control c-input">
                                </p>
                                <p class="form-group">
                                    <label for="ce_zip">Zip</label>
                                    <input type="text" name="ce_zip" id="ce_zip" class="form-control c-input">
                                </p>
                                <h5 class="full-width">
                                    <strong>Phone(s)</strong>
                                    <div class="pull-right" id="contactAddPhone">
                                        <span class="label label-info">
                                            <i class="fa fa-plus-square margin-right-5"></i> Add Phone
                                        </span>
                                    </div>
                                    <hr>
                                </h5>
                                <div id="editPhonesGroup"></div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" id="editContactClose" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" id="editContactSubmit" class="btn btn-primary">Save Contact</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <script>
                // Get the post client id
                var getCid = '<?php echo ((isset($_POST['cid']) && $_POST['cid'] != '') ? $_POST['cid'] : "none"); ?>';
                // Get the post content id
                var getContentId = '<?php echo ((isset($_POST['coid']) && $_POST['coid'] != '') ? $_POST['coid'] : "none"); ?>';
                function subDomain(url) {
                    // IF THERE, REMOVE WHITE SPACE FROM BOTH ENDS
                    url = url.replace(new RegExp(/^\s+/), ""); // START
                    url = url.replace(new RegExp(/\s+$/), ""); // END
                    // IF FOUND, CONVERT BACK SLASHES TO FORWARD SLASHES
                    url = url.replace(new RegExp(/\\/g), "/");
                    // IF THERE, REMOVES 'http://', 'https://' or 'ftp://' FROM THE START
                    url = url.replace(new RegExp(/^http\:\/\/|^https\:\/\/|^ftp\:\/\//i), "");
                    // IF THERE, REMOVES 'www.' FROM THE START OF THE STRING
                    url = url.replace(new RegExp(/^www\./i), "");
                    // REMOVE COMPLETE STRING FROM FIRST FORWARD SLASH ON
                    url = url.replace(new RegExp(/\/(.*)/), "");
                    // REMOVES '.??.??' OR '.???.??' FROM END - e.g. '.CO.UK', '.COM.AU'
                    if (url.match(new RegExp(/\.[a-z]{2,3}\.[a-z]{2}$/i))) {
                        url = url.replace(new RegExp(/\.[a-z]{2,3}\.[a-z]{2}$/i), "");
                    // REMOVES '.??' or '.???' or '.????' FROM END - e.g. '.US', '.COM', '.INFO'
                    } else if (url.match(new RegExp(/\.[a-z]{2,4}$/i))) {
                        url = url.replace(new RegExp(/\.[a-z]{2,4}$/i), "");
                    }
                    // CHECK TO SEE IF THERE IS A DOT '.' LEFT IN THE STRING
                    var subDomain = (url.match(new RegExp(/\./g))) ? true : false;
                    return(subDomain);
                }
                function nl2br(str, is_xhtml) {
                    if (typeof str === 'undefined' || str === null) {
                        return '';
                    }
                    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
                    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
                }
                function scrollToView(area) {
                    // ScrollTo for mobile view
                    if ($(window).innerWidth() < 1024) {
                        $('html, body').animate({
                            scrollTop: ($("#" + area).offset().top - 80)
                        }, 1000);
                    }
                }
                function deleteClientDomain(id) {
                    swal({
                        title: "Are you sure?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-success",
                        confirmButtonText: "Yes",
                        cancelButtonText: "Cancel",
                        closeOnConfirm: true,
                        closeOnCancel: true
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            var url = "<?php echo base_url(); ?>clients/deleteDomain/" + id;
                            $.post(url).done(function(data) {
                                var result = JSON.parse(data);
                                if (result.msg == 'success') {
                                    var curDomains = $("#load-domains-data .metric p .number").html();
                                    $("#load-domains-data .metric p .number").html(curDomains-1);
                                    $(".well_" + id).remove();
                                }
                                if (result.msg == 'failure') swal("Domain Not Deleted!");
                            });
                        }
                    });
                }
                function deleteClientProject(id) {
                    swal({
                        title: "Are you sure?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-success",
                        confirmButtonText: "Yes",
                        cancelButtonText: "Cancel",
                        closeOnConfirm: true,
                        closeOnCancel: true
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            var url = "<?php echo base_url(); ?>clients/deleteProject/" + id;
                            $.post(url).done(function(data) {
                                var result = JSON.parse(data);
                                if (result.msg == 'success') {
                                    var curProjects = $("#load-projects-data .metric p .number").html();
                                    $("#load-projects-data .metric p .number").html(curProjects-1);
                                    $(".well_" + id).remove();
                                }
                                if (result.msg == 'failure') swal("Project Not Deleted!");
                            });
                        }
                    });
                }
                function deleteClientNote(id) {
                    swal({
                        title: "Are you sure?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-success",
                        confirmButtonText: "Yes",
                        cancelButtonText: "Cancel",
                        closeOnConfirm: true,
                        closeOnCancel: true
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            var url = "<?php echo base_url(); ?>clients/deleteNote/" + id;
                            $.post(url).done(function(data) {
                                var result = JSON.parse(data);
                                if (result.msg == 'success') {
                                    var curNotes = $("#load-notes-data .metric p .number").html();
                                    $("#load-notes-data .metric p .number").html(curNotes-1);
                                    $(".well_" + id).remove();
                                }
                                if (result.msg == 'failure') swal("Note Not Deleted!");
                            });
                        }
                    });
                }
                function deleteClientContact(id) {
                    swal({
                        title: "Are you sure?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-success",
                        confirmButtonText: "Yes",
                        cancelButtonText: "Cancel",
                        closeOnConfirm: true,
                        closeOnCancel: true
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            var url = "<?php echo base_url(); ?>clients/deleteContact/" + id;
                            $.post(url).done(function(data) {
                                var result = JSON.parse(data);
                                if (result.msg == 'success') {
                                    var curContacts = $("#load-contacts-data .metric p .number").html();
                                    $("#load-contacts-data .metric p .number").html(curContacts-1);
                                    $(".well_" + id).remove();
                                }
                                if (result.msg == 'failure') swal("Contact Not Deleted!");
                            });
                        } else {
                            swal("Code Error Deleting Contact!");
                        }
                    });
                }
                function deleteClientService(id) {
                    swal({
                        title: "Are you sure?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-success",
                        confirmButtonText: "Yes",
                        cancelButtonText: "Cancel",
                        closeOnConfirm: true,
                        closeOnCancel: true
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            var url = "<?php echo base_url(); ?>clients/deleteService/" + id;
                            $.post(url).done(function(data) {
                                var result = JSON.parse(data);
                                if (result.msg == 'success') {
                                    var curServices = $("#load-services-data .metric p .number").html();
                                    $("#load-services-data .metric p .number").html(curServices-1);
                                    $(".well_" + id).remove();
                                }
                                if (result.msg == 'failure') swal("Service Not Deleted!");
                            });
                        }
                    });
                }
                function deleteClient(id) {
                    swal({
                        title: "Are you sure?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-success",
                        confirmButtonText: "Yes",
                        cancelButtonText: "Cancel",
                        closeOnConfirm: true,
                        closeOnCancel: true
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            var url = "<?php echo base_url(); ?>clients/delete/" + id;
                            $.post(url).done(function(data) {
                                window.location = window.location;
                            });
                        }
                    });
                }
                function deactivateClient(id) {
                    alert("Deactivate Client " + id);
                }
                function activateClient(id) {
                    alert("Activate Client " + id);
                }
                function addClientProject(cid) {
                    $("#p_cid").val(cid);
                    $.ajax({
                        url: '/clients/getClientDomains/' + cid,
                        type: 'GET',
                        success: function(response) {
                            var results = JSON.parse(response);
                            for (var i = 0; i < results.length; i++) {
                                $("#p_domain").append(
                                    '<option value="' + results[i].id + '">' +
                                    results[i].domain +
                                    '</option>'
                                );
                            }
                        }
                    });
                    $.ajax({
                        url: '/users/getTeamMembers/',
                        type: 'GET',
                        success: function(response) {
                            var results = JSON.parse(response);
                            for (var i = 0; i < results.length; i++) {
                                $("#p_members").append(
                                    '<option value="' + results[i].id + '">' +
                                    results[i].first_name + ' ' + results[i].last_name + ' (' +
                                    results[i].email + ')' +
                                    '</option>'
                                );
                            }
                        }
                    });
                    $("#addProject").modal("show");
                }
                function addClientNote(cid) {
                    $("#n_cid").val(cid);
                    $("#addNote").modal("show");
                }
                function editClientNote(id) {
                    $("#n_id").val(id);
                    $.ajax({
                        url: '/clients/loadSingleNoteData/' + id,
                        type: 'GET',
                        success: function(response) {
                            var results = JSON.parse(response);
                            $("#en_title").val(results.note_name);
                            $("#en_link").val(results.url);
                            $("#en_username").val(results.username);
                            $("#en_password").val(results.password);
                            $("#en_notes").val(results.notes);
                        }
                    });
                    $("#editNote").modal("show");
                }
                function addClientContact(cid) {
                    $("#c_cid").val(cid);
                    $("#addContact").modal("show");
                    setModalEmailDeleteOnClick();
                    setModalPhoneDeleteOnClick();
                }
                function editClientContact(id, cid) {
                    $("#ce_cid").val(cid);
                    $("#ce_id").val(id);
                    $.ajax({
                        url: '/clients/loadSingleContactData/' + id,
                        type: 'GET',
                        success: function(response) {
                            var results = JSON.parse(response);
                            $("#ce_firstname").val(results.first_name);
                            $("#ce_lastname").val(results.last_name);
                            $("#ce_title").val(results.title);
                            $("#ce_address").val(results.address);
                            $("#ce_city").val(results.city);
                            $("#ce_state").val(results.state);
                            $("#ce_zip").val(results.zip);
                            // Lets handle the emails sting as an array
                            var emails = results.emails.replace("[","").replace("]","").split(",");
                            var emailsHTML = "";
                            function emailsModalFunction(value, index, array) {
                                emailsHTML = emailsHTML +
                                    '<p class="form-group input-group relative">\n' +
                                    '    <span class="input-group-addon">\n' +
                                    '        <a href="mailto:' + value.replace('"','') + '" target="_blank">\n' +
                                    '            <i class="fas fa-envelope" title="Label"></i></a>\n' +
                                    '    </span>\n' +
                                    '    <input type="text" name="ce_emails[]" class="form-control c-input ce-email"' +
                                    ' value="' + value.replace('"','') + '">\n' +
                                    '    <span class="input-group-btn">\n' +
                                    '        <button class="btn btn-danger modal-email-delete" type="button">Delete</button>\n' +
                                    '    </span>\n' +
                                    '</p>';
                            }
                            emails.forEach(emailsModalFunction);
                            $("#editEmailsGroup").html(emailsHTML);
                            // Lets handle the phones sting as an array {"number":"9049981935","label":"Work"}
                            // WORK ON SPLITTING AND REJOINING PHONES ARRAY
                            var phones = results.phones.replace("[","").replace("]","").split(",").reverse();
                            var i;
                            for (i = 0; i < phones.length; i++) {
                                var phonesHTML = '';
                                var valSplit = phones[i].replace("{","").replace("}","").replace(/"/g,'').split(":");
                                if (valSplit[0] == 'label') {
                                    phonesHTML = phonesHTML +
                                        '<p class="form-group input-group relative">\n' +
                                        '    <span class="input-group-addon"><i class="fas fa-address-book" title="Label"></i></span>\n' +
                                        '    <input type="text" name="ce_phonelabels[]" class="form-control c-input ce-phonelabel"\n' +
                                        '        value="' + valSplit[1] + '">\n' +
                                        '    <span class="input-group-btn">\n' +
                                        '        <button class="btn btn-danger modal-phone-delete" type="button">Delete</button>\n' +
                                        '    </span>\n' +
                                        '</p>';
                                }
                                if (valSplit[0] == 'number') {
                                    phonesHTML = phonesHTML +
                                        '<p class="form-group input-group relative">\n' +
                                        '    <span class="input-group-addon">\n' +
                                        '        <a href="tel:' + valSplit[1] + '" target="_blank">\n' +
                                        '            <i class="fas fa-phone-square" title="Label"></i></a>\n' +
                                        '    </span>\n' +
                                        '    <input type="text" name="ce_phones[]" class="form-control c-input ce-phone"\n' +
                                        ' value="' + valSplit[1] + '">\n' +
                                        '</p>';
                                }
                                $("#editPhonesGroup").append(phonesHTML);
                            }
                            setModalEmailDeleteOnClick();
                            setModalPhoneDeleteOnClick();
                        }
                    });
                    $("#editContact").modal("show");
                }
                function setModalEmailDeleteOnClick() {
                    $(".modal-email-delete").each(function() {
                        $(this).click(function() {
                            var $parent = $(this).parent().parent();
                            swal({
                                title: "Are you Sure About This?\n\n>> Be sure to save your changes! <<",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonClass: "btn-success",
                                confirmButtonText: "Yes",
                                cancelButtonText: "Cancel",
                                closeOnConfirm: true,
                                closeOnCancel: true
                            },
                            function(isConfirm) {
                                if (isConfirm) {
                                    $parent.remove();
                                }
                            });
                        });
                    });
                }
                function setModalPhoneDeleteOnClick() {
                    $(".modal-phone-delete").each(function() {
                        $(this).click(function() {
                            var $next = $(this).parent().parent().next();
                            var $parent = $(this).parent().parent();
                            swal({
                                title: "Are you Sure About This?\n\n>> Be sure to save your changes! <<",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonClass: "btn-success",
                                confirmButtonText: "Yes",
                                cancelButtonText: "Cancel",
                                closeOnConfirm: true,
                                closeOnCancel: true
                            },
                            function(isConfirm) {
                                if (isConfirm) {
                                    $next.remove();
                                    $parent.remove();
                                }
                            });
                        });
                    });
                };
                // Check all project modal selects for enabling ssave button
                function checkAllAPSelects() {
                    var count = 0;
                    $('.p-selection').each(function() {
                        if ($(this).val() != '') {
                            count++;
                        }
                        if ($(this).attr('id') == 'p_category' && $(this).val() == '-1') {
                            $("#p-custom-category").removeClass("hidden");
                        }
                        if ($(this).attr('id') == 'p_category' && $(this).val() != '-1') {
                            $("#p-custom-category").addClass("hidden");
                        }
                    });
                    if (count > 2) {
                        $("#addProjectSubmit").removeClass("disabled").removeAttr("disabled");
                    } else {
                        return false;
                    }
                }
                function viewProject(pid) {
                    let redirectForm = $(
                        '<form action="/admin/projects/" method="post">' +
                        '<input type="hidden" name="pid" value="' + pid + '" />' +
                        '<input type="hidden" name="coid" value="1" />' +
                        '</form>'
                    );
                    $('body').append(redirectForm);
                    redirectForm.submit();
                }
                function returnToProject(pid) {
                    let redirectForm = $(
                        '<form action="/admin/projects/" method="post">' +
                        '<input type="hidden" name="pid" value="' + pid + '" />' +
                        '<input type="hidden" name="coid" value="1" />' +
                        '</form>'
                    );
                    $('body').append(redirectForm);
                    redirectForm.submit();
                }
                $(document).ready(function() {
                    $('body').on('click', function (e) {
                        if (!$("#editContact").hasClass("in")) {
                            $("#ec_firstname").val("");
                            $("#ec_lastname").val("");
                            $("#ec_title").val("");
                            $("#ec_address").val("");
                            $("#ec_city").val("");
                            $("#ec_state").val("");
                            $("#ec_zip").val("");
                            $("#editEmailsGroup").html("");
                            $("#editPhonesGroup").html("");
                        }
                    });
                    $("#n_title").on('keyup', function() {
                        if ($(this).val().length > 3) {
                            $("#addNoteSubmit").removeClass("disabled").removeAttr("disabled");
                        } else {
                            $("#addNoteSubmit").addClass("disabled").attr("disabled", "disabled");
                        }
                    });
                    $("#en_title").on('keyup', function() {
                        if ($(this).val().length > 3) {
                            $("#addNoteSubmit").removeClass("disabled").removeAttr("disabled");
                        } else {
                            $("#addNoteSubmit").addClass("disabled").attr("disabled", "disabled");
                        }
                    });
                    $(".p-selection").each(function() {
                        $(this).on('change', function() {
                            checkAllAPSelects();
                        });
                    });
                    $('.panel-heading').click(false);
                    $("#addProjectClose").on('click', function() {
                        $("#p_cid").val("");
                        $("#p_custom_category").val("");
                        $("#p_estimate").val("");
                        $("#p_contract_info").val("");
                        $("#p-custom-category").addClass("hidden");
                        $(".p-selection").each(function() { $(this).prop('selectedIndex', 0) });
                        $("#p_members").prop('selectedIndex', -1);
                        $("#p_domain").html('<option value="">Select</option>');
                    });
                    $("#addProjectSubmit").on('click', function() {
                        var cid = $("#p_cid").val();
                        var formData = $("#addProjectForm").serialize();
                        $.ajax({
                            url: '/projects/add/',
                            type: 'POST',
                            data: formData,
                            success: function(response) {
                                var redirectForm = $(
                                    '<form action="/admin/clients/" method="post">' +
                                    '<input type="hidden" name="cid" value="' + cid + '" />' +
                                    '<input type="hidden" name="coid" value="3" />' +
                                    '</form>'
                                );
                                $('body').append(redirectForm);
                                redirectForm.submit();
                            }
                        });
                    });
                    $("#addNoteClose").on('click', function() {
                        $("#n_cid").val("");
                        $("#n_title").val("");
                        $("#n_link").val("");
                        $("#n_username").val("");
                        $("#n_password").val("");
                        $("#n_notes").val("");
                    });
                    $("#addNoteSubmit").on('click', function() {
                        var cid = $("#n_cid").val();
                        var formData = $("#addNoteForm").serialize();
                        $.ajax({
                            url: '/clients/addNote/',
                            type: 'POST',
                            data: formData,
                            success: function(response) {
                                var redirectForm = $(
                                    '<form action="/admin/clients/" method="post">' +
                                    '<input type="hidden" name="cid" value="' + cid + '" />' +
                                    '<input type="hidden" name="coid" value="4" />' +
                                    '</form>'
                                );
                                $('body').append(redirectForm);
                                redirectForm.submit();
                            }
                        });
                    });
                    $("#editNoteClose").on('click', function() {
                        $("#n_id").val("");
                        $("#en_title").val("");
                        $("#en_link").val("");
                        $("#en_username").val("");
                        $("#en_password").val("");
                        $("#en_notes").val("");
                    });
                    $("#editNoteSubmit").on('click', function() {
                        var id = $("#n_id").val();
                        var formData = $("#editNoteForm").serialize();
                        $.ajax({
                            url: '/clients/editNote/' + id,
                            type: 'POST',
                            data: formData,
                            success: function(response) {
                                var redirectForm = $(
                                    '<form action="/admin/clients/" method="post">' +
                                    '<input type="hidden" name="cid" value="' + $('#load-notes-data').attr('data-id') + '" />' +
                                    '<input type="hidden" name="coid" value="4" />' +
                                    '</form>'
                                );
                                $('body').append(redirectForm);
                                redirectForm.submit();
                            }
                        });
                    });
                    $("#addContactClose").on('click', function() {
                        $("#c_cid").val("");
                        $("#c_firstname").val("");
                        $("#c_lastname").val("");
                        $("#c_title").val("");
                        $("#c_address").val("");
                        $("#c_city").val("");
                        $("#c_state").val("");
                        $("#c_zip").val("");
                        $(".c-email").each(function() { $(this).val(""); });
                        $(".c-phonelabel").each(function() { $(this).val(""); });
                        $(".c-phone").each(function() { $(this).val(""); });
                    });
                    $("#addContactSubmit").on('click', function() {
                        var cid = $("#c_cid").val();
                        var formData = $("#addContactForm").serialize();
                        $.ajax({
                            url: '/clients/addContact/',
                            type: 'POST',
                            data: formData,
                            success: function(response) {
                                var redirectForm = $(
                                    '<form action="/admin/clients/" method="post">' +
                                    '<input type="hidden" name="cid" value="' + cid + '" />' +
                                    '<input type="hidden" name="coid" value="5" />' +
                                    '</form>'
                                );
                                $('body').append(redirectForm);
                                redirectForm.submit();
                            }
                        });
                    });
                    $("#editContactClose").on('click', function() {
                        $("#ce_id").val("");
                        $("#ec_firstname").val("");
                        $("#ec_lastname").val("");
                        $("#ec_title").val("");
                        $("#ec_address").val("");
                        $("#ec_city").val("");
                        $("#ec_state").val("");
                        $("#ec_zip").val("");
                        $("#editEmailsGroup").html("");
                        $("#editPhonesGroup").html("");
                    });
                    $("#editContactSubmit").on('click', function() {
                        var cid = $("#ce_cid").val();
                        var id = $("#ce_id").val();
                        var formData = $("#editContactForm").serialize();
                        $.ajax({
                            url: '/clients/editContact/' + id,
                            type: 'POST',
                            data: formData,
                            success: function(response) {
                                var redirectForm = $(
                                    '<form action="/admin/clients/" method="post">' +
                                    '<input type="hidden" name="cid" value="' + cid + '" />' +
                                    '<input type="hidden" name="coid" value="5" />' +
                                    '</form>'
                                );
                                $('body').append(redirectForm);
                                redirectForm.submit();
                            }
                        });
                    });
                    $("#contactAddEmail").on('click', function() {
                        $("#editEmailsGroup").append(
                            '<p class="form-group input-group relative">\n' +
                            '    <span class="input-group-addon"><i class="fas fa-envelope" title="Email"></i></span>\n' +
                            '    <input type="text" name="ce_emails[]" class="form-control c-input ce-email">\n' +
                            '    <span class="input-group-btn">\n' +
                            '        <button class="btn btn-danger modal-email-delete" type="button">Delete</button>\n' +
                            '    </span>\n' +
                            '</p>'
                        );
                        setModalEmailDeleteOnClick();
                    });
                    $("#editAddEmail").on('click', function() {
                        $("#addEmailsGroup").append(
                            '<p class="form-group input-group relative">\n' +
                            '    <span class="input-group-addon"><i class="fas fa-envelope" title="Email"></i></span>\n' +
                            '    <input type="text" name="ce_emails[]" class="form-control c-input ce-email">\n' +
                            '    <span class="input-group-btn">\n' +
                            '        <button class="btn btn-danger modal-email-delete" type="button">Delete</button>\n' +
                            '    </span>\n' +
                            '</p>'
                        );
                        setModalEmailDeleteOnClick();
                    });
                    $("#contactAddPhone").on('click', function() {
                        $("#editPhonesGroup").append(
                            '<p class="form-group input-group relative">\n' +
                            '    <span class="input-group-addon"><i class="fas fa-address-book" title="Label"></i></span>\n' +
                            '    <input type="text" name="ce_phonelabels[]" class="form-control c-input ce-phonelabel">\n' +
                            '    <span class="input-group-btn">\n' +
                            '        <button class="btn btn-danger modal-phone-delete" type="button">Delete</button>\n' +
                            '    </span>\n' +
                            '</p>\n' +
                            '<p class="form-group input-group relative">\n' +
                            '    <span class="input-group-addon"><i class="fas fa-phone-square" title="Number"></i></span>\n' +
                            '    <input type="text" name="ce_phones[]" class="form-control c-input ce-phone">\n' +
                            '</p>'
                        );
                        setModalPhoneDeleteOnClick();
                    });
                    $("#editAddPhone").on('click', function() {
                        $("#addPhonesGroup").append(
                            '<p class="form-group input-group relative">\n' +
                            '    <span class="input-group-addon"><i class="fas fa-address-book" title="Label"></i></span>\n' +
                            '    <input type="text" name="ce_phonelabels[]" class="form-control c-input ce-phonelabel">\n' +
                            '    <span class="input-group-btn">\n' +
                            '        <button class="btn btn-danger modal-phone-delete" type="button">Delete</button>\n' +
                            '    </span>\n' +
                            '</p>\n' +
                            '<p class="form-group input-group relative">\n' +
                            '    <span class="input-group-addon"><i class="fas fa-phone-square" title="Number"></i></span>\n' +
                            '    <input type="text" name="ce_phones[]" class="form-control c-input ce-phone">\n' +
                            '</p>'
                        );
                        setModalPhoneDeleteOnClick();
                    });
                    $("#c_firstname").on('keyup blur', function(){
                        if ($(this).val().length > 2) {
                            $("#addContactSubmit").removeClass("disabled").removeAttr("disabled");
                        } else {
                            $("#addContactSubmit").addClass("disabled").attr("disabled", "disabled");
                        }
                    });
                    $(".clearable").each(function() {
                        var $inp = $(this).find("input:text"),
                            $cle = $(this).find(".clearable__clear");
                        $inp.on("input", function(){
                            $cle.toggle(!!this.value);
                        });
                        $cle.on("touchstart click", function(e) {
                            e.preventDefault();
                            $inp.val("").trigger("input");
                            $(".col-sm-4.col-lg-3 .load-clients-data").each(function() {
                                $(this).show();
                            });
                            $('.client-panel-title').html("Select a Client");
                            $('.client-panel-username').html("");
                            $('.client-panel-timeblock').html("");
                            $(".client-panel-body").hide();
                            $inp.focus();
                        });
                    });
                    $('#filterClients').bind("change keyup input cut", function() {
                        if ($('#filterClients').val() != '') {
                            var search = $('#filterClients').val();
                            var show = 0;
                            var hide = 0;
                            $(".col-sm-4.col-lg-3 .load-clients-data").each(function() {
                                var clientsName = $(this).html();
                                if (clientsName.toLowerCase().indexOf(search.toLowerCase()) === -1) {
                                    hide++;
                                    $(this).hide();
                                } else {
                                    show++;
                                    $(this).show();
                                }
                            });
                            if (show > 0 && show < 2) {
                                $('.client-panel-body .metric').css("color", "#3287B2").css("background-color", "#ffffff");
                                var showid = $(".col-sm-4.col-lg-3 .load-clients-data:visible").attr("data-id");
                                // set the data-id values around the main content
                                $('#load-projects-data').attr('data-id', showid);
                                $('#load-notes-data').attr('data-id', showid);
                                $('#load-contacts-data').attr('data-id', showid);
                                $('#load-domains-data').attr('data-id', showid);
                                $('#load-services-data').attr('data-id', showid);
                                $('.client-panel-body').show();
                                $('#clientcontent').html(
                                    '<div class="overlay" style="text-align:center;">\n' +
                                    '<img class="loading" src="/assets/img/loader.gif" alt="Loading" /><br/>\n' +
                                    '<h2>Loading the Clients Data...</h2>\n' +
                                    '</div>'
                                );
                                var url = "<?php echo base_url(); ?>clients/loadClientData/" + showid;
                                $.post(url).done(function(data) {
                                    var result = JSON.parse(data);
                                    $('.client-panel-title').html(result.client_name != 'undefined' ? '(' + result.id + ') ' +
                                        result.client_name : '-- None --');
                                    $('.client-panel-username').html(
                                        'Username:&nbsp; <strong>' +
                                        (result.username != 'undefined' ? result.username : '-- None --') +
                                        '</strong>'
                                    );
                                    $('.client-panel-timeblock').html(
                                        'Timeblock:&nbsp; <strong>' +
                                        (result.is_time_blocked > 0 ? 'Yes' : 'No') +
                                        '</strong>' +
                                        (
                                            result.is_time_blocked == 1 && result.time_remaining > 0 ?
                                            '&nbsp; Time Left:&nbsp; <strong>' + result.time_remaining + '</strong>' :
                                            '&nbsp; Time Left:&nbsp; <strong>None</strong>'
                                        )
                                    );
                                    $("#load-projects-data .number").html(result.p_cnt);
                                    $("#load-notes-data .number").html(result.n_cnt);
                                    $("#load-contacts-data .number").html(result.c_cnt);
                                    $("#load-domains-data .number").html(result.d_cnt);
                                    $("#load-services-data .number").html(result.s_cnt);
                                    $('#clientcontent').html("");
                                    // Load Services by default ////////////////////////////////////////////////////////
                                    $('.client-panel-body .metric').css("color", "#3287B2").css("background-color", "#ffffff");
                                    $('#load-services-data .metric').css("color", "#ffffff").css("background-color", "#2B333E");
                                    $('#clientcontent').html(
                                        '<div class="overlay" style="text-align:center;">\n' +
                                        '<img class="loading" src="/assets/img/loader.gif" alt="Loading" /><br/>\n' +
                                        '<h2>Loading the Services Data...</h2>\n' +
                                        '</div>'
                                    );
                                    var url = "<?php echo base_url(); ?>clients/loadServicesData/" + showid;
                                    $.post(url).done(function(data) {
                                        $('#clientcontent').html("");
                                        var result = JSON.parse(data);
                                        $('#clientcontent').append(
                                            '<div class="row hidden margin-top-5"></div>'
                                        );
                                        if (result.length > 0) {
                                            for (let i = 0; i < result.length; i++) {
                                                if (result[i].service_name !== 'undefined') {
                                                    $('#clientcontent .row').append(
                                                        '<div class="col-sm-12">' +
                                                        '<div class="well well_' + result[i].id + ' client-card relative"' +
                                                        ' style="min-height:47px;">' +
                                                        //'<span class="icon">' +
                                                        //'<i class="fa fa-shopping-cart"></i>' +
                                                        //'</span>' +
                                                        '<h4><strong>' +
                                                        //'<a href="javascript:;"' +
                                                        //' onclick="Utils.copyTextToClipboard(\'' +
                                                        //result[i].service_name +
                                                        //'\', this);"><p>' +
                                                        //'<p class="sprite-copy left" title="Copy" style="margin:2px 3px 0 0;">' +
                                                        //'</p></a>' +
                                                        result[i].service_name +
                                                        //' (' + result[i].service_category + ')' +
                                                        '</strong></h4>' +
                                                        '<p class="service-note">' +
                                                        (result[i].service_note !== 'undefined' ? result[i].service_note : '') +
                                                        '</p>' +
                                                        '<i class="fa fa-trash pull-right client-content-well-trash" ' +
                                                        'onclick="deleteClientService(' + result[i].id + ');"></i>' +
                                                        '</div>' +
                                                        '</div>'
                                                    );
                                                }
                                            }
                                        } else {
                                            $('#clientcontent .row').append('<div class="col-sm-12">No Services</div>');
                                        }
                                        $('#clientcontent .row').removeClass("hidden");
                                    });
                                    // Load Services by default ////////////////////////////////////////////////////////
                                    $('#clientcontentadd').html(
                                        '<a href="/admin/clients/edit/' + showid + '" id="modify-services">' +
                                        '    <button class="btn btn-sm btn-info">' +
                                        '        <i class="fa fa-pencil"></i>&nbsp;&nbsp;Modify Services' +
                                        '    </button>' +
                                        '</a>'
                                    );
                                });
                                scrollToView("clientdata");
                                $('#client-edit').attr("href", "/admin/clients/edit/" + showid).removeClass("hide");
                                $('#client-delete').attr("onclick", "deleteClient(" + showid + ");").removeClass("hide");
                                $('#client-deactivate').attr("href", "/admin/clients/deactivate/" + showid).removeClass("hide");
                            }
                        } else {
                            $(".row.clients-main-page .col-sm-8.col-lg-9.margin-bottom-30 .panel-headline .panel-heading .row").hide();
                            $(".col-sm-4.col-lg-3 .load-clients-data").each(function() {
                                $(this).show();
                            });
                            $('#filterClients').focus();
                            $('#client-edit').attr("href", "javascript:;").addClass("hide");
                            $('#client-delete').attr("onclick", "deleteClient();").addClass("hide");
                            $('#client-deactivate').attr("href", "javascript:;").addClass("hide");
                            setTimeout(function() {
                                $('.client-panel-title').html("Select a Client");
                                $('.client-panel-username').html("");
                                $('.client-panel-timeblock').html("");
                                $(".client-panel-body").hide();
                            }, 100);
                            setTimeout(function() {
                                $(".row.clients-main-page .col-sm-8.col-lg-9.margin-bottom-30 .panel-headline .panel-heading .row").show();
                            }, 200);
                        }
                    });
                    $('.load-clients-data').click(function() {
                        $('.client-panel-body .metric').css("color", "#3287B2").css("background-color", "#ffffff");
                        var thisid = $(this).attr("data-id");
                        // set the data-id values around the main content
                        $('#load-projects-data').attr('data-id', thisid);
                        $('#load-notes-data').attr('data-id', thisid);
                        $('#load-contacts-data').attr('data-id', thisid);
                        $('#load-domains-data').attr('data-id', thisid);
                        $('#load-services-data').attr('data-id', thisid);
                        $('.client-panel-body').show();
                        $('#clientcontent').html(
                            '<div class="overlay" style="text-align:center;">\n' +
                            '<img class="loading" src="/assets/img/loader.gif" alt="Loading" /><br/>\n' +
                            '<h2>Loading the Clients Data...</h2>\n' +
                            '</div>'
                        );
                        var url = "<?php echo base_url(); ?>clients/loadClientData/" + thisid;
                        $.post(url).done(function(data) {
                            var result = JSON.parse(data);
                            $('.client-panel-title').html(result.client_name != 'undefined' ? '(' + result.id + ') ' +
                                result.client_name : '-- None --');
                            $("#filterClients").val(result.client_name);
                            $(".clearable__clear").show();
                            $('.client-panel-username').html(
                                'Username:&nbsp; <strong>' +
                                (result.username != 'undefined' ? result.username : '-- None --') +
                                '</strong>'
                            );
                            $('.client-panel-timeblock').html(
                                'Timeblock:&nbsp; <strong>' +
                                (result.is_time_blocked > 0 ? 'Yes' : 'No') +
                                '</strong>' +
                                (
                                    result.is_time_blocked == 1 && result.time_remaining > 0 ?
                                    '&nbsp; Time Left:&nbsp; <strong>' + result.time_remaining + '</strong>' :
                                    ''
                                )
                            );
                            $("#load-projects-data .number").html(result.p_cnt);
                            $("#load-notes-data .number").html(result.n_cnt);
                            $("#load-contacts-data .number").html(result.c_cnt);
                            $("#load-domains-data .number").html(result.d_cnt);
                            $("#load-services-data .number").html(result.s_cnt);
                            $('#clientcontent').html("");
                            // Load Services by default ////////////////////////////////////////////////////////////////
                            $('.client-panel-body .metric').css("color", "#3287B2").css("background-color", "#ffffff");
                            $('#load-services-data .metric').css("color", "#ffffff").css("background-color", "#2B333E");
                            $('#clientcontent').html(
                                '<div class="overlay" style="text-align:center;">\n' +
                                '<img class="loading" src="/assets/img/loader.gif" alt="Loading" /><br/>\n' +
                                '<h2>Loading the Services Data...</h2>\n' +
                                '</div>'
                            );
                            var url = "<?php echo base_url(); ?>clients/loadServicesData/" + thisid;
                            $.post(url).done(function(data) {
                                $('#clientcontent').html("");
                                var result = JSON.parse(data);
                                $('#clientcontent').append(
                                    '<div class="row hidden margin-top-5"></div>'
                                );
                                if (result.length > 0) {
                                    for (let i = 0; i < result.length; i++) {
                                        if (result[i].service_name !== 'undefined') {
                                            $('#clientcontent .row').append(
                                                '<div class="col-sm-12">' +
                                                '<div class="well well_' + result[i].id + ' client-card relative"' +
                                                ' style="min-height:47px;">' +
                                                //'<span class="icon">' +
                                                //'<i class="fa fa-shopping-cart"></i>' +
                                                //'</span>' +
                                                '<h4><strong>' +
                                                //'<a href="javascript:;"' +
                                                //' onclick="Utils.copyTextToClipboard(\'' +
                                                //result[i].service_name +
                                                //'\', this);"><p>' +
                                                //'<p class="sprite-copy left" title="Copy" style="margin:2px 3px 0 0;">' +
                                                //'</p></a>' +
                                                result[i].service_name +
                                                //' (' + result[i].service_category + ')' +
                                                '</strong></h4>' +
                                                '<p class="service-note">' +
                                                (result[i].service_note !== 'undefined' ? result[i].service_note : '') +
                                                '</p>' +
                                                '<i class="fa fa-trash pull-right client-content-well-trash" ' +
                                                'onclick="deleteClientService(' + result[i].id + ');"></i>' +
                                                '</div>' +
                                                '</div>'
                                            );
                                        }
                                    }
                                } else {
                                    $('#clientcontent .row').append('<div class="col-sm-12">No Services</div>');
                                }
                                $('#clientcontent .row').removeClass("hidden");
                            });
                            // Load Services by default ////////////////////////////////////////////////////////////////
                            $('#clientcontentadd').html(
                                '<a href="/admin/clients/edit/' + thisid + '" id="modify-services">' +
                                '    <button class="btn btn-sm btn-info">' +
                                '        <i class="fa fa-pencil"></i>&nbsp;&nbsp;Modify Services' +
                                '    </button>' +
                                '</a>'
                            );
                        });
                        scrollToView("clientdata");
                        $('#client-edit').attr("href", "/admin/clients/edit/" + thisid).removeClass("hide");
                        $('#client-delete').attr("onclick", "deleteClient(" + thisid + ");").removeClass("hide");
                        $('#client-deactivate').attr("href", "/admin/clients/deactivate/" + thisid).removeClass("hide");
                    });
                    $('#load-domains-data').click(function() {
                        $('.client-panel-body .metric').css("color", "#3287B2").css("background-color", "#ffffff");
                        $('#load-domains-data .metric').css("color", "#ffffff").css("background-color", "#2B333E");
                        var thisid = $(this).attr("data-id");
                        $('#clientcontent').html(
                            '<div class="overlay" style="text-align:center;">\n' +
                            '<img class="loading" src="/assets/img/loader.gif" alt="Loading" /><br/>\n' +
                            '<h2>Loading the Domains Data...</h2>\n' +
                            '</div>'
                        );
                        var url = "<?php echo base_url(); ?>clients/loadDomainsData/" + thisid;
                        $.post(url).done(function(data) {
                            var result = JSON.parse(data);
                            $('#clientcontent').append(
                                '<div class="row hidden margin-top-5"></div>'
                            );
                            if (result.length > 0) {
                                $("#load-domains-data .metric .number").html(result.length);
                                var domainsCheck = [];
                                for (let i = 0; i < result.length; i++) {
                                    if (result[i].domain != 'undefined') {
                                        if (result[i].domain.indexOf("https://") !== -1) {
                                            var domain = result[i].domain.replace("https://", "");
                                        } else if (result[i].domain.indexOf("http://") !== -1) {
                                            var domain = result[i].domain.replace("http://", "");
                                        } else {
                                            var domain = result[i].domain;
                                        }
                                        if (subDomain(domain) !== true) {
                                            if (domain.indexOf("www.") !== -1) {
                                                var domain = domain;
                                            } else {
                                                var domain = 'www.' + domain;
                                            }
                                        }
                                        domainsCheck.push(domain);
                                        $('#clientcontent .row').append(
                                            '<div class="col-sm-12">' +
                                            '<div class="well well_' + result[i].id + ' ' +
                                            domain.replace(/\./g, "-") +
                                            ' client-card padding-bottom-15 relative">' +
                                            '<span class="icon">' +
                                            '<i class="fa fa-globe"></i>' +
                                            '</span>' +
                                            '<h4 style="line-height:24px;"><a href="javascript:;" ' +
                                            'onclick="Utils.copyTextToClipboard(\'' + domain + '\', this);"' +
                                            '><div class="sprite-copy left" title="Copy" style="margin-top:5px;">' +
                                            '</div></a><strong><a href="http://' + domain +
                                            '" target="_blank">' + domain + '</a></strong></h4>' +
                                            '<a href="https://dig.whois.com.au/dig/' + domain + '" target="_blank">' +
                                            '<img src="/assets/img/dns.png" title="DNS Check" ' +
                                            'style="margin-top:-2px;width:28px;" /></a>' +
                                            '<span style="color:silver;margin:0 8px;"> | </span>' +
                                            '<a href="https://www.ssllabs.com/ssltest/analyze.html?d=' + domain + '"' +
                                            ' target="_blank"><img src="/assets/img/padlock.png" ' +
                                            'style="margin-top:-2px;width:28px;" title="SSL Check" /></a>' +
                                            '<span style="color:silver;margin:0 8px;"> | </span>' +
                                            '<a class="globprop_' + i + '" href="https://www.whatsmydns.net/#A/' +
                                            domain + '" target="_blank">' +
                                            '<img src="/assets/img/prop.png" style="margin-top:-2px;width:28px;" ' +
                                            'title="Global Propagation" /></a>' +
                                            (result[i].ada != 'undefined' ?
                                            '<span ' +
                                            'style="color:silver;margin:0 8px;"> | </span>' +
                                            '<img src="/assets/img/ada.jpg" style="margin-top:-2px;width:28px;" ' +
                                            'title="ADA Compliance met on: ' + result[i].ada + '" />' : '') +
                                            '<i class="fa fa-trash pull-right client-content-well-trash" ' +
                                            'onclick="deleteClientDomain(' + result[i].id + ');"></i>' +
                                            '</div>' +
                                            '</div>'
                                        );
                                    }
                                }
                                for (let d = 0; d < domainsCheck.length; d++) {
                                    var url = "<?php echo base_url(); ?>clients/loadDomainIpCheck/" + domainsCheck[d];
                                    $.post(url).done(function(data) {
                                        if (data == 'true') { //' + domainsCheck[d].replace(/\./g, '-')
                                            $('#clientcontent .row .col-sm-12 .well .globprop_' + d).after(
                                                '<span style="color:silver;margin:0 8px;"> | </span>' +
                                                '<img src="/assets/img/yes.png" style="margin-top:-2px;width:28px;" ' +
                                                'title="ELYK Hosted" />'
                                            );
                                        }
                                        if (data == 'false') {
                                            $('#clientcontent .row .col-sm-12 .globprop_' + d).after(
                                                '<span style="color:silver;margin:0 8px;"> | </span>' +
                                                '<a href="https://www.who-hosts-this.com/?s=' + domainsCheck[d] + '" ' +
                                                'target="_blank"><img src="/assets/img/no.png" ' +
                                                'style="margin-top:-2px;width:28px;" title="Not ELYK Hosted" /></a>'
                                            );
                                        }
                                    });
                                    var url2 = "<?php echo base_url(); ?>clients/loadDomainIp/" + domainsCheck[d];
                                    $.post(url2).done(function(data) {
                                        $('#clientcontent .row .col-sm-12 .' + domainsCheck[d].replace(/\./g, '-') + ' h4').append(
                                            '<div><a href="javascript:;" ' +
                                            'onclick="Utils.copyTextToClipboard(\'' + data + '\', this);"' +
                                            '><div class="sprite-copy left" title="Copy" style="margin-top:5px;">' +
                                            '</div></a> <a href="javascript:;">' + data + '</a></div>'
                                        );
                                    });
                                }
                            } else {
                                $('#clientcontent .row').append('<div class="col-sm-12">No Domains</div>');
                            }
                            $('#clientcontentadd').html(
								'<a href="/admin/clients/edit/' + thisid + '" id="add-domain">' +
								'    <button class="btn btn-sm btn-info">' +
								'        <i class="fa fa-plus"></i>&nbsp;&nbsp;Add/Edit Domains' +
								'    </button>' +
								'</a>'
							);
                            $.when(
                                $('#clientcontent .overlay').fadeOut()
                            ).then(
                                setTimeout(function() {
                                    $('#clientcontent .row').removeClass("hidden");
                                    scrollToView("clientcontent");
                                }, 300)
                            );
                        });
                    });
                    $('#load-projects-data').click(function() {
                        $('.client-panel-body .metric').css("color", "#3287B2").css("background-color", "#ffffff");
                        $('#load-projects-data .metric').css("color", "#ffffff").css("background-color", "#2B333E");
                        var thisid = $(this).attr("data-id");
                        $('#clientcontent').html(
                            '<div class="overlay" style="text-align:center;">\n' +
                            '<img class="loading" src="/assets/img/loader.gif" alt="Loading" /><br/>\n' +
                            '<h2>Loading the Projects Data...</h2>\n' +
                            '</div>'
                        );
                        var url = "<?php echo base_url(); ?>clients/loadProjectsData/" + thisid;
                        $.post(url).done(function(data) {
                            $('#clientcontent').html("");
                            var result = JSON.parse(data);
                            $('#clientcontent').append(
                                '<div class="row hidden margin-top-5"></div>'
                            );
                            if (result.length > 0) {
                                $("#load-projects-data .metric .number").html(result.length);
                                for (let i = 0; i < result.length; i++) {
                                    if (result[i].project_statuses_id !== 5) {
                                        $('#clientcontent .row').append(
                                            '<div class="col-sm-12">' +
                                            '<div class="well well_' + result[i].id + ' client-card relative">' +
                                            '<span class="icon">' +
                                            '<i class="fa fa-paperclip"></i>' +
                                            '</span>' +
                                            //'<h4><strong><a href="/admin/projects/edit/' +
                                            '<h4><strong><a href="javascript:;" onclick="viewProject(' + result[i].id + ');">' +
                                            //result[i].id + '">(' + result[i].id + ') ' +
                                            '(' +result[i].id + ') ' +
                                            $('h3.client-panel-title').html().split(')')[1] +
                                            '</a></strong></h4>' +
                                            '<div class="text-left"><strong>' +
                                            $('#p_category option[value=' + result[i].project_statuses_id + ']').text() +
                                            '</strong></div>' +
                                            '<i class="fa fa-trash pull-right client-content-well-trash" ' +
                                            'onclick="deleteClientProject(' + result[i].id + ');"></i>' +
                                            '</div>' +
                                            '</div>'
                                        );
                                        /*var psUrl = "<?php //echo base_url(); ?>clients/loadProjectStatus/" +
                                            result[i].project_statuses_id;
                                        $.post(psUrl).done(function(psData) {
                                            var psResult = JSON.parse(psData);
                                        });*/
                                        if (result[i].domains_id != 0) {
                                            var domUrl = "<?php echo base_url(); ?>clients/loadProjectDomain/" +
                                                result[i].domains_id;
                                            $.post(domUrl).done(function(domData) {
                                                var domResult = JSON.parse(domData);
                                                if (domResult.domain) {
                                                    $('#clientcontent .row .well_' + result[i].id + ' h4 strong').append(
                                                        ' - ' + domResult.domain
                                                    );
                                                }
                                            });
                                        }
                                        setTimeout(function(){
                                            var catUrl = "<?php echo base_url(); ?>clients/loadProjectCategory/" +
                                                result[i].categories_id;
                                            $.post(catUrl).done(function(catData) {
                                                var catResult = JSON.parse(catData);
                                                $('#clientcontent .row .well_' + result[i].id + ' h4 strong').append(
                                                    '<br>' + catResult.name
                                                );
                                            });
                                        }, 10);
                                    }
                                }
                            } else {
                                $('#clientcontent .row').append('<div class="col-sm-12">No Active Projects</div>');
                            }
                            $('#clientcontentadd').html(
                                '<a href="javascript:;" id="add-project" onclick="addClientProject(' + thisid + ');">' +
                                '    <button class="btn btn-sm btn-info">' +
                                '        <i class="fa fa-plus"></i>&nbsp;&nbsp;Add Project' +
                                '    </button>' +
                                '</a>'
                            );
                            $('#clientcontent .row').removeClass("hidden");
                            scrollToView("clientcontent");
                        });
                    });
                    $('#load-notes-data').click(function() {
                        $('.client-panel-body .metric').css("color", "#3287B2").css("background-color", "#ffffff");
                        $('#load-notes-data .metric').css("color", "#ffffff").css("background-color", "#2B333E");
                        var thisid = $(this).attr("data-id");
                        $('#clientcontent').html(
                            '<div class="overlay" style="text-align:center;">\n' +
                            '<img class="loading" src="/assets/img/loader.gif" alt="Loading" /><br/>\n' +
                            '<h2>Loading the Notes Data...</h2>\n' +
                            '</div>'
                        );
                        var url = "<?php echo base_url(); ?>clients/loadNotesData/" + thisid;
                        $.post(url).done(function(data) {
                            $('#clientcontent').html("");
                            var result = JSON.parse(data);
                            $('#clientcontent').append(
                                '<div class="row hidden margin-top-5"></div>'
                            );
                            if (result.length > 0) {
                                $("#load-notes-data .metric .number").html(result.length);
                                for (let i = 0; i < result.length; i++) {
                                    if (result[i].url.indexOf("https") !== -1) {
                                        var url = result[i].url;
                                    } else if (result[i].url.indexOf("http") !== -1) {
                                        var url = result[i].url;
                                    } else {
                                        var url = 'http://' + result[i].url;
                                    }
                                    $('#clientcontent .row').append(
                                        '<div class="col-sm-12">' +
                                        '<div class="well well_' + result[i].id + ' client-card relative">' +
                                        '<span class="icon">' +
                                        '<i class="fa fa-sticky-note"></i>' +
                                        '</span>' +
                                        '<a href="javascript:;" onclick="editClientNote(' + result[i].id + ');">' +
                                        '<h4><i class="fa fa-pencil"></i>&nbsp; <strong>' + result[i].note_name +
                                        '</strong></h4></a>' +
                                        (result[i].url != 'undefined' ? '<div class="margin-bottom-5">URL: ' +
                                        '<a href="javascript:;"' +
                                        ' onclick="Utils.copyTextToClipboard(\'' +
                                        url +
                                        '\', this);"><div class="sprite-copy left" title="Copy"></div></a>' +
                                        '<a href="' + url + '" target="_blank">' +
                                        url + '</a></div>' : '') +
                                        (result[i].username != 'undefined' ? '<div class="margin-bottom-5">UN: ' +
                                        '<a href="javascript:;"' +
                                        ' onclick="Utils.copyTextToClipboard(\'' +
                                            result[i].username +
                                        '\', this);"><div class="sprite-copy left" title="Copy"></div></a>' +
                                        result[i].username + '</div>' : '') +
                                        (result[i].password != 'undefined' ? '<div class="margin-bottom-5">PW: ' +
                                        '<a href="javascript:;"' +
                                        ' onclick="Utils.copyTextToClipboard(\'' +
                                        result[i].password +
                                        '\', this);"><div class="sprite-copy left" title="Copy"></div></a>' +
                                        result[i].password + '</div>' : '') +
                                        (result[i].notes != 'undefined' ? '<div class="margin-bottom-5">' +
                                        nl2br(result[i].notes) + '</div>': '') +
                                        (result[i].author != 'undefined' ? '' +
                                        '<div class="text-left">Last Updated By: ' +
                                        result[i].author + ' &raquo; ' + result[i].date_updated + '</div>': '') +
                                        '<i class="fa fa-trash pull-right client-content-well-trash" ' +
                                        'onclick="deleteClientNote(' + result[i].id + ');"></i>' +
                                        '</div>' +
                                        '</div>'
                                    );
                                }
                            } else {
                                $('#clientcontent .row').append('<div class="col-sm-12">No Notes</div>');
                            }
                            $('#clientcontentadd').html(
                                '<a href="javascript:;" id="add-note" onclick="addClientNote(' + thisid + ');">' +
                                '    <button class="btn btn-sm btn-info">' +
                                '        <i class="fa fa-plus"></i>&nbsp;&nbsp;Add Note' +
                                '    </button>' +
                                '</a>'
                            );
                            $('#clientcontent .row').removeClass("hidden");
                            scrollToView("clientcontent");
                        });
                    });
                    $('#load-contacts-data').click(function() {
                        $('.client-panel-body .metric').css("color", "#3287B2").css("background-color", "#ffffff");
                        $('#load-contacts-data .metric').css("color", "#ffffff").css("background-color", "#2B333E");
                        var thisid = $(this).attr("data-id");
                        $('#clientcontent').html(
                            '<div class="overlay" style="text-align:center;">\n' +
                            '<img class="loading" src="/assets/img/loader.gif" alt="Loading" /><br/>\n' +
                            '<h2>Loading the Contacts Data...</h2>\n' +
                            '</div>'
                        );
                        var url = "<?php echo base_url(); ?>clients/loadContactsData/" + thisid;
                        $.post(url).done(function(data) {
                            $('#clientcontent').html("");
                            var result = JSON.parse(data);
                            $('#clientcontent').append(
                                '<div class="row hidden margin-top-5"></div>'
                            );
                            if (result.length > 0) {
                                $("#load-contacts-data .metric .number").html(result.length);
                                for (let i = 0; i < result.length; i++) {
                                    $('#clientcontent .row').append(
                                        '<div class="col-sm-12">' +
                                        '<div class="well well_' + result[i].id + ' client-card relative">' +
                                        '<span class="icon">' +
                                        '<i class="fa fa-address-card"></i>' +
                                        '</span>' +
                                        '<a href="javascript:;" onclick="editClientContact(' + result[i].id + ',' +
                                        $("#load-contacts-data").attr("data-id") + ');">' +
                                        '<h4><i class="fa fa-pencil"></i>&nbsp; <strong>Edit Contact</strong></h4></a>' +
                                        '<h4>' +
                                        '<a href="javascript:;"' +
                                        ' onclick="Utils.copyTextToClipboard(\'' +
                                        result[i].first_name + ' ' + result[i].last_name +
                                        '\', this);"><p>' +
                                        '<p class="sprite-copy left" title="Copy" style="margin:2px 3px 0 0;">' +
                                        '</p></a>' + result[i].first_name + ' ' + result[i].last_name +
                                        (result[i].title != 'undefined' ? '<span> (' + result[i].title + ')</span>' : '') +
                                        '</h4>' +
                                        '<i class="fa fa-trash pull-right client-content-well-trash" ' +
                                        'onclick="deleteClientContact(' + result[i].id + ');"></i>' +
                                        '</div>' +
                                        '</div>'
                                    );
                                    // Handle the emails ["email1@domain.ext","email2@domain.ext"] etc...
                                    if (result[i].emails != 'undefined') {
                                        var emails = JSON.parse(result[i].emails);
                                        if (emails.length > 0) {
                                            for (let e = 0; e < emails.length; e++) {
                                                $('#clientcontent .row .col-sm-12 .well.well_' + result[i].id).append(
                                                    '<a href="javascript:;" onclick="Utils.copyTextToClipboard(\'' +
                                                    emails[e] +
                                                    '\', this);"><p><div class="sprite-copy left" title="Copy"></div></a>' +
                                                    ' Email: <a href="mailto:' + emails[e] + '">' + emails[e] + '</a></p>'
                                                );
                                            }
                                        }
                                    }
                                    // Hanlde the address, only one line in BF3, possible break to 2 lines in BF4?
                                    if (result[i].address != 'undefined') {
                                        $('#clientcontent .row .col-sm-12 .well.well_' + result[i].id).append(
                                            '<a href="javascript:;" onclick="Utils.copyTextToClipboard(\'' +
                                            (result[i].address != 'undefined' ? result[i].address : '') +
                                            (result[i].city != 'undefined' ? ' ' + result[i].city  : '') +
                                            (result[i].state != 'undefined' ? ' ' + result[i].state  : '') +
                                            (result[i].zip != 'undefined' ? ' ' + result[i].zip : '') + '\', this);">' +
                                            '<div style="margin-left: 20px;">' +
                                            '<div class="sprite-copy left" title="Copy" style="margin-left:-20px;">' +
                                            '</div></a>' + (result[i].address != 'undefined' ? result[i].address : '') + ' ' +
                                            (result[i].city != 'undefined' ? ' ' + result[i].city : '') + ', ' +
                                            (result[i].state != 'undefined' ? result[i].state + ' ' : '') + ' ' +
                                            (result[i].zip != 'undefined' ? result[i].zip : '') +
                                            '</div>'
                                        );
                                    }
                                    // Hanlde the phone numbers
                                    if (result[i].phones != 'undefined') {
                                        // Make an array of [{"number":"9047894561","label":"Work"}]
                                        var phones = [result[i].phones];
                                        // Then foreach array parse the data for use in the html
                                        for (let x = 0; x < phones.length; x++) {
                                            if (phones[x] != 'undefined') {
                                                var tPhones = JSON.parse(phones[x]);
                                                tPhones = tPhones.reverse();
                                                for (let p = 0; p < tPhones.length; p++) {
                                                    $('#clientcontent .row .col-sm-12 .well.well_' + result[i].id).append(
                                                        '<a href="javascript:;" onclick="Utils.copyTextToClipboard(\'' +
                                                        Utils.cleanPhone(tPhones[p].number) +
                                                        '\', this);"><p><div class="sprite-copy left" title="Copy"></div></a>' +
                                                        ' Phone ' +
                                                        (tPhones[p].label != '' ? ' (' + tPhones[p].label + '):' : ':') +
                                                        '<a href="tel:' + Utils.cleanPhone(tPhones[p].number) + '"> ' +
                                                        Utils.formatPhone(tPhones[p].number) + '</a></p>'
                                                    );
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {
                                $('#clientcontent .row').append('<div class="col-sm-12">No Contacts</div>');
                            }
                            $('#clientcontentadd').html(
                                '<a href="javascript:;" id="add-contact" onclick="addClientContact(' + thisid + ');">' +
                                '    <button class="btn btn-sm btn-info">' +
                                '        <i class="fa fa-plus"></i>&nbsp;&nbsp;Add Contact' +
                                '    </button>' +
                                '</a>'
                            );
                            $('#clientcontent .row').removeClass("hidden");
                            scrollToView("clientcontent");
                        });
                    });
                    if (getCid !== 'none') {
                        setTimeout(function() {
                            $('.client-panel-body .metric').css("color", "#3287B2").css("background-color", "#ffffff");
                            var thisid = getCid;
                            // set the data-id values around the main content
                            $('#load-projects-data').attr('data-id', thisid);
                            $('#load-notes-data').attr('data-id', thisid);
                            $('#load-contacts-data').attr('data-id', thisid);
                            $('#load-domains-data').attr('data-id', thisid);
                            $('#load-services-data').attr('data-id', thisid);
                            $('.client-panel-body').show();
                            $('#clientcontent').html(
                                '<div class="overlay" style="text-align:center;">\n' +
                                '<img class="loading" src="/assets/img/loader.gif" alt="Loading" /><br/>\n' +
                                '<h2>Loading the Clients Data...</h2>\n' +
                                '</div>'
                            );
                            var url = "<?php echo base_url(); ?>clients/loadClientData/" + thisid;
                            $.post(url).done(function(data) {
                                var result = JSON.parse(data);
                                $('.client-panel-title').html(result.client_name != 'undefined' ? '(' + result.id + ') ' +
                                result.client_name : '-- None --');
                                $('.client-panel-username').html(
                                    'Username:&nbsp; <strong>' +
                                    (result.username != 'undefined' ? result.username : '-- None --') +
                                    '</strong>'
                                );
                                $('.client-panel-timeblock').html(
                                    'Timeblock:&nbsp; <strong>' +
                                    (result.is_time_blocked > 0 ? 'Yes' : 'No') +
                                    '</strong>' +
                                    (
                                        result.is_time_blocked == 1 && result.time_remaining > 0 ?
                                        '&nbsp; Time Left:&nbsp; <strong>' + result.time_remaining + '</strong>' :
                                            ''
                                    )
                                );
                                $("#load-projects-data .number").html(result.p_cnt);
                                $("#load-notes-data .number").html(result.n_cnt);
                                $("#load-contacts-data .number").html(result.c_cnt);
                                $("#load-domains-data .number").html(result.d_cnt);
                                $("#load-services-data .number").html(result.s_cnt);
                            });
                            // Load Services by default ////////////////////////////////////////////////////////
                            $('.client-panel-body .metric').css("color", "#3287B2").css("background-color", "#ffffff");
                            $('#load-services-data .metric').css("color", "#ffffff").css("background-color", "#2B333E");
                            $('#clientcontent').html(
                                '<div class="overlay" style="text-align:center;">\n' +
                                '<img class="loading" src="/assets/img/loader.gif" alt="Loading" /><br/>\n' +
                                '<h2>Loading the Services Data...</h2>\n' +
                                '</div>'
                            );
                            var url = "<?php echo base_url(); ?>clients/loadServicesData/" + thisid;
                            $.post(url).done(function(data) {
                                $('#clientcontent').html("");
                                var result = JSON.parse(data);
                                $('#clientcontent').append(
                                    '<div class="row hidden margin-top-5"></div>'
                                );
                                if (result.length > 0) {
                                    for (let i = 0; i < result.length; i++) {
                                        if (result[i].service_name !== 'undefined') {
                                            $('#clientcontent .row').append(
                                                '<div class="col-sm-12">' +
                                                '<div class="well well_' + result[i].id + ' client-card relative"' +
                                                ' style="min-height:47px;">' +
                                                //'<span class="icon">' +
                                                //'<i class="fa fa-shopping-cart"></i>' +
                                                //'</span>' +
                                                '<h4><strong>' +
                                                //'<a href="javascript:;"' +
                                                //' onclick="Utils.copyTextToClipboard(\'' +
                                                //result[i].service_name +
                                                //'\', this);"><p>' +
                                                //'<p class="sprite-copy left" title="Copy" style="margin:2px 3px 0 0;">' +
                                                //'</p></a>' +
                                                result[i].service_name +
                                                //' (' + result[i].service_category + ')' +
                                                '</strong></h4>' +
                                                '<p class="service-note">' +
                                                (result[i].service_note !== 'undefined' ? result[i].service_note : '') +
                                                '</p>' +
                                                '<i class="fa fa-trash pull-right client-content-well-trash" ' +
                                                'onclick="deleteClientService(' + result[i].id + ');"></i>' +
                                                '</div>' +
                                                '</div>'
                                            );
                                        }
                                    }
                                } else {
                                    $('#clientcontent .row').append('<div class="col-sm-12">No Services</div>');
                                }
                                $('#clientcontentadd').html(
									'<a href="/admin/clients/edit/' + thisid + '" id="modify-services">' +
									'    <button class="btn btn-sm btn-info">' +
									'        <i class="fa fa-pencil"></i>&nbsp;&nbsp;Modify Services' +
									'    </button>' +
									'</a>'
								);
                                $('#clientcontent .row').removeClass("hidden");
                            });
                            // Load Services by default ////////////////////////////////////////////////////////
                            $('#client-edit').attr("href", "/admin/clients/edit/" + thisid).removeClass("hide");
                            $('#client-delete').attr("onclick", "deleteClient(" + thisid + ");").removeClass("hide");
                            $('#client-deactivate').attr("href", "/admin/clients/deactivate/" + thisid).removeClass("hide");
                            scrollToView("clientdata");
                        }, 250);
                        // added to switch to the content you were on before refresh
                        setTimeout(function() {
                            if (getContentId !== 'none') {
                                if (getContentId == 1) { // Services
                                    $('#load-services-data').click();
                                } else if (getContentId == 2) { // Domains
                                    $('#load-domains-data').click();
                                } else if (getContentId == 3) { // Projects
                                    $('#load-projects-data').click();
                                } else if (getContentId == 4) { // Notes
                                    $('#load-notes-data').click();
                                } else if (getContentId == 5) { // Contacts
                                    $('#load-contacts-data').click();
                                } else if (getContentId == 6) { // Developers
                                    $('#load-developers-data').click();
                                }
                            }
                        }, 300);
                    }
                    $('#load-services-data').click(function() {
                        $('.client-panel-body .metric').css("color", "#3287B2").css("background-color", "#ffffff");
                        $('#load-services-data .metric').css("color", "#ffffff").css("background-color", "#2B333E");
                        var thisid = $(this).attr("data-id");
                        $('#clientcontent').html(
                            '<div class="overlay" style="text-align:center;">\n' +
                            '<img class="loading" src="/assets/img/loader.gif" alt="Loading" /><br/>\n' +
                            '<h2>Loading the Services Data...</h2>\n' +
                            '</div>'
                        );
                        var url = "<?php echo base_url(); ?>clients/loadServicesData/" + thisid + "/1"; // 1 = active, 0 = all
                        $.post(url).done(function(data) {
                            $('#clientcontent').html("");
                            var result = JSON.parse(data);
                            $('#clientcontent').append(
                                '<div class="row hidden margin-top-5"></div>'
                            );
                            if (result.length > 0) {
                                $("#load-services-data .metric .number").html(result.length);
                                for (let i = 0; i < result.length; i++) {
                                    if (result[i].service_name !== 'undefined') {
                                        $('#clientcontent .row').append(
                                            '<div class="col-sm-12">' +
                                            '<div class="well well_' + result[i].id + ' client-card relative"' +
                                            ' style="min-height:47px;">' +
                                            //'<span class="icon">' +
                                            //'<i class="fa fa-shopping-cart"></i>' +
                                            //'</span>' +
                                            '<h4><strong>' +
                                            //'<a href="javascript:;"' +
                                            //' onclick="Utils.copyTextToClipboard(\'' +
                                            //result[i].service_name +
                                            //'\', this);"><p>' +
                                            //'<p class="sprite-copy left" title="Copy" style="margin:2px 3px 0 0;">' +
                                            //'</p></a>' +
                                            result[i].service_name +
                                            //' (' + result[i].service_category + ')' +
                                            '</strong></h4>' +
                                            '<p class="service-note">' +
                                            (result[i].service_note !== 'undefined' ? result[i].service_note : '') +
                                            '</p>' +
                                            '<i class="fa fa-trash pull-right client-content-well-trash" ' +
                                            'onclick="deleteClientService(' + result[i].id + ');"></i>' +
                                            '</div>' +
                                            '</div>'
                                        );
                                    }
                                }
                            } else {
                                $('#clientcontent .row').append('<div class="col-sm-12">No Services</div>');
                            }
                            $('#clientcontentadd').html(
								'<a href="/admin/clients/edit/' + thisid + '" id="modify-services">' +
								'    <button class="btn btn-sm btn-info">' +
								'        <i class="fa fa-pencil"></i>&nbsp;&nbsp;Modify Services' +
								'    </button>' +
								'</a>'
							);
                            $('#clientcontent .row').removeClass("hidden");
                        });
                        scrollToView("clientcontent");
                    });
                    /*$('#load-invoices-data').click(function() {
                        $('.client-panel-body .metric').css("color", "#3287B2").css("background-color", "#ffffff");
                        $('#load-invoices-data .metric').css("color", "#ffffff").css("background-color", "#2B333E");
                        var thisid = $(this).attr("data-id");
                        $('#clientcontent').html(
                            '<div class="overlay" style="text-align:center;">\n' +
                            '<img class="loading" src="/assets/img/loader.gif" alt="Loading" /><br/>\n' +
                            '<h2>Loading the Invoices Data...</h2>\n' +
                            '</div>'
                        );
                        var url = "<?php //echo base_url(); ?>clients/loadInvoicesData/" + thisid;
                        $.post(url).done(function(data) {
                            $('#clientcontent').html("");
                            $('#clientcontentadd').html("");
                            var result = JSON.parse(data);
                            $('#clientcontent').append(
                                '<div class="row hidden margin-top-5"><div class="col-sm-12 text-right listing-count">' +
                                '<h3>' + result.length + ' Invoices(s)</h3>' +
                                '</div></div>'
                            );
                            //$('#clientcontent').html("");
                            //$('#clientcontentadd').html("");
                            //var result = JSON.parse(data);
                            //for (let i = 0; i < result.length; i++) {
                            //$('#clientcontent').append(result[i].first_name + ' ' + result[i].last_name + '<br>')
                            //}
                            $('#clientcontent .row').removeClass("hidden");
                        });
                    });*/
                });
            </script>
