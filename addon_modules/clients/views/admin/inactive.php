<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
    <div class="row">
        <div class="col-lg-12">
			<h3 class="page-title">Inactive Clients</h3>
            <?php if (isset($msg)) { ?>
            <div class="alert alert-<?php echo $msgtype; ?> action-alert" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <?php echo $msg; ?>
            </div>
            <?php } ?>
            <?php if ($this->session->flashdata('msg')) { ?>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-<?php echo $this->session->flashdata('msgtype'); ?> alert-dismissible action-alert" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <?php echo $this->session->flashdata('msg'); ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div class="panel">
                <div class="panel-body margin-top-15 margin-bottom-15">
                    <p>Click on the Client to Re-Activate their account</p>
                    <ul class="list-unstyled margin-top-30">
                        <?php
                        /*
                        Array (
                            [id] => 822
                            [active] => 0
                            [client_name] => About Floors N More
                            [time_remaining] => 0.00
                            [is_time_blocked] => 0
                            [time_block_change_date] =>
                            [username] =>
                            [password] =>
                            [salt] =>
                        */
                            foreach ($clients as $i => $client) {
                        ?>
                        <li id="row_<?php echo $client['id']; ?>">
                            <div class="alert alert-success" role="alert">
                                <a
                                    href="javascript:;"
                                    onclick="activateClient(<?php echo $client['id']; ?>);"
                                    class="reactive-client"
                                >
                                    <i class="fa fa-check-circle"></i>&nbsp;&nbsp; <?php echo $client['client_name']; ?>
                                </a>
                            </div>
                        </li>
                        <?php

                            }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <script>
        function activateClient(id) {
            swal({
                title: "Are you sure?",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-success",
                confirmButtonText: "Yes",
                cancelButtonText: "Cancel",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function(isConfirm) {
                if (isConfirm) {
                    window.location = "<?php echo base_url(); ?>clients/activate/" + id;
                    /*$.post(url).done(function(data) {
                        var result = JSON.parse(data);
                        if (result.msg == 'success') {
                            $("#row_" + id).remove();
                        }
                        if (result.msg == 'failure') swal("There was an error!");
                    });*/
                }
            });
        }
        $(document).ready(function() {
            //
        });
    </script>
