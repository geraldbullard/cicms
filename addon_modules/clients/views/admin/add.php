<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
    <div class="row">
        <div class="col-lg-12">
			<h3 class="page-title">Clients Add</h3>
            <?php if (isset($msg)) { ?>
            <div class="alert alert-<?php echo $msgtype; ?> action-alert" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <?php echo $msg; ?>
            </div>
            <?php } ?>
	        <?php if ($this->session->flashdata('msg')) { ?>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-<?php echo $this->session->flashdata('msgtype'); ?> alert-dismissible action-alert" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
					        <?php echo $this->session->flashdata('msg'); ?>
                        </div>
                    </div>
                </div>
	        <?php } ?>
	        <div class="panel">
                <div class="panel-body margin-bottom-15">
                    <form action="" method="post" id="client_add">
                        <div class="clearfix">
                            <h4>Details</h4>
                            <p>
	                            <label for="client_name">Name</label>
                                <input
                                    type="text"
                                    class="form-control"
                                    id="client_name"
                                    name="client_name"
                                    value="<?php echo set_value('client_name'); ?>"
                                    autocomplete="off"
                                >
                            </p>
                            <p>
                                <label for="username">
                                    Username <a href="javascript:;" id="unfromcn"><small>Get from Client Name</small></a>
                                </label>
                                <input
                                    type="text"
                                    class="form-control"
                                    id="username"
                                    name="username"
                                    value="<?php echo set_value('username'); ?>"
                                    autocomplete="off"
                                >
                            </p>
                            <p>
                                <label for="password">Password</label>
                                <input
                                    type="password"
                                    class="form-control"
                                    id="password"
                                    name="password"
                                    value="<?php echo set_value('password'); ?>"
                                    autocomplete="off"
                                >
                            </p>
                            <p>
                                <label for="passconf">Password Confirm</label>
                                <input
                                    type="password"
                                    class="form-control"
                                    id="passconf"
                                    name="passconf"
                                    value="<?php echo set_value('passconf'); ?>"
                                    autocomplete="off"
                                >
                            </p>
                            <p>
                                <label class="fancy-checkbox" for="is_time_blocked">
                                    <input
                                        type="checkbox"
                                        class="form-control"
                                        id="is_time_blocked"
                                        name="is_time_blocked"
                                        <?php
                                            echo (
                                                set_value('is_time_blocked') == 'on' ?
                                                ' checked' : ''
                                            );
                                        ?>
                                    >
                                    <span><strong>Is Time Blocked</strong></span>
                                </label>
                            </p>
                            <p>
                                <label for="time_remaining">Time Remainnig</label>
                                <input
                                    type="number"
                                    class="form-control"
                                    id="time_remaining"
                                    name="time_remaining"
                                    step=".25"
                                />
                            </p>
                        </div><hr class="margin-top-10 margin-bottom-5 lightgrey-border-1">
                        <div class="clearfix">
                            <h4>
                                <strong>Services</strong>
                                &nbsp;&nbsp;&nbsp;<small>
                                    <a href="javascript:;" id="services_all">All</a> /
                                    <a href="javascript:;" id="services_none">None</a>
                                </small>
                            </h4>
                            <div id="client_services">
                                <div class="margin-bottom-15">
                                    <!-- Design & Build / dnb-->
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <p class="users-services-row usr-first">
                                                <label class="fancy-checkbox">
                                                    <input
                                                        type="checkbox"
                                                        name="services[dnb][]"
                                                        value="Design & Build"
                                                    >
                                                    <span>&nbsp;Design & Build</span>
                                                </label>
                                            </p>
                                        </div>
                                        <div class="col-lg-6">
                                            <p class="users-services-row">
                                                <input
                                                    type="text"
                                                    name="services[dnb][]"
                                                    value=""
                                                    class="form-control"
                                                />
                                                <input
                                                    type="hidden"
                                                    name="services[dnb][]"
                                                    value="dnb"
                                                />
                                            </p>
                                        </div>
                                        <div class="col-lg-4"></div>
                                    </div>
                                    <!-- Web Hosting / wh -->
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <p class="users-services-row usr-first">
                                                <label class="fancy-checkbox">
                                                    <input
                                                        type="checkbox"
                                                        name="services[wh][]"
                                                        value="Web Hosting"
                                                    >
                                                    <span>&nbsp;Web Hosting</span>
                                                </label>
                                            </p>
                                        </div>
                                        <div class="col-lg-6">
                                            <p class="users-services-row">
                                                <input
                                                    type="text"
                                                    name="services[wh][]"
                                                    value=""
                                                    class="form-control"
                                                />
                                                <input
                                                    type="hidden"
                                                    name="services[wh][]"
                                                    value="wh"
                                                />
                                            </p>
                                        </div>
                                        <div class="col-lg-4"></div>
                                    </div>
                                    <!-- SSL / ssl -->
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <p class="users-services-row usr-first">
                                                <label class="fancy-checkbox">
                                                    <input
                                                        type="checkbox"
                                                        name="services[ssl][]"
                                                        value="SSL"
                                                    >
                                                    <span>&nbsp;SSL</span>
                                                </label>
                                            </p>
                                        </div>
                                        <div class="col-lg-6">
                                            <p class="users-services-row">
                                                <input
                                                    type="text"
                                                    name="services[ssl][]"
                                                    value=""
                                                    class="form-control"
                                                />
                                                <input
                                                    type="hidden"
                                                    name="services[ssl][]"
                                                    value="ssl"
                                                />
                                            </p>
                                        </div>
                                        <div class="col-lg-4"></div>
                                    </div>
                                    <!-- Monthly WP/PL Updates / wppl -->
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <p class="users-services-row usr-first">
                                                <label class="fancy-checkbox">
                                                    <input
                                                        type="checkbox"
                                                        name="services[wppl][]"
                                                        value="Monthly WP/PL Updates"
                                                    >
                                                    <span>&nbsp;Monthly WP/PL Updates</span>
                                                </label>
                                            </p>
                                        </div>
                                        <div class="col-lg-6">
                                            <p class="users-services-row">
                                                <input
                                                    type="text"
                                                    name="services[wppl][]"
                                                    value=""
                                                    class="form-control"
                                                />
                                                <input
                                                    type="hidden"
                                                    name="services[wppl][]"
                                                    value="wppl"
                                                />
                                            </p>
                                        </div>
                                        <div class="col-lg-4"></div>
                                    </div>
                                    <!-- SEO / seo -->
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <p class="users-services-row usr-first">
                                                <label class="fancy-checkbox">
                                                    <input
                                                        type="checkbox"
                                                        name="services[seo][]"
                                                        value="SEO"
                                                    >
                                                    <span>&nbsp;SEO</span>
                                                </label>
                                            </p>
                                        </div>
                                        <div class="col-lg-6">
                                            <p class="users-services-row">
                                                <input
                                                    type="text"
                                                    name="services[seo][]"
                                                    value=""
                                                    class="form-control"
                                                />
                                                <input
                                                    type="hidden"
                                                    name="services[seo][]"
                                                    value="seo"
                                                />
                                            </p>
                                        </div>
                                        <div class="col-lg-4"></div>
                                    </div>
                                    <!-- Email Marketing / email -->
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <p class="users-services-row usr-first">
                                                <label class="fancy-checkbox">
                                                    <input
                                                        type="checkbox"
                                                        name="services[emark][]"
                                                        value="Email Marketing"
                                                    >
                                                    <span>&nbsp;Email Marketing</span>
                                                </label>
                                            </p>
                                        </div>
                                        <div class="col-lg-6">
                                            <p class="users-services-row">
                                                <input
                                                    type="text"
                                                    name="services[emark][]"
                                                    value=""
                                                    class="form-control"
                                                />
                                                <input
                                                    type="hidden"
                                                    name="services[emark][]"
                                                    value="emark"
                                                />
                                            </p>
                                        </div>
                                        <div class="col-lg-4"></div>
                                    </div>
                                    <!-- PPC / ppc -->
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <p class="users-services-row usr-first">
                                                <label class="fancy-checkbox">
                                                    <input
                                                        type="checkbox"
                                                        name="services[ppc][]"
                                                        value="PPC"
                                                    >
                                                    <span>&nbsp;PPC</span>
                                                </label>
                                            </p>
                                        </div>
                                        <div class="col-lg-6">
                                            <p class="users-services-row">
                                                <input
                                                    type="text"
                                                    name="services[ppc][]"
                                                    value=""
                                                    class="form-control"
                                                />
                                                <input
                                                    type="hidden"
                                                    name="services[ppc][]"
                                                    value="ppc"
                                                />
                                            </p>
                                        </div>
                                        <div class="col-lg-4"></div>
                                    </div>
                                    <!-- TLC / tlc -->
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <p class="users-services-row usr-first">
                                                <label class="fancy-checkbox">
                                                    <input
                                                        type="checkbox"
                                                        name="services[tlc][]"
                                                        value="TLC"
                                                    >
                                                    <span>&nbsp;TLC</span>
                                                </label>
                                            </p>
                                        </div>
                                        <div class="col-lg-6">
                                            <p class="users-services-row">
                                                <input
                                                    type="text"
                                                    name="services[tlc][]"
                                                    value=""
                                                    class="form-control"
                                                />
                                                <input
                                                    type="hidden"
                                                    name="services[tlc][]"
                                                    value="tlc"
                                                />
                                            </p>
                                        </div>
                                        <div class="col-lg-4"></div>
                                    </div>
                                    <!-- Domain Hosting / dh -->
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <p class="users-services-row usr-first">
                                                <label class="fancy-checkbox">
                                                    <input
                                                        type="checkbox"
                                                        name="services[dh][]"
                                                        value="Domain Hosting"
                                                    >
                                                    <span>&nbsp;Domain Hosting</span>
                                                </label>
                                            </p>
                                        </div>
                                        <div class="col-lg-6">
                                            <p class="users-services-row">
                                                <input
                                                    type="text"
                                                    name="services[dh][]"
                                                    value=""
                                                    class="form-control"
                                                />
                                                <input
                                                    type="hidden"
                                                    name="services[dh][]"
                                                    value="dh"
                                                />
                                            </p>
                                        </div>
                                        <div class="col-lg-4"></div>
                                    </div>
                                    <!-- Plugin Upgrades / plug -->
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <p class="users-services-row usr-first">
                                                <label class="fancy-checkbox">
                                                    <input
                                                        type="checkbox"
                                                        name="services[plug][]"
                                                        value="Plugin Upgrades"
                                                    >
                                                    <span>&nbsp;Plugin Upgrades</span>
                                                </label>
                                            </p>
                                        </div>
                                        <div class="col-lg-6">
                                            <p class="users-services-row">
                                                <input
                                                    type="text"
                                                    name="services[plug][]"
                                                    value=""
                                                    class="form-control"
                                                />
                                                <input
                                                    type="hidden"
                                                    name="services[plug][]"
                                                    value="plug"
                                                />
                                            </p>
                                        </div>
                                        <div class="col-lg-4"></div>
                                    </div>
                                    <!-- WordFence / wf -->
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <p class="users-services-row usr-first">
                                                <label class="fancy-checkbox">
                                                    <input
                                                        type="checkbox"
                                                        name="services[wf][]"
                                                        value="WordFence"
                                                    >
                                                    <span>&nbsp;WordFence</span>
                                                </label>
                                            </p>
                                        </div>
                                        <div class="col-lg-6">
                                            <p class="users-services-row">
                                                <input
                                                    type="text"
                                                    name="services[wf][]"
                                                    value=""
                                                    class="form-control"
                                                />
                                                <input
                                                    type="hidden"
                                                    name="services[wf][]"
                                                    value="wf"
                                                />
                                            </p>
                                        </div>
                                        <div class="col-lg-4"></div>
                                    </div>
                                    <!-- RackSpace Email / rse -->
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <p class="users-services-row usr-first">
                                                <label class="fancy-checkbox">
                                                    <input
                                                        type="checkbox"
                                                        name="services[rse][]"
                                                        value="RackSpace Email"
                                                    >
                                                    <span>&nbsp;RackSpace Email</span>
                                                </label>
                                            </p>
                                        </div>
                                        <div class="col-lg-6">
                                            <p class="users-services-row">
                                                <input
                                                    type="text"
                                                    name="services[rse][]"
                                                    value=""
                                                    class="form-control"
                                                />
                                                <input
                                                    type="hidden"
                                                    name="services[rse][]"
                                                    value="rse"
                                                />
                                            </p>
                                        </div>
                                        <div class="col-lg-4"></div>
                                    </div>
                                    <!-- Maintenance Plan / mp -->
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <p class="users-services-row usr-first">
                                                <label class="fancy-checkbox">
                                                    <input
                                                        type="checkbox"
                                                        name="services[mp][]"
                                                        value="Maintenance Plan"
                                                    >
                                                    <span>&nbsp;Maintenance Plan</span>
                                                </label>
                                            </p>
                                        </div>
                                        <div class="col-lg-6">
                                            <p class="users-services-row">
                                                <input
                                                    type="text"
                                                    name="services[mp][]"
                                                    value=""
                                                    class="form-control"
                                                />
                                                <input
                                                    type="hidden"
                                                    name="services[mp][]"
                                                    value="mp"
                                                />
                                            </p>
                                        </div>
                                        <div class="col-lg-4"></div>
                                    </div>
                                    <!-- Accessibility / acc -->
                                    <div class="row">
                                        <div class="col-lg-2">
                                            <p class="users-services-row usr-first">
                                                <label class="fancy-checkbox">
                                                    <input
                                                        type="checkbox"
                                                        name="services[acc][]"
                                                        value="Accessibility"
                                                    >
                                                    <span>&nbsp;Accessibility</span>
                                                </label>
                                            </p>
                                        </div>
                                        <div class="col-lg-6">
                                            <p class="users-services-row">
                                                <input
                                                    type="text"
                                                    name="services[acc][]"
                                                    value=""
                                                    class="form-control"
                                                />
                                                <input
                                                    type="hidden"
                                                    name="services[acc][]"
                                                    value="acc"
                                                />
                                            </p>
                                        </div>
                                        <div class="col-lg-4"></div>
                                    </div>
                                </div>
                            </div>
                        </div><hr class="margin-top-10 margin-bottom-5 lightgrey-border-1">
                        <div class="clearfix">
                            <h4>Domains</h4>
                            <div id="client_domains">
                                <div class="input-group margin-bottom-15">
                                    <input class="form-control" type="text" name="domains[]" placeholder="www.domain.ext" style="width:50%;">
                                    <input class="form-control datepicker" type="text" name="domains_ada[]" placeholder="ADA Compliance Date" style="width:50%;">
                                    <span class="input-group-btn">
                                        <button class="btn btn-danger domain-delete" type="button">
                                            <i class="fa fa-times"></i>&nbsp; Delete
                                        </button>
                                    </span>
                                </div>
                            </div>
                            <p>
                                <div class="input-group">
                                    <button class="btn btn-default" type="button" id="domain_add">
                                        <i class="fa fa-plus"></i>&nbsp; Add Domain
                                    </button>
                                </div>
                            </p>
                        </div>
                        <div class="margin-top-15 text-right">
                            <button type="submit" class="btn btn-primary margin-right-15">Save</button>
                            <button type="button" class="btn btn-danger" onclick="confirmClientAddCancel();">Cancel</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        function clientNameToUsername(str) {
            return str.toString()                   // Convert to string
                .normalize('NFD')                   // Change diacritics
                .replace(/[\u0300-\u036f]/g, '')    // Remove illegal characters
                .replace(/\s+/g, '')                // Change whitespace to dashes
                .toLowerCase()                      // Change to lowercase
                .replace(/&/g, 'and')               // Replace ampersand
                .replace(/[^a-z0-9\-]/g, '')        // Remove anything that is not a letter, number or dash
                .replace(/-/g, '');                 // Remove dashes
        }
        function setDomainDeleteOnClick() {
            $(".domain-delete").each(function() {
                $(this).click(function() {
                    $(this).parent().parent().remove();
                });
            });
        };
        function confirmClientAddCancel() {
            if (confirm("Are you sure? Any unsaved information will be lost.")) window.location = '/admin/clients/';
        };
        $(document).ready(function() {
            $("#services_all").on('click', function() {
                $("#client_services .fancy-checkbox input").each(function() {
                    $(this).prop("checked", true);
                });
            });
            $("#services_none").on('click', function() {
                $("#client_services .fancy-checkbox input").each(function() {
                    $(this).prop("checked", false);
                });
            });
            $("#domain_add").on('click', function() {
                $("#client_domains").append(
                    '<div class="input-group margin-bottom-15">' +
                    '<input class="form-control" type="text" name="domains[]" placeholder="www.domain.ext" style="width:50%;">' +
                    '<input class="form-control datepicker" type="text" name="domains_ada[]" placeholder="ADA Compliance Date" style="width:50%;">' +
                    '<span class="input-group-btn">' +
                    '<button class="btn btn-danger domain-delete" type="button">' +
                    '<i class="fa fa-times"></i>&nbsp; Delete' +
                    '</button>' +
                    '</span>' +
                    '</div>'
                );
                setDomainDeleteOnClick();
                setDatepickersByClassName();
            });
            setDomainDeleteOnClick();
            setDatepickersByClassName();
            setTimeout(function() {
                $("#username").val("");
                $("#password").val("");
            }, 700);
            $("#unfromcn").on('click', function() {
                if ($("#username").val() == '') {
                    if ($("#client_name").val() != '') {
                        $("#username").val(clientNameToUsername($("#client_name").val()));
                    }
                }
            });
        });
    </script>
