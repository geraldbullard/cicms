<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
    <div class="row">
        <div class="col-lg-12">
			<h3 class="page-title">Migrate Clients</h3>
            <?php if (isset($msg)) { ?>
            <div class="alert alert-<?php echo $msgtype; ?> action-alert" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <?php echo $msg; ?>
            </div>
            <?php } ?>
            <?php if ($this->session->flashdata('msg')) { ?>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="alert alert-<?php echo $this->session->flashdata('msgtype'); ?> alert-dismissible action-alert" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <?php echo $this->session->flashdata('msg'); ?>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div class="panel">
                <div class="panel-body margin-top-15 margin-bottom-15">
                    <div class="alert alert-warning">Use "CTRL+F" to find, then click on the Client to migrate their account from BF3 to BF4</div>
                    <div class="alert alert-success">Green Client rows have already been Migrated.</div>
                    <hr class="margin-top-10 margin-bottom-5 lightgrey-border-1">
                    <ul class="list-unstyled margin-top-30">
                        <?php
                            foreach ($bf3_clients as $i => $client) {
                        ?>
                        <li id="row_<?php echo $client['id']; ?>">
                            <?php if (!in_array($client['id'], $bf4_clients)) { ?>
                            <div class="alert alert-warning" role="alert">
                                <a
                                    href="javascript:;"
                                    onclick="migrateClient(<?php echo $client['id']; ?>);"
                                    class="migrate-client"
                                >
                                    <i class="fa fa-plus-circle"></i>&nbsp;&nbsp; <?php echo $client['name']; ?>
                                </a>
                            </div>
                            <?php } else { ?>
                            <div class="alert alert-success" role="alert">
                                <i class="fa fa-check-circle"></i>&nbsp;&nbsp; <?php echo $client['name']; ?>
                            </div>
                            <?php } ?>
                        </li>
                        <?php
                            }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="modal" id="migrateClient" tabindex="-1" role="dialog"
         aria-labelledby="migrateClientLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="migrateClientLabel">
                        Migrate Client <small>(Import from BF3)</small>
                    </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="migrateClientForm">
                    <input type="hidden" name="clientid" id="cid" value="">
                    <div class="modal-body">
                        <p class="form-group">
                            <label for="c_notes">Migrate Client Notes</label>
                            <select name="notes" id="c_notes" class="form-control c-selection">
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
                        </p>
                        <p class="form-group">
                            <label for="c_contacts">Migrate Client Contacts</label>
                            <select name="contacts" id="c_contacts" class="form-control c-selection">
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
                        </p>
                        <p class="form-group">
                            <label for="c_domains">Migrate Client Domains</label>
                            <select name="domains" id="c_domains" class="form-control c-selection">
                                <option value="">Select</option>
                                <option value="yes">Yes</option>
                                <option value="no">No</option>
                            </select>
                        </p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" id="migrateClientClose" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" id="migrateClientSubmit" class="btn btn-primary disabled" disabled>Migrate Now</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        function checkAllSelects() {
            var count = 0;
            $('.c-selection').each(function() {
                if ($(this).val() != '') {
                    count++;
                }
            });
            if (count == 3) {
                $("#migrateClientSubmit").removeClass("disabled").removeAttr("disabled");
            } else {
                return false;
            }
        }
        function migrateClient(id) {
            // update the modal html before showing it
            $("#cid").val(id);
            $("#migrateClient").modal('show');
        }
        $(document).ready(function() {
            $("#migrateClientClose").on('click', function() {
                $("#cid").val("");
                $(".c-selection").each(function() { this.selectedIndex = 0 });
            });
            $("#migrateClientSubmit").on('click', function() {
                var cid = $("#cid").val();
                var formData = $("#migrateClientForm").serialize();
                $.ajax({
                    url: '/clients/migrateClient/',
                    type: 'POST',
                    data: formData,
                    success: function(response) {
                        setTimeout(function() {
                            window.location = '/admin/clients/migrate';
                        }, 300);
                    }
                });
            });
            $(".c-selection").each(function() {
                $(this).on('change', function() {
                    checkAllSelects();
                });
            });
        });
    </script>
