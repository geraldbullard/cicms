<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
			<h3 class="page-title">
                Clients Edit
                <div class="pull-right">
                    <form action="/admin/clients/" method="post">
                        <input type="hidden" name="cid" value="<?php echo $client['id']; ?>" />
                        <button class="btn btn-sm btn-primary" type="submit">
                            <i class="fa fa-backward"></i>&nbsp;&nbsp;Back
                        </button>
                    </form>
                </div>
            </h3>
			<div class="row">
				<div class="col-lg-12">
                    <?php if ($this->session->flashdata('msg')) { ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="alert alert-<?php echo $this->session->flashdata('msgtype'); ?> alert-dismissible action-alert" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                <?php echo $this->session->flashdata('msg'); ?>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="panel">
                        <div class="panel-body margin-bottom-15">
                            <form action="" method="post" id="client_edit">
                                <div class="clearfix">
                                    <h4><strong>Details</strong></h4>
                                    <?php
                                    /*
                                    Array(
                                        [id] => 820
                                        [active] => 1
                                        [client_name] => A&J Manufacturing, LLC
                                        [time_remaining] => 1.25
                                        [is_time_blocked] => 1
                                        [time_block_change_date] => 2014-01-03 11:05:10
                                        [username] => gbullard@elykinnovation.com
                                        [password] => 5a9a23b7bde2e7c200c3d113b750d1e9c4823324
                                        [salt] => AE7j956T43
                                        [domains] => Array(
                                            [0] => Array(
                                                [id] => 346
                                                [clients_id] => 820
                                                [domain] => chargriller.com
                                                [ada] =>
                                            )
                                            [1] => Array(
                                                [id] => 350
                                                [clients_id] => 820
                                                [domain] => ninja.chargriller.com
                                                [ada] =>
                                            )
                                        )
                                    )
                                    */
                                    ?>
                                    <p>
                                        <label for="client_name">Name</label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="client_name"
                                            name="client_name"
                                            value="<?php echo $client['client_name']; ?>"
                                            autocomplete="off"
                                        >
                                    </p>
                                    <p>
                                        <label for="username">Username</label>
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="username"
                                            name="username"
                                            value="<?php echo $client['username']; ?>"
                                            autocomplete="off"
                                        >
                                    </p>
                                    <p>
                                        <label for="password">Password</label>
                                        <small>(Leave blank to keep existing password)</small>
                                        <input
                                            type="password"
                                            class="form-control"
                                            id="password"
                                            name="password"
                                            autocomplete="off"
                                        >
                                    </p>
                                    <p>
                                        <label for="passconf">Password Confirm</label>
                                        <small>(Leave blank to keep existing password)</small>
                                        <input
                                            type="password"
                                            class="form-control"
                                            id="passconf"
                                            name="passconf"
                                            autocomplete="off"
                                        >
                                    </p>
                                    <p>
                                        <label class="fancy-checkbox" for="is_time_blocked">
                                            <input
                                                type="checkbox"
                                                class="form-control"
                                                id="is_time_blocked"
                                                name="is_time_blocked"
												<?php
                                                    echo (
                                                        $client['is_time_blocked'] == '1' ?
                                                        ' checked' : ''
                                                    );
												?>
                                            >
                                            <span><strong>Is Time Blocked</strong></span>
                                        </label>
                                    </p>
                                    <p>
                                        <label for="time_remaining">Time Remainnig</label>
                                        <input
                                            type="number"
                                            class="form-control"
                                            id="time_remaining"
                                            name="time_remaining"
                                            step=".25"
                                            value="<?php echo $client['time_remaining']; ?>"
                                        />
                                    </p>
                                </div><hr class="margin-top-10 margin-bottom-5 lightgrey-border-1">
                                <div id="services"></div>
                                <div class="clearfix">
                                    <h4>
                                        <strong>Services</strong>
                                        &nbsp;&nbsp;&nbsp;<small>
                                            <a href="javascript:;" id="services_all">All</a> /
                                            <a href="javascript:;" id="services_none">None</a>
                                        </small>
                                    </h4>
                                    <div id="client_services">
                                        <?php
                                            $svcs = array();
                                            foreach ($client['services'] as $i => $service) {
                                                $svcs[] = $service['service_name'];
                                                if ($service['service_code'] == 'dnb') $dnb = $service;
                                                if ($service['service_code'] == 'wh') $wh = $service;
                                                if ($service['service_code'] == 'ssl') $ssl = $service;
                                                if ($service['service_code'] == 'wppl') $wppl = $service;
                                                if ($service['service_code'] == 'seo') $seo = $service;
                                                if ($service['service_code'] == 'emark') $emark = $service;
                                                if ($service['service_code'] == 'ppc') $ppc = $service;
                                                if ($service['service_code'] == 'tlc') $tlc = $service;
                                                if ($service['service_code'] == 'dh') $dh = $service;
                                                if ($service['service_code'] == 'plug') $plug = $service;
                                                if ($service['service_code'] == 'wf') $wf = $service;
                                                if ($service['service_code'] == 'rse') $rse = $service;
                                                if ($service['service_code'] == 'mp') $mp = $service;
                                                if ($service['service_code'] == 'acc') $acc = $service;
                                            }
                                        ?>
                                        <div class="margin-bottom-15">
                                            <!-- Design & Build / dnb-->
                                            <div class="row">
                                                <div class="col-lg-2">
                                                    <p class="users-services-row usr-first">
                                                        <label class="fancy-checkbox">
                                                            <input
                                                                type="checkbox"
                                                                name="services[dnb][1]"
                                                                value="Design & Build"
                                                                <?php echo (in_array("Design & Build", $svcs) ? ' checked' : ''); ?>
                                                            >
                                                            <span>&nbsp;Design & Build</span>
                                                        </label>
                                                    </p>
                                                </div>
                                                <div class="col-lg-6">
                                                    <p class="users-services-row">
                                                        <textarea
                                                            name="services[dnb][2]"
                                                            class="form-control"
                                                        /><?php echo (!empty($dnb) ? $dnb['service_note'] : ''); ?></textarea>
                                                        <input
                                                            type="hidden"
                                                            name="services[dnb][0]"
                                                            value="dnb"
                                                        />
                                                    </p>
                                                </div>
                                                <div class="col-lg-4">
	                                                <?php if (!empty($dnb) && $dnb['last_updated'] != NULL) { ?>
                                                    <p class="users-services-row usr-last">
                                                        <i class="fa fa-calendar"></i>&nbsp;
	                                                    <?php echo (!empty($dnb) && $dnb['last_updated'] != NULL ? date("m/d/Y", strtotime($dnb['last_updated'])) : ''); ?>
                                                        &raquo; <small><?php
		                                                    echo (!empty($dnb) && $dnb['last_updated_by'] != NULL ? $this->Users_model->get_user_info($dnb['last_updated_by'])['first_name'] : '');
		                                                    echo ' ';
		                                                    echo (!empty($dnb) && $dnb['last_updated_by'] != NULL ? $this->Users_model->get_user_info($dnb['last_updated_by'])['last_name'] : '');
                                                        ?></small>
                                                    </p>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <!-- Web Hosting / wh -->
                                            <div class="row">
                                                <div class="col-lg-2">
                                                    <p class="users-services-row usr-first">
                                                        <label class="fancy-checkbox">
                                                            <input
                                                                type="checkbox"
                                                                name="services[wh][1]"
                                                                value="Web Hosting"
                                                                <?php echo (in_array("Web Hosting", $svcs) ? ' checked' : ''); ?>
                                                            >
                                                            <span>&nbsp;Web Hosting</span>
                                                        </label>
                                                    </p>
                                                </div>
                                                <div class="col-lg-6">
                                                    <p class="users-services-row">
                                                        <textarea
                                                            name="services[wh][2]"
                                                            class="form-control"
                                                        /><?php echo (!empty($wh) ? $wh['service_note'] : ''); ?></textarea>
                                                        <input
                                                            type="hidden"
                                                            name="services[wh][0]"
                                                            value="wh"
                                                        />
                                                    </p>
                                                </div>
                                                <div class="col-lg-4">
	                                                <?php if (!empty($wh) && $wh['last_updated'] != NULL) { ?>
                                                    <p class="users-services-row usr-last">
                                                        <i class="fa fa-calendar"></i>&nbsp;
	                                                    <?php echo (!empty($wh) && $wh['last_updated'] != NULL ? date("m/d/Y", strtotime($wh['last_updated'])) : ''); ?>
                                                        &raquo; <small><?php
		                                                    echo (!empty($wh) && $wh['last_updated_by'] != NULL ? $this->Users_model->get_user_info($wh['last_updated_by'])['first_name'] : '');
		                                                    echo ' ';
		                                                    echo (!empty($wh) && $wh['last_updated_by'] != NULL ? $this->Users_model->get_user_info($wh['last_updated_by'])['last_name'] : '');
                                                        ?></small>
                                                    </p>
	                                                <?php } ?>
                                                </div>
                                            </div>
                                            <!-- SSL / ssl -->
                                            <div class="row">
                                                <div class="col-lg-2">
                                                    <p class="users-services-row usr-first">
                                                        <label class="fancy-checkbox">
                                                            <input
                                                                type="checkbox"
                                                                name="services[ssl][1]"
                                                                value="SSL"
                                                                <?php echo (in_array("SSL", $svcs) ? ' checked' : ''); ?>
                                                            >
                                                            <span>&nbsp;SSL</span>
                                                        </label>
                                                    </p>
                                                </div>
                                                <div class="col-lg-6">
                                                    <p class="users-services-row">
                                                        <textarea
                                                            name="services[ssl][2]"
                                                            class="form-control"
                                                        /><?php echo (!empty($ssl) ? $ssl['service_note'] : ''); ?></textarea>
                                                        <input
                                                            type="hidden"
                                                            name="services[ssl][0]"
                                                            value="ssl"
                                                        />
                                                    </p>
                                                </div>
                                                <div class="col-lg-4">
	                                                <?php if (!empty($ssl) && $ssl['last_updated'] != NULL) { ?>
                                                    <p class="users-services-row usr-last">
                                                        <i class="fa fa-calendar"></i>&nbsp;
	                                                    <?php echo (!empty($ssl) && $ssl['last_updated'] != NULL ? date("m/d/Y", strtotime($ssl['last_updated'])) : ''); ?>
                                                        &raquo; <small><?php
		                                                    echo (!empty($ssl) && $ssl['last_updated_by'] != NULL ? $this->Users_model->get_user_info($ssl['last_updated_by'])['first_name'] : '');
		                                                    echo ' ';
		                                                    echo (!empty($ssl) && $ssl['last_updated_by'] != NULL ? $this->Users_model->get_user_info($ssl['last_updated_by'])['last_name'] : '');
                                                        ?></small>
                                                    </p>
	                                                <?php } ?>
                                                </div>
                                            </div>
                                            <!-- Monthly WP/PL Updates / wppl -->
                                            <div class="row">
                                                <div class="col-lg-2">
                                                    <p class="users-services-row usr-first">
                                                        <label class="fancy-checkbox">
                                                            <input
                                                                type="checkbox"
                                                                name="services[wppl][1]"
                                                                value="Monthly WP/PL Updates"
                                                                <?php echo (in_array("Monthly WP/PL Updates", $svcs) ? ' checked' : ''); ?>
                                                            >
                                                            <span>&nbsp;Monthly WP/PL Updates</span>
                                                        </label>
                                                    </p>
                                                </div>
                                                <div class="col-lg-6">
                                                    <p class="users-services-row">
                                                        <textarea
                                                            name="services[wppl][2]"
                                                            class="form-control"
                                                        /><?php echo (!empty($wppl) ? $wppl['service_note'] : ''); ?></textarea>
                                                        <input
                                                            type="hidden"
                                                            name="services[wppl][0]"
                                                            value="wppl"
                                                        />
                                                    </p>
                                                </div>
                                                <div class="col-lg-4">
	                                                <?php if (!empty($wppl) && $wppl['last_updated'] != NULL) { ?>
                                                    <p class="users-services-row usr-last">
                                                        <i class="fa fa-calendar"></i>&nbsp;
	                                                    <?php echo (!empty($wppl) && $wppl['last_updated'] != NULL ? date("m/d/Y", strtotime($wppl['last_updated'])) : ''); ?>
                                                        &raquo; <small><?php
		                                                    echo (!empty($wppl) && $wppl['last_updated_by'] != NULL ? $this->Users_model->get_user_info($wppl['last_updated_by'])['first_name'] : '');
		                                                    echo ' ';
		                                                    echo (!empty($wppl) && $wppl['last_updated_by'] != NULL ? $this->Users_model->get_user_info($wppl['last_updated_by'])['last_name'] : '');
                                                        ?></small>
                                                    </p>
	                                                <?php } ?>
                                                </div>
                                            </div>
                                            <!-- SEO / seo -->
                                            <div class="row">
                                                <div class="col-lg-2">
                                                    <p class="users-services-row usr-first">
                                                        <label class="fancy-checkbox">
                                                            <input
                                                                type="checkbox"
                                                                name="services[seo][1]"
                                                                value="SEO"
                                                                <?php echo (in_array("SEO", $svcs) ? ' checked' : ''); ?>
                                                            >
                                                            <span>&nbsp;SEO</span>
                                                        </label>
                                                    </p>
                                                </div>
                                                <div class="col-lg-6">
                                                    <p class="users-services-row">
                                                        <textarea
                                                            name="services[seo][2]"
                                                            class="form-control"
                                                        /><?php echo (!empty($seo) ? $seo['service_note'] : ''); ?></textarea>
                                                        <input
                                                            type="hidden"
                                                            name="services[seo][0]"
                                                            value="seo"
                                                        />
                                                    </p>
                                                </div>
                                                <div class="col-lg-4">
	                                                <?php if (!empty($seo) && $seo['last_updated'] != NULL) { ?>
                                                    <p class="users-services-row usr-last">
                                                        <i class="fa fa-calendar"></i>&nbsp;
	                                                    <?php echo (!empty($seo) && $seo['last_updated'] != NULL ? date("m/d/Y", strtotime($seo['last_updated'])) : ''); ?>
                                                        &raquo; <small><?php
		                                                    echo (!empty($seo) && $seo['last_updated_by'] != NULL ? $this->Users_model->get_user_info($seo['last_updated_by'])['first_name'] : '');
		                                                    echo ' ';
		                                                    echo (!empty($seo) && $seo['last_updated_by'] != NULL ? $this->Users_model->get_user_info($seo['last_updated_by'])['last_name'] : '');
                                                        ?></small>
                                                    </p>
	                                                <?php } ?>
                                                </div>
                                            </div>
                                            <!-- Email Marketing / email -->
                                            <div class="row">
                                                <div class="col-lg-2">
                                                    <p class="users-services-row usr-first">
                                                        <label class="fancy-checkbox">
                                                            <input
                                                                type="checkbox"
                                                                name="services[emark][1]"
                                                                value="Email Marketing"
                                                                <?php echo (in_array("Email Marketing", $svcs) ? ' checked' : ''); ?>
                                                            >
                                                            <span>&nbsp;Email Marketing</span>
                                                        </label>
                                                    </p>
                                                </div>
                                                <div class="col-lg-6">
                                                    <p class="users-services-row">
                                                        <textarea
                                                            name="services[emark][2]"
                                                            class="form-control"
                                                        /><?php echo (!empty($emark) ? $emark['service_note'] : ''); ?></textarea>
                                                        <input
                                                            type="hidden"
                                                            name="services[emark][0]"
                                                            value="emark"
                                                        />
                                                    </p>
                                                </div>
                                                <div class="col-lg-4">
	                                                <?php if (!empty($emark) && $emark['last_updated'] != NULL) { ?>
                                                    <p class="users-services-row usr-last">
                                                        <i class="fa fa-calendar"></i>&nbsp;
	                                                    <?php echo (!empty($emark) && $emark['last_updated'] != NULL ? date("m/d/Y", strtotime($emark['last_updated'])) : ''); ?>
                                                        &raquo; <small><?php
		                                                    echo (!empty($emark) && $emark['last_updated_by'] != NULL ? $this->Users_model->get_user_info($emark['last_updated_by'])['first_name'] : '');
		                                                    echo ' ';
		                                                    echo (!empty($emark) && $emark['last_updated_by'] != NULL ? $this->Users_model->get_user_info($emark['last_updated_by'])['last_name'] : '');
                                                        ?></small>
                                                    </p>
	                                                <?php } ?>
                                                </div>
                                            </div>
                                            <!-- PPC / ppc -->
                                            <div class="row">
                                                <div class="col-lg-2">
                                                    <p class="users-services-row usr-first">
                                                        <label class="fancy-checkbox">
                                                            <input
                                                                type="checkbox"
                                                                name="services[ppc][1]"
                                                                value="PPC"
                                                                <?php echo (in_array("PPC", $svcs) ? ' checked' : ''); ?>
                                                            >
                                                            <span>&nbsp;PPC</span>
                                                        </label>
                                                    </p>
                                                </div>
                                                <div class="col-lg-6">
                                                    <p class="users-services-row">
                                                        <textarea
                                                            name="services[ppc][2]"
                                                            class="form-control"
                                                        /><?php echo (!empty($ppc) ? $ppc['service_note'] : ''); ?></textarea>
                                                        <input
                                                            type="hidden"
                                                            name="services[ppc][0]"
                                                            value="ppc"
                                                        />
                                                    </p>
                                                </div>
                                                <div class="col-lg-4">
	                                                <?php if (!empty($ppc) && $ppc['last_updated'] != NULL) { ?>
                                                    <p class="users-services-row usr-last">
                                                        <i class="fa fa-calendar"></i>&nbsp;
	                                                    <?php echo (!empty($ppc) && $ppc['last_updated'] != NULL ? date("m/d/Y", strtotime($ppc['last_updated'])) : ''); ?>
                                                        &raquo; <small><?php
		                                                    echo (!empty($ppc) && $ppc['last_updated_by'] != NULL ? $this->Users_model->get_user_info($ppc['last_updated_by'])['first_name'] : '');
		                                                    echo ' ';
		                                                    echo (!empty($ppc) && $ppc['last_updated_by'] != NULL ? $this->Users_model->get_user_info($ppc['last_updated_by'])['last_name'] : '');
                                                        ?></small>
                                                    </p>
	                                                <?php } ?>
                                                </div>
                                            </div>
                                            <!-- TLC / tlc -->
                                            <div class="row">
                                                <div class="col-lg-2">
                                                    <p class="users-services-row usr-first">
                                                        <label class="fancy-checkbox">
                                                            <input
                                                                type="checkbox"
                                                                name="services[tlc][1]"
                                                                value="TLC"
                                                                <?php echo (in_array("TLC", $svcs) ? ' checked' : ''); ?>
                                                            >
                                                            <span>&nbsp;TLC</span>
                                                        </label>
                                                    </p>
                                                </div>
                                                <div class="col-lg-6">
                                                    <p class="users-services-row">
                                                        <textarea
                                                            name="services[tlc][2]"
                                                            class="form-control"
                                                        /><?php echo (!empty($tlc) ? $tlc['service_note'] : ''); ?></textarea>
                                                        <input
                                                            type="hidden"
                                                            name="services[tlc][0]"
                                                            value="tlc"
                                                        />
                                                    </p>
                                                </div>
                                                <div class="col-lg-4">
	                                                <?php if (!empty($tlc) && $tlc['last_updated'] != NULL) { ?>
                                                    <p class="users-services-row usr-last">
                                                        <i class="fa fa-calendar"></i>&nbsp;
	                                                    <?php echo (!empty($tlc) && $tlc['last_updated'] != NULL ? date("m/d/Y", strtotime($tlc['last_updated'])) : ''); ?>
                                                        &raquo; <small><?php
		                                                    echo (!empty($tlc) && $tlc['last_updated_by'] != NULL ? $this->Users_model->get_user_info($tlc['last_updated_by'])['first_name'] : '');
		                                                    echo ' ';
		                                                    echo (!empty($tlc) && $tlc['last_updated_by'] != NULL ? $this->Users_model->get_user_info($tlc['last_updated_by'])['last_name'] : '');
                                                        ?></small>
                                                    </p>
	                                                <?php } ?>
                                                </div>
                                            </div>
                                            <!-- Domain Hosting / dh -->
                                            <div class="row">
                                                <div class="col-lg-2">
                                                    <p class="users-services-row usr-first">
                                                        <label class="fancy-checkbox">
                                                            <input
                                                                type="checkbox"
                                                                name="services[dh][1]"
                                                                value="Domain Hosting"
                                                                <?php echo (in_array("Domain Hosting", $svcs) ? ' checked' : ''); ?>
                                                            >
                                                            <span>&nbsp;Domain Hosting</span>
                                                        </label>
                                                    </p>
                                                </div>
                                                <div class="col-lg-6">
                                                    <p class="users-services-row">
                                                        <textarea
                                                            name="services[dh][2]"
                                                            class="form-control"
                                                        /><?php echo (!empty($dh) ? $dh['service_note'] : ''); ?></textarea>
                                                        <input
                                                            type="hidden"
                                                            name="services[dh][0]"
                                                            value="dh"
                                                        />
                                                    </p>
                                                </div>
                                                <div class="col-lg-4">
	                                                <?php if (!empty($dh) && $dh['last_updated'] != NULL) { ?>
                                                    <p class="users-services-row usr-last">
                                                        <i class="fa fa-calendar"></i>&nbsp;
	                                                    <?php echo (!empty($dh) && $dh['last_updated'] != NULL ? date("m/d/Y", strtotime($dh['last_updated'])) : ''); ?>
                                                        &raquo; <small><?php
		                                                    echo (!empty($dh) && $dh['last_updated_by'] != NULL ? $this->Users_model->get_user_info($dh['last_updated_by'])['first_name'] : '');
		                                                    echo ' ';
		                                                    echo (!empty($dh) && $dh['last_updated_by'] != NULL ? $this->Users_model->get_user_info($dh['last_updated_by'])['last_name'] : '');
                                                        ?></small>
                                                    </p>
	                                                <?php } ?>
                                                </div>
                                            </div>
                                            <!-- Plugin Upgrades / plug -->
                                            <div class="row">
                                                <div class="col-lg-2">
                                                    <p class="users-services-row usr-first">
                                                        <label class="fancy-checkbox">
                                                            <input
                                                                type="checkbox"
                                                                name="services[plug][1]"
                                                                value="Plugin Upgrades"
                                                                <?php echo (in_array("Plugin Upgrades", $svcs) ? ' checked' : ''); ?>
                                                            >
                                                            <span>&nbsp;Plugin Upgrades</span>
                                                        </label>
                                                    </p>
                                                </div>
                                                <div class="col-lg-6">
                                                    <p class="users-services-row">
                                                        <textarea
                                                            name="services[plug][2]"
                                                            class="form-control"
                                                        /><?php echo (!empty($plug) ? $plug['service_note'] : ''); ?></textarea>
                                                        <input
                                                            type="hidden"
                                                            name="services[plug][0]"
                                                            value="plug"
                                                        />
                                                    </p>
                                                </div>
                                                <div class="col-lg-4">
	                                                <?php if (!empty($plug) && $plug['last_updated'] != NULL) { ?>
                                                    <p class="users-services-row usr-last">
                                                        <i class="fa fa-calendar"></i>&nbsp;
	                                                    <?php echo (!empty($plug) && $plug['last_updated'] != NULL ? date("m/d/Y", strtotime($plug['last_updated'])) : ''); ?>
                                                        &raquo; <small><?php
		                                                    echo (!empty($plug) && $plug['last_updated_by'] != NULL ? $this->Users_model->get_user_info($plug['last_updated_by'])['first_name'] : '');
		                                                    echo ' ';
		                                                    echo (!empty($plug) && $plug['last_updated_by'] != NULL ? $this->Users_model->get_user_info($plug['last_updated_by'])['last_name'] : '');
                                                        ?></small>
                                                    </p>
	                                                <?php } ?>
                                                </div>
                                            </div>
                                            <!-- WordFence / wf -->
                                            <div class="row">
                                                <div class="col-lg-2">
                                                    <p class="users-services-row usr-first">
                                                        <label class="fancy-checkbox">
                                                            <input
                                                                type="checkbox"
                                                                name="services[wf][1]"
                                                                value="WordFence"
                                                                <?php echo (in_array("WordFence", $svcs) ? ' checked' : ''); ?>
                                                            >
                                                            <span>&nbsp;WordFence</span>
                                                        </label>
                                                    </p>
                                                </div>
                                                <div class="col-lg-6">
                                                    <p class="users-services-row">
                                                        <textarea
                                                            name="services[wf][2]"
                                                            class="form-control"
                                                        /><?php echo (!empty($wf) ? $wf['service_note'] : ''); ?></textarea>
                                                        <input
                                                            type="hidden"
                                                            name="services[wf][0]"
                                                            value="wf"
                                                        />
                                                    </p>
                                                </div>
                                                <div class="col-lg-4">
	                                                <?php if (!empty($wf) && $wf['last_updated'] != NULL) { ?>
                                                    <p class="users-services-row usr-last">
                                                        <i class="fa fa-calendar"></i>&nbsp;
	                                                    <?php echo (!empty($wf) && $wf['last_updated'] != NULL ? date("m/d/Y", strtotime($wf['last_updated'])) : ''); ?>
                                                        &raquo; <small><?php
		                                                    echo (!empty($wf) && $wf['last_updated_by'] != NULL ? $this->Users_model->get_user_info($wf['last_updated_by'])['first_name'] : '');
		                                                    echo ' ';
		                                                    echo (!empty($wf) && $wf['last_updated_by'] != NULL ? $this->Users_model->get_user_info($wf['last_updated_by'])['last_name'] : '');
                                                        ?></small>
                                                    </p>
	                                                <?php } ?>
                                                </div>
                                            </div>
                                            <!-- RackSpace Email / rse -->
                                            <div class="row">
                                                <div class="col-lg-2">
                                                    <p class="users-services-row usr-first">
                                                        <label class="fancy-checkbox">
                                                            <input
                                                                type="checkbox"
                                                                name="services[rse][1]"
                                                                value="RackSpace Email"
                                                                <?php echo (in_array("RackSpace Email", $svcs) ? ' checked' : ''); ?>
                                                            >
                                                            <span>&nbsp;RackSpace Email</span>
                                                        </label>
                                                    </p>
                                                </div>
                                                <div class="col-lg-6">
                                                    <p class="users-services-row">
                                                        <textarea
                                                            name="services[rse][2]"
                                                            class="form-control"
                                                        /><?php echo (!empty($rse) ? $rse['service_note'] : ''); ?></textarea>
                                                        <input
                                                            type="hidden"
                                                            name="services[rse][0]"
                                                            value="rse"
                                                        />
                                                    </p>
                                                </div>
                                                <div class="col-lg-4">
	                                                <?php if (!empty($rse) && $rse['last_updated'] != NULL) { ?>
                                                    <p class="users-services-row usr-last">
                                                        <i class="fa fa-calendar"></i>&nbsp;
	                                                    <?php echo (!empty($rse) && $rse['last_updated'] != NULL ? date("m/d/Y", strtotime($rse['last_updated'])) : ''); ?>
                                                        &raquo; <small><?php
		                                                    echo (!empty($rse) && $rse['last_updated_by'] != NULL ? $this->Users_model->get_user_info($rse['last_updated_by'])['first_name'] : '');
		                                                    echo ' ';
		                                                    echo (!empty($rse) && $rse['last_updated_by'] != NULL ? $this->Users_model->get_user_info($rse['last_updated_by'])['last_name'] : '');
                                                        ?></small>
                                                    </p>
	                                                <?php } ?>
                                                </div>
                                            </div>
                                            <!-- Maintenance Plan / mp -->
                                            <div class="row">
                                                <div class="col-lg-2">
                                                    <p class="users-services-row usr-first">
                                                        <label class="fancy-checkbox">
                                                            <input
                                                                type="checkbox"
                                                                name="services[mp][1]"
                                                                value="Maintenance Plan"
                                                                <?php echo (in_array("Maintenance Plan", $svcs) ? ' checked' : ''); ?>
                                                            >
                                                            <span>&nbsp;Maintenance Plan</span>
                                                        </label>
                                                    </p>
                                                </div>
                                                <div class="col-lg-6">
                                                    <p class="users-services-row">
                                                        <textarea
                                                            name="services[mp][2]"
                                                            class="form-control"
                                                        /><?php echo (!empty($mp) ? $mp['service_note'] : ''); ?></textarea>
                                                        <input
                                                            type="hidden"
                                                            name="services[mp][0]"
                                                            value="mp"
                                                        />
                                                    </p>
                                                </div>
                                                <div class="col-lg-4">
	                                                <?php if (!empty($mp) && $mp['last_updated'] != NULL) { ?>
                                                    <p class="users-services-row usr-last">
                                                        <i class="fa fa-calendar"></i>&nbsp;
	                                                    <?php echo (!empty($mp) && $mp['last_updated'] != NULL ? date("m/d/Y", strtotime($mp['last_updated'])) : ''); ?>
                                                        &raquo; <small><?php
		                                                    echo (!empty($mp) && $mp['last_updated_by'] != NULL ? $this->Users_model->get_user_info($mp['last_updated_by'])['first_name'] : '');
		                                                    echo ' ';
		                                                    echo (!empty($mp) && $mp['last_updated_by'] != NULL ? $this->Users_model->get_user_info($mp['last_updated_by'])['last_name'] : '');
                                                        ?></small>
                                                    </p>
	                                                <?php } ?>
                                                </div>
                                            </div>
                                            <!-- Accessibility / acc -->
                                            <div class="row">
                                                <div class="col-lg-2">
                                                    <p class="users-services-row usr-first">
                                                        <label class="fancy-checkbox">
                                                            <input
                                                                type="checkbox"
                                                                name="services[acc][1]"
                                                                value="Accessibility"
                                                                <?php echo (in_array("Accessibility", $svcs) ? ' checked' : ''); ?>
                                                            >
                                                            <span>&nbsp;Accessibility</span>
                                                        </label>
                                                    </p>
                                                </div>
                                                <div class="col-lg-6">
                                                    <p class="users-services-row">
                                                        <textarea
                                                            name="services[acc][2]"
                                                            class="form-control"
                                                        /><?php echo (!empty($acc) ? $acc['service_note'] : ''); ?></textarea>
                                                        <input
                                                            type="hidden"
                                                            name="services[acc][0]"
                                                            value="acc"
                                                        />
                                                    </p>
                                                </div>
                                                <div class="col-lg-4">
                                                    <?php if (!empty($acc) && $acc['last_updated'] != NULL) { ?>
                                                    <p class="users-services-row usr-last">
                                                        <i class="fa fa-calendar"></i>&nbsp;
	                                                    <?php echo (!empty($acc) && $acc['last_updated'] != NULL ? date("m/d/Y", strtotime($acc['last_updated'])) : ''); ?>
                                                        &raquo; <small><?php
		                                                    echo (!empty($acc) && $acc['last_updated_by'] != NULL ? $this->Users_model->get_user_info($acc['last_updated_by'])['first_name'] : '');
		                                                    echo ' ';
		                                                    echo (!empty($acc) && $acc['last_updated_by'] != NULL ? $this->Users_model->get_user_info($acc['last_updated_by'])['last_name'] : '');
                                                        ?></small>
                                                    </p>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><hr class="margin-top-10 margin-bottom-5 lightgrey-border-1">
                                <div id="domains"></div>
                                <div class="clearfix">
                                    <h4><strong>Domains</strong></h4>
                                    <div id="client_domains">
                                        <?php foreach ($client['domains'] as $i => $domain) { ?>
                                        <div class="input-group margin-bottom-15">
                                            <input type="hidden" name="domains_id[]" value="<?php echo $domain['id']; ?>" />
                                            <input
                                                class="form-control"
                                                type="text"
                                                name="domains[]"
                                                placeholder="www.domain.ext"
                                                value="<?php echo $domain['domain']; ?>"
                                                style="width:50%;"
                                            >
                                            <input
                                                class="form-control datepicker"
                                                type="text"
                                                name="domains_ada[]"
                                                placeholder="ADA Compliance Date"
                                                value="<?php echo $domain['ada']; ?>"
                                                style="width:50%;"
                                            >
                                            <span class="input-group-btn" onClick="confirm('Delete domain?')">
                                                <button class="btn btn-danger domain-delete" type="button">
                                                    <i class="fa fa-times"></i>&nbsp; Delete
                                                </button>
                                            </span>
                                        </div>
                                        <?php } ?>
                                    </div>
                                    <p>
                                    <div class="input-group">
                                        <button class="btn btn-default" type="button" id="domain_add">
                                            <i class="fa fa-plus"></i>&nbsp; Add Domain
                                        </button>
                                    </div>
                                    </p>
                                </div>
                            </form>
                            <div class="margin-top-15 text-right">
                                <form action="/admin/clients/" method="post" class="pull-right">
                                    <input type="hidden" name="cid" value="<?php echo $client['id']; ?>" />
                                    <button type="submit" class="btn btn-danger" onclick="confirmClientEditCancel();">Cancel</button>
                                </form>
                                <button type="button" class="btn btn-primary pull-right margin-right-15" onclick="$('#client_edit').submit();">Save</button>
                            </div>
                        </div>
                    </div>
                    <script>
                        function setDomainDeleteOnClick() {
                            $(".domain-delete").each(function() {
                                $(this).click(function() {
                                    var cid = '<?php echo $client['id']; ?>';
                                    var thisId = $(this).parent().parent().find(":hidden").val();
                                    if (thisId) {
                                        var url = "<?php echo base_url(); ?>clients/deleteDomain/" + cid + '/' + thisId;
                                        $.post(url).done(function(data) {
                                            var result = JSON.parse(data);
                                            if (result.msg == 'success') {
                                                $("#client_domains").find(":hidden").each(function(){
                                                    if ($(this).val() == thisId) {
                                                        $(this).parent().remove();
                                                    }
                                                });
                                                swal("Domain Deleted");
                                            } else {
                                                swal("Domain Not Deleted");
                                            }
                                        });
                                    } else {
                                        $(this).parent().parent().remove();
                                    }
                                });
                            });
                        };
                        function confirmClientEditCancel() {
                            if (confirm("Are you sure? Any unsaved information will be lost.")) window.location = '/admin/clients/';
                        };
                        function setHeight(jq_in){
                            jq_in.each(function(index, elem){
                                // This line will work with pure Javascript (taken from NicB's answer):
                                elem.style.height = (elem.scrollHeight+3)+'px';
                            });
                        }
                        $(document).ready(function() {
                            $("#services_all").on('click', function() {
                                $("#client_services .fancy-checkbox input").each(function() {
                                    $(this).prop("checked", true);
                                });
                            });
                            $("#services_none").on('click', function() {
                                $("#client_services .fancy-checkbox input").each(function() {
                                    $(this).prop("checked", false);
                                });
                            });
                            $("#domain_add").on('click', function() {
                                $("#client_domains").append(
                                    '<div class="input-group margin-bottom-15">' +
                                    '<input type="hidden" name="domains_id[]" value="" />' +
                                    '<input class="form-control" type="text" name="domains[]" placeholder="www.domain.ext" style="width:50%;">' +
                                    '<input class="form-control datepicker" type="text" name="domains_ada[]" placeholder="ADA Compliance Date" style="width:50%;">' +
                                    '<span class="input-group-btn">' +
                                    '<button class="btn btn-danger domain-delete" type="button">' +
                                    '<i class="fa fa-times"></i>&nbsp; Delete' +
                                    '</button>' +
                                    '</span>' +
                                    '</div>'
                                );
                                setDomainDeleteOnClick();
                                setDatepickersByClassName();
                            });
                            setDomainDeleteOnClick();
                            setDatepickersByClassName();
                            $('.users-services-row textarea').css("height", "34px");
                            setTimeout(function() {
                                setHeight($('.users-services-row textarea'));
                            }, 50);
                        });
                    </script>
				</div>
			</div>
