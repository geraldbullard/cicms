<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api_model extends CI_Model {

    var $client_service = "bp-api-client";
    var $auth_key = "bpapiauthkey";

    public function check_auth_client()
    {
		$client_service = $this->input->get_request_header('Client-Service', TRUE);
        $auth_key = $this->input->get_request_header('Auth-Key', TRUE);

        if ($client_service == $this->client_service && $auth_key == $this->auth_key) {
            return true;
        } else {
            return json_output(401, array('status' => 401,'message' => 'Unauthorized (Books_model)'));
        }
    }

    public function login($username, $password)
    {
        $q = $this->db->select('password, id')->from('ci_apiusers')->where('username', $username)->get()->row();

        if ($q == "") {
            return array('status' => 204,'message' => 'Username not found (Api_model)');
        } else {
            $hashed_password = $q->password;
            $id = $q->id;
            if (hash_equals($hashed_password, crypt($password, $hashed_password))) {
                // First let's empty the authentication table of any existing tokens
                $this->db->trans_start();
                $this->db->where('users_id', $id);
                $this->db->delete('ci_apiusers_authentication');
                $this->db->trans_commit();
                // Now do the work
                $last_login = date('Y-m-d H:i:s');
                $token = crypt(substr(md5(rand()), 0, 7), $this->config->config['encryption_key']);
                $expired_at = date("Y-m-d H:i:s", strtotime('+12 hours'));
                $this->db->trans_start();
                $this->db->where('id', $id)->update('ci_apiusers', array('last_login' => $last_login));
                $this->db->insert('ci_apiusers_authentication', array('users_id' => $id, 'token' => $token, 'expired_at' => $expired_at));
                if ($this->db->trans_status() === FALSE) {
                    $this->db->trans_rollback();
                    return array('status' => 500, 'message' => 'Internal server error (Api_model)');
                } else {
                    $this->db->trans_commit();
                    return array('status' => 200, 'message' => 'Successfully logged in (Api_model)', 'id' => $id, 'token' => $token);
                }
            } else {
                return array('status' => 204, 'message' => 'Wrong password (Api_model)');
            }
        }
    }

    public function logout()
    {
        $users_id = $this->input->get_request_header('User-ID', TRUE);
        $token = $this->input->get_request_header('Token', TRUE);
        $this->db->where('users_id', $users_id)->where('token', $token)->delete('ci_apiusers_authentication');
        return array('status' => 200, 'message' => 'Successfully logged out (Api_model)');
    }

    public function auth()
    {
        $users_id = $this->input->get_request_header('User-ID', TRUE);
        $token = $this->input->get_request_header('Auth-Token', TRUE);
        $q = $this->db->select('expired_at')->from('ci_apiusers_authentication')->where('users_id', $users_id)->where('token', $token)->get()->row();
        if ($q == "") {
            return json_output(401, array('status' => 401, 'message' => 'Unauthorized (Api_model)'));
        } else {
            if ($q->expired_at < date('Y-m-d H:i:s')) {
                return json_output(401, array('status' => 401, 'message' => 'Your session has expired (Api_model)'));
            } else {
                $updated_at = date('Y-m-d H:i:s');
                $expired_at = date("Y-m-d H:i:s", strtotime('+12 hours'));
                $this->db->where('users_id', $users_id)->where('token', $token)->update('ci_apiusers_authentication', array('expired_at' => $expired_at, 'updated_at' => $updated_at));
                return array('status' => 200, 'message' => 'Authorized (Api_model)');
            }
        }
    }

}
