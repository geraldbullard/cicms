<?php
/* API module menu file */

$_menu = array(
    'url' => '/admin/api/',
    'title' => 'API',
    'icon' => 'lnr lnr-cloud',
    'access' => 1,
);