<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
        <h3 class="page-title pull-left">API</h3>
        <!--<div class="pull-right">
            <a href="/admin/api/">
                <button class="btn btn-sm btn-primary" id="dt-add-row">
                    <i class="fa fa-plus"></i>&nbsp;&nbsp;Add API User
                </button>
            </a>
        </div>-->
        <?php if ($this->session->flashdata('msg')) { ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="alert alert-<?php echo $this->session->flashdata('msgtype'); ?> alert-dismissible action-alert" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">x</button>
                    <?php echo $this->session->flashdata('msg'); ?>
                </div>
            </div>
        </div>
        <?php } ?>
        <div class="row">
            <div class="col-lg-12">
                <div id="api">API Content</div>
            </div>
        </div>
        <script>
            $(document).ready(function () {
                // Nothing here
            });
        </script>
