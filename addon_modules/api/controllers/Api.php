<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends MY_Controller 
{

    public function __construct()
    {
		parent::__construct();

        $this->load->model('Api_model');

        $check_auth_client = $this->Api_model->check_auth_client();
        if ($check_auth_client != true) {
            die($this->output->get_output());
        }
    }

    // public function login()
    // {
    //     $method = $_SERVER['REQUEST_METHOD'];

    //     if ($method != 'POST') {
    //         json_output(400, array('status' => 400, 'message' => 'Bad controller login request'));
    //     } else {
    //         $params = $_REQUEST;
    //         $username = $params['username'];
    //         $password = $params['password'];
    //         $response = $this->Api_model->login($username, $password);
    //         json_output($response['status'], $response);
    //     }
    // }

    // public function logout()
    // {
    //     $method = $_SERVER['REQUEST_METHOD'];
    //     if ($method != 'POST') {
    //         json_output(400,array('status' => 400, 'message' => 'Bad controller logout request'));
    //     } else {
    //         $response = $this->Api_model->logout();
    //         json_output($response['status'], $response);
    //     }
    // }

    public function handle_request()
    {
        $request_method = $_SERVER['REQUEST_METHOD'];
        $request_uri = $_SERVER['REQUEST_URI'];

        // Extract endpoint from the URI
        // Examples: http://bpixel/api/books/
        //           http://bpixel/api/books/detail/
        $parts = explode('/', $request_uri);
        $endpoint = $parts[count($parts) - 1];

        // Forward the request based on the endpoint
        switch ($request_method) {
            case 'GET':
                $this->forward_to_controller($endpoint, 'get');
                break;
            case 'POST':
                $this->forward_to_controller($endpoint, 'post');
                break;
            case 'PUT':
                $this->forward_to_controller($endpoint, 'put');
                break;
            case 'DELETE':
                $this->forward_to_controller($endpoint, 'delete');
                break;
            default:
                json_output(400, array('status' => 400, 'message' => 'Unsupported Request Method'));
        }
    }

    private function forward_to_controller($endpoint, $method)
    {
		// Define mapping of endpoints to controllers
        $endpoint_to_controller = array(
            'books' => 'Books',
            'products' => 'Products',
            'categories' => 'Categories',
            // Add more endpoints and corresponding controller names as needed
        );

        // Check if the endpoint is mapped to a controller
        if (isset($endpoint_to_controller[$endpoint])) {
			$controller_name = $endpoint_to_controller[$endpoint];
            $this->load->controller($controller_name);
			die("STOP");
            $controller_instance = new $controller_name();
            $controller_instance->$method();
        } else {
            json_output(404, array('status' => 404, 'message' => 'Endpoint not found'));
        }
    }

}
