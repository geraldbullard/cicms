<?php
/* Module functions file */

// If installing the module run this function to create db table(s)
if (!function_exists('bp_installModule')) {
    function bp_installModule() {
        $CI =& get_instance();

        // Update the module status
        $CI->db->set('status', 1);
        $CI->db->where("module", "time");
        $CI->db->update($CI->db->dbprefix . "modules");

        // Create table(s)
        $CI->db->query("
			CREATE TABLE IF NOT EXISTS `" . $CI->db->dbprefix . "time_records` (
				`id` int(11) NOT NULL AUTO_INCREMENT,
				`users_id` int(10) UNSIGNED NOT NULL,
				`projects_id` int(11) NOT NULL,
				`clients_id` int(10) NOT NULL,
				`domains_id` int(11) NOT NULL,
				`categories_id` int(11) NOT NULL,
				`date` date NOT NULL,
				`start_time` time NOT NULL,
				`end_time` time NOT NULL,
				`notes` text,
				`billable` bit(1) DEFAULT b'1',
				PRIMARY KEY (`id`),
				KEY `bp_time_records_uid_idx` (`users_id`),
				KEY `bp_time_records_pid_idx` (`projects_id`),
				KEY `bp_time_records_cid_idx` (`clients_id`,`domains_id`,`categories_id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");
        $CI->db->query("
			CREATE TABLE IF NOT EXISTS `" . $CI->db->dbprefix . "time_records_to_tasks` (
				`time_records_id` int(11) NOT NULL,
				`tasks_id` int(11) NOT NULL,
				PRIMARY KEY (`time_records_id`,`tasks_id`),
				KEY `bp_time_records_to_tasks_tid_idx` (`tasks_id`),
				KEY `bp_time_records_to_tasks_trid_idx` (`time_records_id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");
        $CI->db->query("
			CREATE TABLE IF NOT EXISTS `" . $CI->db->dbprefix . "categories` (
				`id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
				`name` varchar(63) NOT NULL,
				`standard` tinyint(1) NOT NULL DEFAULT '0',
				PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        ");

        return true;
    }
}

// If uninstalling the module run this function to remove db entries if selected
if (!function_exists('bp_uninstallModule')) {
    function bp_uninstallModule() {
        $CI =& get_instance();

        // Update the module status
        $CI->db->set('status', -1);
        $CI->db->where("module", "time");
        $CI->db->update($CI->db->dbprefix . "modules");

        // Drop table(s)
        $CI->db->query("
            DROP TABLE IF EXISTS `" . $CI->db->dbprefix . "time_records`;
        ");
        $CI->db->query("
            DROP TABLE IF EXISTS `" . $CI->db->dbprefix . "time_records_to_tasks`;
        ");
        $CI->db->query("
            DROP TABLE IF EXISTS `" . $CI->db->dbprefix . "categories`;
        ");

        return true;
    }
}
