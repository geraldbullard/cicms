<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script>
    //TODO: Fix styling on the time entries footer on mobile
    //TODO: Fix responsiveness styling on the total hours footer.
    //TODO: Task allotted hours progress bar
    //TODO: Custom Category Stuff ** need to look into this.
    //TODO: Validation no longer returning and showing on the form.
    //TODO: Only load non-archived tasks into the task select.

    /*
    * Resets the #formErrorAlert element and hides it.
    * */
    function resetFormErrorAlert()
    {
        $("#formErrorAlert").text("");
        $("#formErrorAlert").hide();
    }

    /*
    * Resets all the projects, tasks and domain lists and hides them.
    * */
    function ResetLists()
    {
        $("#tc-domain-list").empty();
        $("#tc-domain-list").parent().addClass("hidden");
        $("#tc-client-list").empty();
        $("#tc-client-list").hide();
        $("#tc-task-select").empty();
        $("#tc-task-select").hide();
        $("#tc-project-list").empty();
        $("#tc-project-list").hide();
    }

    /*
    * Resets the entire time tracking form to the default state.
    * */
    function resetTimeForm()
    {
        $("#tc_category").val("0").change();
        $("#tc_client").val("");
        $("#tc_project").val("");
        $("#tc_edit_project").val("");
        $("#tc_description").val("");
        $("#entry_id").val("");
        $("#clients_id").val("");
        $("#domain_id").val("");
        $("#project_id").val("");
        $("#tc-domain-list").parent().addClass("hidden");
        $("#tc-task-select").parent().addClass("hidden");

        $("#start_hour").val("9").change();
        $("#start_minute").val("00").change();
        $("#start_ampm").val("am").change();
        $("#end_hour").val("9").change();
        $("#end_minute").val("15").change();
        $("#end_ampm").val("am").change();

        $("#billable").prop('checked', "checked").change();

        ResetLists();
        resetFormErrorAlert();
        resetSearches();
        closeAllLists();
        $('#tc_category').empty();
        LoadCategories("tc_category");
        resetButtons("add");
    }

    /*
    * Sets up the page to work with the selected week.
    * */
    function SetTrackTimeWeek(date)
    {
        let dayOfWeek = php_date("N", date);
        let mon = date - (dayOfWeek - 1)*24*60*60;
        let sun = date - (dayOfWeek - 7)*24*60*60;
        let previousWeek = date - (7*24*60*60);
        let nextWeek = date + (7*24*60*60);

        $("#WeekLabel").text("Week Of " + php_date("m/d", mon) + " - " + php_date("m/d", sun));
        $("#SetTimeTrackPreviousWeek").attr("onclick", "javascript:SetTrackTimeWeek(" + previousWeek  + ")");
        $("#SetTimeTrackNextWeek").attr("onclick", "javascript:SetTrackTimeWeek(" + nextWeek  + ")");
        $('#track-time-datepicker').val(php_date('Y-m-d', date));

        $('#week_start').val(mon).change();
        $('#week_end').val(sun).change();

        resetButtons("add");
        resetTimeForm();
        UpdateWeekHoursWorked();

        SetTrackTimeDay(date);
    }

    /*
    * Sets up the page to work with the selected day within the selected week.
    * */
    function SetTrackTimeDay(date, reset_buttons = true)
    {
        let dayOfWeek = php_date("N", date);
        let mon = date - (dayOfWeek - 1)*24*60*60;
        let tue = date - (dayOfWeek - 2)*24*60*60;
        let wed = date - (dayOfWeek - 3)*24*60*60;
        let thu = date - (dayOfWeek - 4)*24*60*60;
        let fri = date - (dayOfWeek - 5)*24*60*60;
        let sat = date - (dayOfWeek - 6)*24*60*60;
        let sun = date - (dayOfWeek - 7)*24*60*60;

        $("#SelectCurWeekMonday").attr("onclick", "javascript:SetTrackTimeDay(" + mon  + ")");
        $("#SelectCurWeekTuesday").attr("onclick", "javascript:SetTrackTimeDay(" + tue  + ")");
        $("#SelectCurWeekWednesday").attr("onclick", "javascript:SetTrackTimeDay(" + wed  + ")");
        $("#SelectCurWeekThursday").attr("onclick", "javascript:SetTrackTimeDay(" + thu  + ")");
        $("#SelectCurWeekFriday").attr("onclick", "javascript:SetTrackTimeDay(" + fri  + ")");
        $("#SelectCurWeekSaturday").attr("onclick", "javascript:SetTrackTimeDay(" + sat  + ")");
        $("#SelectCurWeekSunday").attr("onclick", "javascript:SetTrackTimeDay(" + sun  + ")");

        SetDayButtonsInactive();
        if (dayOfWeek == 1) { $("#SelectCurWeekMonday button:first-child").addClass("active"); }
        if (dayOfWeek == 2) { $("#SelectCurWeekTuesday button:first-child").addClass("active"); }
        if (dayOfWeek == 3) { $("#SelectCurWeekWednesday button:first-child").addClass("active"); }
        if (dayOfWeek == 4) { $("#SelectCurWeekThursday button:first-child").addClass("active"); }
        if (dayOfWeek == 5) { $("#SelectCurWeekFriday button:first-child").addClass("active"); }
        if (dayOfWeek == 6) { $("#SelectCurWeekSaturday button:first-child").addClass("active"); }
        if (dayOfWeek == 7) { $("#SelectCurWeekSunday button:first-child").addClass("active"); }

        ResetTimeCards(date);
        $('#week_start').val(mon).change();
        $('#week_end').val(sun).change();
        $('#track-time-datepicker').val(php_date('Y-m-d', date));
        if (reset_buttons) { resetButtons("add"); resetTimeForm(); }
        ResetHours();
        $("#date_timestamp").val(date).change();
    }

    /*
    * Reset the styling on the selected day buttons.
    * */
    function SetDayButtonsInactive()
    {
        $("#SelectCurWeekMonday button:first-child").removeClass("active");
        $("#SelectCurWeekTuesday button:first-child").removeClass("active");
        $("#SelectCurWeekWednesday button:first-child").removeClass("active");
        $("#SelectCurWeekThursday button:first-child").removeClass("active");
        $("#SelectCurWeekFriday button:first-child").removeClass("active");
        $("#SelectCurWeekSaturday button:first-child").removeClass("active");
        $("#SelectCurWeekSunday button:first-child").removeClass("active");
    }

    /*
    * Resets the day and week hours numbers on the bottom of the time cards list.
    * */
    function ResetHours()
    {
        $("#hoursWorked").text("Totals Day: " + 0);
        $("#weekHoursWorked").text("Totals Week: " + 0);
    }

    /*
    * Resets and Reloads the time-cards for the selected Day.
    * */
    function ResetTimeCards(date)
    {
        $("#TimeCards").empty();
        LoadTimeCards(date);
    }

    function UpdateWeekHoursWorked()
    {
        $.ajax({
            url: `/time/GetWeekHoursWorked/`,
            type: 'POST',
            data: {'week_start' : $("#week_start").val(), 'week_end' : $("#week_end").val()},
            dataType: "json",
            success: function(response) {
                $("#weekHoursWorked").text("Totals Week: " + (response));
            }
        });
    }

    /*
    * Loads the time-cards for the selected day from the database.
    * */
    function LoadTimeCards(date)
    {
        var hours_worked = 0;
        UpdateWeekHoursWorked();

        $.ajax({
            url: `/time/GetTimeRecords/`,
            type: 'POST',
            data: {'date' : php_date("Y-m-d", date), 'week_start' : $("#week_start").val(), 'week_end' : $("#week_end").val()},
            dataType: "json",
            success: function(response) {
                var SortedCards = [];

                /* Create the html objects and store them in an array */
                $.each(response, function(index, element) {
                    hours_worked += element.hours;

                    var startTimeArray = tConvert(element.start_time).split(":");
                    var endTimeArray = tConvert(element.end_time).split(":");

                    SortedCards.push({
                        HtmlObject:
                        `
                        <div class="well client-card relative">
                            <span class="icon"><i class="fa fa-clock"></i></span>

                            <a href="javascript:timeRecordEdit(${element.id})" id="editTimeById" dataid="${element.id}">
                                <h4>
                                    <i class="fa fa-pencil"></i>&nbsp;<strong>${element.client.client_name} - ${element.domain}</strong>
                                </h4>

                                ${element.category} <br/>
                                ${startTimeArray[0] + ":" +  startTimeArray[1] + " " + startTimeArray[3]} - ${endTimeArray[0] + ":" + endTimeArray[1] + " " + endTimeArray[3]} <br/>
                                ${element.hours} Hours
                            </a>

                            <div class="margin-bottom-1">
                                <a href="javascript:deleteTimeRecord(${element.id})" id="deleteTimeById" dataid="${element.id}"><i class="fa fa-trash pull-right client-content-well-trash"></i></a>
                                <br/>
                            </div>
                        </div>
                        `,
                        SortID: element.sort_id
                    });
                });

                SortedCards.sort((a, b) => a.SortID - b.SortID);

                $.each(SortedCards, function(index, element) {
                    $("#TimeCards").append(element.HtmlObject);
                });

                $("#hoursWorked").text("Totals Day: " + hours_worked);
            }
        });
    }

    /*
    * Opens a time card for editing by id, Load_day is an optional parameter only used for opening a time card for edit from outside the time module.
    * If loading from outside the time module pass load_day as true!
    * */
    function timeRecordEdit(id, load_day = false)
    {
        resetTimeForm();

        $.ajax({
            url: '/time/GetTimeRecord/' + id,
            type: 'GET',
            success: function(response) {
                /* Set edit form values from the ajax response */
                var results = JSON.parse(response);
                console.log(response);

                //Call for receiving time card edit call from outside the time module.
                if (load_day) { SetTrackTimeDay(toTimestamp(results['date']), false); }

                if (results['projects_id'] != null)
                {
                    $("#tc_project").val(`${results['client_name']['client_name']} - ${results['domain']} - ${results['category']}`).change();
                    $("#project_id").val(results['projects_id']).change();

                    $("#tc-task-select").append(`<option>Select a Domain</option> <option value="${results['task_id']}" selected>T${results['task_id']} - ${results['task_info']}</option>`);
                    $("#tc-task-select").show();
                    $("#tc-task-select").parent().removeClass("hidden");
                }
                else
                {
                    $("#tc_category").val(results['category_id']).change();
                    $("#tc_client").val(results['client_name']['client_name']).change();
                    $("#client_id").val(results['client_name']['client_id']).change();
                    $("#domain_id").val(results['domains_id']).change();
                    $("#tc-domain-list").parent().removeClass("hidden");
                    $("#tc-domain-list").empty();
                    $("#tc-domain-list").append(`<option>Select a Domain</option> <option value="${results['domains_id']}" selected>${results['domain']}</option>`);
                    $("#tc-domain-list").change();
                }

                //$("#tc_edit_project").val(results['project_name']['project_name']); TODO: Implement on backend. [Task integration]
                $("#tc_description").val(results['notes']);
                $("#track-time-datepicker").val(results['date']);
                $("#entry_id").val(id);

                const startTimeArray = tConvert(results['start_time']).split(":"); // 0 = Hour, 1 = Minute, 2 = Seconds, 3 = Am/Pm
                $("#start_hour").val(startTimeArray[0]).change();
                $("#start_minute").val(startTimeArray[1]).change();
                $("#start_ampm").val(startTimeArray[3].toLowerCase()).change();
                const endTimeArray = tConvert(results['end_time']).split(":"); // 0 = Hour, 1 = Minute, 2 = Seconds, 3 = Am/Pm
                $("#end_hour").val(endTimeArray[0]).change();
                $("#end_minute").val(endTimeArray[1]).change();
                $("#end_ampm").val(endTimeArray[3].toLowerCase()).change();

                if (results['billable'] == 1) { $("#billable").prop('checked', "checked").change(); } else { $("#billable").prop('checked', "").change(); }

                scrollToView('addTimeEntryForm');
            }
        });

        resetSearches();
        $("#updateTimeEntrySubmit").show();
        $("#cancelUpdateTimeEntry").show();
        $("#addTimeEntrySubmit").hide();
        console.log("Editing Time Entry ID: " + id);
    }

    /*
    * Given a specific input will set all of the form submit buttons to specific states.
    * */
    function resetButtons(type)
    {
        switch (type)
        {
            case "add":
                $("#addTimeEntrySubmit").show();
                $("#updateTimeEntrySubmit").hide();
                $("#cancelUpdateTimeEntry").hide();
                break;
            case "edit":
                $("#updateTimeEntrySubmit").show();
                $("#cancelUpdateTimeEntry").show();
                $("#addTimeEntrySubmit").hide();
                break;
            case "update":
                $("#addTimeEntrySubmit").show();
                $("#updateTimeEntrySubmit").hide();
                $("#cancelUpdateTimeEntry").hide();
                break;
            case "delete":
                $("#updateTimeEntrySubmit").hide();
                $("#cancelUpdateTimeEntry").hide();
                $("#addTimeEntrySubmit").show();
                break;
            case "default":
                $("#updateTimeEntrySubmit").hide();
                $("#cancelUpdateTimeEntry").hide();
                $("#addTimeEntrySubmit").show();
                break;
        }
    }

    /*
    * Deletes a specific time record from the database by id.
    * */
    function deleteTimeRecord(id)
    {
        swal({
            title: "Are you sure?",
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Yes",
            cancelButtonText: "Cancel",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }, function () {
            $.ajax({
                url: '/time/DeleteRecord/' + id,
                type: 'POST',
                dataType: "json",
                success: function(response) {
                    //TODO: Implement error messages when the status_codes are set to custom errors.
                    if (response['status_code'] == 0)
                    {
                        swal("Time entry deleted successfully!");
                        ResetTimeCards($("#date_timestamp").val());
                        resetTimeForm();
                        resetButtons("delete");
                    }
                    else
                    {
                        swal("Error occurred deleting the time entry.");
                    }
                }
            });
        })
    }

    /* Resets the search lists */ /* !Possibly redundant, Can probably remove in favor of resetLists()! */
    function resetSearches()
    {
        $('#tc-client-list').hide();
        $('#tc-client-list').empty();
        $('#tc-project-list').hide();
        $('#tc-project-list').empty();
    }

    async function selectClient(selection, idName) {
        let id = idName.split("-").pop();
        $('#tc_client').val(selection).change();
        $('#client_id').val(id).change();
        let domains = await getDomains(id);
        populateDomainList(domains, $('#tc-domain-list'));
        $('#tc-domain-list').parent().removeClass("hidden");
        domainSelectEventListener();
        resetSearches();
    }

    async function selectProject(selection, idName) {
        let id = idName.split("-").pop();
        $('#tc_project').val(selection).change();
        $('#project_id').val(id).change();
        let tasks = await getTasks(id);
        populateTaskList(tasks, $('#tc-task-select'));
        $("#tc-task-select").parent().removeClass("hidden").show();
        $("#tc-task-select").show();
        resetSearches();
    }

    function domainSelectEventListener() {
        $('#tc-domain-list').on("change", (e) => {
            $('#domain_id').val(e.target.value).change();
        });
    }

    function populateTaskList(listItems, target) {
        target.empty();
        target.append($('<option>', {
            value: 0,
            text: 'Select a Task'
        }));

        let element = $('#tc-task-select');
        for (let i of listItems) {
            element.append('<option value="' + i.id + '">' + 'T' + i.id + ' - ' + i.task + '</option>');
        }
    }

    function populateDomainList(listItems, target) {
        target.empty();
        target.append($('<option>', {
            value: 0,
            text: 'Select a Domain'
        }));

        let element = $('#tc-domain-list');

        for (let i of listItems) {
            element.append('<option value="' + i.id + '">' + i.domain + '</option>');
        }
    }

    /*
    * These are functions that need the document to be fully loaded to work correctly.
    * */
    $(document).ready(function() {
        /*
        * Set the active week to the current week and day when the page is first loaded.
        * */
        SetTrackTimeWeek(<?php echo time(); ?>);

        /*
        * Cleanup the buttons and categories just in case there is any weird caching conundrums going on.
        * */
        resetButtons("default");

        $('#tc_category').empty();
        LoadCategories("tc_category");

        /* Set up date-time picker. */
        $(function() {
            $("#track-time-datepicker").datepicker({
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                yearRange: "-100:+0",
                dateFormat: 'yy-mm-dd'
            });
        });

        /* From here on are all event listeners for buttons, text boxes and lists etc. */
        $("#cancelUpdateTimeEntry").on('click', function() {
           resetTimeForm();
           scrollToView('TimeCards');
        });

        $("#resetTimeForm").on('click', function() {
            resetTimeForm();
        });

        $("#tc_client").keyup(debounce(async (e) => {
            if (e.target.value != '' && e.which != 40 && e.which != 38 && e.which != 13 && e.which != 9) {
                closeAllLists();
                let res = await searchClients(e.target.value);
                populateSuggestionList($('#tc-client-list'), res, (target, i) => {
                    target.append('<div id="suggestion-id-' + i.id + '" class="suggestion-item suggestion-list-item">' + i.client_name + '</div>');
                });
                addSuggestionClickEventListener(selectClient);
                arrowSelect($('#tc-client-list'), selectClient);
            } else {
                if (e.which != 40 && e.which != 38 && e.which != 9) {
                    closeAllLists();
                    $('#tc-client-list').hide();
                }
            }
        }, 400));

        $('#tc_project').keyup(debounce(async (e) => {
            if (e.target.value != '' && e.which != 40 && e.which != 38 && e.which != 13 && e.which != 9) {
                closeAllLists();
                let res = await searchProjects(e.target.value);
                populateSuggestionList($('#tc-project-list'), res, (target, i) => {
                    target.append('<div id="suggestion-id-' + i.id + '" class="suggestion-item suggestion-list-item">' + i.client_name + ' - ' + i.domain + ' - ' + i.cat_name + '</div>');
                });
                addSuggestionClickEventListener(selectProject);
                arrowSelect($('#tc-project-list'), selectProject);
            } else {
                if (e.which != 40 && e.which != 38 && e.which != 9) {
                    closeAllLists();
                    //$('#tc-domain-list').hide();
                }
            }
        }, 400));

        $("#tc_client").keydown((e) => {
            if (e.which != 40 && e.which != 38 && e.which != 9) {
                closeAllLists();
                ResetLists();
                $("#tc_project").val("").change();
            }
        });

        $("#tc_project").keydown((e) => {
            if (e.which != 40 && e.which != 38 && e.which != 9) {
                closeAllLists();
                ResetLists();
                $("#tc_client").val("").change();
            }
        });

        //Submit the create time entry form via ajax, receive the response and display status message.
        $("#addTimeEntrySubmit").on('click', function() {
            swal({
                title: "Are you sure?",
                showCancelButton: true,
                confirmButtonClass: "btn-success",
                confirmButtonText: "Yes",
                cancelButtonText: "Cancel",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }, function () {
                //TODO: Complete PHP Validation
                var formData = $("#addTimeEntryForm").serialize();
                $.ajax({
                    url: '/time/CreateRecord/',
                    type: 'POST',
                    data: formData,
                    dataType: "json",
                    success: function(response) {
                        if (response['status_code'] == 0)
                        {
                            swal("Time entry added successfully!");
                            ResetTimeCards($("#date_timestamp").val());
                            resetTimeForm();
                            resetButtons("add");
                            scrollToView('TimeCards');
                        }
                        else
                        {
                            swal("Error occurred adding time entry.");
                            resetFormErrorAlert();
                            $("#formErrorAlert").append(response['status_message']);
                            $("#formErrorAlert").show();
                        }
                    }
                });
            })
        });

        //Submit the edit time entry form via ajax, receive the response and display status message.
        $("#updateTimeEntrySubmit").on('click', function() {
            swal({
                title: "Are you sure?",
                showCancelButton: true,
                confirmButtonClass: "btn-success",
                confirmButtonText: "Yes",
                cancelButtonText: "Cancel",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }, function () {
                //TODO: Complete PHP Validation
                var formData = $("#addTimeEntryForm").serialize();
                var id = $("#entry_id").val();
                $.ajax({
                    url: '/time/EditRecord/' + id,
                    type: 'POST',
                    data: formData,
                    dataType: "json",
                    success: function(response) {
                        console.log("Update success, checking status code: " + response['status_code']);
                        if (response['status_code'] == 0)
                        {
                            console.log("Evaluating success code");
                            swal("Time entry updated successfully!");
                            ResetTimeCards($("#date_timestamp").val());
                            resetTimeForm();
                            resetButtons("update");
                            scrollToView('TimeCards');
                        }
                        else
                        {
                            swal("Error updating time entry!");
                            resetFormErrorAlert();
                            $("#formErrorAlert").append(response['status_message']);
                            $("#formErrorAlert").show();
                        }
                    }
                });
            })
        });
    });
</script>