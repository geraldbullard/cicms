<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<script src="<?php echo base_url('assets/klorofil/scripts/time-reports.js'); ?>"></script>

<?php if ($this->session->flashdata('msg')) { ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="alert alert-<?php echo $this->session->flashdata('msgtype'); ?> alert-dismissible action-alert" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <?php echo $this->session->flashdata('msg'); ?>
            </div>
        </div>
    </div>
<?php } ?>

<?php
$time=time();
if( isset($_GET['date']) )
$time = $_GET['date'];

$dayOfWeek = date("N", $time);
$mon = $time - ($dayOfWeek - 1)*24*60*60;
$tue = $time - ($dayOfWeek - 2)*24*60*60;
$wed = $time - ($dayOfWeek - 3)*24*60*60;
$thu = $time - ($dayOfWeek - 4)*24*60*60;
$fri = $time - ($dayOfWeek - 5)*24*60*60;
$sat = $time - ($dayOfWeek - 6)*24*60*60;
$sun = $time - ($dayOfWeek - 7)*24*60*60;
?>
    <!-- Include local javascripts -->
    <?php include('scripts.php'); ?>

    <?php if (isset($_GET['reporting_time_edit'])): ?>
    <script>
        $(document).ready(function() {
            timeRecordEdit(<?php echo $_GET['reporting_time_edit']?>, true);
        });
    </script>
    <?php endif; ?>

    <h3 class="page-title hide-480 hide-940">        &nbsp;
        <div class="pull-left hide-480 hide-940">
            <a id="SetTimeTrackPreviousWeek">
                <button class="btn btn-sm btn-primary">«</button>
            </a>
            <span id="WeekLabel">Week of <?php echo date("m/d", $mon)?> - <?php echo date("m/d", $sun)?></span>
            <a id="SetTimeTrackNextWeek">
                <button class="btn btn-sm btn-primary">»</button>
            </a>
        </div>

        <div class="pull-right hide-480 hide-940" style="padding-bottom:10px;">
            <a onclick="javascript:SetTrackTimeDay(<?php echo $mon ?>)" id="SelectCurWeekMonday">
                <button class="btn btn-sm btn-primary <?php echo ($dayOfWeek == 1) ? 'active' : ''?>">M<span class="hide-below-480">onday</span></button>
            </a>
            <a onclick="javascript:SetTrackTimeDay(<?php echo $tue ?>)" id="SelectCurWeekTuesday">
                <button class="btn btn-sm btn-primary <?php echo ($dayOfWeek == 2) ? 'active' : ''?>">T<span class="hide-below-480">uesday</span></button>
            </a>
            <a onclick="javascript:SetTrackTimeDay(<?php echo $wed ?>)" id="SelectCurWeekWednesday">
                <button class="btn btn-sm btn-primary <?php echo ($dayOfWeek == 3) ? 'active' : ''?>">W<span class="hide-below-480">ednesday</span></button>
            </a>
            <a onclick="javascript:SetTrackTimeDay(<?php echo $thu ?>)" id="SelectCurWeekThursday">
                <button class="btn btn-sm btn-primary <?php echo ($dayOfWeek == 4) ? 'active' : ''?>">T<span class="hide-below-480">hursday</span></button>
            </a>
            <a onclick="javascript:SetTrackTimeDay(<?php echo $fri ?>)" id="SelectCurWeekFriday">
                <button class="btn btn-sm btn-primary <?php echo ($dayOfWeek == 5) ? 'active' : ''?>">F<span class="hide-below-480">riday</span></button>
            </a>
            <a onclick="javascript:SetTrackTimeDay(<?php echo $sat ?>)" id="SelectCurWeekSaturday">
                <button class="btn btn-sm btn-primary <?php echo ($dayOfWeek == 6) ? 'active' : ''?>">Sa<span class="hide-below-480">turday</span></button>
            </a>
            <a onclick="javascript:SetTrackTimeDay(<?php echo $sun ?>)" id="SelectCurWeekSunday">
                <button class="btn btn-sm btn-primary <?php echo ($dayOfWeek == 7) ? 'active' : ''?>">Su<span class="hide-below-480">nday</span></button>
            </a>
        </div>
    </h3>

    <div class="row time-main-page">
        <div class="col-sm-4 col-lg-3">
            <div class="panel panel-headline" style="height:70vh;">
                <div class="panel-heading padding-10">
                    <p>Time For Selected Day</p>
                </div>
                <div class="panel-body margin-bottom-15" id="TimeCards" style="padding-left:10px;padding-bottom:15px;border-left:1px solid #eee;height:79%;overflow-y:scroll;margin-left:10px;margin-right:10px;-moz-box-shadow: inset 0 -10px 10px -10px #ddd;-webkit-box-shadow: inset 0 -10px 10px -10px #ddd;box-shadow: inset 0 -10px 10px -10px #ddd;">
                    <!-- Procedurally loaded by javascript -->
                </div>
                <div class="panel panel-footer" style="padding-bottom:27px;margin-left:10px;margin-right:10px;">
                    <span class="pull-right" id="hoursWorked"></span><br/>
                    <span class="pull-right" id="weekHoursWorked"></span>
                </div>
            </div>
        </div>

        <div class="col-sm-8 col-lg-9 margin-bottom-30" id="clientdata">
            <div class="panel" style="padding: 3% 3% 3% 3%;">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="content">
                            <form id="addTimeEntryForm" name="addTimeEntrySubmit">
                                <div class="body">
                                    <h4 style="text-align: center">Select Or Create A Time Entry</h4>
                                    <p id="formErrorAlert" class="alert-danger" style="padding:1% 1% 1% 1%;" hidden></p>
                                    <p class="form-group">
                                        <label for="tc_client" id="tc_client_label">Client:</label>
                                        <input type="text" name="tc_client" id="tc_client" class="form-control" rows="1" placeholder="Start typing to see suggestions."/>
                                        <span id="tc-client-list" class="panel suggestion-list" hidden></span>
                                        <input id="domain_id" name="domain_id" type="text" class="no-dirty-check hidden" style="display:none;" autocomplete="off" value="">
                                        <input id="client_id" name="client_id" type="text" class="no-dirty-check hidden" style="display:none;" autocomplete="off" value="">
                                    </p>
                                    <p class="form-group hidden">
                                        <label for="tc-domain-list">Domain:</label>
                                        <select id="tc-domain-list" class="form-control"></select>
                                    </p>
                                    <p class="form-group">
                                        <label for="tc_category">Category:</label>
                                        <select name="tc_category" id="tc_category" class="form-control tc-selection" required></select>
                                    </p>
                                    <p class="form-group hidden" id="tc-custom-category">
                                        <label for="tc_custom_category">Custom Category (Other):</label>
                                        <input type="text" name="tc_custom_category" id="tc_custom_category" class="form-control">
                                    </p>
                                    <hr class="margin-bottom-10">
                                    <p class="form-group">
                                        <label for="tc_project" id="tc_project_label">Project:</label>
                                        <input type="text" name="tc_project" id="tc_project" class="form-control" rows="1" placeholder="Start typing to see suggestions."/>
                                        <span id="tc-project-list" class="panel suggestion-list" style="display:none;" hidden></span>
                                        <input id="project_id" name="project_id" type="text" class="no-dirty-check hidden" style="display:none;" autocomplete="off" value="">
                                    </p>
                                    <p class="form-group hidden">
                                        <label for="tc-task-select" id="tc_task_label">Task:</label>
                                        <select id="tc-task-select" name="tc-task-select" class="form-control">
                                            <option val="" disabled>Select Task</option>
                                        </select>
                                    </p>
                                    <p class="form-group">
                                        <label for="tc_description">Description:</label>
                                        <textarea name="tc_description" id="tc_description" class="form-control" rows="3" required minlength="3" placeholder="Enter time entry description here."></textarea>
                                    </p>
                                    <p class="form-group">
                                        <div class="time-section">
                                            <div class="edit-only">
                                                <label><span class="small-label">Date:</span></label>
                                                <input style="margin-bottom:10px;" id="track-time-datepicker" name="track-time-datepicker" type="text" class="no-dirty-check form-control" autocomplete="off" value="<?php echo date("Y-m-d"); ?>">
                                                <label><span class="small-label">Start:</span></label>
                                                <select name="start_hour" id="start_hour" class="timeSelect">
                                                    <option value="12">12</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                </select>
                                                <strong>:</strong>
                                                <select name="start_minute" id="start_minute" class="timeSelect">
                                                    <option value="00">00</option>
                                                    <option value="15">15</option>
                                                    <option value="30">30</option>
                                                    <option value="45">45</option>
                                                </select>
                                                <select name="start_ampm" id="start_ampm" class="timeSelect">
                                                    <option value="am">AM</option>
                                                    <option value="pm">PM</option>
                                                </select>
                                                <label>
                                                    <span class="small-label">End:</span>
                                                </label>
                                                <select name="end_hour" id="end_hour" class="timeSelect">
                                                    <option value="12">12</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                </select>
                                                <strong>:</strong>
                                                <select name="end_minute" id="end_minute" class="timeSelect">
                                                    <option value="00" >00</option>
                                                    <option value="15">15</option>
                                                    <option value="30">30</option>
                                                    <option value="45">45</option>
                                                </select>
                                                <select name="end_ampm" id="end_ampm" class="timeSelect">
                                                    <option value="am">AM</option>
                                                    <option value="pm">PM</option>
                                                </select>
                                            </div>
                                            <hr>
                                            <div class="edit-only">
                                                <div>
                                                    <label style="float: none;">
                                                        <input type="checkbox" name="billable" id="billable" value="1" checked="checked"><span class="small-label">&nbsp;Billable</span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="clear"></div>
                                            <hr>
                                            <div class="clear"></div>
                                            <!-- PHP call to session to get username? -->
                                            <div class="edit-only">
                                                <div>
                                                    <span class="small-label" style="float:left">Team Member:&nbsp;</span>
                                                    <div class="empl-selector edit-only">
                                                        <?php
                                                        $userId = $this->ion_auth->get_user_id();

                                                        $this->db->select('first_name, last_name');
                                                        $this->db->where('id', $userId);
                                                        $this->db->from('ci_users');
                                                        $this->db->limit('1');
                                                        $query = $this->db->get();

                                                        $user = array();
                                                        foreach ($query->result() as $row)
                                                        {
                                                            $user['first_name'] = $row->first_name;
                                                            $user['last_name'] = $row->last_name;
                                                        }
                                                        ?>
                                                        <span id="tt-employee-name"><?php echo $user['first_name'] . ' ' . $user['last_name'] . ' | User ID: ' . $userId; ?></span>
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </p>
                                </div>

                                <input id="user_id" name="user_id" type="text" class="no-dirty-check hidden" hidden autocomplete="off" value="<?php echo $userId; ?>">
                                <input id="entry_id" name="entry_id" type="text" class="no-dirty-check hidden" hidden autocomplete="off" value="">
                                <input id="date_timestamp" name="date_timestamp" type="text" class="no-dirty-check hidden" hidden autocomplete="off" value="">
                                <input id="week_start" name="week_start" type="text" class="no-dirty-check hidden" hidden autocomplete="off" value="<?php echo $mon; ?>">
                                <input id="week_end" name="week_end" type="text" class="no-dirty-check hidden" hidden autocomplete="off" value="<?php echo $sun; ?>">

                                <p class="form-group">
                                    <button type="button" id="addTimeEntrySubmit" class="btn btn-success btn-lg time-track-btn"><span class="">Track Time</span></button>
                                    <button type="button" id="updateTimeEntrySubmit" class="btn btn-success btn-lg" style="display:none"><span class="">Update Time Entry</span></button>
                                    <!--<button type="button" id="cancelUpdateTimeEntry" class="btn btn-danger" style="display:none"><span class="">Cancel</span></button>-->
                                    <button type="button" id="resetTimeForm" class="btn btn-danger pull-right" ><span class="">Reset Form</span></button>
                                </p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
