<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Time extends MY_Controller
{
	function __construct()
	{
		parent::__construct();
		// Get site wide settings first
		$this->Settings_model->loadSitewideSettings();

		// Load This Model
		$this->load->model('Time_model');
	}

	public function admin_index()
	{
		if (!$this->session->userdata('user_id'))
		{
			// set the url they were trtying to go to in session
			$this->session->set_userdata('page_url', current_url());

			//Redirect to login
			redirect('user/login');
		} else {
			$this->data = array();
			$this->data['title'] = 'Time Index';

			// load the config from the current module
			$this->load->config('time');
			$this->data['time_config'] = $this->config->config['time'];
			// Added for developer dropdown
			$this->data['dev_data'] = '';

			//TODO: Set up a sorted query to select and show time records for the current day from the db.

			$this->load->view('layout/admin/header.php', $this->data);
			$this->load->view('admin/index', $this->data);
			$this->load->view('layout/admin/footer.php', $this->data);
		}
	}

	/*
	 * Gets a list of categories from the database via the time model and returns them as a json object.
	 * */
	public function GetCategories()
    {
        echo json_encode($this->Time_model->GetCategories());
        return json_encode($this->Time_model->GetCategories());
    }

    /*
     * Gets a list of tasks from the database via the time model and returns them as a json object.
     * */
    public function GetTasks()
    {
        echo json_encode($this->Time_model->GetTasks($this->input->post('projectId')));
        return json_encode($this->Time_model->GetTasks($this->input->post('projectId')));
    }

    /*
     * Gets the total hours worked from the week by sending user id, week start and weekend to the database from the time model and returns a json encoded integer variable.
     * */
    public function GetWeekHoursWorked()
    {
        echo json_encode($this->Time_model->GetHoursWorkedForWeek($this->ion_auth->get_user_id(), $this->input->post('week_start'), $this->input->post('week_end')));
        return json_encode($this->Time_model->GetHoursWorkedForWeek($this->ion_auth->get_user_id(), $this->input->post('week_start'), $this->input->post('week_end')));
    }

    /* Edits the selected time entry by id */
    public function GetTimeRecord($id)
    {
        $this->db->select('*');
        $this->db->where('id', $id);
        $this->db->from('ci_time_records');
        $this->db->limit('1');
        $query = $this->db->get();

        $info = array();
        foreach ($query->result() as $row)
        {
            //If the task was created from a project, load the projects task information into the data.
            if ($row->projects_id != 0)
            {
                $project = $this->Time_model->GetProjectInfoByID($row->projects_id);
                $assocTask = $this->Time_model->GetTaskAssociationByID($id);
                $task = $this->Time_model->GetTaskByID($assocTask[0]['tasks_id']);

                $info['task_info'] = $task[0]['task'];
                $info['task_id'] = $assocTask[0]['tasks_id'];
                $info['projects_id'] = $row->projects_id;
                $info['client_name'] = $this->Time_model->GetClientNameByID($project[0]['clients_id']);
                $info['clients_id'] = $row->clients_id;

                $info['category'] = $this->Time_model->GetCategoryNameByID($project[0]['categories_id']);
                $info['category_id'] = $project[0]['categories_id'];
                $info['domains_id'] = $project[0]['domains_id'];
                $info['domain'] = $this->Time_model->GetDomainByID($project[0]['domains_id']);
                $info['project'] = $project[0];
            }
            else
            {
                $info['client_name'] = $this->Time_model->GetClientNameByID($row->clients_id);
                $info['clients_id'] = $row->clients_id;
                $info['category'] = $this->Time_model->GetCategoryNameByID($row->categories_id);
                $info['category_id'] = $row->categories_id;
                $info['domains_id'] = $row->domains_id;
                $info['domain'] = $this->Time_model->GetDomainByID($row->domains_id);
            }

            $info['date'] = $row->date;
            $info['id'] = $row->id;
            $info['users_id '] = $row->users_id;
            $info['start_time'] = $row->start_time;
            $info['end_time'] = $row->end_time;
            $info['notes'] = $row->notes;
            $info['billable'] = $row->billable;
        }

        echo json_encode($info);
        return json_encode($info);
    }

    /* Gets all time cards for the day */
    public function GetTimeRecords()
    {
        $time_cards = $this->Time_model->GetTimeCardsByDay(
            $this->ion_auth->get_user_id(),
            date("Y-m-d", strtotime($this->input->post('date')))
        );

        echo json_encode($time_cards);
        return json_encode($time_cards);
    }

    /* Validate that the passed fields are not empty/0/null */
    public function ValidateFields($fields = array())
    {
        foreach ($fields as $key => $field)
        {
            $cur = $key;
            if (empty($field))
            {
                return array(
                    'status_message' => $this->GetFieldError($cur),
                    'status_code' => $this->GetStatusCode($cur),
                );
            }
        }

        return array(
            'status_message' => 'Success!',
            'status_code' => '0',
        );
    }

    /* Returns error message to the validation method */
    public function GetFieldError($field)
    {
        switch ($field)
        {
            case 'client_name':
                return "Invalid Client.";
            case 'user_id':
                return "You must be logged in to use this function.";
            case 'tc_category':
                return "A Category is required!";
            case 'tc_description':
                return "A description is required!";
            case 'track-time-datepicker':
                return "A date is required!";
        }
    }

    /* Returns the error codes to the validation method */
    public function GetStatusCode($field)
    {
        switch ($field)
        {
            case 'client_name':
                return "901";
            case 'user_id':
                return "902";
            case 'tc_category':
                return "903";
            case 'tc_description':
                return "904";
            case 'track-time-datepicker':
                return "905";
        }
    }

    /* Sets the DATA array for update/creation methods */
    public function SetData()
    {
        /* Set the start and end time vars */
        $start_time = strtotime($_POST['start_hour'] + ($_POST['start_ampm'] == 'PM' && $_POST['start_hour'] < 12 ? 12 : 0) . ':' . $_POST['start_minute'] . ':00 ' . $_POST['start_ampm']);
        $end_time = strtotime($_POST['end_hour'] + ($_POST['end_ampm'] == 'PM' && $_POST['end_hour'] < 12 ? 12 : 0) . ':' . $_POST['end_minute'] . ':00 ' . $_POST['end_ampm']);

        /* Check if the user has a time machine */
        if ($start_time >= $end_time) {
            $response = array(
                'status_message' => "Do you have a time machine? If not, fix the the start and end times.",
                'status_code' => "901"
            );
            echo json_encode($response); //Echo ajax json response.
            return false;
        }

        $data = array();

        /* Check for basic empty fields, If project is selected rather than client omit the categories id field. */
        if ($this->input->post('project_id')) {
            $Validation = $this->ValidateFields(array(
                'user_id' => $this->input->post('user_id'),
                'tc_description' => $this->input->post('tc_description'),
                'track-time-datepicker' => $this->input->post('track-time-datepicker')
            ));
        }
        else
        {
            $Validation = $this->ValidateFields(array(
                'user_id' => $this->input->post('user_id'),
                'tc_category' => $this->input->post('tc_category'),
                'tc_description' => $this->input->post('tc_description'),
                'track-time-datepicker' => $this->input->post('track-time-datepicker')
            ));
        }

        if ($Validation['status_code'] != 0)
        {
            echo json_encode($Validation);
            return false;
        }

        $data['userId'] = $this->ion_auth->get_user_id();

        if ($this->input->post('project_id') != "")
        {
            $projectData = $this->Time_model->GetProjectInfoByID($this->input->post("project_id"));

            $data['clientId'] = $projectData[0]['clients_id'];
            $data['domainsId'] = $projectData[0]['domains_id'];
            $data['projectId'] = $this->input->post("project_id");
            $data['categoryId'] = $projectData[0]['categories_id'];
            $data['hours'] = ($end_time - $start_time) / 3600;
            $data['notes'] = $this->input->post('tc_description');
            $data['startTime'] = $start_time;
            $data['endTime'] = $end_time;
            $data['billable'] = $this->input->post("billable");
            $data['date'] = $this->input->post('track-time-datepicker');
            $data['task_id'] = $this->input->post('tc-task-select');
        }
        else
        {
            $data['clientId'] = $this->input->post("client_id");
            $data['client_name'] = $this->input->post("tc_client");
            $data['domainsId'] = $this->input->post("domain_id");
            $data['projectId'] = 0;
            $data['categoryId'] = $this->input->post('tc_category');
            $data['hours'] = ($end_time - $start_time) / 3600;
            $data['notes'] = $this->input->post('tc_description');
            $data['startTime'] = $start_time;
            $data['endTime'] = $end_time;
            $data['billable'] = $this->input->post("billable");
            $data['date'] = $this->input->post('track-time-datepicker');
        }

        return $data;
    }

    /* Create a time_entry and submit it to the database */
	public function CreateRecord()
    {
        if ($this->Time_model->AddTimeEntry($this->SetData()))
        {
            $response = array(
                'status_message' => "Success!",
                'status_code' => '0'
            );

            echo json_encode($response); //Echo ajax json response.
            return true;
        }
        else
        {
            $response = array(
                'status_message' => "Failed to add time entry!",
                'status_code' => "907"
            );
            echo json_encode($response); //Echo ajax json response.
            return false;
        }
    }

    /* Create the edit data array and edit the time entry by ID */
    public function EditRecord($id)
    {
        if ($this->Time_model->UpdateTimeEntry($this->SetData(), $id))
        {
            $response = array(
                'status_code' => '0',
                'status_message' => "Update time entry success!",
            );

            echo json_encode($response); //Echo ajax json response.
            return true;
        }
        else
        {
            $response = array(
                'status_message' => "Update Failed!",
                'status_code' => "907"
            );
            echo json_encode($response); //Echo ajax json response.
            return false;
        }
    }

    /* Deletes the time_record from the database by ID */
    //TODO: Return unknown error information back to the frontend.
    public function DeleteRecord($id)
    {
        $success = $this->Time_model->DeleteTimeEntry($id);
        if ($success)
        {
            $response = array(
                'status_message' => "Success.",
                'status_code' => "0"
            );
            echo json_encode($response); //Echo ajax json response.
            return true;
        }
        else {
            $response = array(
                'status_message' => "Unknown error occurred.",
                'status_code' => "1776"
            );
            echo json_encode($response); //Echo ajax json response.
            return false;
        }
    }
}
