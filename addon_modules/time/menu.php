<?php
/* Track Time module menu file */

$_menu = array(
    'url' => '/admin/time/',
    'title' => 'Time',
    'icon' => 'lnr lnr-clock',
    'access' => 1,
);