<?php
/* Time module initialization file */

// If uninstalling the content module run this function to remove db entries if selected
if (!function_exists('bp_initializeModule')) {
    function bp_initializeModule($db) {
        /**
            [dbdriver] => mysqli
            [dsn] =>
            [username] => root
            [password] =>
            [hostname] => localhost
            [database] => ci
            [subdriver] =>
            [dbprefix] => ci_
            [char_set] => utf8
            [dbcollat] => utf8_general_ci
        */
        $mysqli = new mysqli($db->hostname, $db->username, $db->password, $db->database);
        // Module table entry
        $mysqli->query("
            INSERT IGNORE INTO `" . $db->dbprefix . "modules` (`module`, `title`, `summary`, `theme`, `icon`, `sort`, `visibility`, `status`, `system`)
            VALUES ('time', 'Time', 'The Time Module Summary', 'adminTheme', 'lnr lnr-clock', 0, 1, -1, 0);
        ");
        // Developer Permissions
        $mysqli->query("
            INSERT IGNORE INTO `" . $db->dbprefix . "permissions` (`groups_id`, `module`, `access`, `override`)
            VALUES (1, 'time', 5, NULL);
        ");
        // Admin Permissions
        $mysqli->query("
            INSERT IGNORE INTO `" . $db->dbprefix . "permissions` (`groups_id`, `module`, `access`, `override`)
            VALUES (2, 'time', 4, NULL);
        ");
        // Settings
        // $mysqli->query("
        //     INSERT INTO `ci_settings` (`define`, `title`, `summary`, `value`, `type`, `edit`, `system`, `visibility`)
        //     VALUES ('settingDefine', 'Setting Name', 'The setting description/instructions.', 'Broken Pixel CMS', 'content', 1, 0, 1);
        // ");
        return true;
    }
}