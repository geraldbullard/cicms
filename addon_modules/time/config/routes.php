<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* Time Routing */
$route['admin/time'] = 'time/admin_index';
$route['admin/time/add'] = 'time/add';
$route['admin/time/getTime'] = 'time/getTime';
$route['admin/time/edit/(:any)'] = 'time/edit/$1';
$route['admin/time/update/(:any)'] = 'time/update/$1';
$route['admin/time/delete/(:any)'] = 'time/delete/$1';

