<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Time Model
 */
class Time_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    /* Returns client name by id */
    public function GetClientNameByID($id)
    {
        $this->db->select('*');
        $this->db->where('id', $id);
        $this->db->from('ci_clients');
        $this->db->limit('1');
        $query = $this->db->get();

        $info = array();
        foreach ($query->result() as $row) {
            $info['client_name'] = $row->client_name;
            $info['client_id'] = $row->id;
        }

        return $info;
    }

    /* Returns client info by name */
    public function GetClientInfoByName($name)
    {
        $this->db->select('*');
        $this->db->where('client_name', $name);
        $this->db->from('ci_clients');
        $this->db->limit('1');
        $query = $this->db->get();

        $data = array();
        foreach ($query->result() as $row)
        {
            $data['clientId'] = $row->id;
            $data['client_name'] = $row->client_name;
        }

        return $data;
    }

    //TODO: Figure out a solution for this.
    public function GetProjectNameByID($id)
    {
        $this->db->select('*');
        $this->db->where('id', $id);
        $this->db->from('ci_projects');
        $this->db->limit('1');
        $query = $this->db->get();

        $info = array();
        foreach ($query->result() as $row) {
            $info['projects_id'] = $row->client_name;
        }

        return $info['projects_id'];
    }

    /* Returns domain by id */
    public function GetDomainByID($id)
    {
        $this->db->select('*');
        $this->db->where('id', $id);
        $this->db->from('ci_domains');
        $this->db->limit('1');
        $query = $this->db->get();

        $info = array();
        foreach ($query->result() as $row) {
            $info['domain'] = $row->domain;
        }

        if (isset($info['domain'])) {return $info['domain']; } else {return null;}
    }

    /* Returns array of categories */
    public function GetCategories()
    {
        $this->db->select("*");
        $query = $this->db->get('ci_categories');

        $cats = array();
        foreach ($query->result() as $row)
        {
            $cats[$row->id] = array($row->id, $row->name);
        }

        return $cats;
    }

    /* Returns array of time cards for specific day */
    public function GetTimeCardsByDay($user, $day)
    {
        $this->db->select("*");
        $this->db->where('users_id', $user);
        $this->db->where('date', $day);
        $this->db->order_by('start_time', 'ASC');
        $this->db->order_by('end_time', 'ASC');
        $query = $this->db->get('ci_time_records');
        $cards = array();

        $sortid = 0;
        foreach ($query->result() as $row)
        {
            $card = array();
            $card['category'] = $this->GetCategoryNameByID($row->categories_id);
            $card['category_id'] = $row->categories_id;
            $card['id'] = $row->id;
            $card['date'] = str_replace("/", "-", date("Y-m-d", strtotime($this->input->post('date'))));
            $card['users_id '] = $row->users_id;
            $card['start_time'] = $row->start_time;
            $card['end_time'] = $row->end_time;
            $card['billable'] = $row->billable;
            $card['client'] = $this->GetClientNameByID($row->clients_id);
            $card['domain'] = $this->GetDomainByID($row->domains_id);
            $card['domain_id'] = $row->domains_id;
            $card['hours'] = (strtotime($row->end_time) - strtotime($row->start_time)) / 3600;
            $card['sort_id'] = $sortid;
            $sortid++;

            $cards[$row->id] = $card;
        }

        return $cards;
    }

    /* Returns array of tasks by project id */
    public function GetTasks($ProjectID)
    {
        $this->db->select('*');
        $this->db->where('projects_id', $ProjectID);
        $this->db->where('archived', 0);
        $query = $this->db->get('ci_tasks');
        return $query->result_array();
    }

    /* Gets the total hours for a given week */
    public function GetHoursWorkedForWeek($user, $mon, $sun)
    {
        $sql = "SELECT * FROM `ci_time_records` WHERE users_id = " . $user . " AND date BETWEEN '" . date("Y-m-d", $mon) . "' AND '" . date("Y-m-d", $sun) . "'";
        $query = $this->db->query($sql);

        $hours = 0;
        foreach ($query->result() as $row)
        {
            $hours += (strtotime($row->end_time) - strtotime($row->start_time)) / 3600;
        }

        return $hours;
    }

    /* Updates a time entry by id */
    public function UpdateTimeEntry($params = array(), $id)
    {
        $data = array(
            'projects_id' => $params['projectId'],
            'clients_id' => $params['clientId'],
            'domains_id' => $params['domainsId'],
            'categories_id ' => $params['categoryId'],
            'date' => date("Y-m-d", strtotime($params['date'])),
            'start_time' => date("H:i:s", intval($params['startTime'])),
            'end_time' => date("H:i:s", intval($params['endTime'])),
            'notes' => trim($params['notes']),
            'billable' => $params['billable'],
            'users_id' => $params['userId'],
        );

        $this->db->where('id', $id);
        $this->db->update('ci_time_records', $data);

        return $this->db->affected_rows() == 1 ? true : false;
    }

    /* Adds a new time entry with a param array of data values */
    public function AddTimeEntry($params = array())
    {
        $data = array(
            'projects_id' => $params['projectId'],
            'clients_id' => $params['clientId'],
            'domains_id' => $params['domainsId'],
            'categories_id ' => $params['categoryId'],
            'date' => date("Y-m-d", strtotime($params['date'])),
            'start_time' => date("H:i:s", intval($params['startTime'])),
            'end_time' => date("H:i:s", intval($params['endTime'])),
            'notes' => trim($params['notes']),
            'billable' => $params['billable'],
            'users_id' => $params['userId'],
        );

        $this->db->insert('ci_time_records', $data);

        if (isset($params['task_id']))
        {
            $this->AddTaskAssocication($this->db->insert_id(), $params['task_id']);
        }

        return $this->db->affected_rows() == 1 ? true : false;
    }

    public function GetProjectInfoByID($id)
    {
        $this->db->select('*');
        $this->db->where('id', $id);
        $query = $this->db->get('ci_projects');
        return $query->result_array();
    }

    public function AddTaskAssocication($entry_id, $task_id)
    {
        $data = array(
            'time_records_id' => $entry_id,
            'tasks_id' => $task_id
        );
        $this->db->insert('ci_time_records_to_tasks', $data);
    }

    public function GetTaskAssociationByID($id)
    {
        $this->db->select('*');
        $this->db->where('time_records_id', $id);
        $query = $this->db->get('ci_time_records_to_tasks');
        return $query->result_array();
    }

    public function GetTaskByID($id)
    {
        $this->db->select('*');
        $this->db->where('id', $id);
        $query = $this->db->get('ci_tasks');
        return $query->result_array();
    }

    /* Deletes a time entry by id */
    public function DeleteTimeEntry($id)
    {
        $this->db->where("id", $id);
        $this->db->delete('ci_time_records');
        return $this->db->affected_rows() == 1 ? true : false;
    }

    /* Gets the name of a category by its id */
    public function GetCategoryNameByID($id)
    {
        $this->db->select('name');
        $this->db->where('id', $id);
        $this->db->limit('1');
        $query = $this->db->get('ci_categories');

        $result = "";
        foreach ($query->result() as $row)
        {
            $result = $row->name;
        }

        return $result;
    }
}
