<?php

// API Login Funtion
function loginAPI() {
    $url = "http://bpixel/auth/login";
    $params = array(
        'username' => 'admin',
        'password' => 'Admin123$',
    );
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url . '?' . http_build_query($params));
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Client-Service: bp-api-client',
        'Auth-Key: bpapiauthkey',
    ));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    //curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    $response = curl_exec($ch);
    //echo $response;
    curl_close($ch);
    $result = json_decode($response);
    if ($result->status == '200') {
      return $result;
    } else {
      echo "Login Failure";
    }
}

// Call API Function
function callAPI($method, $url, $data, $apisession) {
    if ($apisession->status !== 200) {
        return json_encode(array('status', $result->status));
    } else {
        $curl = curl_init();
        // METHOD CONFIG:
        switch ($method) {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);
                if ($data) {
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                }
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
                if ($data) {
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                }
                break;
            default:
                if ($data) {
                    $url = sprintf("%s?%s", $url, http_build_query($data));
                }
        }
        // OPTIONS:
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Client-Service: bp-api-client',
            'Auth-Key: bpapiauthkey',
            'Content-Type: application/x-www-form-urlencoded',
            'User-ID: ' . $apisession->id,
            'Auth-Token: ' . $apisession->token,
        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        //curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        // EXECUTE:
        $result = curl_exec($curl);
        //echo $result;
        if ($result === null) {
            die('JSON decoding error: ' . json_last_error_msg());
        }
        curl_close($curl);
        return json_encode($result);
    }
}

// Login to API, on every call
$apisession = loginAPI();

// GET All Book Data //////////////////////////////////////////////////
$url = "http://bpixel/api/books/all";
$api_call = callAPI('GET', $url, false, $apisession);
$response = json_decode($api_call, true);
echo 'All:<br>' . $response . '<hr>';

// GET Single Book Data (ID: ?) ///////////////////////////////////////
$url = "http://bpixel/api/books/detail/2";
$api_call = callAPI('GET', $url, false, $apisession);
$response = json_decode($api_call, true);
echo 'Detail [2]:<br>' . $response . '<hr>';

// POST (Create) Book: /////////////////////////////////////////////////
// $data =  array(
//     'title' => 'Book Nine',
//     'author' => 'Author Nine',
//     'description' => 'Description Nine',
//     'current_version' => '2023',
// );
// $api_call = callAPI('POST', 'http://bpixel/books/create', json_encode($data), $apisession);
// $response = json_decode($api_call, true);
// echo 'Create:<br>' . $response . '<hr>';

// $newid = json_decode($response)->id;

// GET Single Book Data (ID: ?) ////////////////////////////////////////
// $url = "http://bpixel/books/detail/" . json_decode($response)->id;
// $api_call = callAPI('GET', $url, false, $apisession);
// $response = json_decode($api_call, true);
// echo 'Detail [' . $newid . ']:<br>' . $response . '<hr>';

// sleep(5); // Check DB

// (PUT) Update Book: //////////////////////////////////////////////////
// $data =  array(
//     'title' => 'Book Ten',
//     'author' => 'Author Ten',
//     'description' => 'Description Ten',
//     'current_version' => '2023',
// );
// $api_call = callAPI('PUT', 'http://bpixel/books/update/' . $newid, json_encode($data), $apisession);
// $response = json_decode($api_call, true);
// echo 'Update:<br>' . $response . '<hr>';

// GET Single Book Data (ID: ?) ////////////////////////////////////////
// $url = "http://bpixel/books/detail/" . $newid;
// $api_call = callAPI('GET', $url, false, $apisession);
// $response = json_decode($api_call, true);
// echo 'Detail [' . $newid . ']:<br>' . $response . '<hr>';

// (GET) Delete Book (ID ?): ///////////////////////////////////////////
// $api_call = callAPI('GET', 'http://bpixel/books/delete/' . $newid, false, $apisession);
// $response = json_decode($api_call, true);
// echo 'Delete [' . $newid . ']:<br>' . $response . '<hr>';
