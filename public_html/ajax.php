<?php
// include the CodeIgniter system functionality
$CI = require_once($_SERVER['DOCUMENT_ROOT'] . '/external.php');

// if the $_GET is set and it's not empty
if (isset($_GET) && $_GET != '') {
    // Delete User
	if (isset($_GET['deleteuser']) && $_GET['deleteuser'] != '') {
		if ($CI->ion_auth->delete_user($_GET['uid']) !== false) {
            $response_array['status'] = 'success';
			echo json_encode($response_array);
		} else {
			$response_array['status'] = 'error';
			echo json_encode($response_array);
		}
	}
    // Delete Role
	if (isset($_GET['deleterole']) && $_GET['deleterole'] != '') {
		if ($CI->ion_auth->delete_group($_GET['rid']) !== false) {
            $response_array['status'] = 'success';
			echo json_encode($response_array);
		} else {
			$response_array['status'] = 'error';
			echo json_encode($response_array);
		}
	}
    // Delete Setting
	if (isset($_GET['deletesetting']) && $_GET['deletesetting'] != '') {
		if ($CI->Settings_model->delete($_GET['sid']) !== false) {
            $response_array['status'] = 'success';
			echo json_encode($response_array);
		} else {
			$response_array['status'] = 'error';
			echo json_encode($response_array);
		}
	}
}
	
// if the $_POST is set and it's not empty
if (isset($_POST) && $_POST != '') {
	// if $_POST is set...
}

exit;
