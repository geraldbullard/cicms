<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
        <div class="vertical-align-wrap">
            <div class="vertical-align-middle">
                <div class="auth-box">
                    <div class="left">
                        <div class="content">
                            <div class="header">
                                <?php if (isset($msg) && $msg != '') { ?>
                                <div class="alert alert-danger action-alert margin-top-10" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    <div class="text-left"><?php echo $msg; ?></div>
                                </div>
                                <?php } ?>
                                <p class="lead">Reset Password</p>
                            </div>
	                        <?php
                            if ($hideform != 'true') {
                                echo form_open('/user/resetpw/?key=' . $this->input->get('key'));
                                echo form_label('Password:','password', array('class' => 'control-label pull-left')) . '<br />';
                                echo form_error('password','<div class="alert alert-danger" role="alert">','</div>');
                                echo form_password('password','', array('class' => 'form-control')) . '<br />';
                                echo form_label('Password Confirm:','password_confirm', array('class' => 'control-label pull-left')) . '<br />';
                                echo form_error('password_confirm','<div class="alert alert-danger" role="alert">','</div>');
                                echo form_password('password_confirm','', array('class' => 'form-control')) . '<br />';
                                echo form_submit('submit','Reset Password', array('class' => 'btn btn-sm btn-primary pull-left'));
                                //echo '<a href="/user/resendfpw/">';
                                //echo form_button('resend','Resend Forgot Password Email', array('class' => 'btn btn-sm btn-info pull-right'));
                                //echo '</a>';
                                echo form_close();
                            }
	                        ?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
