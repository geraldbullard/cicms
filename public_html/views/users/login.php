<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php
    $sayings = array(
        array(
            'saying' => 'The Login is not one to be trifled with.',
            'author' => ' -- The Resident Elder'
        ),
        array(
            'saying' => 'Show Me The Login!',
            'author' => ' -- Jerry McGuire'
        ),
        array(
            'saying' => 'Hey Good Lookin\'!',
            'author' => ' -- Hank Williams Sr.'
        ),
        array(
            'saying' => 'May the Login be with you.',
            'author' => ' -- Obi Wan Kenobi'
        ),
        array(
            'saying' => 'Houston, we have a Login.',
            'author' => ' -- John "Jack" Swigert'
        ),
        array(
            'saying' => 'Life moves pretty fast. If you don\'t stop and login once in a while, you could miss it.',
            'author' => ' -- Ferris Beuller'
        ),
        array(
            'saying' => 'I have no quarrel with you, good login screen, but I must cross!',
            'author' => ' -- King Arthur (Monty Python)'
        ),
	    array(
		    'saying' => 'This is my login screen! There are many like it, but this one is mine!',
		    'author' => ' -- Leonard "Gomer Pyle" Lawrence'
	    ),
	    array(
		    'saying' => 'Log me in Scotty.',
		    'author' => ' -- James T. Kirk'
	    ),
	    array(
		    'saying' => 'Go ahead, make my Login.',
		    'author' => ' -- Dirty Harry'
	    ),
	    array(
		    'saying' => 'This is a Login you can\'t refuse.',
		    'author' => ' -- Don Vito Corleone'
	    ),
	    array(
		    'saying' => 'If you build it, they will Login.',
		    'author' => ' -- Shoeless Joe Jackson'
	    ),
	    array(
		    'saying' => 'You had me at Login.',
		    'author' => ' -- Dorothy Boyd'
	    ),
    );
// get random index from array $arrX
$randIndex = array_rand($sayings);
?>
		<div class="vertical-align-wrap">
            <div class="vertical-align-middle">
                <div class="auth-box">
                    <div class="left">
                        <div class="content">
                            <div class="header">
                                <div class="login-logo"><img width="158" src="/assets/img/bp-logo-blk-sm.png" /></div>
                                <div class="auth-message">
                                    <?php
                                        if (isset($_SESSION['auth_message']) && $_SESSION['auth_message'] != '') {
                                            echo $_SESSION['auth_message'];
                                        }
                                    ?>
                                </div>
                                <p class="lead">Login to your account</p>
                            </div>
	                        <?php
                                echo form_open();
                                echo form_label('Username:','username', array('class' => 'control-label sr-only')) . '<br />';
                                echo form_error('username','<div class="alert alert-danger" role="alert">','</div>');
                                echo form_input('username','', array('class' => 'form-control', 'placeholder' => 'Username')) . '<br />';
                                echo form_label('Password:','password', array('class' => 'control-label sr-only')) . '<br />';
                                echo form_error('password','<div class="alert alert-danger" role="alert">','</div>');
                                echo form_password('password','', array('class' => 'form-control', 'placeholder' => 'Password', 'autocomplete' => 'on')) . '<br />';
                                //echo form_checkbox('remember','1',FALSE) . ' Remember Me?<br /><br />';
                                echo form_submit('submit','Log In', array('class' => 'btn btn-sm btn-primary pull-left'));
                                echo '<a href="/user/forgotpw/">';
                                echo form_button('forgotpw','Forgot Password', array('class' => 'btn btn-sm btn-info pull-right'));
                                echo '</a>';
                                echo form_close();
	                        ?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
