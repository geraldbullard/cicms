<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
        <div class="vertical-align-wrap">
            <div class="vertical-align-middle">
                <div class="auth-box">
                    <div class="left">
                        <div class="content">
                            <div class="header">
                                <?php if (isset($msg) && $msg != '') { ?>
                                <div class="alert alert-danger alert-dismissible action-alert" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    <div class="text-left"><?php echo $msg; ?></div>
                                </div>
                                <?php } ?>
                                <p class="lead">Forgotten Password</p>
                                <p>Check your email for the forgotten password link.</p>
                                <p>If you do not receive it within a few minutes check your spam folder.</p>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
