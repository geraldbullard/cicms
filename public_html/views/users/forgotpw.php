<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
        <div class="vertical-align-wrap">
            <div class="vertical-align-middle">
                <div class="auth-box">
                    <div class="left">
                        <div class="content">
                            <div class="header">
                                <?php if (isset($msg) && $msg != '') { ?>
                                <div class="alert alert-danger action-alert margin-top-10" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                    <div class="text-left"><?php echo $msg; ?></div>
                                </div>
                                <?php } ?>
                                <p class="lead">Forgotten Password</p>
                            </div>
	                        <?php
                                echo form_open();
                                echo form_label('Email:','email', array('class' => 'control-label pull-left')) . '<br />';
                                echo form_error('email','<div class="alert alert-danger" role="alert">','</div>');
                                echo form_input('email','', array('class' => 'form-control')) . '<br />';
                                echo form_submit('submit','Reset Password', array('class' => 'btn btn-sm btn-primary pull-left'));
                                echo '<a href="/user/login/">';
                                echo form_button('login','Back to Login', array('class' => 'btn btn-sm btn-info pull-right'));
                                echo '</a>';
                                echo form_close();
	                        ?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
