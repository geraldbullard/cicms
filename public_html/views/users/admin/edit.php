<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

// Check if exists and set a user image accordingly
if (!empty($this->session->userdata('file_name')) && file_exists($this->session->userdata('full_path'))) {
	$userimage = base_url('bp_uploads/' . $this->session->userdata('file_name'));
} else {
	$userimage = base_url('assets/klorofil/img/user-medium.png');
}

// Set the time zone to UTC
$utcTimeZone = new DateTimeZone('UTC');
// Set needed variables from timestamps
$created_on = $user[0]['created_on'];
// Create a DateTime object from the timestamps
$created_on = new DateTime('@' . $created_on);
$created_on->setTimezone($utcTimeZone);
// Adjust the date and time to -5 UTC offset
$offset = -5; // UTC offset in hours
$interval = DateInterval::createFromDateString($offset . ' hours');
// Apply the offset to the variables
$created_on->add($interval);
// Format the adjusted date and time for variables
$created_on = $created_on->format('m-d-Y h:i A');
// Now do that for last login
if ($user[0]['last_login'] != '') {
	$last_login = $user[0]['last_login'];
	$last_login = new DateTime('@' . $last_login);
	$last_login->setTimezone($utcTimeZone);
	$last_login->add($interval);
	$last_login = $last_login->format('m-d-Y h:i A');
} else {
	$last_login = '---';
}
?>
		<h3 class="page-title">
            <a href="/admin/users">Users</a> &raquo; <?php echo $title; ?>
        </h3>
		<?php if (isset($msg)) { ?>
		<div class="alert alert-<?php echo $msgtype; ?> alert-dismissible action-alert" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			<?php echo $msg; ?>
		</div>
		<?php } ?>
		<div class="panel panel-profile">
			<div class="clearfix">
				<!-- LEFT COLUMN -->
				<div class="profile-left">
					<!-- PROFILE HEADER -->
					<div class="profile-header">
						<div class="overlay"></div>
						<div class="profile-main">
							<img src="<?php echo $userimage; ?>" class="img-circle" width="100" alt="User Avatar">
							<h3 class="name">
								<?php echo $user[0]['first_name']; ?> <?php echo $user[0]['last_name']; ?>
							</h3>
							<span class="online-status status-<?php
								echo ($user[0]['active'] == 1 ? 'available' : 'unavailable');
							?>"><?php
								echo ($user[0]['active'] == 1 ? 'Active' : 'Inactive');
							?></span>
						</div>
						<!--<div class="profile-stat">
							<div class="row">
								<div class="col-md-4 stat-item">
									Created <span><?php echo $this->session->userdata('created_on'); ?></span>
								</div>
								<div class="col-md-4 stat-item">
									Last Login <span><?php echo $this->session->userdata('last_login'); ?></span>
								</div>
								<div class="col-md-4 stat-item">
									Active <span><?php echo $this->session->userdata('active'); ?></span>
								</div>
							</div>
						</div>-->
					</div>
					<!-- END PROFILE HEADER -->
					<!-- PROFILE DETAIL -->
					<div class="profile-detail">
						<div class="profile-info">
							<h4 class="heading">User Information</h4>
							<ul class="list-unstyled list-justify">
								<li>Date Created <span><?php
									echo $created_on;
								?></span></li>
								<li>Last Login <span><?php
									echo $last_login;
								?></span></li>
								<li>Status <span><?php
									echo ($user[0]['active'] > 0 ? 'Active' : 'Inactive');
								?></span></li>
								<li>Activity <span>See All</span></li>
							</ul>
                            <hr />
                            <?php if ($this->session->flashdata('msg')) { ?>
							<div class="row">
								<div class="col-lg-12">
									<div class="alert alert-<?php echo $this->session->flashdata('msgtype'); ?> action-alert" role="alert">
										<button type="button" class="close" data-dismiss="alert" aria-label="Close">x</button>
										<?php echo $this->session->flashdata('msg'); ?>
									</div>
								</div>
							</div>
                            <?php } ?>
							<h4 class="heading">User Images</h4>
							<div class="row profile-gallery">
								<div class="col-sm-3">
									<img class="profile-gallery-thumb" src="<?php echo base_url('bp_uploads/A1C16316-DBE0-11EC-B39D-16054EFBBEFF.jpg'); ?>" />
								</div>
								<div class="col-sm-3">
									<img class="profile-gallery-thumb" src="<?php echo base_url('bp_uploads/A1C16316-DBE0-11EC-B39D-16054EFBBEFF.jpg'); ?>" />
								</div>
								<div class="col-sm-3">
									<img class="profile-gallery-thumb" src="<?php echo base_url('bp_uploads/A1C16316-DBE0-11EC-B39D-16054EFBBEFF.jpg'); ?>" />
								</div>
								<div class="col-sm-3">
									<img class="profile-gallery-thumb" src="<?php echo base_url('bp_uploads/A1C16316-DBE0-11EC-B39D-16054EFBBEFF.jpg'); ?>" />
								</div>
							</div>
							<?php echo form_open_multipart('upload/do_upload'); ?>
								<input type="hidden" name="userid" value="<?php echo $user[0]['id']; ?>" />
								<input type="hidden" name="redirect" value="/users/edit/<?php echo $user[0]['id']; ?>" />
								<div class="row">
                                    <div class="col-sm-12">
                                        <input class="form-control" type="file" name="userfile" size="20" />
                                    </div><br /><br />
                                    <div class="col-sm-12">
                                        <button type="submit" class="btn btn-primary btn-block">Upload New Image</button>
                                    </div>
                                </div>
							</form>
						</div>
					</div>
					<!-- END PROFILE DETAIL -->
				</div>
				<!-- END LEFT COLUMN -->
				<!-- RIGHT COLUMN -->
				<div class="profile-right">
					<h4>Details</h4>
					<form action="" method="post" id="user_edit">
						<p>
                            <label for="first_name">First Name</label>
                            <input type="text" class="form-control" name="first_name" value="<?php echo $user[0]['first_name']; ?>">
                        </p>
                        <p>
                            <label for="last_name">Last Name</label>
                            <input type="text" class="form-control" name="last_name" value="<?php echo $user[0]['last_name']; ?>">
                        </p>
                        <p>
                            <label for="username">Username</label>
                            <input type="text" class="form-control" name="username" value="<?php echo $user[0]['username']; ?>">
                        </p>
                        <p>
                            <label for="email">Email</label>
                            <input type="text" class="form-control" name="email" value="<?php echo $user[0]['email']; ?>">
                        </p>
                        <p>
                            <label for="company">Company</label>
                            <input type="text" class="form-control" name="company" value="<?php echo $user[0]['company']; ?>">
                        </p>
                        <p>
                            <label for="phone">Phone</label>
                            <input type="text" class="form-control" name="phone" value="<?php echo $user[0]['phone']; ?>">
                        </p>
                        <p>
                            <label for="password">Password <small>(Leave empty to keep current password)</small></label>
                            <input type="password" class="form-control" id="password" name="password" value="">
                        </p>
                        <p>
                            <label for="passconf">Password Confirm</label>
                            <input type="password" class="form-control" id="passconf" name="passconf" value="">
                        </p>
						<p>
							<?php
								$enabled = ' hidden';
								if ($this->session->userdata('system_access')['roles'] > 1) {
									$enabled = '';
                                }
							?>
                            <label for="groups" class="control-label<?php echo $enabled; ?>">User Groups</label>
							<select class="form-control<?php echo $enabled; ?>" id="groups" name="groups[]" multiple>
                                <?php
                                foreach ($this->ion_auth->groups()->result() as $group) {
                                    // let's bypass the Developer role for non developers
                                    if ($group->id == 1 && !$this->ion_auth_model->in_group("developer", $this->session->userdata('user_id'))) {
                                        continue;
                                    }
                                    echo '<option value="' . $group->id . '"';
                                    echo ($this->ion_auth->in_group($group->id, $user[0]['id']) > 0 ? ' selected' : null);
                                    echo '>' . $group->description;
                                    echo '</option>';
                                }
                                ?>
							</select>
						</p>
                        <p class="text-right">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </p>
					</form>
				</div>
				<!-- END RIGHT COLUMN -->
			</div>
		</div>
