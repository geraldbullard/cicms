<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
        <h3 class="page-title">
            <a href="/admin/users">Users</a> &raquo; <?php echo $title; ?>
        </h3>
		<?php if (isset($msg)) { ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="alert alert-<?php echo $msgtype; ?> alert-dismissible action-alert" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <?php echo $msg; ?>
                </div>
            </div>
        </div>
		<?php } ?>
		<div class="panel">
			<div class="panel-body">
				<!-- RIGHT COLUMN -->
				<div class="clearfix">
					<h4>Details</h4>
					<form action="" method="post" id="user_add">
						<p>
                            <label for="first_name">First Name</label>
                            <input type="text" class="form-control" name="first_name" value="<?php echo set_value('first_name'); ?>">
                        </p>
                        <p>
                            <label for="last_name">Last Name</label>
                            <input type="text" class="form-control" name="last_name" value="<?php echo set_value('last_name'); ?>">
                        </p>
                        <p>
                            <label for="username">Username</label>
                            <input type="text" class="form-control" name="username" value="<?php echo set_value('username'); ?>">
                        </p>
                        <p>
                            <label for="email">Email</label>
                            <input type="text" class="form-control" name="email" value="<?php echo set_value('email'); ?>">
                        </p>
                        <p>
                            <label for="company">Company</label>
                            <input type="text" class="form-control" name="company" value="<?php echo set_value('company'); ?>">
                        </p>
                        <p>
                            <label for="phone">Phone</label>
                            <input type="text" class="form-control" name="phone" value="<?php echo set_value('phone'); ?>">
                        </p>
                        <p>
                            <label for="password">Password</label>
                            <input type="password" class="form-control" id="password" name="password" value="">
                        </p>
                        <p>
                            <label for="passconf">Password Confirm</label>
                            <input type="password" class="form-control" id="passconf" name="passconf" value="">
                        </p>
						<p>
                            <label for="groups">User Groups</label>
							<select class="form-control" id="groups" name="groups[]" multiple>
                                <?php
                                foreach ($this->ion_auth->groups()->result() as $group) {
                                    echo '<option value="' . $group->id . '">' . $group->description . '</option>';
                                }
                                ?>
							</select>
						</p>
                        <p class="text-right">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </p>
					</form>
				</div>
				<!-- END RIGHT COLUMN -->
			</div>
		</div>
