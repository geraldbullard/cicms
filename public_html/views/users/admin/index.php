<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
        <h3 class="page-title pull-left">Users</h3>
        <div class="pull-right">
            <a href="/admin/users/add/">
                <button class="btn btn-sm btn-primary" id="dt-add-row">
                    <i class="fa fa-plus"></i>&nbsp;&nbsp;Add User
                </button>
            </a>
        </div>
        <?php if ($this->session->flashdata('msg')) { ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="alert alert-<?php echo $this->session->flashdata('msgtype'); ?> alert-dismissible action-alert" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">x</button>
                    <?php echo $this->session->flashdata('msg'); ?>
                </div>
            </div>
        </div>
        <?php } ?>
        <div class="row">
            <div class="col-lg-12">
                <div id="usersListingTable"></div><hr />
            </div>
        </div>
        <!--<div>
            <?php
            /* maybe check locked out users and adjust row color, show unlock icon etc...
            foreach ($users as $u => $user) {
                if ($this->ion_auth->is_max_login_attempts_exceeded($user['username'])) {
                    echo $user['username'];
                }
            }*/
            ?>
        </div>-->
        <script>
            $(document).ready(function () {
                $("#clearFilter").on("click", function (evt) {
                    evt.preventDefault();
                    $('#users_filter').trigger("reset");
                    $.fn.loadUsers();
                    $("#clearFilter").blur();
                });
                $("#usersFilter").on("click", function (evt) {
                    evt.preventDefault();
                    $.fn.loadUsers();
                    $("#usersFilter").blur();
                });
                $.fn.loadUsers = function () {
                    $.blockUI({target: "#usersListingTable"});
                    $params = $("#users_filter").serialize();
                    $.get('<?php echo base_url('/admin/users/getUsers'); ?>?' + $params, function (data) {
                        $("#usersListingTable").html(data);
                        $.unblockUI({target: "#usersListingTable"});
                    }).fail(function () {
                        alert("Error!", "Something went wrong loading the Users!", "error");
                    });
                };
                $.fn.loadUsers();
            });
        </script>
