<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php if (!empty($users)) { ?>
        <div class="panel">
            <div class="panel-body">
                <table class="table table-striped" id="users-table" width="100%">
                    <thead>
                        <tr>
                            <th scope="col" width="10%">ID</th>
                            <th scope="col">Username</th>
                            <th scope="col">First Name</th>
                            <th scope="col">Last Name</th>
                            <th scope="col">Email</th>
                            <th scope="col">Phone</th>
                            <th scope="col">Company</th>
                            <th scope="col">Status</th>
                            <th scope="col" class="text-right">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($users as $user) {
                            if (
                                (
                                    $this->ion_auth_model->in_group("developer", $user['id']) &&
                                    $this->ion_auth_model->in_group("developer", $this->session->userdata('user_id'))
                                ) || !$this->ion_auth_model->in_group("developer", $user['id'])
                            ) {
                            // [id] => 1
                            // [ip_address] => 127.0.0.1
                            // [username] => admin
                            // [password] => $2y$12$iLc6Bll5g8SmsirEunkVt.ac4OAqhxzseAuoQAU6pOilhY94d1wM.
                            // [email] => admin@admin.com
                            // [activation_selector] =>
                            // [activation_code] =>
                            // [forgotten_password_selector] =>
                            // [forgotten_password_code] =>
                            // [forgotten_password_time] =>
                            // [remember_selector] =>
                            // [remember_code] =>
                            // [created_on] => 1268889823
                            // [last_login] => 1568842074
                            // [active] => 1
                            // [first_name] => Admin
                            // [last_name] => Istrator
                            // [company] => ADMIN
                            // [phone] => 9049049043
                        ?>
                        <tr>
                            <td><?php echo $user['id']; ?></td>
                            <td>
                                <?php echo $user['username']; ?>
                            </td>
                            <td>
	                            <?php echo $user['first_name']; ?>
                            </td>
                            <td>
	                            <?php echo $user['last_name']; ?>
                            </td>
                            <td>
	                            <?php echo $user['email']; ?>
                            </td>
                            <td>
	                            <?php echo $user['phone']; ?>
                            </td>
                            <td>
	                            <?php echo $user['company']; ?>
                            </td>
                            <td>
                                <?php echo ($user['active'] == 1 ? 'Active' : 'Inactive'); ?>
                            </td>
                            <td class="text-right">
                                <?php if ($user['id'] != 1) { if ($user['active'] == 1) { ?>
                                <a href="/admin/users/deactivate/<?php echo $user['id']; ?>" class="activate-deactivate">
                                    <span class="label label-warning">Deactivate</span></a>
	                            <?php } else { ?>
                                <a href="/admin/users/activate/<?php echo $user['id']; ?>" class="activate-deactivate">
                                    <span class="label label-info">Activate</span></a>
	                            <?php }} ?>
                                <a href="/admin/users/edit/<?php echo $user['id']; ?>">
                                    <span class="label label-primary">Edit</span></a>
                                <?php
                                if ($user['id'] != 1) {
                                    // Module Access or FPO (Feature Permission Override)
                                    if ($access >= 5 || isset($users_enable_disable) && $users_enable_disable >= 3) {
                                ?>
                                <a href="javascript:;" onclick="confirmDeleteUser(<?php echo $user['id']; ?>);">
                                    <span class="label label-danger">Delete</span></a>
                                <?php
                                    }
                                }
                                ?>
                            </td>
                        </tr>
                        <?php }} ?>
                    </tbody>
                </table>
            </div>
        </div>
        <script>
            function confirmDeleteUser(id) {
                swal({
                    title: "Delete User",
                    text: "Are you sure you wish to delete this User?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Delete",
                    closeOnConfirm: false
                },
                function() {
                    $.ajax({
                        url: '/ajax.php?deleteuser=1&uid=' + id,
                        type: 'GET',
                        success: function(response) {
                            var data = JSON.parse(response);
                            if (data.status == 'success') {
                                $.fn.loadUsers();
                                swal({
                                    title: "Success",
                                    text: "The User was successfully deleted.",
                                    type: "success",
                                    confirmButtonClass: "btn-success",
                                    confirmButtonText: "OK",
                                    showCancelButton: false
                                });
                            } else {
                                swal({
                                    title: "Error",
                                    text: "The was an error deleting the User.",
                                    type: "warning",
                                    confirmButtonClass: "btn-danger",
                                    confirmButtonText: "OK",
                                    showCancelButton: false
                                });
                            }
                        }
                    });
                });
            }
            jQuery(document).ready(function() {
                var $table = $('#users-table').DataTable({
                    "dom": 'Blftip',
                    "bSortCellsTop": true,
                    "pageLength": <?php echo $this->config->item('usersAdminPagination', 'settings'); ?>,
                    "lengthMenu": [
                        [<?php echo $this->config->item('usersAdminPagination', 'settings'); ?>, 30, 60, 100, -1],
                        [<?php echo $this->config->item('usersAdminPagination', 'settings'); ?>, 30, 60, 100, "All"]
                    ],
                    "order": [
                        [0, "desc"] // "desc" newest first, "asc" for oldest first
                    ],
                    responsive: true, // make table responsive
                    buttons: [ // relabel the export button
                        //'copy', 'excel'
                        { extend: 'excel', text: 'Export', className: 'btn btn-sm btn-primary shadow-sm' }
                    ],
                    "initComplete": function(settings, json) { // do something immediately after the table is drawn
                        //applyEnrollerEmailFilter($table);
                    },
                    "oLanguage": { // adjust the text for the rows dropdown
                        "sLengthMenu": "_MENU_ Rows"
                    },
                    "aoColumns": [ // needed to keep Actions col from being sortable and searchable
                        /* id */ { "bSearchable": true, "bSortable": true },
                        /* username */ { "bSearchable": true, "bSortable": true },
                        /* first_name */ { "bSearchable": true, "bSortable": true },
                        /* last_name */ { "bSearchable": true, "bSortable": true },
                        /* email */ { "bSearchable": true, "bSortable": true },
                        /* phone */ { "bSearchable": true, "bSortable": true },
                        /* company */ { "bSearchable": true, "bSortable": true },
                        /* status */ { "bSearchable": true, "bSortable": true },
                        /* actions */ { "bSearchable": false, "bSortable": false }
                    ]
                });
                $('.dt-buttons').css('float', 'right');
                $table.on('draw', function () {
                    // run a function or other action
                });
                // maybe check locked out users and adjust row color, show unlock icon etc...
                /*$table.rows().every(function(){
                    this.data()[1]
                });*/
                /*$('#dt-add-row').on('click', function() { // add new table row
                    // add row to db (ajax.php) and return id here for adding visible row to table
                    $table.row.add([
                        '3',
                        'username',
                        'first name',
                        'last name',
                        'email',
                        'phone',
                        'company',
                        'status',
                        'actions'
                    ]).draw(); // false if no redraw of table
                });*/
            });
        </script>
<?php } else { ?>
    None
<?php } ?>