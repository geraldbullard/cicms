<?php
//echo anchor('dashboard', 'Dashboard');
//echo $title;
//echo base_url('assets/bootstrap/css/bootstrap.css');// Get site wide modules info
$this->config->set_item('modules', $this->Modules_model->moduleInfo());

if (!empty($this->session->userdata('file_name')) && file_exists($this->session->userdata('full_path'))) {
	$userimage = base_url('bp_uploads/' . $this->session->userdata('file_name'));
} else {
	$userimage = base_url('assets/klorofil/img/user-medium.png');
}
?>
<!doctype html>
<html lang="en">

<head>
    <title><?php echo $title; ?> | <?php echo $this->config->item('siteName', 'settings'); ?></title>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <link href="<?php echo base_url('assets/klorofil/css/bootstrap.min.css'); ?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/vendor/jquery-ui/jquery-ui.min.css'); ?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/vendor/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/vendor/font-awesome/css/fa-all.min.css'); ?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/vendor/linearicons/style.min.css'); ?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/vendor/toastr/toastr.min.css'); ?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/klorofil/css/main.min.css'); ?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/vendor/bootstrap/css/bootstrap-slider.min.css'); ?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/klorofil/css/bp_custom.css'); ?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/klorofil/img/apple-icon.png'); ?>" rel="apple-touch-icon" sizes="76x76" />
    <link href="<?php echo base_url('assets/klorofil/img/favicon.png'); ?>" rel="icon" sizes="96x96" type="image/png" />
    <link href="<?php echo base_url('assets/vendor/sweetalert/sweetalert.css'); ?>" rel="stylesheet" />
    <link href="<?php echo base_url('assets/vendor/chartist/css/chartist-custom.css'); ?>" rel="stylesheet" />
    <link href="//cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet" />
    <link href="//cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.css" rel="stylesheet" />
    <link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet" />
    <script src="<?php echo base_url('assets/vendor/jquery/jquery.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/vendor/jquery-ui/jquery-ui.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/vendor/bage/Utils.js'); ?>"></script>
    <script src="<?php echo base_url('assets/vendor/bootstrap/js/bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/klorofil/scripts/klorofil-common.js'); ?>"></script>
    <script src="<?php echo base_url('assets/vendor/jquery-slimscroll/jquery.slimscroll.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/vendor/toastr/toastr.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/vendor/sweetalert/sweetalert.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/vendor/bootstrap/js/bootstrap-slider.js'); ?>"></script>
    <script src="<?php echo base_url('assets/vendor/jquery-ui/jquery-sortable-lists.min.js'); ?>"></script>
    <script src="<?php echo base_url('assets/vendor/chartist/js/chartist.min.js'); ?>"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery.blockUI/2.70/jquery.blockUI.min.js"></script>
    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="//cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="//cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    <script src="//cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.js"></script>
    <!--<link rel="stylesheet" type="text/css" href="<?php /*echo base_url('assets/grid/grideditor.css'); */?>" />
    <script src="<?php /*echo base_url('assets/grid/jquery.grideditor.js'); */?>"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/ckeditor/4.9.2/ckeditor.js"></script>-->
</head>

<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="brand">
                <a href="/admin/dashboard">
                    b<span class="hide-below-640">roken</span><span>P<span class="hide-below-640">IXEL</span></span></a>
            </div>
            <div class="container-fluid">
                <div class="navbar-btn">
                    <button type="button" class="btn-toggle-fullwidth"><i class="lnr lnr-arrow-left-circle"></i></button>
                </div>
                <!--<form class="navbar-form navbar-left">
					<div class="input-group">
						<input type="text" value="" class="form-control" placeholder="Search...">
						<span class="input-group-btn"><button type="button" class="btn btn-primary">Go</button></span>
					</div>
				</form>-->
                <div id="navbar-menu">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle icon-menu" data-toggle="dropdown">
                                <img src="<?php echo $userimage; ?>" class="img-circle" alt="Avatar">
                                <span><?php echo $this->Users_model->get_user_info(
                                    $this->session->userdata('user_id')
                                )['first_name'] . ' ' . $this->Users_model->get_user_info(
                                    $this->session->userdata('user_id')
                                )['last_name']; ?></span>
                                <i class="icon-submenu lnr lnr-chevron-down"></i></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="/admin/users/edit/<?php echo $this->session->userdata('user_id'); ?>">
                                        <i class="lnr lnr-user"></i> <span>My Profile</span></a>
                                </li>
                                <!--<li><a href="#"><i class="lnr lnr-envelope"></i> <span>Message</span></a></li>
								<li><a href="#"><i class="lnr lnr-cog"></i> <span>Settings</span></a></li>-->
                                <li><a href="/user/logout/"><i class="lnr lnr-exit"></i> <span>Logout</span></a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="/" class="icon-menu" title="View Website">
                                <i class="lnr lnr-home"></i>
                            </a>
                        </li>
                </div>
            </div>
        </nav>
        <div id="sidebar-nav" class="sidebar">
            <div class="sidebar-scroll">
                <nav>
                    <ul class="nav">
                        <li>
                            <a href="/admin/dashboard/" class="<?php echo (
                                $this->uri->segment(2) == 'dashboard' ? 'active' : null
                            ); ?>">
                                <i class="lnr lnr-screen"></i> <span>Dashboard</span></a>
                        </li>
                        <!-- START Other menu items will go here -->
                        <?php
                        $system_access = $this->session->userdata('system_access'); // @ added for non existing access per user
                        foreach ($this->config->config['modules'] as $m => $module) {
                            if ($module['system'] == 0 && $module['status'] == 1 && @$system_access[$module['module']] > 0) {
                                if (is_dir(APPPATH . 'modules/' . $module['module'])) {
                                    $_menu = null;
                                    $files = scandir(APPPATH . 'modules/' . $module['module']);
                                    foreach ($files as $f => $file) {
                                        if ($file == 'menu.php') {
                                            include(APPPATH . 'modules/' . $module['module'] . '/' . $file);
                                            if ($_menu) {
                                                $_menuaccess = 0;
                                                if (isset($_menu['access'])) $_menuaccess = $_menu['access'];
	                                            if (@$system_access[$module['module']] >= $_menuaccess) {
                                                    echo '<li>';
                                                    echo '<a href="';
                                                    if (isset($_menu['sub_items'])) {
                                                        echo '#sub' . ucfirst($module['module']);
                                                        echo '" data-toggle="collapse" aria-expanded="';
                                                        echo $this->uri->segment(2) == $module['module'] ? 'true' : 'false';
                                                        echo '"';
                                                    } else {
                                                        echo $_menu['url'] . '"';
                                                    }
                                                    echo ' class="';
                                                    echo ($this->uri->segment(2) == $module['module'] ? 'active' : 'collapsed');
                                                    echo '">';
                                                    echo '<i class="' . $_menu['icon'] . '"></i> <span>' . $_menu['title'] .
                                                        '</span>';
                                                    if (isset($_menu['sub_items']))
                                                        echo '<i class="icon-submenu lnr lnr-chevron-left"></i>';
                                                    echo '</a>';
                                                    if (isset($_menu['sub_items'])) {
                                                        echo '<div id="sub' . ucfirst($module['module']) . '" class="collapse';
                                                        echo $this->uri->segment(2) == $module['module'] ? ' in' : null;
                                                        echo '" aria-expanded="';
                                                        echo $this->uri->segment(2) == $module['module'] ? 'true' : 'false';
                                                        echo '">';
                                                        echo '<ul class="nav">';
                                                        foreach ($_menu['sub_items'] as $sub => $item) {
	                                                        $_itemaccess = 0;
	                                                        if (isset($item['access'])) $_itemaccess = $item['access'];
	                                                        if (@$system_access[$module['module']] >= $_itemaccess) {
                                                                echo '<li>';
                                                                echo '<a href="' . $item['url'] . '" class="';
                                                                if ($sub != $module['module'] && $this->uri->segment(3) != '') {
                                                                    echo $this->uri->segment(2) == $module['module'] &&
                                                                    $this->uri->segment(3) == $sub ? 'active' : null;
                                                                } else if ($sub == $module['module'] && $this->uri->segment(3) == '') {
                                                                    echo $this->uri->segment(2) == $module['module'] ? 'active' : null;
                                                                }
                                                                echo '">';
                                                                echo $item['title'];
                                                                echo '</a>';
                                                                echo '</li>';
	                                                        }
                                                        }
                                                        echo '</ul>';
                                                        echo '</div>';
                                                    }
                                                    echo '</li>';
	                                            }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        ?>
                        <!-- END Other menu items will go here -->
                        <?php if ($system_access['users'] > 2) { ?>
                        <li>
                            <a href="/admin/users/" class="<?php echo(
                            $this->uri->segment(2) == 'users' ? 'active' : null
                            ); ?>">
                                <i class="lnr lnr-users"></i> <span>Users</span></a>
                        </li>
                        <?php
                        }
                        if ($system_access['roles'] > 1) {
                        ?>
                        <li>
                            <a href="/admin/roles/" class="<?php echo (
                                $this->uri->segment(2) == 'roles' ? 'active' : null
                            ); ?>">
                                <i class="lnr lnr-lock"></i> <span>Roles</span></a>
                        </li>
                        <?php
                        }
                        if ($system_access['settings'] > 1) {
                        ?>
                        <li>
                            <a href="/admin/settings/" class="<?php echo (
                                ($this->uri->segment(1) != 'modules' && $this->uri->segment(2) == 'settings') ? 'active' : null
                            ); ?>">
                                <i class="lnr lnr-cog"></i> <span>Settings</span></a>
                        </li>
                        <li>
                            <a href="#subModules" data-toggle="collapse" aria-expanded="<?php echo (
                                ($this->uri->segment(2) == 'modules' && $this->uri->segment(3) == 'settings' ||
                                $this->uri->segment(2) == 'modules') ? 'true' : 'false'
                            ); ?>" class="<?php echo (
                                ($this->uri->segment(2) == 'modules' && $this->uri->segment(3) == 'settings' ||
                                $this->uri->segment(2) == 'modules') ? 'active' : 'collapsed'
                            ); ?>">
                                <i class="lnr lnr-layers"></i> <span>Modules</span>
                                <i class="icon-submenu lnr lnr-chevron-left"></i>
                            </a>
                            <div id="subModules" class="collapse<?php echo (
                                $this->uri->segment(2) == 'modules' ? ' in' : null
                            ); ?>" aria-expanded="<?php echo (
                                $this->uri->segment(2) == '' ? 'true' : 'false'
                            ); ?>">
                                <ul class="nav">
                                    <li>
                                        <a href="/admin/modules/" class="<?php echo (
                                            $this->uri->segment(2) == 'modules' && !$this->uri->segment(3)
                                            ? 'active' : null
                                        ); ?>">Core Modules</a>
                                    </li>
                                    <li>
                                        <a href="/admin/modules/addon/" class="<?php echo (
                                            $this->uri->segment(2) == 'modules' && $this->uri->segment(3) == 'addon'
                                            ? 'active' : null
                                        ); ?>">Addon Modules</a>
                                    </li>
                                    <li>
                                        <a href="/admin/modules/rci/" class="<?php echo (
                                            $this->uri->segment(2) == 'modules' && $this->uri->segment(3) == 'rci'
                                            ? 'active' : null
                                        ); ?>">RCI Plugins</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <?php
                        }
                        if ($system_access['audit'] > 0) {
                        ?>
                        <li>
                            <a href="/admin/audit/" class="<?php echo (
                                $this->uri->segment(2) == 'audit' ? 'active' : null
                            ); ?>">
                                <i class="lnr lnr-history"></i> <span>Audit</span></a>
                        </li>
                        <?php } ?>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="main">
            <div class="main-content">
                <div class="container-fluid">
                    <?php
					if (
                        $this->ion_auth->in_group('developer')
                    ) {
					?>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel debug-panel margin-bottom-15">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Developer Debug Panel</h3>
                                    <div class="right">
                                        <span class="hide-below-640">PHP Version: <strong><?php echo phpversion(); ?></strong>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;</span>
                                        <span class="hide-below-640">CodeIgniter <strong><?php echo CI_VERSION; ?></strong>&nbsp;</span>
                                        <button type="button" class="btn-toggle-dev-debug"><i class="lnr lnr-chevron-down"></i></button>
                                    </div>
                                </div>
                                <div class="panel-body" style="display: none;">
                                    SESSION USERDATA:
                                    <pre><?php print_r($this->session->userdata); ?></pre>
                                    SESSION USER:
                                    <pre><?php print_r($this->Users_model->get_user_info(
										$this->session->userdata('user_id')
									)); ?></pre>
                                    THIS DATA:
                                    <pre><?php print_r($dev_data); ?></pre>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
					}
					?>