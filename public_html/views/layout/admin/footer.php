				</div>
			</div>
		</div>
		<div class="clearfix"></div>
		<footer>
			<div class="container-fluid">
				<p class="copyright">&copy; <?php echo date("Y"); ?> <strong>brokenPIXEL</strong>. All Rights Reserved &nbsp;&nbsp;::&nbsp;&nbsp; Built on CodeIgniter <?php echo CI_VERSION; ?></p>
			</div>
		</footer>
	</div>
    <button onclick="topFunction();" id="toTopBtn" style="display:none;">Top</button>
</body>
</html>