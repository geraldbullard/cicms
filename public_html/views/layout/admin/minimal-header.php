<?php
//echo anchor('dashboard', 'Dashboard');
//echo $title;
//echo base_url('assets/bootstrap/css/bootstrap.css');
?>
<!doctype html>
<html lang="en" class="fullscreen-bg">
<head>
	<title><?php echo $title; ?> | brokenPIXEL</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<link href="<?php echo base_url('assets/vendor/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet" />
	<link href="<?php echo base_url('assets/vendor/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" />
	<link href="<?php echo base_url('assets/vendor/linearicons/style.min.css'); ?>" rel="stylesheet" />
	<link href="<?php echo base_url('assets/vendor/toastr/toastr.min.css'); ?>" rel="stylesheet" />
	<link href="<?php echo base_url('assets/klorofil/css/main.min.css'); ?>" rel="stylesheet" />
	<link href="<?php echo base_url('assets/klorofil/css/bp_custom.css'); ?>" rel="stylesheet" />
	<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet" />
	<link href="<?php echo base_url('assets/klorofil/img/apple-icon.png'); ?>" rel="apple-touch-icon" sizes="76x76" />
	<link href="<?php echo base_url('assets/klorofil/img/favicon.png'); ?>" rel="icon" sizes="96x96" type="image/png" />
    <style>
        body {
            /*background-image: url();
            height: 100%;
            background-position: center top;
            background-repeat: no-repeat;
            background-size: cover;*/
        }
        body::after {
            content: "";
            background: url('/assets/img/login/<?php echo mt_rand(1, 8); ?>.jpg');
            opacity: 0.3;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            position: absolute;
            background-position: center top;
            background-repeat: no-repeat;
            background-size: cover;
            z-index: -1;
        }
    </style>
</head>
<body>
	<div id="wrapper">
