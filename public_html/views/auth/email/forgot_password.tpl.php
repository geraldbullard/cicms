<?php
    $user = $this->Users_model->get_user_info_by_email($identity);
    /*
    [id] => 1
    [ip_address] => 127.0.0.1
    [username] => developer
    [password] => $2y$12$iLc6Bll5g8SmsirEunkVt.ac4OAqhxzseAuoQAU6pOilhY94d1wM.
    [email] => dev@mail.ext
    [activation_selector] =>
    [activation_code] =>
    [forgotten_password_selector] => fc19b5d988bf0ba898fd
    [forgotten_password_code] => $2y$10$OvD8HtDiNDuhF8pS3y8I/OUYivtvNvm/jav3Q3.FogOIwk6cQm3fq
    [forgotten_password_time] => 1611631896
    [remember_selector] =>
    [remember_code] =>
    [created_on] => 1268889823
    [last_login] => 1611626273
    [active] => 1
    [first_name] => Top
    [last_name] => Developer
    [company] => Development Company
    [phone] => 2147483647
    */
?>
<html>
<body>
Hello <?php echo $user['first_name']; ?>,<br><br>
Click the following link to reset your forgotten password:
<?php echo 'http://ci/user/resetpw/?key=' . $forgotten_password_code; ?>
</body>
</html>