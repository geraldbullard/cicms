<?php
/* Eterna Shortcodes */

// Manually include the Shortcode class file for debugging
if (!class_exists('Shortcode')) {
    require_once APPPATH . 'libraries/Shortcode.php';
}

// About brokenPIXEL
function aboutbrokenpixel() {
    $CI =& get_instance(); // Always include the CodeIgniter Instance
    ob_start(); 
    echo 'Cras fermentum odio eu feugiat lide par naso tierra. Justo eget nada terra videa magna derita valies darta donna mare.';
    $output_string = ob_get_contents();
    ob_end_clean();

    return $output_string;
}
Shortcode::add('aboutbrokenpixel', 'aboutbrokenpixel');

// About brokenPIXEL
function brokenpixelcontactinfo() {
    $CI =& get_instance(); // Always include the CodeIgniter Instance
    ob_start(); 
    echo 'A108 Adam Street<br>
        New York, NY 535022<br>
        United States <br>
        <strong>Phone:</strong> +1 5589 55488 55<br>
        <strong>Email:</strong> info@example.com<br>';
    $output_string = ob_get_contents();
    ob_end_clean();

    return $output_string;
}
Shortcode::add('brokenpixelcontactinfo', 'brokenpixelcontactinfo');