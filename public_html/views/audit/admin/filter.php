<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php if (!empty($audits)) { ?>
        <div class="panel">
            <div class="panel-body">
                <table class="table table-striped" id="audits-table" width="100%">
                    <thead>
                        <tr>
                            <th scope="col" width="10%">ID</th>
                            <th scope="col">Action</th>
                            <th scope="col">URL</th>
                            <th scope="col">User</th>
                            <th scope="col">IP</th>
                            <th scope="col">Date/Time</th>
                            <th scope="col">Module</th>
                            <th scope="col" class="text-right">Related ID</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($audits as $audit) {
                        ?>
                        <tr>
                            <td><?php echo $audit['id']; ?></td>
                            <td><?php echo $audit['message']; ?></td>
                            <td><?php echo $audit['url']; ?></td>
                            <td><?php echo $audit['users_id']; ?></td>
                            <td><?php echo $audit['remote_host']; ?></td>
                            <td><?php echo date("m/d/Y h:i A", strtotime($audit['datetime'])); ?></td>
                            <td><?php echo $audit['module']; ?></td>
                            <td class="text-right" style="padding-right:10px;"><?php echo $audit['related_id']; ?></td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <script>
            jQuery(document).ready(function() {
                var $table = $('#audits-table').DataTable({
                    "dom": 'Blftip',
                    "bSortCellsTop": true,
                    "pageLength": <?php echo $this->config->item('auditsAdminPagination', 'settings'); ?>,
                    "lengthMenu": [
                        [<?php echo $this->config->item('auditsAdminPagination', 'settings'); ?>, 30, 60, 100, -1],
                        [<?php echo $this->config->item('auditsAdminPagination', 'settings'); ?>, 30, 60, 100, "All"]
                    ],
                    "order": [
                        [0, "desc"] // "desc" newest first, "asc" for oldest first
                    ],
                    responsive: true, // make table responsive
                    buttons: [ // relabel the export button
                        //'copy', 'excel'
                        { extend: 'excel', text: 'Export', className: 'btn btn-sm btn-primary shadow-sm' }
                    ],
                    "initComplete": function(audits, json) { // do something immediately after the table is drawn
                        //applyEnrollerEmailFilter($table);
                    },
                    "oLanguage": { // adjust the text for the rows dropdown
                        "sLengthMenu": "_MENU_ Rows"
                    },
                    "aoColumns": [ // needed to keep Actions col from being sortable and searchable
                        /* id */ { "bSearchable": true, "bSortable": true },
                        /* url */ { "bSearchable": true, "bSortable": true },
                        /* audit_types_id, message */ { "bSearchable": true, "bSortable": true },
                        /* users_id */ { "bSearchable": true, "bSortable": true },
                        /* remote_host */ { "bSearchable": true, "bSortable": true },
                        /* datetime */ { "bSearchable": true, "bSortable": true },
                        /* module */ { "bSearchable": true, "bSortable": true },
                        /* related_id */ { "bSearchable": true, "bSortable": true }
                    ]
                });
                $('.dt-buttons').css('float', 'right');
                $table.on('draw', function () {
                    // run a function or other action
                });
            });
        </script>
<?php } else { ?>
    None
<?php } ?>