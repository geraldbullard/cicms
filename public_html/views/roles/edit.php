<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
        <h3 class="page-title">
            <a href="/admin/roles">Roles</a> &raquo; <?php echo $title; ?>
        </h3>
		<?php if (isset($msg)) { ?>
		<div class="alert alert-<?php echo $msgtype; ?> alert-dismissible action-alert" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			<?php echo $msg; ?>
		</div>
		<?php } ?>
        <form action="" method="post" id="role_edit">
        <div class="panel">
			<div class="panel-body clearfix">
                <h4>Details</h4>
                <p>
                    <label for="name">Name</label>
                    <input type="text" class="form-control" name="name" value="<?php echo $role[0]['name']; ?>" disabled readonly>
                </p>
                <p>
                    <label for="last_name">Description</label>
                    <input type="text" class="form-control" name="description" value="<?php echo $role[0]['description']; ?>">
                </p>
                <div class="clearfix">&nbsp;</div>
                <h4>Permissions</h4>
                <div class="clearfix">&nbsp;</div>
                <div class="row">
                    <div class="col-md-6">
                        <?php
                            foreach ($this->Modules_model->moduleInfo() as $m => $module) {
                                $mod_perms = $this->Modules_model->modulePermissions($module['module'], $role[0]['id']);
                                /*
                                [id] => 7
                                [module] => vanilla
                                [title] => Vanilla
                                [summary] => The Vanilla Module Summary
                                [theme] => adminTheme
                                [icon] => lnr lnr-leaf
                                [sort] => 6
                                [visibility] => 1
                                [status] => 1
                                [system] => 0
                                */
                                //print_r($module);
                                echo '<label>' . $module['title'] . '</label>';
                                echo '<input name="permissions[' . $module['module'] . ']" type="text"';
                                echo 'class="form-control permission-slider"';
                                echo 'data-slider-value="' . (isset($mod_perms->access) ? $mod_perms->access : '0') . '"';
                                echo 'data-slider-ticks="[0, 1, 2, 3, 4, 5]"';
                                echo 'data-slider-ticks-snap-bounds="30"';
                                echo 'data-slider-ticks-labels=\'["None", "View", "Edit", "Add", "Delete", "Global"]\'';
                                echo '/>';
                            }
                        ?>
                        <script>
                            $(".permission-slider").bootstrapSlider({
                                ticks: [0, 1, 2, 3, 4, 5],
                                ticks_labels: ['None', 'View', 'Edit', 'Add', 'Delete', 'Global'],
                                ticks_snap_bounds: 30
                            });
                        </script>
                    </div>
                </div>
                <p class="text-right">
                    <button type="submit" class="btn btn-primary">Save</button>
                </p>
			</div>
		</div><br />
        </form>
