<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
        <h3 class="page-title pull-left">Roles</h3>
        <div class="pull-right">
            <a href="/admin/roles/add/">
                <button class="btn btn-sm btn-primary" id="dt-add-row">
                    <i class="fa fa-plus"></i>&nbsp;&nbsp;Add Role
                </button>
            </a>
        </div>
        <?php if ($this->session->flashdata('msg')) { ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="alert alert-<?php echo $this->session->flashdata('msgtype'); ?> alert-dismissible action-alert" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">x</button>
                    <?php echo $this->session->flashdata('msg'); ?>
                </div>
            </div>
        </div>
        <?php } ?>
        <!-- START Role Filtering //////////////////////////////////////////////////////////////////////////////////////
		<div class="row">
	        <div class="col-lg-12">
		        <!-- START Role Filtering /////////////heading">
				<h3 class="panel-title">Role Filter</h3>
				<div class="right">
					<button type="button" class="btn-toggle-dev-debug"><i class="lnr lnr-chevron-down"></i></button>
				</div>
			</div>
			<div class="panel-body" style="display: none;">
				<form id="roles_filter" onSubmit="return false;">
					<table class="table table-striped">
						<thead>
							<tr>
								<th scope="col">First Name</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<input class="form-control form-filter input-sm" name="filterfirstname" value="" />
								</td>
							</tr>
						</tbody>
					</table>
					<div class="roles-filter-submit">
						<button class="btn btn-info btn-outline margin-bottom text-right" id="clearFilter">
							<i class="fa fa-trash-alt"></i> Clear Filter
						</button>&nbsp;
						<button class="btn btn-primary btn-outline margin-bottom text-right" id="rolesFilter">
							<i class="fa fa-search"></i> Filter
						</button>
					</div>
				</form>
			</div>
		</div>
		END Role Filtering ///////////////////////////////////////////////////////////////////////////////////////// -->
        <div class="row">
            <div class="col-lg-12">
                <div id="rolesListingTable"></div><hr />
            </div>
        </div>
        <script>
            $(document).ready(function () {
                $("#clearFilter").on("click", function (evt) {
                    evt.preventDefault();
                    $('#roles_filter').trigger("reset");
                    $.fn.loadRoles();
                    $("#clearFilter").blur();
                });
                $("#rolesFilter").on("click", function (evt) {
                    evt.preventDefault();
                    $.fn.loadRoles();
                    $("#rolesFilter").blur();
                });
                $.fn.loadRoles = function () {
                    $.blockUI({target: "#rolesListingTable"});
                    $params = $("#roles_filter").serialize();
                    $.get('<?php echo base_url('/admin/roles/getRoles'); ?>?' + $params, function (data) {
                        $("#rolesListingTable").html(data);
                        $.unblockUI({target: "#rolesListingTable"});
                    }).fail(function () {
                        alert("Error!", "Something went wrong loading the Roles!", "error");
                    });
                };
                $.fn.loadRoles();
            });
        </script>