<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php if (!empty($roles)) { ?>
        <div class="panel">
            <div class="panel-body">
                <table class="table table-striped" id="roles-table" width="100%">
                    <thead>
                        <tr>
                            <th scope="col" width="10%">ID</th>
                            <th scope="col">Name</th>
                            <th scope="col">Description</th>
                            <th scope="col" class="text-right">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($roles as $role) {
                            if (
                                (
                                    $role['id'] === '1' &&
                                    $this->ion_auth_model->in_group("developer", $this->session->userdata('user_id'))
                                ) || $role['id'] !== '1'
                            ) {
                                // [id] => 1
                                // [name] =>
                                // [description] =>
                                ?>
                                <tr>
                                    <td><?php echo $role['id']; ?></td>
                                    <td><?php echo $role['name']; ?></td>
                                    <td><?php echo $role['description']; ?></td>
                                    <td class="text-right">
                                        <!--<a href="javascript:;"><span class="label label-default">DEFAULT</span></a>
	                            <?php //if ($role['active'] == 1) { ?>
                                <a href="/roles/deactivate/<?php //echo $role['id']; ?>" class="activate-deactivate">
                                    <span class="label label-warning">Deactivate</span></a>
	                            <?php //} else { ?>
                                <a href="/roles/activate/<?php //echo $role['id']; ?>" class="activate-deactivate">
                                    <span class="label label-info">Activate</span></a>
	                            <?php //} ?>-->
                                        <?php if ($role['id'] !== '1') { ?>
                                            <a href="/admin/roles/edit/<?php echo $role['id']; ?>">
                                                <span class="label label-primary">Edit</span></a>
                                            <!--<a href="javascript:;"><span class="label label-success">SUCCESS</span></a>
                                            <a href="javascript:;"><span class="label label-info">INFO</span></a>-->
                                            <a href="javascript:;" onclick="confirmDeleteRole(<?php echo $role['id']; ?>);">
                                                <span class="label label-danger">Delete</span></a>
                                        <?php } ?>
                                    </td>
                                </tr>
                            <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <script>
            function confirmDeleteRole(id) {
                swal({
                    title: "Delete Role",
                    text: "Are you sure you wish to delete this Role?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Delete",
                    closeOnConfirm: false
                },
                function() {
                    $.ajax({
                        url: '/ajax.php?deleterole=1&rid=' + id,
                        type: 'GET',
                        success: function(response) {
                            var data = JSON.parse(response);
                            if (data.status == 'success') {
                                $.fn.loadRoles();
                                swal({
                                    title: "Success",
                                    text: "The Role was successfully deleted.",
                                    type: "success",
                                    confirmButtonClass: "btn-success",
                                    confirmButtonText: "OK",
                                    showCancelButton: false
                                });
                            } else {
                                swal({
                                    title: "Error",
                                    text: "The was an error deleting the Role.",
                                    type: "warning",
                                    confirmButtonClass: "btn-danger",
                                    confirmButtonText: "OK",
                                    showCancelButton: false
                                });
                            }
                        }
                    });
                });
            }
            jQuery(document).ready(function() {
                var $table = $('#roles-table').DataTable({
                    "dom": 'Blftip',
                    "bSortCellsTop": true,
                    "pageLength": <?php echo $this->config->item('rolesAdminPagination', 'settings'); ?>,
                    "lengthMenu": [
                        [<?php echo $this->config->item('rolesAdminPagination', 'settings'); ?>, 30, 60, 100, -1],
                        [<?php echo $this->config->item('rolesAdminPagination', 'settings'); ?>, 30, 60, 100, "All"]
                    ],
                    "order": [
                        [0, "asc"] // "desc" newest first, "asc" for oldest first
                    ],
                    responsive: true, // make table responsive
                    buttons: [ // relabel the export button
                        //'copy', 'excel'
                        { extend: 'excel', text: 'Export', className: 'btn btn-sm btn-primary shadow-sm' }
                    ],
                    "initComplete": function(settings, json) {
                        // do something immediately after the table is drawn
                    },
                    "oLanguage": { // adjust the text for the rows dropdown
                        "sLengthMenu": "_MENU_ Rows"
                    },
                    "aoColumns": [ // needed to keep Actions col from being sortable and searchable
                        /* id */ { "bSearchable": true, "bSortable": true },
                        /* name */ { "bSearchable": true, "bSortable": true },
                        /* description */ { "bSearchable": true, "bSortable": true },
                        /* actions */ { "bSearchable": false, "bSortable": false }
                    ]
                });
                $('.dt-buttons').css('float', 'right');
                $table.on('draw', function () {
                    // run a function or other action after page load if table is re-drawn
                });
            });
        </script>
<?php } else { ?>
    None
<?php } ?>