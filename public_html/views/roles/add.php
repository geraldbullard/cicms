<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
        <h3 class="page-title">
            <a href="/admin/roles">Roles</a> &raquo; <?php echo $title; ?>
        </h3>
		<?php if (isset($msg)) { ?>
		<div class="alert alert-<?php echo $msgtype; ?> alert-dismissible action-alert" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			<?php echo $msg; ?>
		</div>
		<?php } ?>
		<div class="panel">
			<div class="panel-body clearfix">
                <h4>Details</h4>
                <form action="" method="post" id="role_edit">
                    <p>
                        <label for="name">Name</label>
                        <input type="text" class="form-control" name="name" value="">
                    </p>
                    <p>
                        <label for="last_name">Description</label>
                        <input type="text" class="form-control" name="description" value="">
                    </p>
                    <p class="text-right">
                        <button type="button" class="btn btn-danger" id="role_add_cancel">Cancel</button>
                        <button type="submit" class="btn btn-primary" id="role_add_submit">Save</button>
                    </p>
                </form>
			</div>
		</div>
