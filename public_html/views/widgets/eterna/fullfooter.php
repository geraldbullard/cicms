<?php

class Fullfooter extends Widget
{

    public function display($data)
	{
        // This code depends on the content module which we are not yet using for this project.
        $contents_model = $this->load->model('contents/contents_model');
        $data['menu1'] = $this->contents_model->getMenuByDesignation('secondary');
        $data['menu2'] = $this->contents_model->getMenuByDesignation('tertiary');
        $this->view('widgets/eterna/fullfooter_view', $data);
    }
    
}