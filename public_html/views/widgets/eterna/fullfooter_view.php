<?php
// Manually include the Shortcode class file for debugging
if (!class_exists('Shortcode')) {
    require_once APPPATH . 'libraries/Shortcode.php';
}
?>
<footer id="footer">
    <?php //echo $this->template->widget('eterna/newsletter'); ?>
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 footer-links">
                    <h4>Useful Links</h4>
                    <?php
                        // Set theme specific options if needed
                        $options = array(
                            'ulClass' => '',
                            'ulStyle' => '',
                            'liClass' => '',
                            'dropClass' => 'drop-down',
                            'dropHrefClass' => '',
                        );
                        $this->load->helper('contents/functions');
                        $tree = unserialize($menu1->menuData);
                        $treeMenu = parseTree($tree['item'], 0);
                        $newTreeMenu = mergeExtrasIntoTree($treeMenu, $tree);
                        printPlainMenuTree($newTreeMenu, $options);
                    ?>
                </div>
                <div class="col-lg-3 col-md-6 footer-links">
                    <h4>Our Services</h4>
                    <?php
                        // Set theme specific options if needed
                        $options = array(
                            'ulClass' => '',
                            'ulStyle' => '',
                            'liClass' => '',
                            'dropClass' => 'drop-down',
                            'dropHrefClass' => '',
                        );
                        $this->load->helper('contents/functions');
                        $tree = unserialize($menu2->menuData);
                        $treeMenu = parseTree($tree['item'], 0);
                        $newTreeMenu = mergeExtrasIntoTree($treeMenu, $tree);
                        printPlainMenuTree($newTreeMenu, $options);
                    ?>
                </div>
                <div class="col-lg-3 col-md-6 footer-contact">
                    <h4>Contact Us</h4>
                    <p><?php  
                        // In the content we use {aboutbrokenpixel} since it will get parsed and return output
                        // Here (in the html) we can simply echo the return output from the function directly
                        echo brokenpixelcontactinfo(); 
                    ?></p>
                </div>
                <div class="col-lg-3 col-md-6 footer-info">
                    <h4>About Eterna</h4>
                    <p><?php echo aboutbrokenpixel(); ?></p>
                    <div class="social-links mt-3">
                        <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
                        <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
                        <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
                        <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
                        <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <p class="container">
    <div class="text-center">&copy; Copyright <?php echo date("Y"); ?> brokenPIXEL. All Rights Reserved.</div>
    <div class="credits">
        <!-- All the links in the footer should remain intact. -->
        <!-- You can delete the links only if you purchased the pro version. -->
        <!-- Licensing information: https://bootstrapmade.com/license/ -->
        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/eterna-free-multipurpose-bootstrap-template/ -->
        Theme Design by <a target="_blank" href="https://bootstrapmade.com/">BootstrapMade</a><br>
        Modified for <a target="_blank" href="https://brokenpixelcms.com/">brokenPIXEL</a> by <a
            href="javascript:;">ForgottenStudios</a>
    </div>
    </div>
</footer><!-- End Footer -->