<nav class="nav-menu d-none d-lg-block">
    <ul>
        <?php
        foreach ($items as $item) {
            if (!empty($item)) {
        ?>
        <li><a href="<?php echo $item['url']; ?>"><?php echo $item['title']; ?></a></li>
        <?php
            }
        }
        ?>
    </ul>
</nav>
<!-- .nav-menu -->
