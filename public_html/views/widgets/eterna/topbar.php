<?php

class Topbar extends Widget
{

    public function display($data)
	{
        $data['items'] = array(
            // get from db settings later
            'email' => 'info@website.ext',
            'phone' => '(904) 999-8765',
            'twitter' => 'https://www.twitter.com/',
            'facebook' => 'https://www.facebook.com/',
            'instagram' => 'https://www.instagram.com/',
            'skype' => 'https://www.skype.com/',
            'linkedin' => 'https://www.linkedin.com/',
        );
        $this->view('widgets/eterna/topbar_view', $data);
    }
    
}