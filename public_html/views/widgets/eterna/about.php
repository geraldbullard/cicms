<?php

class About extends Widget
{

    public function display($data)
	{
        $this->view('widgets/eterna/about_view', $data);
    }
    
}