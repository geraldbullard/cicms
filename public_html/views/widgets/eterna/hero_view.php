        <!-- ======= Hero Section ======= -->
        <section id="hero">
            <div class="hero-container">
                <div id="heroCarousel" class="carousel slide carousel-fade" data-ride="carousel">
                    <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>
                    <div class="carousel-inner" role="listbox">
                        <?php foreach ($slides as $i => $slide) { ?>
                        <!-- Slide <?php echo $i; ?> -->
                        <div class="carousel-item<?php echo ($i == 0 ? ' active' : NULL); ?>" style="background: url(<?php echo $slide['background']; ?>)">
                            <div class="carousel-container">
                                <div class="carousel-content">
                                    <h2 class="animate__animated animate__fadeInDown">
                                        <?php echo $slide['h2']; ?>
                                    </h2>
                                    <p class="animate__animated animate__fadeInUp">
                                        <?php echo $slide['text']; ?>
                                    </p>
                                    <a
                                        target="<?php echo $slide['target']; ?>"
                                        href="<?php echo $slide['href']; ?>"
                                        class="btn-get-started animate__animated animate__fadeInUp"
                                    >
                                        <?php echo $slide['link']; ?>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                    <a class="carousel-control-prev" href="#heroCarousel" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon icofont-rounded-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#heroCarousel" role="button" data-slide="next">
                        <span class="carousel-control-next-icon icofont-rounded-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </section><!-- End Hero -->
