<nav id="nav-menu-container">
    <?php
        /* eStartup menu html for reference
        <nav id="nav-menu-container">
            <ul class="nav-menu sf-js-enabled sf-arrows" style="touch-action: pan-y;">
                <li class="menu-active"><a href="index.html">Home</a></li>
                <li><a href="#about-us">About</a></li>
                <li><a href="#features">Features</a></li>
                <li><a href="#screenshots">Screenshots</a></li>
                <li><a href="#team">Team</a></li>
                <li><a href="#pricing">Pricing</a></li>
                <li class="menu-has-children">
                    <a href="" class="sf-with-ul">Drop Down</a>
                    <ul style="display: none;">
                        <li><a href="#">Drop Down 1</a></li>
                        <li><a href="#">Drop Down 3</a></li>
                        <li><a href="#">Drop Down 4</a></li>
                        <li><a href="#">Drop Down 5</a></li>
                    </ul>
                </li>
                <li><a href="#blog">Blog</a></li>
                <li><a href="#contact">Contact</a></li>
            </ul>
        </nav>
        */
        // Set theme specific options if needed
        $options = array(
            'ulClass' => 'nav-menu sf-js-enabled sf-arrows',
            'ulStyle' => 'touch-action: pan-y;',
            'liClass' => '',
            'dropClass' => 'menu-has-children',
            'dropHrefClass' => 'sf-with-ul',
        );
        // Now do the default work
        $this->load->helper('contents/functions');
        $tree = unserialize($menu->menuData);
        $treeMenu = parseTree($tree['item'], 0);
        $newTreeMenu = mergeExtrasIntoTree($treeMenu, $tree);
        printPlainMenuTree($newTreeMenu, $options);
    ?>
</nav>
