<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
        <h3 class="page-title pull-left">Settings</h3>
        <div class="pull-right">
            <a href="/admin/settings/add/">
                <button class="btn btn-sm btn-primary" id="dt-add-row">
                    <i class="fa fa-plus"></i>&nbsp;&nbsp;Add Setting
                </button>
            </a>
        </div>
        <?php if ($this->session->flashdata('msg')) { ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="alert alert-<?php echo $this->session->flashdata('msgtype'); ?> alert-dismissible action-alert" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">x</button>
                    <?php echo $this->session->flashdata('msg'); ?>
                </div>
            </div>
        </div>
        <?php } ?>
        <!-- START Setting Filtering //////////////////////////////////////////////////////////////////////////////////////
        <div class="row">
            <div class="col-lg-12">
                <h3 class="panel-title">Settings Filter</h3>
                <div class="right">
                    <button type="button" class="btn-toggle-dev-debug"><i class="lnr lnr-chevron-down"></i></button>
                </div>
            </div>
            <div class="panel-body" style="display: none;">
                <form id="settings_filter" onSubmit="return false;">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th scope="col">First Name</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <input class="form-control form-filter input-sm" name="filterfirstname" value="" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="button-filter-submit">
                        <button class="btn btn-info btn-outline margin-bottom text-right" id="clearFilter">
                            <i class="fa fa-trash-alt"></i> Clear Filter
                        </button>&nbsp;
                        <button class="btn btn-primary btn-outline margin-bottom text-right" id="doFilter">
                            <i class="fa fa-search"></i> Filter
                        </button>
                    </div>
                </form>
            </div>
        </div>
        END  User Filtering //////////////////////////////////////////////////////////////////////////////////////// -->
        <div class="row">
            <div class="col-lg-12">
                <div id="settingsListingTable"></div><hr />
            </div>
        </div>
        <script>
            $(document).ready(function () {
                $("#clearFilter").on("click", function (evt) {
                    evt.preventDefault();
                    $('#settings_filter').trigger("reset");
                    $.fn.loadSettings();
                    $("#clearFilter").blur();
                });
                $("#doFilter").on("click", function (evt) {
                    evt.preventDefault();
                    $.fn.loadSettings();
                    $("#doFilter").blur();
                });
                $.fn.loadSettings = function () {
                    $.blockUI({target: "#settingsListingTable"});
                    $params = $("#settings_filter").serialize();
                    $.get('<?php echo base_url('/admin/settings/getSettings'); ?>?' + $params, function (data) {
                        $("#settingsListingTable").html(data);
                        $.unblockUI({target: "#settingsListingTable"});
                    }).fail(function () {
                        alert("Error!", "Something went wrong loading the Settings!", "error");
                    });
                };
                $.fn.loadSettings();
            });
        </script>
