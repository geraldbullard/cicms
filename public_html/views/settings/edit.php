<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
        <h3 class="page-title">
            <a href="/admin/settings">Settings</a> &raquo; <?php echo $title; ?>
        </h3>
		<?php if (isset($msg)) { ?>
		<div class="alert alert-<?php echo $msgtype; ?> alert-dismissible action-alert" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			<?php echo $msg; ?>
		</div>
		<?php } ?>
		<div class="panel">
			<div class="panel-body clearfix">
                <h4>Details</h4>
                <form action="" method="post" id="role_edit">
                    <p>
                        <label for="define">Define</label>
                        <!-- Later we need to control readonly status if not Core Define -->
                        <input type="hidden" name="define" value="<?php echo $setting[0]['define']; ?>">
                        <!-- Later, controlled by Group -->
                        <input type="text" class="form-control" id="define" value="<?php echo $setting[0]['define']; ?>" disabled readonly>
                    </p>
                    <p>
                        <label for="title">Title</label>
                        <input type="text" class="form-control" name="title" value="<?php echo $setting[0]['title']; ?>">
                    </p>
                    <p>
                        <label for="summary">Summary</label>
                        <input type="text" class="form-control" name="summary" value="<?php echo $setting[0]['summary']; ?>">
                    </p>
                    <p>
                        <label for="value">Value</label>
                        <input type="text" class="form-control" name="value" value="<?php echo $setting[0]['value']; ?>">
                    </p>
                    <p>
                        <label for="type">Type</label>
                        <!-- Later, controlled by Group -->
                        <select class="form-control" name="type">
                            <option value="core"<?php echo ($setting[0]['type'] == 'core' ? ' selected' : null); ?>>Core</option>
                            <option value="design"<?php echo ($setting[0]['type'] == 'design' ? ' selected' : null); ?>>Design</option>
                            <option value="seo"<?php echo ($setting[0]['type'] == 'seo' ? ' selected' : null); ?>>SEO</option>
                        </select>
                    </p>
                    <p class="text-right">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </p>
                </form>
			</div>
		</div>
