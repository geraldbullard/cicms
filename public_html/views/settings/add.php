<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
        <h3 class="page-title">
            <a href="/admin/settings">Settings</a> &raquo; <?php echo $title; ?>
        </h3>
		<?php if (isset($msg)) { ?>
		<div class="alert alert-<?php echo $msgtype; ?> alert-dismissible action-alert" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
			<?php echo $msg; ?>
		</div>
		<?php } ?>
		<div class="panel">
			<div class="panel-body clearfix">
                <h4>Details</h4>
                <form action="" method="post" id="setting_add">
                    <p>
                        <label for="define">Define <small>(camelCaseFormat)</small></label>
                        <input type="text" class="form-control" name="define" value="<?php echo set_value('define'); ?>">
                    </p>
                    <p>
                        <label for="title">Title</label>
                        <input type="text" class="form-control" name="title" value="<?php echo set_value('title'); ?>">
                    </p>
                    <p>
                        <label for="summary">Summary</label>
                        <input type="text" class="form-control" name="summary" value="<?php echo set_value('summary'); ?>">
                    </p>
                    <p>
                        <label for="value">Value</label>
                        <input type="text" class="form-control" name="value" value="<?php echo set_value('value'); ?>">
                    </p>
                    <p>
                        <label for="type">Type</label>
                        <select class="form-control" name="type">
                            <option value="">Select Type</option>
                            <option value="core"<?php echo (set_value('type') == 'core' ? ' selected' : null); ?>>Core</option>
                            <option value="design"<?php echo (set_value('type') == 'design' ? ' selected' : null); ?>>Design</option>
                            <option value="seo"<?php echo (set_value('type') == 'seo' ? ' selected' : null); ?>>SEO</option>
                        </select>
                    </p>
                    <p>
                        <label for="edit">Edit</label>
                        <select class="form-control" name="edit">
                            <option value="">Select Edit Capability</option>
                            <option value="0"<?php echo (set_value('edit') == '0' ? ' selected' : null); ?>>No</option>
                            <option value="1"<?php echo (set_value('edit') == '1' ? ' selected' : null); ?>>Yes</option>
                        </select>
                    </p>
                    <p>
                        <label for="system">System</label>
                        <select class="form-control" name="system">
                            <option value="">Select System Designation</option>
                            <option value="0"<?php echo (set_value('system') == '0' ? ' selected' : null); ?>>No</option>
                            <option value="1"<?php echo (set_value('system') == '1' ? ' selected' : null); ?>>Yes</option>
                        </select>
                    </p>
                    <p>
                        <label for="visibility">Visibility</label>
                        <select class="form-control" name="visibility">
                            <option value="">Select Visibility</option>
                            <option value="0"<?php echo (set_value('visibility') == '0' ? ' selected' : null); ?>>No</option>
                            <option value="1"<?php echo (set_value('visibility') == '1' ? ' selected' : null); ?>>Yes</option>
                        </select>
                    </p>
                    <p class="text-right">
                        <button type="button" class="btn btn-danger" id="setting_add_cancel">Cancel</button>
                        <button type="submit" class="btn btn-primary" id="setting_add_submit">Save</button>
                    </p>
                </form>
			</div>
		</div>
