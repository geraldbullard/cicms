<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php if (!empty($settings)) { ?>
        <div class="panel">
            <div class="panel-body">
                <table class="table table-striped" id="settings-table" width="100%">
                    <thead>
                        <tr>
                            <th scope="col" width="10%">ID</th>
                            <th scope="col">Define</th>
                            <th scope="col">Title</th>
                            <th scope="col">Value</th>
                            <th scope="col">Type</th>
                            <th scope="col" class="text-right">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($settings as $setting) {
                            // values
                        ?>
                        <tr>
                            <td><?php echo $setting['id']; ?></td>
                            <td><?php echo $setting['define']; ?></td>
                            <td><?php echo $setting['title']; ?></td>
                            <td><?php echo $setting['value']; ?></td>
                            <td><?php echo $setting['type']; ?></td>
                            <td class="text-right">
                                <a href="/admin/settings/edit/<?php echo $setting['id']; ?>">
                                    <span class="label label-primary">Edit</span></a>
                                <!--<a href="javascript:;" onclick="confirmDeleteSetting(<?php //echo $setting['id']; ?>);">
                                    <span class="label label-danger">Delete</span></a>-->
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <script>
            function confirmDeleteSetting(id) {
                swal({
                    title: "Delete Setting",
                    text: "Are you sure you wish to delete this Setting?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Delete",
                    closeOnConfirm: false
                },
                function() {
                    $.ajax({
                        url: '/ajax.php?deletesetting=1&sid=' + id,
                        type: 'GET',
                        success: function(response) {
                            var data = JSON.parse(response);
                            if (data.status == 'success') {
                                $.fn.loadSettings();
                                swal({
                                    title: "Success",
                                    text: "The Setting was successfully deleted.",
                                    type: "success",
                                    confirmButtonClass: "btn-success",
                                    confirmButtonText: "OK",
                                    showCancelButton: false
                                });
                            } else {
                                swal({
                                    title: "Error",
                                    text: "The was an error deleting the Setting.",
                                    type: "warning",
                                    confirmButtonClass: "btn-danger",
                                    confirmButtonText: "OK",
                                    showCancelButton: false
                                });
                            }
                        }
                    });
                });
            }
            jQuery(document).ready(function() {
                var $table = $('#settings-table').DataTable({
                    "dom": 'Blftip',
                    "bSortCellsTop": true,
                    "pageLength": <?php echo $this->config->item('settingsAdminPagination', 'settings'); ?>,
                    "lengthMenu": [
                        [<?php echo $this->config->item('settingsAdminPagination', 'settings'); ?>, 30, 60, 100, -1],
                        [<?php echo $this->config->item('settingsAdminPagination', 'settings'); ?>, 30, 60, 100, "All"]
                    ],
                    "order": [
                        [0, "asc"] // "desc" newest first, "asc" for oldest first
                    ],
                    responsive: true, // make table responsive
                    buttons: [ // relabel the export button
                        //'copy', 'excel'
                        { extend: 'excel', text: 'Export', className: 'btn btn-sm btn-primary shadow-sm' }
                    ],
                    "initComplete": function(settings, json) { // do something immediately after the table is drawn
                        //applyEnrollerEmailFilter($table);
                    },
                    "oLanguage": { // adjust the text for the rows dropdown
                        "sLengthMenu": "_MENU_ Rows"
                    },
                    "aoColumns": [ // needed to keep Actions col from being sortable and searchable
                        /* id */ { "bSearchable": true, "bSortable": true },
                        /*  */ { "bSearchable": true, "bSortable": true },
                        /*  */ { "bSearchable": true, "bSortable": true },
                        /*  */ { "bSearchable": true, "bSortable": true },
                        /*  */ { "bSearchable": true, "bSortable": true },
                        /* actions */ { "bSearchable": false, "bSortable": false }
                    ]
                });
                $('.dt-buttons').css('float', 'right');
                $table.on('draw', function () {
                    // run a function or other action
                });
            });
        </script>
<?php } else { ?>
    None
<?php } ?>