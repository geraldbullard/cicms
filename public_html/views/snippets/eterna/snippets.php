<!-- START Thumbnail Panel -->
<div data-type="container" data-preview="/assets/vendor/kedit/snippets/preview/thumbnail_panel.png" data-keditor-title="Thumbnail Panel" data-keditor-categories="Eterna;Image">
    <div class="row">
        <div class="col-sm-12">
            <div class="thumbnail" data-type="container-content">
                <div data-type="component-photo">
                    <div class="photo-panel">
                        <img src="/assets/vendor/kedit/snippets/img/somewhere_bangladesh.jpg" width="100%" height="" class="img-responsive" />
                    </div>
                </div>
                <div data-type="component-text">
                    <h3>Thumbnail label</h3>
                    <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                    <p>
                        <a href="#" class="btn btn-primary" role="button">Button</a>
                        <a href="#" class="btn btn-default" role="button">Button</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END Thumbnail Panel -->

<!-- START Spacer -->
<div data-type="container" data-preview="/views/snippets/preview/eterna_spacer.jpg" data-keditor-title="Content Row Spacer" data-keditor-categories="Eterna;Spacer">
    <div class="row">
        <div class="col-sm-12">&nbsp;</div>
    </div>
</div>
<!-- END Spacer -->

<!-- START Features/Skills -->
<div data-type="container" data-preview="/views/snippets/preview/eterna_features_skills_section.png" data-keditor-title="Features & Skills Section" data-keditor-categories="Eterna;Sections">
    <div class="row">
        <div class="col-md-4 col-sm-6">
            <div class="features-skills" data-type="container-content">
                <div class="custom-module" data-type="component-text">
                    <h4>We are MrDigit</h4>
                    <p>Nec ad timeam torquatos, eam debitis sententiae exs. Vel debet doctus ei, privitae laoreet scriptorem ate Cu sed homero alienum. Odec adesterc timeam toruatos, eam debitis sententiae ex.Vel debet doctus smpleose, privitae laoreet scrit optorems.</p>
                    <div class="btn-wrapper">
                        <a href="#" class="btn btn-primary">Learn More</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class="features-skills" data-type="container-content">
                <div class="custom-module" data-type="component-text">
                    <h4>Our Features</h4>
                    <ul class="list-style-2">
                        <li><i class="fa fa-star-o"></i> Powerful &amp; Creative Design Layouts</li>
                        <li><i class="fa fa-star-o"></i> Drag &amp; Drop Visual Page Builder Included</li>
                        <li><i class="fa fa-star-o"></i> Stunning Portfolio and Blog Layout Styles</li>
                        <li><i class="fa fa-star-o"></i> Parallax &amp; Video Slider Backgrounds</li>
                        <li><i class="fa fa-star-o"></i> Looks Awesome in All Mobile Devices</li>
                        <li><i class="fa fa-star-o"></i> Woo Commerce Support</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-6">
            <div class="features-skills" data-type="container-content">
                <div class="custom-module" data-type="component-text">
                    <h4>This is another Paragraph</h4>
                    <p>Nec ad timeam torquatos, eam debitis sententiae exs. Vel debet doctus ei, privitae laoreet scriptorem ate Cu sed homero alienum. Odec adesterc timeam toruatos, eam debitis sententiae ex.Vel debet doctus smpleose, privitae laoreet scrit optorems.</p>
                    <div class="btn-wrapper">
                        <a href="#" class="btn btn-primary">Click Here</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END Features/Skills -->
