<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
		<h3 class="page-title">
            <a href="/admin/<?php echo $parenturl; ?>/"><?php echo $parent; ?></a> &raquo; <?php echo $title; ?>
        </h3>
        <div class="row">
            <div class="col-lg-12">
                <?php if (!empty($module_settings)) { ?>
                <div class="panel">
                    <div class="panel-body">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th class="hide-below-1024">ID</th>
                                    <th class="hide-below-768">Title</th>
                                    <th class="hide-below-1200">Summary</th>
                                    <th>Define</th>
                                    <th class="hide-below-1024">Value</th>
                                    <th class="text-right" style="width:130px;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($module_settings as $i => $setting) { ?>
                                    <?php
                                    /*
                                    [id] => 23
                                    [define] => auditTypeModuleInstalled
                                    [title] => Audit Type Module Installed
                                    [summary] => Audit Type Module Installed
                                    [value] => 6
                                    [type] => audit
                                    [edit] => 1
                                    [system] => 1
                                    [visibility] => 0
                                    */
                                    ?>
                                <tr>
                                    <td class="hide-below-1024"><?php echo $setting['id']; ?></td>
                                    <td class="hide-below-768"><?php echo $setting['title']; ?></td>
                                    <td class="hide-below-1200"><?php echo $setting['summary']; ?></td>
                                    <td><?php echo $setting['define']; ?></td>
                                    <td class="hide-below-1024"><?php echo $setting['value']; ?></td>
                                    <td class="text-right">
                                        <a href="/admin/modules/editsetting/<?php echo $setting['id']; ?>">
                                            <span class="label label-primary">Edit</span></a>
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <?php } else { ?>
                <p>No Settings for this module.</p>
                <?php } ?>
            </div>
        </div>
        <script>
            jQuery(document).ready(function() {
                /*var $table = $('#settings-table').DataTable({
                    "dom": 'Blftip',
                    "bSortCellsTop": true,
                    "pageLength": 15,
                    "lengthMenu": [[15, 30, 60, 100, -1], [15, 30, 60, 100, "All"]],
                    "order": [
                        [0, "asc"] // "desc" newest first, "asc" for oldest first
                    ],
                    responsive: true, // make table responsive
                    buttons: [ // relabel the export button
                        //'copy', 'excel'
                        { extend: 'excel', text: 'Export', className: 'btn btn-sm btn-primary shadow-sm' }
                    ],
                    "initComplete": function(settings, json) { // do something immediately after the table is drawn
                        //applyEnrollerEmailFilter($table);
                    },
                    "oLanguage": { // adjust the text for the rows dropdown
                        "sLengthMenu": "_MENU_ Rows"
                    },
                    "aoColumns": [ // needed to keep Actions col from being sortable and searchable
                        { "bSearchable": true, "bSortable": true },
                        { "bSearchable": true, "bSortable": true },
                        { "bSearchable": true, "bSortable": true },
                        { "bSearchable": true, "bSortable": true },
                        { "bSearchable": true, "bSortable": true },
                        { "bSearchable": false, "bSortable": false }
                    ]
                });
                $('.dt-buttons').css('float', 'right');
                $table.on('draw', function () {
                    // run a function or other action
                });*/
            });
        </script>
