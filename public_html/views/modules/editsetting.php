<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
        <?php
            /*
            [id] => 14
            [define] => auditsAdminPagination
            [title] => Admin Audit Listing Pagination
            [summary] => The number of items to show per page in the admin audit listing page.
            [value] => 15
            [type] => audit
            [edit] => 1
            [system] => 1
            [visibility] => 0
            */
        ?>
		<h3 class="page-title">
            <a href="/admin/modules/"><?php
	            if ($module_setting[0]['system'] == '0') echo 'Addon Modules';
	            if ($module_setting[0]['system'] == '1') echo 'Core Modules';
	            if ($module_setting[0]['system'] == '2') echo 'RCI Modules';
            ?></a> &raquo;
            <a href="/admin/modules/settings/<?php echo $module_setting[0]['type']; ?>"><?php
                echo $this->Modules_model->moduleInfo($module_setting[0]['type'])->title;
                echo ($module_setting[0]['type'] == 'rci' ? ' Plugin' : ' Module');
            ?> Settings</a> &raquo;
            <?php echo $title; ?>
        </h3>
        <?php if (isset($msg)) { ?>
        <div class="alert alert-<?php echo $msgtype; ?> alert-dismissible action-alert" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <?php echo $msg; ?>
        </div>
        <?php } ?>
        <div class="panel">
            <div class="panel-body clearfix">
                <h4>Details</h4>
                <form action="" method="post" id="module_setting_edit">
                    <p>
                        <label for="define">Define</label>
                        <!-- Later we need to control readonly status if not Core Define -->
                        <input type="hidden" name="define" value="<?php echo $module_setting[0]['define']; ?>">
                        <!-- Later, controlled by Group -->
                        <input type="text" class="form-control" id="define" value="<?php echo $module_setting[0]['define']; ?>" disabled readonly>
                    </p>
                    <p>
                        <label for="title">Title</label>
                        <input type="text" class="form-control" name="title" value="<?php echo $module_setting[0]['title']; ?>">
                    </p>
                    <p>
                        <label for="summary">Summary</label>
                        <input type="text" class="form-control" name="summary" value="<?php echo $module_setting[0]['summary']; ?>">
                    </p>
                    <p>
                        <label for="value">Value</label>
                        <input type="text" class="form-control" name="value" value="<?php echo $module_setting[0]['value']; ?>">
                    </p>
                    <!-- The "type" must not be changeable for a module setting, it must match the module name -->
                    <input type="hidden" name="type" value="<?php echo $module_setting[0]['type']; ?>">
                    <p class="text-right">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </p>
                </form>
            </div>
        </div>

