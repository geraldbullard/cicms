<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
		<h3 class="page-title">Core Modules</h3>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel">
                    <div class="panel-body">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th style="width:30px;padding-left:15px;">&nbsp;</th>
                                    <th>Module</th>
                                    <th class="text-right" style="width:130px;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($modules as $i => $module) { ?>
                                    <?php /*
                                    [id] => 1
                                    [module] => dashboard
                                    [title] => Dashboard
                                    [summary] => The Dashboard Module Summary
                                    [theme] => adminTheme
                                    [icon] => lnr lnr-screen
                                    [sort] => 0
                                    [visibility] => 1
                                    [status] => 1
                                    [system] => 1
                                    */ ?>
                                <tr>
                                    <td style="width:30px;padding-left:15px;"><i class="<?php echo $module['icon']; ?>"></i></td>
                                    <td>
                                        <h5 style="margin:0;"><?php echo $module['title']; ?></h5>
                                        <small class="hide-below-640"><?php echo $module['summary']; ?></small>
                                    </td>
                                    <td class="text-right" style="width:200px;">
                                        <a href="/admin/modules/settings/<?php echo $module['module']; ?>"><span class="label label-primary">Settings</span></a>
                                        <!--<a href="javascript:;"><span class="label label-warning">Disable</span></a>
                                        <a href="javascript:;"><span class="label label-danger">Delete</span></a>-->
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
