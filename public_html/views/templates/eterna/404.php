<?php
/** Name: 404 */
?>
<section id="main">
    <div id="maincontent" class="container">
        <div class="row">
            <div id="fullcontent" class="col-lg-12 full-content">
                <h1>Well Now...</h1>
                <p>Sorry, we couldn't find that page/content you were looking for.</p>
            </div>
        </div>
    </div>    
    <!-- Include Handlebars.js -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.7.7/handlebars.min.js"></script>
    <!-- Create a template using Handlebars.js syntax -->
    <script id="my-template" type="text/x-handlebars-template">
        <h1>{{title}}</h1>
        <ul style="list-style:none;margin:0;padding:0 0 0 3px;">
            {{#each items}}
            <li>
                <div>{{title}} ({{current_version}}) - {{author}}</div>
                <div>{{description}}</div>
            </li>
            {{/each}}
        </ul>
    </script>

    <!-- Container to render the template -->
    <div id="my-container"></div>

    <!-- Include your JavaScript code -->
    <script>
        // Define your API endpoint
        var apiUrl = 'http://bpixel/api/books/all';

        // Fetch data from the API
        $.get(apiUrl, function(data) {
            // Define your Handlebars template
            var source = $('#my-template').html();
            var template = Handlebars.compile(source);

            // Define the data to be passed to the template
            var context = {
                title: 'Books API',
                items: data
            };

            // Render the template and inject it into the container
            var html = template(context);
            $('#my-container').html(html);
        });
    </script>
</section>
