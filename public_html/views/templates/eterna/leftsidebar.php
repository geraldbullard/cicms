<?php
/** Name: Left Sidebar (3-9, Content on Right) */
?>
<section id="main">
    <div id="maincontent" class="container">
        <div class="row">
            <div id="leftcontent" class="col-lg-3 left-content">
                <h3>Left Sidebar</h3>
                Sidebar<br>
                Sidebar<br>
                Sidebar<br>
                Sidebar<br>
                Sidebar<br>
                Sidebar<br>
                Sidebar<br>
                Sidebar<br>
                Sidebar<br>
                Sidebar<br>
                Sidebar<br>
                Sidebar<br>
                Sidebar<br>
                Sidebar<br>
                Sidebar<br>
                Sidebar<br>
                Sidebar<br>
            </div>
            <div id="rightcontent" class="col-lg-9 right-content">
                <h1 class="page-heading"><?php echo $pagedata->title; ?></h1>
                <?php echo $pagedata->content; ?>
            </div>
        </div>
    </div>
</section>