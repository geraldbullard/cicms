<?php
/** Name: Default (Full Width) */
?>
<section id="main">
    <div id="maincontent" class="container">
        <div class="row">
            <div id="fullcontent" class="col-lg-12 full-content">
                <h1 class="page-heading"><?php echo $pagedata->title; ?></h1>
                <?php echo $pagedata->content; ?>
            </div>
        </div>
    </div>
</section>
