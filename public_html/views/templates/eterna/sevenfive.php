<?php
/** Name: 2 Column (7-5, Content on Left) */
?>
<section id="main">
    <div id="maincontent" class="container">
        <div class="row">
            <div id="leftcontent" class="col-lg-7 left-content">
                <h1 class="page-heading"><?php echo $pagedata->title; ?></h1>
                <?php echo $pagedata->content; ?>
            </div>
            <div id="rightcontent" class="col-lg-5 right-content">
                <h3>Right Content</h3>
                Content<br>
                Content<br>
                Content<br>
                Content<br>
                Content<br>
                Content<br>
                Content<br>
                Content<br>
                Content<br>
                Content<br>
                Content<br>
                Content<br>
                Content<br>
                Content<br>
                Content<br>
                Content<br>
                Content<br>
            </div>
        </div>
    </div>
</section>
