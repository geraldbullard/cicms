<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Files extends MY_Controller
{

	function __construct()
	{
		parent::__construct();

		// First check if logged in
		if (!$this->session->userdata('user_id'))
		{
			// set the url they were trtying to go to in session
			$this->session->set_userdata('page_url', current_url());

			//Redirect to login
			redirect('user/login');
		}

		// Get site wide settings first
		$this->Settings_model->loadSitewideSettings();

		// Load This Model
		$this->load->model('Files_model');
	}

	public function admin_index()
	{
		$this->data = array();
		$this->data['title'] = 'Files Index';

		// load the config from the current module
		$this->load->config('files');
		$this->data['files_config'] = $this->config->config['files'];

		// Added for developer dropdown
		$this->data['dev_data'] = '';

		// Secondary DB data from MSSQL Server
		//$mssql_query = "SELECT * FROM Agents WHERE AgentID = 'FL039495tJ'";
		// $mssql_query = "SELECT DatePart(YY, A.WhenMailed) AS TranYear, DatePart(MM, A.WhenMailed) AS TranMM,  
		// 	Count(DISTINCT(A.AgentId)) AS DistAgt, Sum(A.NumMailed) AS SumMailed
		// 	FROM RspAdmin.dbo.LettersMailedInfo A
		// 	WHERE DatePart(YY,WhenMailed) > 2002
		// 	GROUP BY DatePart(YY, A.WhenMailed), DatePart(MM, A.WhenMailed)
		// 	ORDER BY DatePart(YY, A.WhenMailed), DatePart(MM, A.WhenMailed)";
		// $mssql_stmt = sqlsrv_query($this->config->config['sqlsrv_conn'], $mssql_query);
		// while ($mssql_row = sqlsrv_fetch_array($mssql_stmt, SQLSRV_FETCH_ASSOC)) {
		// 	$this->data['ReportInfo'][] = $mssql_row;
		// }

		$this->load->view('layout/admin/header.php', $this->data);
		$this->load->view('admin/index', $this->data);
		$this->load->view('layout/admin/footer.php', $this->data);
	}
	
}
