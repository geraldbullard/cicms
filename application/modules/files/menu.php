<?php
/* Kanban module menu file */

$_menu = array(
    'url' => '/admin/files/',
    'title' => 'Files',
    'icon' => 'lnr lnr-picture',
    'access' => 1,
);