<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php if (!empty($files)) { ?>
        <div class="panel">
            <div class="panel-body">
                <table class="table table-striped" id="files-table" width="100%">
                    <thead>
                        <tr>
                            <th scope="col" width="10%">ID</th>
                            <th scope="col">Name</th>
                            <th scope="col" class="text-right">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($files as $file) {
                        ?>
                        <tr>
                            <td><?php echo $file['id']; ?></td>
                            <td>
                                <?php echo $file['vname']; ?>
                            </td>
                            <td class="text-right">
                                <a href="/admin/files/edit/<?php echo $file['id']; ?>">
                                    <span class="label label-primary">Edit</span></a>
                                <a href="javascript:;" onclick="confirmDeleteFile(<?php echo $file['id']; ?>);">
                                    <span class="label label-danger">Delete</span></a>
                                </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <script>
            function confirmDeleteFile(id) {
                swal({
                    title: "Delete File",
                    text: "Are you sure you wish to delete this File?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Delete",
                    closeOnConfirm: false
                },
                function() {
                    $.ajax({
                        url: '/admin/files/delete/' + id,
                        type: 'GET',
                        success: function(response) {
                            var data = JSON.parse(response);
                            if (data.status == 'success') {
                                $.fn.loadFiles();
                                swal({
                                    title: "Success",
                                    text: "The File was successfully deleted.",
                                    type: "success",
                                    confirmButtonClass: "btn-success",
                                    confirmButtonText: "OK",
                                    showCancelButton: false
                                });
                            } else {
                                swal({
                                    title: "Error",
                                    text: "The was an error deleting the File.",
                                    type: "warning",
                                    confirmButtonClass: "btn-danger",
                                    confirmButtonText: "OK",
                                    showCancelButton: false
                                });
                            }
                        }
                    });
                });
            }
            jQuery(document).ready(function() {
                var $table = $('#files-table').DataTable({
                    "dom": 'Blftip',
                    "bSortCellsTop": true,
                    "pageLength": <?php echo $this->config->item('filesAdminPagination', 'settings'); ?>,
                    "lengthMenu": [
                        [<?php echo $this->config->item('filesAdminPagination', 'settings'); ?>, 30, 60, 100, -1],
                        [<?php echo $this->config->item('filesAdminPagination', 'settings'); ?>, 30, 60, 100, "All"]
                    ],
                    "order": [
                        [0, "asc"] // "desc" newest first, "asc" for oldest first
                    ],
                    responsive: true, // make table responsive
                    buttons: [ // relabel the export button
                        //'copy', 'excel'
                        {
                            extend: 'excel',
                            text: 'Export',
                            title: 'Files-Export',
                            className: 'btn btn-sm btn-primary shadow-sm',
                            exportOptions: {
                                columns: [0, 1]
                            },
                        }
                    ],
                    "initComplete": function(settings, json) { // do something immediately after the table is drawn
                        //applyFilesEmailFilter($table);
                    },
                    "oLanguage": { // adjust the text for the rows dropdown
                        "sLengthMenu": "_MENU_ Rows"
                    },
                    "aoColumns": [ // needed to keep Actions col from being sortable and searchable
                        /* id */ { "bSearchable": true, "bSortable": true },
                        /* vname */ { "bSearchable": true, "bSortable": true },
                        /* actions */ { "bSearchable": false, "bSortable": false }
                    ]
                });
                $('.dt-buttons').css('float', 'right');
                $table.on('draw', function () {
                    // run a function or other action
                });
            });
        </script>
<?php } else { ?>
    None
<?php } ?>