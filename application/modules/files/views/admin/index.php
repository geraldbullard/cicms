<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
        <h3 class="page-title pull-left" style="width:100%;">Files</h3>
        <div class="row" style="min-height:70vh;">
            <div class="col-lg-12" style="min-height:max-content;">
                <!-- elFinder CSS (REQUIRED) -->
                <link href="<?php echo base_url('assets/vendor/elfinder/css/elfinder.min.css'); ?>" rel="stylesheet" />
                <link href="<?php echo base_url('assets/vendor/elfinder/css/theme.css'); ?>" rel="stylesheet" />
                <!-- elFinder JS (REQUIRED) -->
                <script src="<?php echo base_url('assets/vendor/elfinder/js/elfinder.min.js'); ?>"></script>
                <!-- Extra contents editors (OPTIONAL) -->
                <script src="<?php echo base_url('assets/vendor/elfinder/js/extras/editors.default.min.js'); ?>"></script>
                <!-- GoogleDocs Quicklook plugin for GoogleDrive Volume (OPTIONAL) -->
                <!--<script src="js/extras/quicklook.googledocs.js"></script>-->
                <!-- elFinder initialization (REQUIRED) -->
                <script charset="utf-8">
                    // Documentation for client options:
                    // https://github.com/Studio-42/elFinder/wiki/Client-configuration-options
                    $(document).ready(function() {
                        $('#elfinder').elfinder(
                            // 1st Arg - options
                            {
                                cssAutoLoad : false, // Disable CSS auto loading
                                baseUrl : './', // Base URL to css/*, js/*
                                url : '<?php echo base_url('/assets/vendor/elfinder/php/connector.minimal.php'); ?>', // connector URL (REQUIRED)
                                lang: 'en', // language (OPTIONAL)
                            },
                            // 2nd Arg - before boot up function
                            function(fm, extraObj) {
                                // `init` event callback function
                                fm.bind('init', function() {
                                    // Optional for Japanese decoder "encoding-japanese.js"
                                    if (fm.lang === 'ja') {
                                        fm.loadScript(
                                            [ '//cdn.rawgit.com/polygonplanet/encoding.js/1.0.26/encoding.min.js' ],
                                            function() {
                                                if (window.Encoding && Encoding.convert) {
                                                    fm.registRawStringDecoder(function(s) {
                                                        return Encoding.convert(s, {to:'UNICODE',type:'string'});
                                                    });
                                                }
                                            },
                                            { loadType: 'tag' }
                                        );
                                    }
                                });
                                // Optional for set document.title dynamically.
                                var title = document.title;
                                fm.bind('open', function() {
                                    var path = '',
                                        cwd  = fm.cwd();
                                    if (cwd) {
                                        path = fm.path(cwd.hash) || null;
                                    }
                                    document.title = path? path + ':' + title : title;
                                }).bind('destroy', function() {
                                    document.title = title;
                                });
                            }
                        );
                    });
                </script>
                <style>
                    #elfinder {
                        height: 65vh!important;
                    }
                    .elfinder-cwd-view-icons {
                        margin: 15px;
                    }
                </style>
                <div id="elfinder"></div>
            </div>
        </div>
        <script>
            $(document).ready(function () {
                // Nothing Yet
            });
        </script>
