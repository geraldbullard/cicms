<?php
/* Module functions file */

// If installing the module run this function to create db table(s)
if (!function_exists('bp_installModule')) {
    function bp_installModule() {
        $CI =& get_instance();

        // Update the module status
        $CI->db->set('status', 1);
        $CI->db->where("module", "contents");
        $CI->db->update($CI->db->dbprefix . "modules");

        // Create table(s)
	    $CI->db->query("
			CREATE TABLE IF NOT EXISTS `" . $CI->db->dbprefix . "contents` (
                `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
                `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                `parent` tinyint(11) DEFAULT NULL,
                `sort` tinyint(7) DEFAULT NULL,
                `summary` text COLLATE utf8_unicode_ci,
                `content` mediumtext COLLATE utf8_unicode_ci,
                `metaTitle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                `metaDescription` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                `metaKeywords` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
                `status` int(1) UNSIGNED NOT NULL DEFAULT '1',
                `siteIndex` int(1) UNSIGNED NOT NULL DEFAULT '0',
                `template` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
                `type` int(1) UNSIGNED NOT NULL DEFAULT '1',
                `created` datetime DEFAULT NULL,
                `lastModified` datetime DEFAULT NULL,
                PRIMARY KEY (`id`),
                KEY `ci_content_title_idx` (`title`) USING BTREE,
                KEY `ci_content_slug_idx` (`slug`) USING BTREE,
                KEY `ci_content_type_idx` (`type`) USING BTREE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
	    ");

        // TODO: `contents_meta` is not being used currently, but prepping for future use
	    $CI->db->query("
			CREATE TABLE IF NOT EXISTS `" . $CI->db->dbprefix . "contents_meta` (
                `id` int(20) UNSIGNED NOT NULL AUTO_INCREMENT,
                `post_id` int(20) UNSIGNED NOT NULL DEFAULT '0',
                `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
                `meta_value` longtext COLLATE utf8mb4_unicode_ci,
                PRIMARY KEY (`id`),
                KEY `ci_content_post_id_idx` (`post_id`),
                KEY `ci_content_meta_key_idx` (`meta_key`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
	    ");

	    $CI->db->query("
			CREATE TABLE IF NOT EXISTS `" . $CI->db->dbprefix . "forms` (
                `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
                `title` varchar(255) NOT NULL,
                `formId` varchar(64) DEFAULT NULL,
                `formVerb` varchar(64) DEFAULT NULL,
                `data` text,
                `showTitle` tinyint(1) NOT NULL DEFAULT '0',
                `recaptcha` tinyint(1) NOT NULL DEFAULT '1',
                `storeSubmission` tinyint(1) NOT NULL DEFAULT '1',
                `adminNotify` tinyint(1) NOT NULL DEFAULT '1',
                `notifyEmail` text,
                `ccEmails` text,
                `redirect` text,
                `successMsg` text,
                `hideAfter` tinyint(1) NOT NULL DEFAULT '1',
                `created` datetime DEFAULT NULL,
                `lastModified` datetime DEFAULT NULL,
                PRIMARY KEY (`id`),
                KEY `ci_form_id_idx` (`formId`),
                KEY `ci_form_verb_idx` (`formVerb`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
	    ");

	    $CI->db->query("
			CREATE TABLE IF NOT EXISTS `" . $CI->db->dbprefix . "form_submissions` (
                `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
                `form_id` int(11) DEFAULT NULL,
                `user_id` int(11) DEFAULT NULL,
                `data` text,
                `created` timestamp NULL DEFAULT NULL,
                PRIMARY KEY (`id`),
                KEY `ci_form_id_idx` (`form_id`),
                KEY `ci_user_id_idx` (`user_id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
	    ");

	    $CI->db->query("
			CREATE TABLE IF NOT EXISTS `" . $CI->db->dbprefix . "menus` (
                `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
                `designation` varchar(64) DEFAULT NULL,
                `title` varchar(255) NOT NULL,
                `menuData` text DEFAULT NULL,
                `created` datetime DEFAULT NULL,
                `lastModified` datetime DEFAULT NULL,
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
	    ");

        // First Content
        $CI->db->query("
			INSERT INTO `" . $CI->db->dbprefix . "contents` (
                `id`, `title`, `slug`, `parent`, `sort`, `summary`, `content`, `metaTitle`, `metaDescription`, `metaKeywords`, `status`, `siteIndex`, `template`, `type`, `created`, `lastModified`
            ) VALUES 
            (1, 'First Content', 'first-content', 0, 0, 'Summary', '', 'First Meta Title', 'First Meta Description', 'first, meta, key, words', 1, 1, 'stockhome', 1, '2020-11-17 20:38:00', '2021-12-03 18:50:41'),
            (2, 'Contact Us', 'contact-us', 0, 0, '', '<div class=\"row\">\r\n<div class=\"col-sm-4 ui-resizable\" data-type=\"container-content\"><div data-type=\"component-googlemap\">\r\n<div class=\"googlemap-wrapper\">\r\n<div class=\"embed-responsive embed-responsive-16by9\">\r\n<iframe class=\"embed-responsive-item\" src=\"https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d14897.682811563638!2d105.82315895!3d21.0158462!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2s!4v1456466192755\"></iframe>\r\n</div>\r\n</div>\r\n</div></div>\r\n<div class=\"col-sm-8 ui-resizable\" data-type=\"container-content\"><div data-type=\"component-text\">\r\n<p>{bpForms id=\'1\'}</p>\r\n</div>\r\n</div>\r\n</div>', '', '', '', 1, 0, NULL, 1, '2021-12-01 21:16:15', '2021-12-01 21:16:15'),
            (3, 'About Us', 'about-us', 0, 0, 'About Us Page', '', '', '', '', 1, 0, NULL, 1, '2021-12-03 18:53:49', '2021-12-03 18:53:49');
	    ");

        // First Menu
        $CI->db->query("
			INSERT INTO `" . $CI->db->dbprefix . "menus` (
                `id`, `designation`, `title`, `menuData`, `created`, `lastModified`
            ) VALUES (
                NULL, 'primary', 'Primary Menu', 'a:5:{s:4:\"item\";a:6:{i:100002;s:9:\"no-parent\";i:3;s:9:\"no-parent\";i:100004;s:9:\"no-parent\";i:100005;s:6:\"100004\";i:2;s:9:\"no-parent\";i:100003;s:9:\"no-parent\";}s:5:\"title\";a:6:{i:100002;s:4:\"Home\";i:3;s:8:\"About Us\";i:100004;s:11:\"Parent Menu\";i:100005;s:8:\"Sub Menu\";i:2;s:10:\"Contact Us\";i:100003;s:6:\"Google\";}s:4:\"slug\";a:6:{i:100002;s:1:\"/\";i:3;s:8:\"about-us\";i:100004;s:1:\"#\";i:100005;s:1:\"#\";i:2;s:10:\"contact-us\";i:100003;s:23:\"https://www.google.com/\";}s:5:\"class\";a:6:{i:100002;s:7:\"a-class\";i:3;s:7:\"a-class\";i:100004;s:7:\"a-class\";i:100005;s:7:\"a-class\";i:2;s:7:\"a-class\";i:100003;s:7:\"a-class\";}s:6:\"target\";a:6:{i:100002;s:5:\"_self\";i:3;s:5:\"_self\";i:100004;s:5:\"_self\";i:100005;s:5:\"_self\";i:2;s:5:\"_self\";i:100003;s:6:\"_blank\";}}', '2021-12-02 01:52:49', '2021-12-03 19:00:00'
            );
	    ");

        // First Form
        $CI->db->query("
			INSERT INTO `" . $CI->db->dbprefix . "forms` (
                `id`, `title`, `formId`, `formVerb`, `data`, `showTitle`, `recaptcha`, `storeSubmission`, `adminNotify`, `notifyEmail`, `ccEmails`, `redirect`, `successMsg`, `hideAfter`
            ) VALUES (
                NULL, 'Contact Us', 'contact_us', 'contact-us', '[{\"type\":\"text\",\"required\":true,\"label\":\"Name\",\"className\":\"form-control\",\"name\":\"text-1638410991847\",\"access\":false,\"subtype\":\"text\"},{\"type\":\"textarea\",\"required\":true,\"label\":\"Message\",\"className\":\"form-control\",\"name\":\"textarea-1638411014565\",\"access\":false,\"subtype\":\"textarea\"},{\"type\":\"button\",\"subtype\":\"submit\",\"label\":\"Submit\",\"className\":\"btn btn-primary\",\"name\":\"button-1638411034424\",\"value\":\"Send\",\"access\":false,\"style\":\"primary\"}]', 0, 0, 1, 1, NULL, NULL, '', 'Thank You! Someone will contact you shortly.', 1
            );
	    ");

        return true;
    }
}

// If uninstalling the module run this function to remove db entries if selected
if (!function_exists('bp_uninstallModule')) {
    function bp_uninstallModule() {
        $CI =& get_instance();

        // Update the module status
        $CI->db->set('status', -1);
        $CI->db->where("module", "contents");
        $CI->db->update($CI->db->dbprefix . "modules");

        // Drop table(s)
        $CI->db->query("
            DROP TABLE IF EXISTS `" . $CI->db->dbprefix . "contents`;
        ");
        $CI->db->query("
            DROP TABLE IF EXISTS `" . $CI->db->dbprefix . "contents_meta`;
        ");
        $CI->db->query("
            DROP TABLE IF EXISTS `" . $CI->db->dbprefix . "forms`;
        ");
        $CI->db->query("
            DROP TABLE IF EXISTS `" . $CI->db->dbprefix . "form_submissions`;
        ");
        $CI->db->query("
            DROP TABLE IF EXISTS `" . $CI->db->dbprefix . "menus`;
        ");

        return true;
    }
}