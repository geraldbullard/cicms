<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/* Default Controller */
$route['default_controller'] = 'contents';

/* Contents Routes */
$route['admin/contents'] = 'contents/admin_index';
$route['admin/contents/getContents'] = 'contents/getContents';
$route['admin/contents/add'] = 'contents/add';
$route['admin/contents/edit/(:any)'] = 'contents/edit/$1';
$route['admin/contents/update/(:any)'] = 'contents/update/$1';
$route['admin/contents/delete/(:any)'] = 'contents/delete/$1';
$route['admin/contents/setSiteIndex/(:any)'] = 'contents/setSiteIndex/$1';

/* Menus Routes */
$route['admin/contents/menus'] = 'contents/menus';
$route['admin/contents/getMenus'] = 'contents/getMenus';
$route['admin/contents/addMenu'] = 'contents/addMenu';
$route['admin/contents/insertMenu'] = 'contents/insertMenu';
$route['admin/contents/insertMenuMeta'] = 'contents/insertMenuMeta';
$route['admin/contents/editMenu/(:any)'] = 'contents/editMenu/$1';
$route['admin/contents/updateMenu/(:any)'] = 'contents/updateMenu/$1';
$route['admin/contents/deleteMenu/(:any)'] = 'contents/deleteMenu/$1';

/* Form Routes */
$route['admin/contents/forms'] = 'contents/forms';
$route['admin/contents/getForms'] = 'contents/getForms';
$route['admin/contents/addForm'] = 'contents/addForm';
$route['admin/contents/editForm/(:any)'] = 'contents/editForm/$1';
$route['admin/contents/updateForm/(:any)'] = 'contents/updateForm/$1';
$route['admin/contents/deleteForm/(:any)'] = 'contents/deleteForm/$1';

/* Submissions Routes */
$route['admin/contents/submissions'] = 'contents/submissions';
$route['admin/contents/getSubmissions'] = 'contents/getSubmissions';
$route['admin/contents/viewSubmission/(:any)'] = 'contents/viewSubmission/$1';

// Dynamic routes for now
$route['(:any)'] = 'contents/index/$1';