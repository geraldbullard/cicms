<?php
// Custom Contents Module Misc Functions

// Parse array into a hierarchical tree structure
if (!function_exists('parseTree')) {
    function parseTree($tree, $root) {
        if (empty($return)) $return = array();
        # Traverse the tree items and search for direct children of the root
        foreach($tree as $child => $parent) {
            $parent = ($parent == 'no-parent' ? 0 : $parent);
            # A direct child is found
            if ($parent == $root) {
                # Remove item from tree (we don't need to traverse this again)
                unset($tree[$child]);
                # Append the child into result array and parse its children
                $return[] = array(
                    'name' => $child,
                    'children' => parseTree($tree, $child)
                );
            }
        }
        return empty($return) ? null : $return;
    }
}

// Traverse tree to print out an unordered list
if (!function_exists('printEditMenuTree')) {
    function printEditMenuTree($tree, $run) {
        if (!is_null($tree) && count($tree) > 0) {
            if ($run < 1) {
                echo '<ul class="sTree listsClass" id="sTree">';
            } else {
                echo '<ul class="" style="display: none;">';
            }
            foreach ($tree as $node) {
                echo '<li ';
                echo 'id="item_' . $node['name'] . '" ';
                echo 'data-name="' . $node['title'] . '" '; // added for titles
                echo 'data-slug="' . $node['slug'] . '" '; // added for slugs
                echo 'data-class="' . $node['class'] . '" '; // added for class names
                echo 'data-target="' . $node['target'] . '" '; // added for target
                // add new extras here
                echo 'data-module="' . $node['name'] . '">';
                echo '<div>' . $node['title'] . '</div>';
                echo '<i class="far fa-times-circle red pull-right clickable" title="Delete Menu Item"></i>';
                printEditMenuTree($node['children'], ($run+1));
                echo '</li>';
            }
            echo '</ul>';
        }
    }
}

// Traverse tree to print out an unordered list for the front end
if (!function_exists('printPlainMenuTree')) {
    function printPlainMenuTree($tree, $options='') {
        if (!is_null($tree) && count($tree) > 0) {
            echo '<ul' . 
            (!empty($options['ulClass']) ? ' class="' . $options['ulClass'] . '"' : '') . 
            (!empty($options['ulId']) ? ' id="' . $options['ulId'] . '"' : '') . 
            '>';
            foreach ($tree as $node) {
                if (isset($node['slug']) && $node['slug'] != '') {
                    if (
                        stristr($node['slug'], 'http') ||
                        stristr($node['slug'], 'javascript:;')
                    ) {
                        $thisurl = $node['slug'];
                    } else {
                        $thisurl = base_url($node['slug']);
                    }
                } else {
                    $thisurl = 'javascript:;';
                }
                echo '<li' . (!empty($node['children']) ? (!empty($options['dropClass']) ? ' class="' . $options['dropClass'] . '"' : '') : null) . '>';
                echo '<a href="' . $thisurl . '"';
                echo ($node['class'] != '' ? ' class="' . $node['class'] . '"' : '');
                echo ($node['target'] != '' ? ' target="' . $node['target'] . '"' : '');
                echo '>';
                echo (isset($node['title']) ? $node['title'] : 'Missing Title') . '</a>';
                printPlainMenuTree($node['children']);
                echo '</li>';
            }
            echo '</ul>';
        }
    }
}

// Traverse tree to merge extra data into tree list
if (!function_exists('mergeExtrasIntoTree')) {
    function mergeExtrasIntoTree($treeMenu, $tree) {
        foreach ($treeMenu as $i => $item) {
            // added for titles
            foreach ($tree['title'] as $t => $title) {
                if ($t == $item['name']) {
                    $item['title'] = $title;
                }
            }
            // added for slugs
            $slugs = array();
            foreach ($tree as $tr => $x) {
                if ($tr == 'slug') {
                    $slugs[] = $x;
                }
            }
            foreach($slugs[0] as $k => $v) {
                if ($k == $item['name']) {
                    $item['slug'] = $v;
                }
            }
            // added for class names
            $classes = array();
            foreach ($tree as $tr => $x) {
                if ($tr == 'class') {
                    $classes[] = $x;
                }
            }
            foreach($classes[0] as $k => $v) {
                if ($k == $item['name']) {
                    $item['class'] = $v;
                }
            }
            // added for target
            $targets = array();
            foreach ($tree as $tr => $x) {
                if ($tr == 'target') {
                    $targets[] = $x;
                }
            }
            foreach($targets[0] as $k => $v) {
                if ($k == $item['name']) {
                    $item['target'] = $v;
                }
            }
            // dupe the slugs code for more extras here
            // now set the final array
            if (empty($item['children'])) { // no children, this branch is done
                $newMenu[] = $item;
            } else { // we have children, set this item arr and restart recursive with children
                $newMenu[] = array(
                    'name' => $item['name'],
                    'title' => $item['title'],
                    'slug' => $item['slug'],
                    'class' => $item['class'],
                    'target' => $item['target'],
                    'children' => mergeExtrasIntoTree($item['children'], $tree),
                );
            }
        }
        return $newMenu;
    }
}

/**
 * Return first doc comment found in this file.
 *
 * @return string
*/
if (!function_exists('getFileDocBlock')) {
    function getFileDocBlock($file) {
        $docComments = array_filter(
            token_get_all( file_get_contents( $file ) ), function($entry) {
            return $entry[0] == T_DOC_COMMENT;
        }
        );
        $fileDocComment = array_shift( $docComments );
        $search  = array('/', '*', 'Name', ':');
        $replace = array('', '', '', '');
        return trim(str_replace($search, $replace, $fileDocComment[1]));
    }
}
