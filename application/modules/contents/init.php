<?php
/* Content module initialization file */

// If uninstalling the content module run this function to remove db entries if selected
if (!function_exists('bp_initializeModule')) {
    function bp_initializeModule($db) {
        $mysqli = new mysqli($db->hostname, $db->username, $db->password, $db->database);
        // Module table entry
        $mysqli->query("
            INSERT IGNORE INTO `" . $db->dbprefix . "modules` (`module`, `title`, `summary`, `theme`, `icon`, `sort`, `visibility`, `status`, `system`)
            VALUES ('contents', 'Content', 'The Content Module Summary', 'adminTheme', 'lnr lnr-list', 99, 1, -1, 0);
        ");
        // Developer Permissions
        $mysqli->query("
            INSERT IGNORE INTO `" . $db->dbprefix . "permissions` (`groups_id`, `module`, `access`, `override`)
            VALUES (1, 'contents', 5, NULL);
        ");
        // Admin Permissions
        $mysqli->query("
            INSERT IGNORE INTO `" . $db->dbprefix . "permissions` (`groups_id`, `module`, `access`, `override`)
            VALUES (2, 'contents', 4, NULL);
        ");
        // Settings (If Needed)
        $mysqli->query("
            INSERT IGNORE INTO `" . $db->dbprefix . "settings` (`define`, `title`, `summary`, `value`, `type`, `edit`, `system`, `visibility`)
            VALUES ('contentsAdminPagination', 'Content Module Pagination Default', 'Content Module Pagination Default', '20', 'contents', 1, 0, 1);
        ");
        $mysqli->query("
            INSERT IGNORE INTO `" . $db->dbprefix . "settings` (`define`, `title`, `summary`, `value`, `type`, `edit`, `system`, `visibility`)
            VALUES ('menusAdminPagination', 'Content Module Menus Pagination Default', 'Content Module Menus Pagination Default', '20', 'contents', 1, 0, 1);
        ");
        $mysqli->query("
            INSERT IGNORE INTO `" . $db->dbprefix . "settings` (`define`, `title`, `summary`, `value`, `type`, `edit`, `system`, `visibility`)
            VALUES ('formsAdminPagination', 'Content Module Forms Pagination Default', 'Content Module Forms Pagination Default', '20', 'contents', 1, 0, 1);
        ");
        $mysqli->query("
            INSERT IGNORE INTO `" . $db->dbprefix . "settings` (`define`, `title`, `summary`, `value`, `type`, `edit`, `system`, `visibility`)
            VALUES ('submissionsAdminPagination', 'Content Module Submissions Pagination Default', 'Content Module Submissions Pagination Default', '20', 'contents', 1, 0, 1);
        ");
        return true;
    }
}