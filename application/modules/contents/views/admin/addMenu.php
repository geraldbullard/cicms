<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<h3 class="page-title">
    <a href="/admin/contents/menus">Menus</a> &raquo; <?php echo $title; ?>
</h3>
<?php if (isset($msg)) { ?>
<div class="row">
    <div class="col-lg-12">
        <div class="alert alert-<?php echo $msgtype; ?> alert-dismissible action-alert" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <?php echo $msg; ?>
        </div>
    </div>
</div>
<?php } ?>
<?php if ($this->session->flashdata('msg')) { ?>
<div class="row">
    <div class="col-lg-12">
        <div class="alert alert-<?php echo $this->session->flashdata('msgtype'); ?> alert-dismissible action-alert" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">x</button>
            <?php echo $this->session->flashdata('msg'); ?>
        </div>
    </div>
</div>
<?php } ?>
<div class="panel margin-bottom-50" id="editorpanel">
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-12 margin-top-10">
                <div class="form-group">
                    <div style="height:10px;"></div>
                    <p>
                        <label for="title">Title</label>
                        <input type="text" name="title" id="c_title" class="form-control" value="<?php echo set_value('title'); ?>">
                    </p>
                    <p>
                        <label for="designation">Location</label>
                        <input type="text" name="designation" id="c_designation" class="form-control" value="<?php echo set_value('designation'); ?>">
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 margin-top-10 margin-bottom-50">
                <div class="panel-group" id="menuItemsAccordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#menuItemsAccordion" href="#collapseOne">
                                    Pages
                                </a> <small>(Click name to add to menu)</small>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <?php
                                    $contents = $this->db->get($this->db->dbprefix . "contents")->result_array();
                                    foreach ($contents as $i => $item) {
                                        echo '<div class="pages-menu-item alert alert-info" ';
                                        echo 'onclick="appendPageToSorting(' .
                                            $item['id'] . ',\'' . $item['title'] .  '\',\'' . $item['slug'] .
                                        '\');">';
                                        echo $item['title'];
                                        echo '<span class="pull-right">&raquo;</span>';
                                        echo '</div>';
                                    }
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#menuItemsAccordion" href="#collapseTwo">
                                    Custom Links
                                </a> <small>(Enter values and click 'Add')</small>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse">
                            <div class="panel-body">
                                <div>
                                    <p>
                                        <label for="nextNavTitle">Text <span class="red">(Required)</span></label>
                                        <input class="form-control" type="text" id="nextNavTitle">
                                    </p>
                                    <p>
                                        <label for="nextNavSlug">Slug/URL</label>
                                        <input class="form-control" type="text" id="nextNavSlug">
                                    </p>
                                    <p>
                                        <label for="nextNavClassName">Class Name (single or multiple)</label>
                                        <input class="form-control" type="text" id="nextNavClassName">
                                    </p>
                                    <p>
                                        <label for="nextNavTarget">Target</label>
                                        <select class="form-control" id="nextNavTarget">
                                            <option value="_self">Same Window/Tab (Default)</option>
                                            <option value="_blank">New Window/Tab</option>
                                        </select>
                                    </p>
                                    <p>
                                        <button id="nextNavSubmit" class="btn btn-primary pull-right disabled" type="button" onclick="appendCustomToSorting(
                                            $('#nextNavTitle').val(),
                                            $('#nextNavSlug').val(),
                                            $('#nextNavClassName').val(),
                                            $('#nextNavTarget').val()
                                        );" disabled>Add</button>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-8 margin-bottom-50">
                <div id="sTreeWrap">
                    <ul class="sTree listsClass" id="sTree"></ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="full-width" style="margin:-35px 0 130px;"><span id="addMenu" class="btn btn-primary pull-right">Save</span></div>
<script>
function appendPageToSorting(id, title, slug) { // target and className ???
    $("#sTree").append(
        '<li id="item_' + id +
        '" data-name="' + title +
        '" data-slug="' + slug +
        '" data-class="' + 'a-class' +
        '" data-target="' + '_self' +
        '" data-module="' + id +
        '"><div>' + title + '</div>' +
        '<i class="far fa-times-circle red pull-right clickable" title="Delete Menu Item"></i></li>'
    );
    setClickableRemove();
}

function appendCustomToSorting(title, slug, className, target) {
    if (slug.length < 1) var slug = 'javascript:;';
    if (className.length < 1) var className = 'a-class';
    var iDs = [];
    $("#sTree li").each(function() {
        var liID = $(this).attr("id").split("_");
        iDs.push(liID[1]);
    });
    iDs.sort(function(a, b) {
        return a - b;
    });
    var highestID = iDs[iDs.length - 1];
    if (highestID < 100000) {
        var nextID = (+highestID + 1 + 100000);
    } else {
        var nextID = (+highestID + 1);
    }
    $("#sTree").append(
        '<li id="item_' + nextID +
        '" data-name="' + title +
        '" data-slug="' + slug +
        '" data-class="' + className +
        '" data-target="' + target +
        '" data-module="' + nextID +
        '"><div>' + title + '</div><i class="far fa-times-circle red pull-right clickable" title="Delete Menu Item"></i></li>'
    );
    $('#nextNavTitle').val("");
    $('#nextNavSlug').val("");
    $('#nextNavClassName').val("");
    $('#nextNavTarget').val("");
    setClickableRemove();
}

function setClickableRemove() {
    $('.clickable').each(function(e) {
        $(this).on('click', function(e) {
            $(this).parent().remove();
        });
    });
}

function addMenu() {
    // Combine sortable list string and title into a data object
    let sortableData = $('#sTree').sortableListsToString();
    let title = $("#c_title").val();
    let designation = $("#c_designation").val();
    // Create a data object
    let data = {
        sortableList: sortableData,
        title: title,
        designation: designation,
    };
    // Make the ajax call
    $.ajax({
        url: '/admin/contents/insertMenu',
        type: 'POST',
        data: data,
        dataType: 'json',
        success: function(response) {
            if (response.status == 'success') {
                window.location = "/admin/contents/editMenu/" + response.insertid;
            } else {
                swal("Update Failure");
            }
        },
        error: function(xhr, status, error) {
            console.error('AJAX Error:', status, error);
            swal("Ajax Error: " + error);
        }
    });
}
$(document).ready(function() {
    $("#nextNavTitle").on('keyup blur', function() {
        if ($(this).val().length < 3) {
            $("#nextNavSubmit").addClass("disabled").attr("disabled", "disabled");
        } else {
            $("#nextNavSubmit").removeClass("disabled").removeAttr("disabled");
        }
    });
    // https://camohub.github.io/jquery-sortable-lists/
    var options = {
        placeholderCss: {
            'background-color': '#ff8'
        },
        hintCss: {
            'background-color': '#bbf'
        },
        onChange: function(cEl) {
            console.log('onChange');
        },
        complete: function(cEl) {
            console.log('complete');
        },
        isAllowed: function(cEl, hint, target) {
            hint.css('background-color', '#99ff99');
            return true;
        },
        opener: {
            active: true,
            as: 'html', // if as is not set plugin uses background image
            close: '<i class="fa fa-minus c3"></i>', // or 'fa-minus c3'
            open: '<i class="fa fa-plus"></i>', // or 'fa-plus'
            openerCss: {
                'display': 'inline-block',
                //'width': '18px', 'height': '18px',
                'float': 'left',
                'margin-left': '-35px',
                'margin-right': '5px',
                //'background-position': 'center center', 'background-repeat': 'no-repeat',
                'font-size': '1.1em'
            }
        },
        ignoreClass: 'clickable'
    };
    $('#sTree').sortableLists(options);
    $('#addMenu').on('click', function() {
        addMenu();
    });
    setClickableRemove();
});
</script>