<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php if (!empty($contents)) { ?>
<div class="panel">
    <div class="panel-body">
        <table class="table table-striped" id="contents-table" width="100%">
            <thead>
                <tr>
                    <th scope="col" width="10%">ID</th>
                    <th scope="col">Title</th>
                    <th scope="col" class="text-right">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php
                        foreach ($contents as $content) {
                        ?>
                <tr>
                    <td width="10%"><?php echo $content['id']; ?></td>
                    <td>
                        <?php echo $content['title']; ?>
                    </td>
                    <td class="text-right">
                        <?php if ($content['siteIndex'] !== '1') { ?>
                        <a href="javascript:;" onclick="setSiteIndex(this);" id="<?php echo $content['id']; ?>">
                            <span class="label label-default">Set Index</span></a>
                        <?php } else { ?>
                        <a href="javascript:;">
                            <span class="label label-success disabled btn btn-xxs">Index</span></a>
                        <?php } ?>
                        <a href="/admin/contents/edit/<?php echo $content['id']; ?>">
                            <span class="label label-primary">Edit</span></a>
                        <a href="javascript:;" onclick="confirmDeleteContent(<?php echo $content['id']; ?>);">
                            <span class="label label-danger">Delete</span></a>
                        <a href="<?php echo base_url($content['slug']); ?>" target="_blank">
                            <span class="label label-info">View</span></a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<script>
function confirmDeleteContent(id) {
    swal({
            title: "Delete Page",
            text: "Are you sure you wish to delete this Page?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Delete",
            closeOnConfirm: false
        },
        function() {
            $.ajax({
                url: '/admin/contents/delete/' + id,
                type: 'GET',
                success: function(response) {
                    var data = JSON.parse(response);
                    if (data.status == 'success') {
                        $.fn.loadContents();
                        swal({
                            title: "Success",
                            text: "The Page was successfully deleted.",
                            type: "success",
                            confirmButtonClass: "btn-success",
                            confirmButtonText: "OK",
                            showCancelButton: false
                        });
                    } else {
                        swal({
                            title: "Error",
                            text: "The was an error deleting the Page.",
                            type: "warning",
                            confirmButtonClass: "btn-danger",
                            confirmButtonText: "OK",
                            showCancelButton: false
                        });
                    }
                }
            });
        });
}
jQuery(document).ready(function() {
    var $table = $('#contents-table').DataTable({
        "dom": 'Blftip',
        "bSortCellsTop": true,
        "pageLength": <?php echo $this->config->item('contentsAdminPagination', 'settings'); ?>,
        "lengthMenu": [
            [<?php echo $this->config->item('contentsAdminPagination', 'settings'); ?>, 30, 60, 100, -1],
            [<?php echo $this->config->item('contentsAdminPagination', 'settings'); ?>, 30, 60, 100, "All"]
        ],
        "order": [
            [0, "asc"] // "desc" newest first, "asc" for oldest first
        ],
        responsive: true, // make table responsive
        buttons: [ // relabel the export button
            //'copy', 'excel'
            {
                extend: 'excel',
                text: 'Export',
                title: 'Page-Export',
                className: 'btn btn-sm btn-primary shadow-sm',
                exportOptions: {
                    columns: [0, 1]
                },
            }
        ],
        "initComplete": function(settings, json) { // do something immediately after the table is drawn
            //applyContentsEmailFilter($table);
        },
        "oLanguage": { // adjust the text for the rows dropdown
            "sLengthMenu": "_MENU_ Rows"
        },
        "aoColumns": [ // needed to keep Actions col from being sortable and searchable
            /* id */
            {
                "bSearchable": true,
                "bSortable": true
            },
            /* vname */
            {
                "bSearchable": true,
                "bSortable": true
            },
            /* actions */
            {
                "bSearchable": false,
                "bSortable": false
            }
        ]
    });
    $('.dt-buttons').css('float', 'right');
    $table.on('draw', function() {
        // run a function or other action
    });
});
</script>
<?php } else { ?>
None
<?php } ?>