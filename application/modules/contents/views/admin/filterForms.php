<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php if (!empty($forms)) { ?>
<div class="panel">
    <div class="panel-body">
        <table class="table table-striped" id="forms-table" width="100%">
            <thead>
                <tr>
                    <th scope="col" width="10%">ID</th>
                    <th scope="col">Title</th>
                    <th scope="col">Shortcode</th>
                    <th scope="col" class="text-right">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php
                        foreach ($forms as $form) {
                        ?>
                <tr>
                    <td><?php echo $form['id']; ?></td>
                    <td><?php echo $form['title']; ?></td>
                    <td>
                        <a href="javascript:;" onclick="Utils.copyTextToClipboard('{bpForms id=\'<?php echo $form['id']; ?>\'}', this);">
                            <p class="sprite-copy left" title="Copy" style="margin:2px 3px 0 0;"></p>&nbsp;
                        </a>
                        {bpForms id="<?php echo $form['id']; ?>"}
                    </td>
                    <td class="text-right">
                        <a href="/admin/contents/viewSubmissions/<?php echo $form['id']; ?>">
                            <span class="label label-default">Submissions</span></a>
                        <a href="/admin/contents/editForm/<?php echo $form['id']; ?>">
                            <span class="label label-primary">Edit</span></a>
                        <a href="javascript:;" onclick="confirmDeleteForm(<?php echo $form['id']; ?>);">
                            <span class="label label-danger">Delete</span></a>
                    </td>
                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<script>
function confirmDeleteForm(id) {
    swal({
            title: "Delete Form",
            text: "Are you sure you wish to delete this Form?",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Delete",
            closeOnConfirm: false
        },
        function() {
            $.ajax({
                url: '/admin/deleteForm/' + id,
                type: 'GET',
                success: function(response) {
                    var data = JSON.parse(response);
                    if (data.status == 'success') {
                        $.fn.loadForms();
                        swal({
                            title: "Success",
                            text: "The Form was successfully deleted.",
                            type: "success",
                            confirmButtonClass: "btn-success",
                            confirmButtonText: "OK",
                            showCancelButton: false
                        });
                    } else {
                        swal({
                            title: "Error",
                            text: "The was an error deleting the Form.",
                            type: "warning",
                            confirmButtonClass: "btn-danger",
                            confirmButtonText: "OK",
                            showCancelButton: false
                        });
                    }
                }
            });
        });
}
jQuery(document).ready(function() {
    var $table = $('#forms-table').DataTable({
        "dom": 'Blftip',
        "bSortCellsTop": true,
        "pageLength": <?php echo $this->config->item('formsAdminPagination', 'settings'); ?>,
        "lengthMenu": [
            [<?php echo $this->config->item('formsAdminPagination', 'settings'); ?>, 30, 60, 100, -1],
            [<?php echo $this->config->item('formsAdminPagination', 'settings'); ?>, 30, 60, 100, "All"]
        ],
        "order": [
            [0, "asc"] // "desc" newest first, "asc" for oldest first
        ],
        responsive: true, // make table responsive
        buttons: [ // relabel the export button
            //'copy', 'excel'
            {
                extend: 'excel',
                text: 'Export',
                title: 'Forms-Export',
                className: 'btn btn-sm btn-primary shadow-sm',
                exportOptions: {
                    columns: [0, 1]
                },
            }
        ],
        "initComplete": function(settings, json) { // do something immediately after the table is drawn
            //applyContentsEmailFilter($table);
        },
        "oLanguage": { // adjust the text for the rows dropdown
            "sLengthMenu": "_MENU_ Rows"
        },
        "aoColumns": [ // needed to keep Actions col from being sortable and searchable
            /* id */
            {
                "bSearchable": true,
                "bSortable": true
            },
            /* title */
            {
                "bSearchable": true,
                "bSortable": true
            },
            /* shortcode */
            {
                "bSearchable": false,
                "bSortable": false
            },
            /* actions */
            {
                "bSearchable": false,
                "bSortable": false
            }
        ]
    });
    $('.dt-buttons').css('float', 'right');
    $table.on('draw', function() {
        // run a function or other action
    });
});
</script>
<?php } else { ?>
None
<?php } ?>