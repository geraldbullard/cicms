<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<h3 class="page-title" style="font-family:'Source Sans Pro',sans-serif;">
    <a href="/admin/contents">Pages</a> &raquo; <?php echo $title; ?>
</h3>
<?php if (isset($msg)) { ?>
<div class="row">
    <div class="col-lg-12">
        <div class="alert alert-<?php echo $msgtype; ?> alert-dismissible action-alert" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <?php echo $msg; ?>
        </div>
    </div>
</div>
<?php } ?>
<?php if ($this->session->flashdata('msg')) { ?>
<div class="row">
    <div class="col-lg-12">
        <div class="alert alert-<?php echo $this->session->flashdata('msgtype'); ?> alert-dismissible action-alert" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">x</button>
            <?php echo $this->session->flashdata('msg'); ?>
        </div>
    </div>
</div>
<?php } ?>
<div class="row">
    <form action="" method="post" type="multipart/form-data" id="content_editor_form">
        <div class="col-lg-9">
            <div class="panel" id="editorpanel">
                <div class="panel-body">
                    <!-- ======================================================================================================= -->
                    <link rel="stylesheet" href="/assets/vendor/kedit/plugins/bootstrap-3.4.1/css/bootstrap.min.css" data-type="keditor-style" />
                    <link rel="stylesheet" href="/assets/vendor/kedit/plugins/font-awesome-4.7.0/css/font-awesome.min.css" data-type="keditor-style" />
                    <link rel="stylesheet" href="/assets/vendor/kedit/css/keditor.css" data-type="keditor-style" />
                    <link rel="stylesheet" href="/assets/vendor/kedit/css/keditor-components.css" data-type="keditor-style" />
                    <link rel="stylesheet" href="/assets/vendor/kedit/plugins/code-prettify/src/prettify.css" />
                    <link rel="stylesheet" href="/assets/vendor/kedit/css/examples.css" />

                    <div class="form-group">
                        <input type="hidden" name="siteIndex" id="c_siteIndex" value="<?php echo $content->siteIndex; ?>" />
                        <div class="row">
                            <div class="col-sm-6">
                                <p>
                                    <label for="title">Name</label>
                                    <input type="text" name="title" id="c_title" class="form-control" value="<?php echo $content->title; ?>" onkeyup="setSlug(this);">
                                </p>
                            </div>
                            <div class="col-sm-6">
                                <p>
                                    <label for="slug">Slug <small style="font-weight:400;">(No Slashes)</small></label>
                                    <input type="text" name="slug" id="c_slug" class="form-control" value="<?php echo $content->slug; ?>">
                                </p>
                            </div>
                        </div>
                        <div data-keditor="html" style="height:600px;">
                            <div id="content-area">
                                <?php echo $content->content; ?>
                            </div>
                        </div>
                        <div style="height:10px;"></div>
                        <p>
                            <label for="summary">Summary/Excerpt</label>
                            <input type="text" name="summary" id="c_summary" class="form-control" value="<?php echo $content->summary; ?>">
                        </p>
                        <p>
                            <label for="metaTitle">Meta Title</label>
                            <input type="text" name="metaTitle" id="c_metaTitle" class="form-control" value="<?php echo $content->metaTitle; ?>">
                        </p>
                        <p>
                            <label for="metaDescription">Meta Description</label>
                            <input type="text" name="metaDescription" id="c_metaDescription" class="form-control" value="<?php echo $content->metaDescription; ?>">
                        </p>
                        <p>
                            <label for="metaKeywords">Meta Keywords</label>
                            <input type="text" name="metaKeywords" id="c_metaKeywords" class="form-control" value="<?php echo $content->metaKeywords; ?>">
                        </p>
                    </div>

                    <!-- Called by main Klorofil theme
                    <script src="/assets/vendor/kedit/plugins/jquery-1.11.3/jquery-1.11.3.min.js"></script>
                    <script src="/assets/vendor/kedit/plugins/bootstrap-3.4.1/js/bootstrap.min.js"></script>
                    <script src="/assets/vendor/kedit/plugins/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>-->
                    <script src="/assets/vendor/kedit/plugins/jquery-ui.touch-punch-0.2.3/jquery.ui.touch-punch.min.js"></script>
                    <script src="/assets/vendor/kedit/plugins/ckeditor-4.11.4/ckeditor.js"></script>
                    <script src="/assets/vendor/kedit/plugins/formBuilder-2.5.3/form-builder.min.js"></script>
                    <script src="/assets/vendor/kedit/plugins/formBuilder-2.5.3/form-render.min.js"></script>
                    <script src="/assets/vendor/kedit/js/keditor.js"></script>
                    <script src="/assets/vendor/kedit/js/keditor-components.js"></script>
                    <script data-keditor="script">
                    function toggleIndividual() {
                        function print_r(obj) {
                            // define tab spacing
                            var tab = '';
                            // check if it's array
                            var isArr = Object.prototype.toString.call(obj) === '[object Array]' ? true : false;
                            // use {} for object, [] for array
                            var str = isArr ? ('Array [' + tab + '\n') : ('Object {' + tab + '\n');
                            // walk through it's properties
                            for (var prop in obj) {
                                if (obj.hasOwnProperty(prop)) {
                                    var val1 = obj[prop];
                                    var val2 = '';
                                    var type = Object.prototype.toString.call(val1);
                                    switch (type) {
                                        // recursive if object/array
                                        case '[object Array]':
                                        case '[object Object]':
                                            val2 = print_r(val1, (tab + '\t'));
                                            break;
                                        case '[object String]':
                                            val2 = '\'' + val1 + '\'';
                                            break;
                                        default:
                                            val2 = val1;
                                    }
                                    str += tab + '\t' + prop + ' => ' + val2 + ',\n';
                                }
                            }
                            // remove extra comma for last property
                            str = str.substring(0, str.length - 2) + '\n' + tab;
                            return isArr ? (str + ']') : (str + '}');
                            var var_dump = print_r; // equivalent function
                        }
                        var allHidden = $(".container-padding-all").hasClass("hidden");
                        if (allHidden) {
                            $(".toggle-container-padding-label").html("All");
                            $(".container-padding-all").removeClass("hidden");
                            $(".container-padding-individual").addClass("hidden");
                            /*$(".txt-container-padding-top").val('');
                            $(".txt-container-padding-right").val('');
                            $(".txt-container-padding-bottom").val('');
                            $(".txt-container-padding-left").val('');*/
                        } else {
                            var allVal = $(".txt-container-padding-all").val();
                            /*$(".txt-container-padding-all").val('');
                            $(".txt-container-padding-top").val(allVal);
                            $(".txt-container-padding-right").val(allVal);
                            $(".txt-container-padding-bottom").val(allVal);
                            $(".txt-container-padding-left").val(allVal);*/
                            $(".toggle-container-padding-label").html("Individual");
                            $(".container-padding-all").addClass("hidden");
                            $(".container-padding-individual").removeClass("hidden");
                        }
                    }

                    function initToolbar() {
                        var toolbar = $('<div class="toolbar"></div>');
                        //var btnViewSource = $('<button type="button" class="view-source"><i class="fa fa-code"></i> View source</button>');
                        var btnViewContent = $('<button type="button" class="view-content"><i class="fa fa-file-text-o"></i> Get content</button>');
                        var btnBackToList = '';
                        var editorCode = '';
                        toolbar.appendTo(document.body);
                        //toolbar.append(btnViewSource);
                        toolbar.append(btnViewContent);
                        toolbar.append(btnBackToList);
                        /*btnViewSource.on('click', function () {
                        editorCode = $('.keditor-ui.keditor-iframe').contents().find(".keditor-content-area-inner").html();
                        //var cleancode = $.strRemove('keditor-toolbar', editorCode);
                        var jHtmlObject = jQuery(editorCode);
                        var editor = jQuery("<p>").append(jHtmlObject);
                        editor.find(".keditor-toolbar").remove();
                        editor.find(".keditor-toolbar-container-content").remove();
                        editor.find(".ui-resizable-handle").remove();
                        var newHtml = editor.html();
                        $('#source-html').html('<pre>' + newHtml + '</pre>');
                        $('#modal-source').modal('show');
                        });*/
                        btnViewContent.on('click', function() {
                            var modal = $('#modal-content');
                            modal.find('.content-html').html(
                                //beautifyHtml(
                                $('#content-area').keditor('getContent')
                                //)
                            );
                            modal.modal('show');
                        });
                    }

                    function initModalContent() {
                        var modal = $(
                            '<div id="modal-content" class="modal fade" tabindex="-1">' +
                            '    <div class="modal-dialog modal-lg">' +
                            '        <div class="modal-content">' +
                            '            <div class="modal-header">' +
                            '                <button type="button" class="close" data-dismiss="modal">&times;</button>' +
                            '                <h4 class="modal-title">Content</h4>' +
                            '            </div>' +
                            '            <div class="modal-body">' +
                            '                <pre class="prettyprint lang-html content-html"></pre>' +
                            '            </div>' +
                            '            <div class="modal-footer">' +
                            '                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' +
                            '            </div>' +
                            '        </div>' +
                            '    </div>' +
                            '</div>'
                        );
                        modal.appendTo(document.body);
                    }

                    function initModalSource() {
                        var modal = $(
                            '<div id="modal-source" class="modal fade" tabindex="-1">' +
                            '    <div class="modal-dialog modal-lg">' +
                            '        <div class="modal-content">' +
                            '            <div class="modal-header">' +
                            '                <button type="button" class="close" data-dismiss="modal">&times;</button>' +
                            '                <h4 class="modal-title">Source</h4>' +
                            '            </div>' +
                            '            <div class="modal-body">' +
                            '                <ul class="nav nav-tabs">' +
                            '                    <li class="active"><a href="#source-html" data-toggle="tab"><i class="fa fa-html5"></i> HTML</a></li>' +
                            //'                    <li><a href="#source-js" data-toggle="tab"><i class="fa fa-code"></i> JavaScript</a></li>' +
                            '                </ul>' +
                            '                <div class="tab-content">' +
                            '                    <div class="tab-pane active" id="source-html">' +
                            //'                        <pre class="prettyprint lang-html source-html"></pre>' +
                            '                    </div>' +
                            //'                    <div class="tab-pane" id="source-js">' +
                            //'                        <pre class="prettyprint lang-js source-js"></pre>' +
                            //'                    </div>' +
                            '                </div>' +
                            '            </div>' +
                            '            <div class="modal-footer">' +
                            '                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' +
                            '            </div>' +
                            '        </div>' +
                            '    </div>' +
                            '</div>'
                        );
                        var htmlCode = $('[data-keditor="html"]').html();
                        /*var htmlInclude = $('<div />').html($('[data-keditor="html-include"]').clone()).html();
                        htmlInclude = htmlInclude.replace('data-keditor="html-include"', '');
                        htmlInclude = htmlInclude.replace(/\</g, '&lt;').replace(/\>/g, '&gt;');
                        modal.find('.source-html').html(beautifyHtml(htmlCode + htmlInclude));*/
                        modal.find('.source-html').html(htmlCode);
                        /*var jsCode = $('[data-keditor="script"]').html();
                        jsCode =  jsCode.replace(/\</g, '&lt;').replace(/\>/g, '&gt;');
                        modal.find('.source-js').html(beautifyJs(jsCode));*/
                        modal.appendTo(document.body);
                    }
                    /*function beautifyHtml(htmlCode) {
                        htmlCode = html_beautify(htmlCode, {
                            'indent_size': '4',
                            'indent_char': ' ',
                            'space_after_anon_function': true,
                            'end_with_newline': true
                        });
                        htmlCode = htmlCode.replace(/</g, '&lt;').replace(/>/g, '&gt;');
                        return PR.prettyPrintOne(htmlCode, 'lang-html');
                    }*/
                    /*function beautifyJs(jsCode) {
                    jsCode = js_beautify(jsCode, {
                    'indent_size': '4',
                    'indent_char': ' ',
                    'space_after_anon_function': true,
                    'end_with_newline': true
                    });
                    return PR.prettyPrintOne(jsCode, 'lang-js');
                    }*/
                    $(function() {
                        initModalSource();
                        initModalContent();
                        initToolbar();
                    });
                    $('#content-area').keditor({
                        // Page Settings
                        extraTopbarItems: {
                            pageSetting: {
                                html: '<a href="javascript:void(0);" class="btn-page-setting" data-extra-setting="pageSetting"><i class="fa fa-fw fa-cog"></i></a>'
                            },
                            /*pageXxxxxxxxx: {
                                html: '<a href="javascript:void(0);" class="btn-page-xxxxxxxx" data-extra-setting="pageXxxxxxxxx"><i class="fa fa-fw fa-th-large"></i></a>'
                            },*/
                        },
                        extraSettings: {
                            pageSetting: {
                                title: 'Page Settings',
                                trigger: '.btn-page-setting',
                                settingInitFunction: function(form, keditor) {
                                    form.append('<div>This is content of page settings.</div><hr />');
                                },
                                /*settingShowFunction: function (form, container, keditor) {
                                    form.append('<p>This content is added when showing setting</p><br />');
                                },*/
                            },
                        },
                        containerSettingEnabled: true,
                        containerSettingInitFunction: function(form, keditor) {
                            // Add control for settings form
                            form.append(
                                '<div class="form-horizontal">' +
                                '   <div class="form-group">' +
                                '       <div class="col-sm-12">' +
                                // Section (Container) ID
                                '           <div class="container-section-id"><p>' +
                                '               <label>Row ID#</label>' +
                                '               <input type="text" class="form-control txt-container-id" />' +
                                '           </p></div>' +
                                /* Padding
                                '           <div class="container-padding">' +
                                '               <label>' +
                                '                   Padding' +
                                '                   <input type="checkbox" name="toggleIndividualPadding" class="form-check-input" onchange="toggleIndividual();">' +
                                '                   <label class="form-check-label toggle-container-padding-label" for="toggleContainerPadding">All</label>' +
                                '               </label>' +
                                '               <p class="container-padding-all">' +
                                '                   <input min="0" type="number" class="form-control txt-container-padding-all" />' +
                                '               </p>' +
                                '           </div>' +
                                '           <div class="container-padding-individual hidden">' +
                                '               <p>' +
                                '                   <label>Top</label>' +
                                '                   <input min="0" type="number" class="form-control txt-container-padding-top" />' +
                                '               </p>' +
                                '               <p>' +
                                '                   <label>Right</label>' +
                                '                   <input min="0" type="number" class="form-control txt-container-padding-right" />' +
                                '               </p>' +
                                '               <p>' +
                                '                   <label>Bottom</label>' +
                                '                   <input min="0" type="number" class="form-control txt-container-padding-bottom" />' +
                                '               </p>' +
                                '               <p>' +
                                '                   <label>Left</label>' +
                                '                   <input min="0" type="number" class="form-control txt-container-padding-left" />' +
                                '               </p>' +
                                '           </div>' +*/
                                '       </div>' +
                                '   </div>' +
                                '</div>'
                            );
                            // Add event handle for background color textbox
                            form.find('.txt-container-id').on('change keyup blur', function() {
                                // Get current setting container
                                var container = keditor.getSettingContainer();
                                // Find '.row' for setting background color
                                // Note: Make sure you have a div for setting background color
                                var row = container.find('.row');
                                if (container.hasClass('keditor-sub-container')) {
                                    // Do nothing
                                } else {
                                    row = row.filter(function() {
                                        return $(this).parents('.keditor-container').length === 1;
                                    });
                                }
                                // Set background color with value of textbox
                                row.attr('id', this.value);
                            });
                            // Add event handle for padding
                            /*form.find('.txt-container-padding-all').on('change keyup', function () {
                            // Get current setting container
                            var container = keditor.getSettingContainer();
                            // Find '.row' for setting background color
                            // Note: Make sure you have a div for setting background color
                            var row = container.find('.row');
                            if (container.hasClass('keditor-sub-container')) {
                            // Do nothing
                            } else {
                            row = row.filter(function () {
                            return $(this).parents('.keditor-container').length === 1;
                            });
                            }
                            // Set background color with value of textbox
                            row.css('padding', this.value);
                            });
                            // Add event handle for only TOP padding
                            form.find('.txt-container-padding-top').on('change keyup', function () {
                            // Get current setting container
                            var container = keditor.getSettingContainer();
                            // Find '.row' for setting background color
                            // Note: Make sure you have a div for setting background color
                            var row = container.find('.row');
                            if (container.hasClass('keditor-sub-container')) {
                            // Do nothing
                            } else {
                            row = row.filter(function () {
                            return $(this).parents('.keditor-container').length === 1;
                            });
                            }
                            // Set background color with value of textbox
                            row.css('padding-top', this.value);
                            });
                            // Add event handle for only RIGHT padding
                            form.find('.txt-container-padding-right').on('change keyup', function () {
                            // Get current setting container
                            var container = keditor.getSettingContainer();
                            // Find '.row' for setting background color
                            // Note: Make sure you have a div for setting background color
                            var row = container.find('.row');
                            if (container.hasClass('keditor-sub-container')) {
                            // Do nothing
                            } else {
                            row = row.filter(function () {
                            return $(this).parents('.keditor-container').length === 1;
                            });
                            }
                            // Set background color with value of textbox
                            row.css('padding-right', this.value);
                            });
                            // Add event handle for only BOTTOM padding
                            form.find('.txt-container-padding-bottom').on('change keyup', function () {
                            // Get current setting container
                            var container = keditor.getSettingContainer();
                            // Find '.row' for setting background color
                            // Note: Make sure you have a div for setting background color
                            var row = container.find('.row');
                            if (container.hasClass('keditor-sub-container')) {
                            // Do nothing
                            } else {
                            row = row.filter(function () {
                            return $(this).parents('.keditor-container').length === 1;
                            });
                            }
                            // Set background color with value of textbox
                            row.css('padding-bottom', this.value);
                            });
                            // Add event handle for only LEFT padding
                            form.find('.txt-container-padding-left').on('change keyup', function () {
                            // Get current setting container
                            var container = keditor.getSettingContainer();
                            // Find '.row' for setting background color
                            // Note: Make sure you have a div for setting background color
                            var row = container.find('.row');
                            if (container.hasClass('keditor-sub-container')) {
                            // Do nothing
                            } else {
                            row = row.filter(function () {
                            return $(this).parents('.keditor-container').length === 1;
                            });
                            }
                            // Set background color with value of textbox
                            row.css('padding-left', this.value);
                            });*/
                        },
                        containerSettingShowFunction: function(form, container, keditor) {
                            // Find '.row' div
                            // Note: Make sure you have a div for setting background color
                            var row = container.find('.row');
                            // User "prop()" method to get properties of HTML element
                            var backgroundColor = row.prop('style').backgroundColor || '';
                            // User 'backgroundColor' for value of background color textbox
                            form.find('.txt-bg-color').val(backgroundColor);
                        },
                        containerSettingHideFunction: function(form, keditor) {
                            // Clean value of background color textbox when hiding settings form
                            form.find('.txt-bg-color').val('');
                        },
                    });
                    </script>
                    <!-- ======================================================================================================= -->
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="panel" id="editorsidebar">
                <div class="panel-body">
                    <h4><?php echo $content->title; ?>
                        <div class="pull-right">
                            <a href="<?php echo base_url($content->slug); ?>" target="_blank">
                                <button type="button" class="btn btn-xs btn-primary" id="dt-add-row">
                                    <i class="fas fa-external-link-alt"></i>&nbsp;&nbsp;View Content
                                </button>
                            </a>
                        </div>
                    </h4>
                    <div id="sidebarContent">
                        <div class="form-group">
                            <label for="template">Page Template</label>
                            <?php
                            $temapltes = array();
                            $files = array_diff(scandir(VIEWPATH . 'templates/' . $this->config->item('siteTheme', 'settings')), array('.', '..', '404.php'));
                            foreach ($files as $i => $file) {
                                $templates[$i]['title'] = getFileDocBlock(VIEWPATH . 'templates/' . $this->config->item('siteTheme', 'settings') . '/' . $file);
                                $templates[$i]['value'] = str_replace(".php", "", $file);
                            }
                            echo '<select name="template" class="form-control">';
                            echo '<option value="">Select a Page Template</option>';
                            foreach (array_values($templates) as $template) {
                                $selected = '';
                                if (!empty($content->template) && $content->template == $template['value'])
                                    $selected = ' selected="selected"';
                                echo '<option value="' . $template['value'] .'"' . $selected . '>' . $template['title'] . '</option>';
                            }
                            echo '</select>';
                            ?>
                        </div>
                    </div>
                    <div class="margin-bottom-10 text-right">
                        <button type="button" class="btn btn-primary" id="content_save">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<script>
function resizeDefaultEditorWidth() {
    var editorWidth = $(".keditor-ui.keditor-iframe-wrapper").width();
    $(".keditor-ui.keditor-iframe-width-switcher").removeAttr("style").width(editorWidth);
}

function createSlug(name) {
    // Mapping of numbers to words
    const numberMap = {
        '0': 'zero',
        '1': 'one',
        '2': 'two',
        '3': 'three',
        '4': 'four',
        '5': 'five',
        '6': 'six',
        '7': 'seven',
        '8': 'eight',
        '9': 'nine'
    };
    // Replace numbers with their corresponding words
    name = name.replace(/[0-9]/g, (match) => numberMap[match]);
    let slug = '';
    let lastCharWasSpace = false;
    for (let i = 0; i < name.length; i++) {
        const char = name[i];
        // Check for alphanumeric characters
        if (/[a-zA-Z]/.test(char)) {
            if (lastCharWasSpace) {
                slug += '-';
                lastCharWasSpace = false;
            }
            slug += char.toLowerCase();
        } else if (char === ' ') {
            // If the current character is a space, mark that the last character was a space
            lastCharWasSpace = true;
        }
    }
    // Handle trailing spaces by trimming
    slug = slug.replace(/-+$/, ''); // Remove trailing dashes
    return slug;
}

function setSlug(title) {
    let nameToSlug = createSlug(title.value);
    $("#c_slug").val(nameToSlug);
}
$(document).ready(function() {
    $(window).resize(function() {
        resizeDefaultEditorWidth();
    });
    resizeDefaultEditorWidth();
    $(window).scroll(function(e) {
        if ($(window).width() > 1199) {
            var $el = $('#editorsidebar');
            var isPositionFixed = ($el.css('position') == 'fixed');
            if ($(this).scrollTop() > 200 && !isPositionFixed) {
                $el.css({
                    'position': 'fixed',
                    'top': '80px',
                    'right': '15px',
                    'width': '21%'
                });
            }
            if ($(this).scrollTop() < 200 && isPositionFixed) {
                $el.css({
                    'position': 'static',
                    'top': '0px',
                    'width': '100%'
                });
            }
        }
    });
    $("#content_save").on('click', function() {
        var data = $('#content_editor_form').serializeArray();
        data.push({
            name: 'content',
            value: $('#content-area').keditor('getContent')
        });
        $.ajax({
            type: "POST",
            url: "/admin/contents/update/<?php echo $this->uri->segment(4); ?>",
            data: data,
            success: function(response) {
                var result = JSON.parse(response);
                if (result.status == 'success') {
                    window.location = window.location;
                } else {
                    swal("Error Updating");
                }
            },
            error: function() {
                swal("Error Sending");
            }
        });
    });
});
</script>