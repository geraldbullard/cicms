<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
        <div class="row">
            <div class="col-lg-12">
                <h3 class="page-title pull-left">Menus</h3>
                <div class="pull-right">
                    <a href="/admin/contents/addMenu/">
                        <button class="btn btn-sm btn-primary" id="dt-add-row">
                            <i class="fa fa-plus"></i>&nbsp;&nbsp;Add Menu
                        </button>
                    </a>
                </div>
            </div>
        </div>
        <?php if ($this->session->flashdata('msg')) { ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="alert alert-<?php echo $this->session->flashdata('msgtype'); ?> alert-dismissible action-alert" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">x</button>
                    <?php echo $this->session->flashdata('msg'); ?>
                </div>
            </div>
        </div>
        <?php } ?>
        <div class="row">
            <div class="col-lg-12">
                <div id="menusListingTable"></div>
            </div>
        </div>
        <script>
            $(document).ready(function () {
                $("#clearFilter").on("click", function (evt) {
                    evt.preventDefault();
                    $('#menus_filter').trigger("reset");
                    $.fn.loadMenus();
                    $("#clearFilter").blur();
                });
                $("#menusFilter").on("click", function (evt) {
                    evt.preventDefault();
                    $.fn.loadMenus();
                    $("#menusFilter").blur();
                });
                $.fn.loadMenus = function () {
                    $.blockUI({target: "#menusListingTable"});
                    $params = $("#menus_filter").serialize();
                    $.get('<?php echo base_url('/admin/contents/getMenus'); ?>?' + $params, function (data) {
                        $("#menusListingTable").html(data);
                        $.unblockUI({target: "#menusListingTable"});
                    }).fail(function () {
                        alert("Error!", "Something went wrong loading the Menus!", "error");
                    });
                };
                $.fn.loadMenus();
            });
        </script>
