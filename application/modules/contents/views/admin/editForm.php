<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php if (!$this->ion_auth->in_group('developer')) { ?>
<style scoped="scoped">
#fb-editor .form-actions.btn-group {
    display: none;
}
</style>
<?php } ?>
<h3 class="page-title">
    <a href="/admin/contents/forms">Forms</a> &raquo; <?php echo $title; ?>
    <button type="button" id="save-action-button" class="btn btn-primary save-all pull-right">Save</button>
</h3>
<?php if (isset($msg)) { ?>
<div class="row">
    <div class="col-lg-12">
        <div class="alert alert-<?php echo $msgtype; ?> alert-dismissible action-alert" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <?php echo $msg; ?>
        </div>
    </div>
</div>
<?php } ?>
<?php if ($this->session->flashdata('msg')) { ?>
<div class="row">
    <div class="col-lg-12">
        <div class="alert alert-<?php echo $this->session->flashdata('msgtype'); ?> alert-dismissible action-alert" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">x</button>
            <?php echo $this->session->flashdata('msg'); ?>
        </div>
    </div>
</div>
<?php } ?>
<div class="row">
    <div class="col-lg-12">
        <form action="/admin/contents/updateForm/<?php echo $this->uri->segment(4); ?>" method="post" type="multipart/form-data" id="form_edit">
            <div class="form-group">
                <p>
                    <label for="title">Form Name</label>
                    <input type="text" name="title" id="title" class="form-control" value="<?php echo $form->title; ?>" onkeyup="setFormVerb();">
                </p>
            </div>
            <div class="panel" id="editorpanel">
                <div class="panel-body">
                    <input type="hidden" id="formData" name="data" value="" />
                    <ul class="nav nav-tabs" id="formsTabs" role="tablist">
                        <li class="nav-item active" id="formedit-li">
                            <a class="nav-link active" id="formedit-tab" data-toggle="tab" href="#formedit" role="tab" aria-controls="formedit" aria-selected="true">Form Builder</a>
                        </li>
                        <li class="nav-item" id="actions-li">
                            <a class="nav-link" id="actions-tab" data-toggle="tab" href="#actions" role="tab" aria-controls="actions" aria-selected="false">Emails & Actions</a>
                        </li>
                        <li class="nav-item" id="display-li">
                            <a class="nav-link" id="display-tab" data-toggle="tab" href="#display" role="tab" aria-controls="display" aria-selected="false">Display</a>
                        </li>
                        <li class="nav-item" id="advanced-li">
                            <a class="nav-link" id="advanced-tab" data-toggle="tab" href="#advanced" role="tab" aria-controls="advanced" aria-selected="false">Advanced</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="formsTabsContent">
                        <div class="tab-pane fade active in" id="formedit" role="tabpanel" aria-labelledby="formedit-tab">
                            <div id="fb-editor"></div>
                        </div>
                        <div class="tab-pane fade" id="actions" role="tabpanel" aria-labelledby="actions-tab">
                            <div class="form-group">
                                <p>
                                    <label for="adminNotify">Admin Email Notification (Site Admin)</label>
                                    <select name="adminNotify" id="adminNotify" class="form-control">
                                        <option value="1" <?php echo ($form->adminNotify == 1 ? ' selected' : ''); ?>>Yes</option>
                                        <option value="0" <?php echo ($form->adminNotify == 0 ? ' selected' : ''); ?>>No</option>
                                    </select>
                                </p>
                                <p>
                                    <label for="notifyEmail">Notification Email (Not Site Admin)</label>
                                    <input type="text" name="notifyEmail" id="notifyEmail" placeholder="Leave empty for no notification" class="form-control" value="<?php echo $form->notifyEmail; ?>" />
                                </p>
                                <p>
                                    <label for="ccEmails">CC Notification Email(s) (Comma separated, will not send if 'Notification Email' is empty)</label>
                                    <input type="text" name="ccEmails" id="ccEmails" placeholder="Comma separated emails" class="form-control" value="<?php echo $form->ccEmails; ?>" />
                                </p>
                                <p>
                                    <label for="redirect">Redirect</label>
                                    <input type="url" name="redirect" id="redirect" placeholder="https://" class="form-control" value="<?php echo $form->redirect; ?>" />
                                </p>
                                <p>
                                    <label for="successMsg">Success Message</label>
                                    <textarea name="successMsg" id="successMsg" rows="4" class="form-control"><?php
                                        echo $form->successMsg;
                                    ?></textarea>
                                </p>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="display" role="tabpanel" aria-labelledby="display-tab">
                            <p>
                                <label for="showTitle">Show Form Name</label>
                                <select name="showTitle" id="showTitle" class="form-control">
                                    <option value="1" <?php echo ($form->showTitle == 1 ? ' selected' : ''); ?>>Yes</option>
                                    <option value="0" <?php echo ($form->showTitle == 0 ? ' selected' : ''); ?>>No</option>
                                </select>
                            </p>
                        </div>
                        <div class="tab-pane fade" id="advanced" role="tabpanel" aria-labelledby="advanced-tab">
                            <div class="form-group">
                                <p>
                                    <label for="storeSubmission">Store Submission</label>
                                    <select name="storeSubmission" id="storeSubmission" class="form-control">
                                        <option value="1" <?php echo ($form->storeSubmission == 1 ? ' selected' : ''); ?>>Yes</option>
                                        <option value="0" <?php echo ($form->storeSubmission == 0 ? ' selected' : ''); ?>>No</option>
                                    </select>
                                </p>
                                <p>
                                    <label for="hideAfter">Hide Form After Successful Submission</label>
                                    <select name="hideAfter" id="hideAfter" class="form-control">
                                        <option value="1" <?php echo ($form->hideAfter == 1 ? ' selected' : ''); ?>>Yes</option>
                                        <option value="0" <?php echo ($form->hideAfter == 0 ? ' selected' : ''); ?>>No</option>
                                    </select>
                                </p>
                                <p>
                                    <label for="recaptcha">Add Google reCAPCTHA</label>
                                    <select name="recaptcha" id="recaptcha" class="form-control">
                                        <option value="1" <?php echo ($form->recaptcha == 1 ? ' selected' : ''); ?>>Yes</option>
                                        <option value="0" <?php echo ($form->recaptcha == 0 ? ' selected' : ''); ?>>No</option>
                                    </select>
                                </p>
                                <p>
                                    <label for="formId">Form ID (ie: contact_us, must be unique)</label>
                                    <input type="text" name="formId" id="formId" class="form-control" value="<?php echo $form->formId; ?>">
                                </p>
                                <p>
                                    <label for="formVerb">Form Verb (Used to process $_POST values in form submission, must be unique)</label>
                                    <input type="text" name="formVerb" id="formVerb" class="form-control" value="<?php echo $form->formVerb; ?>">
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <script src="<?php echo base_url('assets/vendor/formBuilder/form-builder.min.js'); ?>"></script>
        <script>
        jQuery(function($) {
            let $fbEditor = document.getElementById('fb-editor');
            // Later we set data from database
            let form_data = <?php echo ($form->data ? $form->data : '[]'); ?>;
            let formBuilder = $($fbEditor).formBuilder({
                formData: form_data
            });
            document.body.addEventListener('click', function(evt) {
                if (evt.target.className === 'btn btn-primary save-all pull-right') {
                    //console.log(formBuilder.formData);
                    $("#formData").val(formBuilder.formData);
                    $("#form_edit").submit();
                }
            }, false);
        });

        function clearActiveTabs() {
            $("#formedit-li").removeClass("active");
            $("#actions-li").removeClass("active");
            $("#display-li").removeClass("active");
            $("#advanced-li").removeClass("active");
            $("#formedit-li a").removeClass("active");
            $("#actions-li a").removeClass("active");
            $("#display-li a").removeClass("active");
            $("#advanced-li a").removeClass("active");
            $('#formsTabsContent .tab-pane').each(function(e) {
                $(this).removeClass("active in show");
            });
        }

        function setFormVerb() {
            let fId = $("#title").val().replace(/[^a-zA-Z ]/g, "").toLowerCase().replace(/ /g, "_");
            let fVerb = $("#title").val().replace(/[^a-zA-Z ]/g, "").toLowerCase().replace(/ /g, "-");
            $("#formId").val(fId);
            $("#formVerb").val(fVerb);
        }
        $(document).ready(function() {
            $('#form_edit').on('keypress', function(e) {
                let keyCode = e.keyCode || e.which;
                if (keyCode === 13) {
                    e.preventDefault();
                    return false;
                }
            });
            $('#formedit-li').on('click', function(e) {
                clearActiveTabs();
                $(this).addClass("active");
                $("#formedit").addClass("in");
            });
            $('#actions-li').on('click', function(e) {
                clearActiveTabs();
                $(this).addClass("active");
                $("#actions").addClass("in");
            });
            $('#display-li').on('click', function(e) {
                clearActiveTabs();
                $(this).addClass("active");
                $("#display").addClass("in");
            });
            $('#advanced-li').on('click', function(e) {
                clearActiveTabs();
                $(this).addClass("active");
                $("#advanced").addClass("in");
            });
        });
        </script>
    </div>
</div>