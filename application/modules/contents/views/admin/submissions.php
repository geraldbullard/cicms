<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
        <h3 class="page-title pull-left">Form Submissions</h3>
        <form action="" id="submissions_filter" method="post" accept-charset="utf-8">
            <div class="form-group pull-right">
                <select name="filterformid" class="form-control" id="submissionsFilter" />
                    <option value="">Filter Submissions (All Forms)</option>
                    <?php foreach ($forms as $form) { ?>
                    <option value="<?php echo $form['id']; ?>"><?php echo $form['title']; ?></option>
                    <?php } ?>
                </select>
            </div>
        </form>
        <?php if ($this->session->flashdata('msg')) { ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="alert alert-<?php echo $this->session->flashdata('msgtype'); ?> alert-dismissible action-alert" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">x</button>
                    <?php echo $this->session->flashdata('msg'); ?>
                </div>
            </div>
        </div>
        <?php } ?>
        <div class="row">
            <div class="col-lg-12">
                <div id="submissionsListingTable"></div><hr />
            </div>
        </div>
        <script>
            $(document).ready(function () {
                /*$("#clearFilter").on("click", function (evt) {
                    evt.preventDefault();
                    $('#submissions_filter').trigger("reset");
                    $.fn.loadSubmissions();
                    $("#clearFilter").blur();
                });*/
                $("#submissionsFilter").on("change", function (evt) {
                    evt.preventDefault();
                    $.fn.loadSubmissions();
                    $("#submissionsFilter").blur();
                });
                $.fn.loadSubmissions = function () {
                    $.blockUI({target: "#submissionsListingTable"});
                    $params = $("#submissions_filter").serialize();
                    $.get('<?php echo base_url('/admin/contents/getSubmissions'); ?>?' + $params, function (data) {
                        $("#submissionsListingTable").html(data);
                        $.unblockUI({target: "#submissionsListingTable"});
                    }).fail(function () {
                        alert("Error!", "Something went wrong loading the Form Submissions!", "error");
                    });
                };
                $.fn.loadSubmissions();
            });
        </script>
