<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php if (!empty($apiusers)) { ?>
        <div class="panel">
            <div class="panel-body">
                <table class="table table-striped" id="apiusers-table" width="100%">
                    <thead>
                        <tr>
                            <th scope="col" width="10%">ID</th>
                            <th scope="col">Username</th>
                            <th scope="col">Name</th>
                            <th scope="col">Created</th>
                            <th scope="col">Last Login</th>
                            <th scope="col" class="text-right">Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($apiusers as $apiuser) {
                        ?>
                        <tr>
                            <td><?php echo $apiuser['id']; ?></td>
                            <td><?php echo $apiuser['username']; ?></td>
                            <td><?php echo $apiuser['name']; ?></td>
                            <td><?php echo $apiuser['created_at']; ?></td>
                            <td><?php echo $apiuser['last_login']; ?></td>
                            <td class="text-right">
                                <a href="/admin/apiusers/edit/<?php echo $apiuser['id']; ?>">
                                    <span class="label label-primary">Edit</span></a>
                                <a href="javascript:;" onclick="confirmDeleteApiuser(<?php echo $apiuser['id']; ?>);">
                                    <span class="label label-danger">Delete</span></a>
                                </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <script>
            function confirmDeleteApiuser(id) {
                swal({
                    title: "Delete API User",
                    text: "Are you sure you wish to delete this API User?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Delete",
                    closeOnConfirm: false
                },
                function() {
                    $.ajax({
                        url: '/admin/apiusers/delete/' + id,
                        type: 'GET',
                        success: function(response) {
                            var data = JSON.parse(response);
                            if (data.status == 'success') {
                                $.fn.loadApiusers();
                                swal({
                                    title: "Success",
                                    text: "The API User was successfully deleted.",
                                    type: "success",
                                    confirmButtonClass: "btn-success",
                                    confirmButtonText: "OK",
                                    showCancelButton: false
                                });
                            } else {
                                swal({
                                    title: "Error",
                                    text: "The was an error deleting the API User.",
                                    type: "warning",
                                    confirmButtonClass: "btn-danger",
                                    confirmButtonText: "OK",
                                    showCancelButton: false
                                });
                            }
                        }
                    });
                });
            }
            jQuery(document).ready(function() {
                var $table = $('#apiusers-table').DataTable({
                    "dom": 'Blftip',
                    "bSortCellsTop": true,
                    "pageLength": <?php echo $this->config->item('apiusersAdminPagination', 'settings'); ?>,
                    "lengthMenu": [
                        [<?php echo $this->config->item('apiusersAdminPagination', 'settings'); ?>, 30, 60, 100, -1],
                        [<?php echo $this->config->item('apiusersAdminPagination', 'settings'); ?>, 30, 60, 100, "All"]
                    ],
                    "order": [
                        [0, "asc"] // "desc" newest first, "asc" for oldest first
                    ],
                    responsive: true, // make table responsive
                    buttons: [ // relabel the export button
                        //'copy', 'excel'
                        {
                            extend: 'excel',
                            text: 'Export',
                            title: 'Api-Users-Export',
                            className: 'btn btn-sm btn-primary shadow-sm',
                            exportOptions: {
                                columns: [0, 1]
                            },
                        }
                    ],
                    "initComplete": function(settings, json) { // do something immediately after the table is drawn
                        //applyApiusersEmailFilter($table);
                    },
                    "oLanguage": { // adjust the text for the rows dropdown
                        "sLengthMenu": "_MENU_ Rows"
                    },
                    "aoColumns": [ // needed to keep Actions col from being sortable and searchable
                        /* id */ { "bSearchable": true, "bSortable": true },
                        /* username */ { "bSearchable": true, "bSortable": true },
                        /* name */ { "bSearchable": true, "bSortable": true },
                        /* created_at */ { "bSearchable": true, "bSortable": false },
                        /* last_login */ { "bSearchable": true, "bSortable": false },
                        /* actions */ { "bSearchable": false, "bSortable": false }
                    ]
                });
                $('.dt-buttons').css('float', 'right');
                $table.on('draw', function () {
                    // run a function or other action
                });
            });
        </script>
<?php } else { ?>
    None
<?php } ?>