<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
        <h3 class="page-title pull-left">API Users</h3>
        <div class="pull-right">
            <a href="/admin/apiusers/add/">
                <button class="btn btn-sm btn-primary" id="dt-add-row">
                    <i class="fa fa-plus"></i>&nbsp;&nbsp;Add API User
                </button>
            </a>
        </div>
        <?php if ($this->session->flashdata('msg')) { ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="alert alert-<?php echo $this->session->flashdata('msgtype'); ?> alert-dismissible action-alert" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">x</button>
                    <?php echo $this->session->flashdata('msg'); ?>
                </div>
            </div>
        </div>
        <?php } ?>
        <div class="row">
            <div class="col-lg-12">
                <div id="apiusersListingTable"></div><hr />
            </div>
        </div>
        <script>
            $(document).ready(function () {
                $("#clearFilter").on("click", function (evt) {
                    evt.preventDefault();
                    $('#apiusers_filter').trigger("reset");
                    $.fn.loadApiusers();
                    $("#clearFilter").blur();
                });
                $("#apiusersFilter").on("click", function (evt) {
                    evt.preventDefault();
                    $.fn.loadApiusers();
                    $("#apiusersFilter").blur();
                });
                $.fn.loadApiusers = function () {
                    $.blockUI({target: "#apiusersListingTable"});
                    $params = $("#apiusers_filter").serialize();
                    $.get('<?php echo base_url('/admin/apiusers/getapiusers'); ?>?' + $params, function (data) {
                        $("#apiusersListingTable").html(data);
                        $.unblockUI({target: "#apiusersListingTable"});
                    }).fail(function () {
                        alert("Error!", "Something went wrong loading the API Users!", "error");
                    });
                };
                $.fn.loadApiusers();
            });
        </script>
