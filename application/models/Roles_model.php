<?php

class Roles_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    // Filter Roles
    public function filter($filter)
    {
        $this->db->distinct();

        /*if (isset($filter->first_name) && $filter->first_name !== null) {
            $this->db->where("first_name", $filter->first_name);
        }*/
        // see Users Model for much more filtering code

	    $this->db->order_by("id", "asc");
	    $roles = $this->db->get($this->db->dbprefix . "groups");

        if ($roles->num_rows() < 1) {
            return false;
        }
        return $roles->result_array();
    }

    public function delete($id) {
	    $this->db->where("id", $id);
	    if ($this->db->delete($this->db->dbprefix . "groups")) {
	    	return true;
	    }
	    return false;
    }

    public function getRolePermissions($roleid = null, $module = null, $access = null, $override = null)
    {
        if ($roleid) $this->db->where('groups_id', $roleid);
        if ($module) $this->db->where('module', $module);
        if ($access) $this->db->where('access', $access);
        if ($override) $this->db->where('override', $override);
        $perms = $this->db->get('ci_permissions');

        return $perms->result_array();
    }

}
