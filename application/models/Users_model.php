<?php

class Users_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    // Filter Enrollers
    public function filter($filter)
    {
        $this->db->distinct();

        if (isset($filter->first_name) && $filter->first_name !== null) {
            $this->db->where("first_name", $filter->first_name);
        }
        /*if (isset($filter->id) && $filter->id !== null) {
            $this->db->where("E.id", $filter->id);
        }

        if (isset($filter->state[0]) && !empty($filter->state[0])) {
            $where = array();
            foreach ($filter->state as $state) {
                $where[] = "ELI.resident_state = '" . $state . "' OR ENRLI.non_resident_state = '" . $state . "'";
            }
            $where_str = implode(" OR ", $where);
            $this->db->where($where_str);
        }

        if (isset($filter->software[0]) && !empty($filter->software[0])) {
            $where = array();
            foreach ($filter->software as $soft) {
                $where[] = "EES.id_software = '" . $soft . "'";
            }
            $where_str = implode(" OR ", $where);
            $this->db->where($where_str);
        }

        if (isset($filter->multilingual[0]) && !empty($filter->multilingual[0])) {
            $where = array();
            foreach ($filter->multilingual as $ml) {
                $where[] = "EEL.id_language = '" . $ml . "'";
            }
            $where_str = implode(" OR ", $where);
            $this->db->where($where_str);
        }

        if (isset($filter->status) && $filter->status !== null) {
            $this->db->where("E.status", $filter->status);
        }

        if (isset($filter->callcenter) && $filter->callcenter !== null) {
            $this->db->where("EE.call_center_enrollment", $filter->callcenter);
        }*/

        //$this->db->select('U.*');
        $users = $this->db->get($this->db->dbprefix . "users");
        //$this->data['users'] = $users->result_array();
        //$this->db->join('enrollers_experience EE', 'E.id = EE.id_enroller', 'left');
        //$this->db->group_by('E.id');
        /*echo '<pre>';
        print_r($this->db->last_query());
        die();*/

        if ($users->num_rows() < 1) {
            return false;
        }
        return $users->result_array();
    }

    function get_user_info($id = '') {
        if (empty($id)) return false;
        $user = $this->db->where($this->db->dbprefix . "users.id", $id);
        $user = $this->db->join($this->db->dbprefix . "users_files", $this->db->dbprefix . 'users.id = ' . $this->db->dbprefix . 'users_files.userid', 'left');
        $user = $this->db->get($this->db->dbprefix . "users");
        
        if ($user->num_rows() < 1) {
            return false;
        }
        $arr = $user->result_array();
        return $arr[0];
    }

    function get_user_info_by_email($email = '') {
        if (empty($email)) return false;
        $this->db->where("email", $email);
        $user = $this->db->get($this->db->dbprefix . "users");

        if ($user->num_rows() < 1) {
            return false;
        }
        $arr = $user->result_array();
        return $arr[0];
    }

    function get_user_info_by_key($key = '') {
        if (empty($key)) return false;
        $this->db->where("forgotten_password_selector", $key);
        $user = $this->db->get($this->db->dbprefix . "users");

        if ($user->num_rows() < 1) {
            return false;
        }
        $arr = $user->result_array();
        return $arr[0];
    }

    function verify_key($key = '') {
        $parts = explode(".", $key);
        if (empty($parts[0])) return false;
        $this->db->where("forgotten_password_selector", $parts[0]);
        $user = $this->db->get($this->db->dbprefix . "users");

        if ($user->num_rows() < 1) {
            return false;
        }
        return true;
    }

    function set_password($identity, $password)
    {
        $this->ion_auth_model->identity_column = "email";
        $hash = $this->Ion_auth_model->hash_password($password, $identity);

        if ($hash === false)
        {
            return false;
        }

        // When setting a new password, invalidate any other token
        $data = [
            'password' => $hash,
            'remember_code' => NULL,
            'forgotten_password_selector' => NULL,
            'forgotten_password_code' => NULL,
            'forgotten_password_time' => NULL
        ];
        if ($this->db->update($this->db->dbprefix . "users", $data, [$this->ion_auth_model->identity_column => $identity]))
        {
            return true;
        }
        return false;
    }

}
