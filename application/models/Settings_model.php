<?php
class Settings_model extends CI_Model
{

	public function __construct()
	{
	    parent::__construct();
	}

	/**
	 * Load the sitewide settings from the database for use anywhere in the code.
	 */
	public function loadSitewideSettings() {
		$settings = array();

		// Get the settings from the database
		$this->db->order_by("id", "asc");
		$query = $this->db->get('ci_settings');
		foreach ($query->result() as $row) {
			$settings[$row->define] = $row->value;
		}

		$this->config->set_item('settings', $settings);
	}

	// Filter Settings
	public function filter($filter)
	{
		$this->db->distinct();

		/*if (isset($filter->first_name) && $filter->first_name !== null) {
			$this->db->where("first_name", $filter->first_name);
		}*/

		$this->db->where("visibility", 1);
		$this->db->order_by("id", "asc");
		$settings = $this->db->get($this->db->dbprefix . "settings");

		if ($settings->num_rows() < 1) {
			return false;
		}
		return $settings->result_array();
	}

	// Delete Setting
	public function delete($id)
	{
		$this->db->where("id", $id);
		if ($this->db->delete($this->db->dbprefix . "settings")) {
			return true;
		}
		return false;
	}

}
