<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Books_model extends CI_Model {

    public function all()
    {
        return $this->db->select('*')->from('books')->order_by('id', 'desc')->get()->result();
    }

    public function detail($id)
    {
        return $this->db->select('id, title, author')->from('books')->where('id', $id)->order_by('id', 'desc')->get()->row();
    }

    public function create($data)
    {
        $this->db->trans_start();
        $this->db->insert('books', $data);
        $id = $this->db->insert_id();
        $this->db->trans_complete();
        return array(
            'status' => 201, 
            'message' => 'Data has been created',
            'id' => $id,
        );
    }

    public function update($id, $data)
    {
        $this->db->trans_start();
        $this->db->where('id', $id)->update('books', $data);
        $this->db->trans_complete();
        return array('status' => 200, 'message' => 'Data for Book [' . $id . '] has been updated');
    }

    public function delete($id)
    {
        $this->db->trans_start();
        $this->db->where('id', $id)->delete('books');
        $this->db->trans_complete();
        return array('status' => 200, 'message' => 'Data for Book [' . $id . '] has been deleted');
    }

}
