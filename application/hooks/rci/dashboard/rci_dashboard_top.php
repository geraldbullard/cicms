<?php
// define module settings for this RCI Location
$this_rci = array(
	'module' => 'dashboard_top',
	'title' => 'Dashboard Top RCI',
	'summary' => 'The RCI Plugin for the Dashboard >> Top Location',
	'theme' => 'adminTheme',
	'icon' => 'lnr lnr-layers',
	'sort' => 0,
	'visibility' => 1,
	'status' => 0,
	'system' => 2,
);

// ToDo: Add Individual Settings for this RCI Location

$CI = &get_instance();
if (
	isset($CI->Modules_model->moduleInfo('dashboard_top')->status) &&
	$CI->Modules_model->moduleInfo('dashboard_top')->status === '1' &&
	$CI->uri->segment(2) == 'dashboard'
) {
	echo '<hr style="border-top: 1px dashed red; margin: 0 0 10px;">';
	echo '<p> >>> Dashboard Top RCI <<< </p>';
	echo '<p> ( Edit this content in the file located at: ' . __FILE__ . ' ) </p>';
	echo '<hr style="border-bottom: 1px dashed red; margin: 0 0 20px;">';
}
