<?php

class Navigation extends Widget
{

    public function display($data)
	{
        if ($this->session->userdata('user_id')) {
            $admin = array(
                'title' => 'Admin',
                'url' => '/admin/dashboard/'
            );
            $dashboard = array(
                'title' => 'Dashboard',
                'url' => '/dashboard/'
            );
        }
        if (!isset($data['items'])) {
            $data['items'] = array(
                $admin,
                $dashboard,
                array(
                    'title' => 'First Content',
                    'url' => '/first-content/',
                ),
                array(
                    'title' => 'About Us',
                    'url' => '/about-us/',
                ),
                array(
                    'title' => 'Blog',
                    'url' => '/blog/',
                ),
                array(
                    'title' => 'Contact Us',
                    'url' => '/contact-us/',
                ),
                array(
                    'title' => (!$this->session->userdata('user_id')) ? 'Login' : 'Logout',
                    'url' => (!$this->session->userdata('user_id')) ? '/user/login/' : '/user/logout/',
                ),
            );
        }
        $this->view('widgets/eterna/navigation', $data);
    }
    
}