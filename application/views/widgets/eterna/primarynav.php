<?php
class PrimaryNav extends Widget
{

    public function display($data)
	{
        $contents_model = $this->load->model('contents/contents_model');
        $data['menu'] = $this->contents_model->getMenuByDesignation('primary');
        if (empty(unserialize($data['menu']->menuData))) {
            $data['menu']->menuData = 'a:5:{s:4:"item";a:1:{i:1;s:9:"no-parent";}s:5:"title";a:1:{i:1;s:13:"No Menu Items";}s:4:"slug";a:1:{i:1;s:12:"javascript:;";}s:5:"class";a:1:{i:1;s:7:"a-class";}s:6:"target";a:1:{i:1;s:5:"_self";}}';
        }
        $this->view('widgets/eterna/primarynav', $data);
    }
    
}