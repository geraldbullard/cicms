<?php

class Hero extends Widget
{

    public function display($data)
	{
        if (!isset($data['slides'])) {
            $data['slides'] = array(
                array(
                    'h2' => 'Welcome to <span>Eterna</span>',
                    'background' => '/assets/eterna/img/slide/slide-1.jpg',
                    'text' => 'Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse doloremque accusamus repellendus deleniti vel. Minus et tempore modi architecto.',
                    'href' => 'https://www.brokenpixelcms.com/',
                    'link' => 'Learn More',
                    'target' => '_blank',
                ),
                array(
                    'h2' => 'Lorem <span>Ipsum Dolor</span>',
                    'background' => '/assets/eterna/img/slide/slide-2.jpg',
                    'text' => 'Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse doloremque accusamus repellendus deleniti vel. Minus et tempore modi architecto.',
                    'href' => 'https://www.google.com/',
                    'link' => 'Lorem Ipsum',
                    'target' => '_blank',
                ),
                array(
                    'h2' => 'Sequi ea <span>Dime Lara</span>',
                    'background' => '/assets/eterna/img/slide/slide-3.jpg',
                    'text' => 'Ut velit est quam dolor ad a aliquid qui aliquid. Sequi ea ut et est quaerat sequi nihil ut aliquam. Occaecati alias dolorem mollitia ut. Similique ea voluptatem. Esse doloremque accusamus repellendus deleniti vel. Minus et tempore modi architecto.',
                    'href' => 'https://www.bing.com/',
                    'link' => 'Sequi ea',
                    'target' => '_self',
                ),
            );
        }
        $this->view('widgets/eterna/hero', $data);
    }
    
}