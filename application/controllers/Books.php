<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Books extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Auth_model');
        $this->load->model('Books_model');

        //$check_auth_client = $this->Auth_model->check_auth_client();
        //if ($check_auth_client != true) {
        //    die($this->output->get_output());
        //}
    }

    public function all()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET') {
            json_output(400, array('status' => 400, 'message' => 'Bad Request Books Controller index'));
        } else {
            //$auth = $this->Auth_model->auth();
            //if ($auth['status'] == 200) {
                $resp = $this->Books_model->all();
                json_output(200, $resp);
            //}
        }
    }

    public function detail($id)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET' || $this->uri->segment(3) == '' || is_numeric($this->uri->segment(3)) == FALSE) {
            json_output(400, array('status' => 400, 'message' => 'Bad Request Books Controller detail'));
        } else {
            //$auth = $this->Auth_model->auth();
            //if ($auth['status'] == 200) {
                $resp = $this->Books_model->detail($id);
                json_output(200, $resp);
            //}
        }
    }

    public function create()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'POST') {
            json_output(400, array('status' => 400, 'message' => 'Bad Request Books Controller create'));
        } else {
            $auth = $this->Auth_model->auth();
            $respStatus = $auth['status'];
            if ($respStatus == 200) {
                $params = json_decode(file_get_contents('php://input'), TRUE);
                $params['created_at'] = date('Y-m-d H:i:s');
                $params['updated_at'] = date('Y-m-d H:i:s');
                if ($params['title'] == "" || $params['author'] == "") {
                    $respStatus = 400;
                    $resp = array('status' => 400, 'message' => 'Title & Author cannot be empty');
                } else {
                    $resp = $this->Books_model->create($params);
                }
                json_output($respStatus, $resp);
            }
        }
    }

    public function update($id)
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'PUT' || $this->uri->segment(3) == '' || is_numeric($this->uri->segment(3)) == FALSE) {
            json_output(400, array('status' => 400, 'message' => 'Bad Request Books Controller update'));
        } else {
            $auth = $this->Auth_model->auth();
            $respStatus = $auth['status'];
            if ($respStatus == 200) {
                $params = json_decode(file_get_contents('php://input'), TRUE);
                $params['updated_at'] = date('Y-m-d H:i:s');
                if ($params['title'] == "" || $params['author'] == "") {
                    $respStatus = 400;
                    $resp = array('status' => 400, 'message' => 'Title & Author cannot be empty');
                } else {
                    $resp = $this->Books_model->update($id, $params);
                }
                json_output($respStatus, $resp);
            }
        }
    }

    public function delete()
    {
        $method = $_SERVER['REQUEST_METHOD'];
        if ($method != 'GET' || $this->uri->segment(3) == '' || is_numeric($this->uri->segment(3)) == FALSE) {
            json_output(400, array('status' => 400, 'message' => 'Bad Request Books Controller delete'));
        } else {
            $auth = $this->Auth_model->auth();
            if ($auth['status'] == 200) {
                $resp = $this->Books_model->delete($this->uri->segment(3));
                json_output($auth['status'], $resp);
            }
        }
    }

}
