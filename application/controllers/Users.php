<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MY_Controller
{

    function __construct()
    {
        parent::__construct();

        // Make sure the user is logged in for this module
        if (!$this->session->userdata('user_id'))
        {
            // set the url they were trtying to go to in session
            $this->session->set_userdata('page_url', current_url());

            //Redirect to login
            redirect('user/login');
        }

        // Get site wide settings first
        $this->Settings_model->loadSitewideSettings();
    }

    public function admin_index()
    {
        $this->data = array();
        $this->data['title'] = "Users";

        // Added for developer dropdown
        $users = $this->db->get($this->db->dbprefix . "users");
        $this->data['dev_data']['users'] = $users->result_array();
        $this->data['dev_data']['permissions'] = $this->Roles_model->getRolePermissions(
            $this->session->userdata('group'), 'users'
        );

        $this->load->view('layout/admin/header.php', $this->data);
        $this->load->view('users/admin/index', $this->data);
        $this->load->view('layout/admin/footer.php', $this->data);
    }

    public function getUsers()
    {
        $filter = new stdClass();
        //$filter->first_name = ($this->input->get_post('filterfirstname') != '') ? $this->input->get_post('filterfirstname') : null;

        $this->load->model('Users_model');
        $data['users'] = $this->Users_model->filter($filter);
        // Module Permissions
        foreach ($this->Roles_model->getRolePermissions($this->session->userdata('group'), 'users') as $permission) {
            if ($permission['override'] == '') {
                $data['access'] = $permission['access'];
            }
            // Working on FPO (Feature Permission Overrides)
            if ($permission['override'] != '') {
                $data[$permission['override']] = $permission['access'];
            }
        }

        return json_encode($this->load->view('users/admin/filter', $data));
    }

    public function deactivate()
    {
        $userid = $this->uri->segment(4);

        if ($userid !== '1')
        {
            if (!$this->ion_auth->deactivate($userid))
            {
                // Set the error to the flashdata array:
                $this->session->set_flashdata(array("msg" => "Error Deactivating the User!", "msgtype" => "danger"));
            } else {
                // Set the success message to the flashdata array:
                $this->session->set_flashdata(array("msg" => "User Successfully Deactivated!", "msgtype" => "success"));
            }
        }
        else
        {
            $this->session->set_flashdata(array("msg" => "You cannot deactivate the Developer User!", "msgtype" => "danger"));
        }
        redirect('/admin/users', 'refresh');
    }

    public function activate()
    {
        $userid = $this->uri->segment(4);

        if (!$this->ion_auth->activate($userid))
        {
            // Set the error to the flashdata array:
            $this->session->set_flashdata(array("msg" => "Error Activating the User!", "msgtype" => "danger"));
        } else {
            // Set the success message to the flashdata array:
            $this->session->set_flashdata(array("msg" => "User Successfully Activated!", "msgtype" => "success"));
        }
        redirect('/admin/users', 'refresh');
    }

    public function delete()
    {
        $userid = $this->uri->segment(4);

        if ($userid !== '1') {
            if (!$this->ion_auth->delete_user($userid))
            {
                // Set the error to the flashdata array:
                $this->session->set_flashdata(array("msg" => "Error Deleting the User!", "msgtype" => "danger"));
            } else {
                // Set the success message to the flashdata array:
                $this->session->set_flashdata(array("msg" => "User Successfully Deleted!", "msgtype" => "success"));
            }
        }
        else
        {
            $this->session->set_flashdata(array("msg" => "You cannot delete the Developer User!", "msgtype" => "danger"));
        }
        redirect('/admin/users/', 'refresh');
    }

    public function add()
    {
        $this->data = array();

        // START - The Edit Stuff //////////////////////////////////////////////////////////////////////////////////////
        if ($this->input->post('first_name') != '') {
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<div class="alert alert-danger" role="alert">', '</div>');
            $this->form_validation->set_rules('first_name','First Name', 'trim|required');
            $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
            $this->form_validation->set_rules('username', 'Username', 'trim|required|is_unique[ci_users.username]');
            $this->form_validation->set_message('is_unique', '%s already registered!');
            $this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email|is_unique[ci_users.email]');
            $this->form_validation->set_message('is_unique', '%s already registered!');
            $this->form_validation->set_rules('company', 'Company', 'trim');
            $this->form_validation->set_rules('phone', 'Phone Number', 'trim|required');
            //$this->form_validation->set_rules('groups', 'User Group', 'required');
            if ($this->input->post('password') != '') {
                $this->form_validation->set_rules('password', 'Password', 'trim|required|matches[passconf]|callback_is_password_strong'); //callback_check_password
                $this->form_validation->set_rules('passconf', 'Confirm Password', 'trim|required');
                $this->form_validation->set_message(
                    'is_password_strong',
                    '<strong>Your password must have the following:</strong>
	                <ul>
		                <li>Must contain one Uppercase Letter</li>
		                <li>Must contain one Lowercase Letter</li>
		                <li>Must contain a Number</li>
		                <li>Must contain a Special Character</li>
	                </ul>'
                );
                //$this->form_validation->set_message('check_password', 'Password Issue!');
            }
            if ($this->form_validation->run() !== FALSE)
            {
                // Set the User information
                $userinfo = array(
                    'first_name' => $this->input->post('first_name'),
                    'last_name' => $this->input->post('last_name'),
                    'phone' => $this->input->post('phone'),
                    'company' => $this->input->post('company')
                );

                // If it doesn't register:
                if (!$userid = $this->ion_auth->register(
                    $this->input->post('username'),
                    $this->input->post('password'),
                    $this->input->post('email'),
                    $userinfo,
                    $this->input->post('groups')
                )) {
                    // Set the error to the data array:
                    $this->data['msg'] = "Error Creating the User!";
                    $this->data['msgtype'] = "danger";

                }
                // If we get here, it means the process was successful.
                else
                {
                    // Add Audit Trail
                    $this->load->model('Audit_model');
                    $this->Audit_model->addAuditLog(
                        $this->config->config['settings']['auditTypeRecordCreated'],
                        '/admin/users/add',
                        'User Created',
                        'users',
                        $userid
                    );

                    // Set the success message:
                    $this->data['msg'] = "Users Created Successfully!";
                    $this->data['msgtype'] = "success";
                }
            }
            else
            {
                //$this->data['msg'] = "Your submission has the following Problems:";
                $this->data['msg'] = "Your submission has the following Problems:<br>" . validation_errors('<div>', '</div>');
                $this->data['msgtype'] = "danger";
                /*$remember = (bool)$this->input->post('remember');
                if ($this->ion_auth->login($this->input->post('username'), $this->input->post('password'), $remember))
                {
                    // first we see if there is a page they wished to go to
                    if ($this->session->userdata('page_url')) {
                        redirect($this->session->userdata('page_url'));
                        // if not we send them to the welcome/dashboard
                    } else {
                        //Load the Audit Model and Insert a log entry of this action
                        $this->load->model('Audit_model');
                        $this->Audit_model->addAuditLog(
                            $this->config->config['settings']['auditTypeUserLogin'],
                            '/user/login',
                            'User Logged In',
                            'user',
                            $this->session->userdata('user_id')
                        );
                        redirect('/', 'refresh');
                        // Later we redirect based on user types or config setting per user acct etc...
                        //redirect('dashboard');
                    }
                }
                else
                {
                    $_SESSION['auth_message'] = $this->ion_auth->errors();
                    $this->session->mark_as_flash('auth_message');
                    redirect('user/login');
                }*/
            }
        }
        // END - The Edit Stuff ////////////////////////////////////////////////////////////////////////////////////////

        $this->data['title'] = "Create User";

        $this->load->view('layout/admin/header.php', $this->data);
        $this->load->view('users/admin/add', $this->data);
        $this->load->view('layout/admin/footer.php', $this->data);
    }

    public function is_password_strong()
    {
        if(1 != preg_match('~[0-9]~', $this->input->post('password'))){
            #does not have numbers
            return false;
        }

        if(!preg_match("/[a-z]/i", $this->input->post('password'))){
            #does not have letters
            return false;
        }

        if(!preg_match('/[A-Z]/', $this->input->post('password'))){
            #does not have uppercase letters
            return false;
        }

        if (preg_match('/[^a-zA-Z\d]/', $this->input->post('password')) == 0)
        {
            #does not have special chars
            return false;
        }

        return true;
    }

    /*public function check_password()
    {
        if($this->input->post('password') == "Password123!" || $this->input->post('password') == $this->input->post('pwexample'))
        {
            return false;
        }
        return true;
    }*/

    public function edit()
    {
        $segcnt = count($this->uri->segments);
        $userid = $this->uri->segment($segcnt);

        if (
            (
                $userid === '1' &&
                $this->ion_auth_model->in_group("developer", $this->session->userdata('user_id'))
            ) ||
            $userid !== '1'
        )
        {
            $this->data = array();

            // START - The Edit Stuff //////////////////////////////////////////////////////////////////////////////////
            if ($this->input->post('first_name') != '') {
                $this->load->library('form_validation');
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger" role="alert">', '</div>');
                $this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
                $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required');
                $this->form_validation->set_rules('username', 'Username', 'trim|required');
                $this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email');
                $this->form_validation->set_rules('company', 'Company', 'trim');
                $this->form_validation->set_rules('phone', 'Phone Number', 'trim|required');
                //$this->form_validation->set_rules('role', 'User Group', 'trim|required');
                if ($this->input->post('password') != '') {
                    $this->form_validation->set_rules('password', 'Password', 'trim|required|matches[passconf]|callback_is_password_strong');
                    $this->form_validation->set_rules('passconf', 'Confirm Password', 'trim|required');
                    $this->form_validation->set_message(
                        'is_password_strong',
                        '<strong>Your password must have the following:</strong>
                        <ul>
                            <li>Must contain one Uppercase Letter</li>
                            <li>Must contain one Lowercase Letter</li>
                            <li>Must contain a Number</li>
                            <li>Must contain a Special Character</li>
                        </ul>'
                    );
                }
                $this->form_validation->set_message('is_unique', '%s already registered!');
                if ($this->form_validation->run() !== FALSE) {
                    // Set the User information
                    $userinfo = array(
                        'id' => $userid,
                        'first_name' => $this->input->post('first_name'),
                        'last_name' => $this->input->post('last_name'),
                        'username' => $this->input->post('username'),
                        'phone' => $this->input->post('phone'),
                        'company' => $this->input->post('company'),
                        'email' => $this->input->post('email')
                    );

                    if ($this->input->post('password') != '') {
                        $userinfo['password'] = $this->input->post('password');
                    }
                    // If it doesn't update:
                    if (!$this->ion_auth->update($userid, $userinfo)) {
                        // Set the error to the data array:
                        $this->data['msg'] = "Error Updating the Users Information!";
                        $this->data['msgtype'] = "danger";

                    } // If we get here, it means the process was successful.
                    else {
                        // remove from all groups first thing
                        $this->ion_auth->remove_from_group(NULL, $userid);

                        // Lets update the Users Groups Assignments from $_POST
                        if (!empty($this->input->post('groups'))) {
                            foreach ($this->input->post('groups') as $group) {
                                $this->ion_auth->add_to_group($group, $userid);
                            }
                        }

                        // Add Audit Trail
                        $this->load->model('Audit_model');
                        $this->Audit_model->addAuditLog(
                            $this->config->config['settings']['auditTypeRecordUpdated'],
                            '/admin/users/edit',
                            'User Information Updated',
                            'users',
                            $userid
                        );

                        // Set the success message:
                        $this->data['msg'] = "Users Information Successfully Updated!";
                        $this->data['msgtype'] = "success";
                    }
                } else {
                    $this->data['msg'] = "Your submission has the following Problems:<br>" . validation_errors('<div>', '</div>');
                    $this->data['msgtype'] = "danger";
                }
            }
            // END - The Edit Stuff ////////////////////////////////////////////////////////////////////////////////////
        }
        else
        {
            $this->session->set_flashdata(array("msg" => "You cannot edit the Developer User!", "msgtype" => "danger"));
            redirect('/admin/users/', 'refresh');
        }

        $this->data['title'] = "Edit User";

        $user = $this->db->where("id", $userid);
        $user = $this->db->get($this->db->dbprefix . "users");
        $this->data['user'] = $user->result_array();

        // Get users Group(s)
        $this->data['users_groups'] = $this->ion_auth->get_users_groups($userid)->result();

        // Added for developer dropdown
        $this->data['dev_data'] = $user->result_array();

        $this->load->view('layout/admin/header.php', $this->data);
        $this->load->view('users/admin/edit', $this->data);
        $this->load->view('layout/admin/footer.php', $this->data);
    }

    public function getTeamMembers() {
	    $users_groups = $this->db->where("group_id", "3");
	    $users_groups = $this->db->get($this->db->dbprefix . "users_groups");
	    $users_groups = $users_groups->result_array();

	    $users = array();
	    foreach ($users_groups as $u => $user) {
	    	$this->load->model('Users_model');
	    	$this_user = $this->Users_model->get_user_info($user['user_id']);
	    	$users[] = $this_user;
	    }
	    echo json_encode($users);
    }

}