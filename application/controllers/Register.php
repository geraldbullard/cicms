<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends Auth_Controller
{

	function __construct()
	{
		parent::__construct();

		// Get site wide settings first
		$this->Settings_model->loadSitewideSettings();
		
		// If not an admin get them outta here!
		if ($this->ion_auth->is_admin() === FALSE)
		{
			redirect('/');
		}
	}

	public function index()
	{
		$this->load->library('form_validation');
		$this->form_validation->set_rules('first_name', 'First name', 'trim|required');
		$this->form_validation->set_rules('last_name', 'Last name', 'trim|required');
		$this->form_validation->set_rules('username', 'Username', 'trim|required|is_unique[users.username]');
		$this->form_validation->set_rules('email', 'Email', 'trim|valid_email|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|min_length[8]|max_length[20]|required');
		$this->form_validation->set_rules('confirm_password', 'Confirm password', 'trim|matches[password]|required');

		if ($this->form_validation->run() === FALSE)
		{
			$this->data = array();
			$this->data['title'] = "Create New User";

			$this->load->helper('form');
			$this->load->view('layout/admin/header.php', $this->data);
			$this->load->view('register/index', $this->data);
			$this->load->view('layout/admin/footer.php', $this->data);
		}
		else
		{
			$first_name = $this->input->post('first_name');
			$last_name = $this->input->post('last_name');
			$username = $this->input->post('username');
			$email = $this->input->post('email');
			$password = $this->input->post('password');

			$additional_data = array(
				'first_name' => $first_name,
				'last_name' => $last_name
			);

			$this->load->library('ion_auth');
			if ($this->ion_auth->register($username,$password,$email,$additional_data))
			{
				$_SESSION['auth_message'] = 'The account has been created. You may now login.';
				$this->session->mark_as_flash('auth_message');
				redirect('user/login');
			}
			else
			{
				$_SESSION['auth_message'] = $this->ion_auth->errors();
				$this->session->mark_as_flash('auth_message');
				redirect('register');
			}
		}
	}

}
