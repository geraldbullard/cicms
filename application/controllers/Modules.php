<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Modules extends MY_Controller {

    function __construct() {
        parent::__construct();

        // Make sure the user is logged in for this module
        if (!$this->session->userdata('user_id')) {
            // set the url they were trtying to go to in session
            $this->session->set_userdata('page_url', current_url());

            //Redirect to login
            redirect('user/login');
        }

        // Get site wide settings first
        $this->Settings_model->loadSitewideSettings();
    }

    public function index() {
        $this->data = array();
        $this->data['title'] = "Core Modules";

        $this->db->where("system", 1);
        $modules = $this->db->get($this->db->dbprefix . "modules");
        $this->data['modules'] = $modules->result_array();
        // Added for developer dropdown
        $this->data['dev_data'] = $modules->result_array();

        $this->load->view('layout/admin/header.php', $this->data);
        $this->load->view('modules/admin/index', $this->data);
        $this->load->view('layout/admin/footer.php', $this->data);
    }

    public function addon() {
        $this->data = array();
        $this->data['title'] = "Addon Modules";

        $this->db->where("system", 0);
        $modules = $this->db->get($this->db->dbprefix . "modules");
        $this->data['modules'] = $modules->result_array();
        // Added for developer dropdown
        $this->data['dev_data'] = $modules->result_array();

        $this->load->view('layout/admin/header.php', $this->data);
        $this->load->view('modules/admin/addon', $this->data);
        $this->load->view('layout/admin/footer.php', $this->data);
    }

    public function rci() {
        $this->data = array();
        $this->data['title'] = "RCI Plugins";

        // Let's add some logic to find RCI Plugin (Modules) 
        // that have not been added to the database

        // Let's scan the hoooks/rci directory and see if there are 
        // any plugin folders/locations that are not in the database
        $rci_plugins_path = APPPATH . 'hooks/rci/';
        $rci_plugin_folders = scandir($rci_plugins_path);

        // If there are any rci plugins (locations) that are not in the database
        // and let's loop thru the folders and add them
        foreach ($rci_plugin_folders as $rci_folder) {
            if ($rci_folder !== '.' && $rci_folder !== '..' && $rci_folder !== 'index.html') {
                // We found an rci directory, it could have many locations
                if (is_dir($rci_plugins_path . $rci_folder)) {
                    // Now let's scan this individual folder
                    // and see how many rci location files exist
                    $this_rci_locations = scandir($rci_plugins_path . $rci_folder);
                    foreach ($this_rci_locations as $this_rci_location) {
                        if ($this_rci_location !== '.' && $this_rci_location !== '..' && $this_rci_location !== 'index.html') {
                            // Get the individual location data
                            include_once($rci_plugins_path . $rci_folder . '/' . $this_rci_location);
                            // Check the database for this ocation
                            $this->db->where("module", $this_rci['module']);
                            $db_rci = $this->db->get($this->db->dbprefix . "modules");
                            // Check if we have row resault
                            if (!$db_rci->num_rows === 1) {
                                // No results let's add the entry
                                $this->db->insert($this->db->dbprefix . "modules", $this_rci);
                            }
                        }
                    }
                }
            }
        }

        $this->db->where("system", 2);
        $modules = $this->db->get($this->db->dbprefix . "modules");
        $this->data['modules'] = $modules->result_array();
        // Added for developer dropdown
        $this->data['dev_data'] = $modules->result_array();

        $this->load->view('layout/admin/header.php', $this->data);
        $this->load->view('modules/admin/rci', $this->data);
        $this->load->view('layout/admin/footer.php', $this->data);
    }

    public function settings() {
        $module = $this->uri->segment(4);

        $this->data = array();
        if ($this->Modules_model->moduleInfo($module)->system == 0) {
            $this->data['title'] = $this->Modules_model->moduleInfo($module)->title . " Module Settings";
            $this->data['parent'] = 'Addon Modules';
            $this->data['parenturl'] = 'modules/addon';
        } else if ($this->Modules_model->moduleInfo($module)->system == 1) {
            $this->data['title'] = $this->Modules_model->moduleInfo($module)->title . " Module Settings";
            $this->data['parent'] = 'Core Modules';
            $this->data['parenturl'] = 'modules';
        } else if ($this->Modules_model->moduleInfo($module)->system == 2) {
            $this->data['title'] = $this->Modules_model->moduleInfo($module)->title . " Plugin Settings";
            $this->data['parent'] = 'RCI Plugins';
            $this->data['parenturl'] = 'modules/rci';
        }

        $module_settings = $this->Modules_model->settings($module);
        $this->data['module_settings'] = $module_settings;
        // Added for developer dropdown
        $this->data['dev_data'] = $module_settings;

        $this->load->view('layout/admin/header.php', $this->data);
        $this->load->view('modules/admin/settings', $this->data);
        $this->load->view('layout/admin/footer.php', $this->data);
    }

    public function editsetting() {
        $modulesettingid = $this->uri->segment(4);

        $this->data = array();

        // START - The Edit Stuff //////////////////////////////////////////////////////////////////////////////////////
        if ($this->input->post('define') != '') {
            $this->load->library('form_validation');
            $this->form_validation->set_error_delimiters('<div class="alert alert-danger" role="alert">', '</div>');
            $this->form_validation->set_rules('define', 'Setting Define', 'trim|required');
            if ($this->form_validation->run() !== FALSE) {
                // Set the Module Setting information !!!!! (define is not changeable, delete and re-add for now) !!!!!
                // any other columns in the groups table not id or name, add any new columns to this array
                $settinginfo = array(
                    'define' => $this->input->post('define'),
                    'title' => $this->input->post('title'),
                    'summary' => $this->input->post('summary'),
                    'value' => $this->input->post('value'),
                    'type' => $this->input->post('type')
                );

                // If it doesn't update:
                if (!$this->db->update($this->db->dbprefix . "settings", $settinginfo, ['id' => $modulesettingid])) {
                    // Set the error to the data array:
                    $this->data['msg'] = "Error Updating the Setting Information!";
                    $this->data['msgtype'] = "danger";
                }
                // If we get here, it means the process was successful.
                else {
                    // Add Audit Trail
                    $this->load->model('Audit_model');
                    $this->Audit_model->addAuditLog(
                        $this->config->config['settings']['auditTypeRecordUpdated'],
                        '/modules/editsetting',
                        'Module Setting Information Updated',
                        'audit',
                        $modulesettingid
                    );

                    // Set the success message:
                    $this->data['msg'] = "Setting Information Successfully Updated!";
                    $this->data['msgtype'] = "success";
                }
            } else {
                $this->data['msg'] = "Your submission has the following Problems:<br>" . validation_errors('<div>', '</div>');
                $this->data['msgtype'] = "danger";
            }
        }
        // END - The Edit Stuff ////////////////////////////////////////////////////////////////////////////////////////

        $this->data['title'] = "Edit Setting";

        $module_setting = $this->Modules_model->editsetting($modulesettingid);
        $this->data['module_setting'] = $module_setting;
        // Added for developer dropdown
        $this->data['dev_data'] = $module_setting;

        $this->load->view('layout/admin/header.php', $this->data);
        $this->load->view('modules/admin/editsetting', $this->data);
        $this->load->view('layout/admin/footer.php', $this->data);
    }

    public function enable() {
        $module = $this->uri->segment(4);
        $system = $this->Modules_model->moduleInfo($module)->system;

        // If it doesn't enable:
        if (!$this->Modules_model->enable($module)) {
            // Set the error
            $this->session->set_flashdata(array("msg" => "Error Enabling the Module!", "msgtype" => "danger"));
        }
        // If we get here, it means the process was successful.
        else {
            // Add Audit Trail
            $this->load->model('Audit_model');
            $this->Audit_model->addAuditLog(
                $this->config->config['settings']['auditTypeModuleEnabled'],
                '/modules/enable/',
                'Module Enabled',
                'modules',
                $module
            );

            // Set the success message:
            $this->session->set_flashdata(array("msg" => "Module Successfully Enabled!", "msgtype" => "success"));
        }
        if ($system == 0) redirect('/modules/addon/', 'refresh');
        if ($system == 2) redirect('/modules/rci/', 'refresh');
    }

    public function disable() {
        $module = $this->uri->segment(4);
        $system = $this->Modules_model->moduleInfo($module)->system;

        // If it doesn't enable:
        if (!$this->Modules_model->disable($module)) {
            // Set the error
            $this->session->set_flashdata(array("msg" => "Error Disabling the Module!", "msgtype" => "danger"));
        }
        // If we get here, it means the process was successful.
        else {
            // Add Audit Trail
            $this->load->model('Audit_model');
            $this->Audit_model->addAuditLog(
                $this->config->config['settings']['auditTypeModuleDisabled'],
                '/modules/disable/',
                'Module Disabled',
                'modules',
                $module
            );

            // Set the success message:
            $this->session->set_flashdata(array("msg" => "Module Successfully Disabled!", "msgtype" => "success"));
        }
        if ($system == 0) redirect('/modules/addon/', 'refresh');
        if ($system == 2) redirect('/modules/rci/', 'refresh');
    }

    public function install() {
        $module = $this->uri->segment(4);

        // If it doesn't install:
        if (!$this->Modules_model->install($module)) {
            // Set the error
            $this->session->set_flashdata(array("msg" => "Error Installing the Module!", "msgtype" => "danger"));
        }
        // If we get here, it means the process was successful.
        else {
            // Add Audit Trail
            $this->load->model('Audit_model');
            $this->Audit_model->addAuditLog(
                $this->config->config['settings']['auditTypeModuleInstalled'],
                '/modules/install/',
                'Module Installed',
                'modules',
                $module
            );

            // Set the error
            $this->session->set_flashdata(array("msg" => "Successfully Installed the Module!", "msgtype" => "success"));
        }
        if ($this->Modules_model->moduleInfo($module)->system == 0) redirect('/modules/addon/', 'refresh');
        if ($this->Modules_model->moduleInfo($module)->system == 2) redirect('/modules/rci/', 'refresh');
    }

    public function uninstall() {
        $module = $this->uri->segment(4);
        $system = $this->Modules_model->moduleInfo($module)->system;

        // If it doesn't enable:
        if (!$this->Modules_model->uninstall($module)) {
            // Set the error
            $this->session->set_flashdata(array("msg" => "Error Uninstalling the Module from the Database!", "msgtype" => "danger"));
        }
        // If we get here, it means the process was successful.
        else {
            // Add Audit Trail
            $this->load->model('Audit_model');
            $this->Audit_model->addAuditLog(
                $this->config->config['settings']['auditTypeModuleUninstalled'],
                '/modules/uninstall/',
                'Module Uninstalled',
                'modules',
                $module
            );

            // Set the success message:
            $this->session->set_flashdata(array("msg" => "Module Successfully Uninstalled!", "msgtype" => "success"));
        }
        if ($system == 0) redirect('/modules/addon/', 'refresh');
        if ($system == 2) redirect('/modules/rci/', 'refresh');
    }

    public function delete() {
        $module = $this->uri->segment(4);
        $system = $this->Modules_model->moduleInfo($module, true)->system;

        // Now remove the folder/files

        if (!$this->Modules_model->delete($module)) {
            // Set the error
            $this->session->set_flashdata(array("msg" => "Error Deleting the Module!", "msgtype" => "danger"));
        } else {
            // Add Audit Trail
            $this->load->model('Audit_model');
            $this->Audit_model->addAuditLog(
                $this->config->config['settings']['auditTypeModuleDeleted'],
                '/modules/delete/',
                'Module Deleted',
                'modules',
                $module
            );

            // Set the success message:
            $this->session->set_flashdata(array("msg" => "Module Successfully Deleted!", "msgtype" => "success"));
        }
        if ($system == 0) redirect('/modules/addon/', 'refresh');
        if ($system == 2) redirect('/modules/rci/', 'refresh');
    }
}
