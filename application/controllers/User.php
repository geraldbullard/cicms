<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller
{

	function __construct()
	{
		parent::__construct();
		
		// Get site wide settings first
		$this->Settings_model->loadSitewideSettings();
	}

	public function login()
	{
		$this->data = array();
		$this->data['title'] = "Login";

		$this->load->library('form_validation');
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		if ($this->form_validation->run() === FALSE)
		{
			$this->load->helper('form');			
			$this->load->view('layout/admin/minimal-header.php', $this->data);
			$this->load->view('users/login.php', $this->data);
			$this->load->view('layout/admin/minimal-footer.php', $this->data);
		}
		else
		{
			$remember = (bool)$this->input->post('remember');
			if ($this->ion_auth->login($this->input->post('username'), $this->input->post('password'), $remember))
			{
				// first we see if there is a page they wished to go to
				if ($this->session->userdata('page_url')) {
					redirect($this->session->userdata('page_url'));
					// if not we send them to the welcome/dashboard
				} else {
					//Load the Audit Model and Insert a log entry of this action
					$this->load->model('Audit_model');
					$this->Audit_model->addAuditLog(
						$this->config->config['settings']['auditTypeUserLogin'],
						'/user/login',
						'User Logged In',
						'user',
						$this->session->userdata('user_id')
					);
					redirect('/admin/dashboard/', 'refresh');
					// Later we redirect based on user types or config setting per user acct etc...
					//redirect('dashboard');
				}
			}
			else
			{
			    $_SESSION['auth_message'] = $this->ion_auth->errors();
				$this->session->mark_as_flash('auth_message');
				redirect('user/login');
			}
		}
	}

	public function logout()
	{
		//Load the Audit Model and Insert a log entry of this action
		$this->load->model('Audit_model');
		$this->Audit_model->addAuditLog(
			$this->config->config['settings']['auditTypeUserLogout'],
			'/user/logout',
			'User Logged Out',
			'user',
			$this->session->userdata('user_id')
		);
		$this->ion_auth->logout();
		redirect('/');
	}

    public function forgotpw()
    {
        $this->data = array();
        $this->data['title'] = "Forgot Password";
        $this->data['msg'] = "";

        $email = $this->input->post('email');
        if (isset($email) && $email != '')
        {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            if ($this->form_validation->run() === FALSE)
            {
                // did not validate
            }
            else
            {
                $this->ion_auth_model->identity_column = "email";
                if ($this->ion_auth->forgotten_password($this->input->post('email')))
                {
                    //Load the Audit Model and Insert a log entry of this action
                    $this->load->model('Audit_model');
                    $this->Audit_model->addAuditLog(
                        $this->config->config['settings']['auditTypeUserLogin'],
                        '/user/forgotpw',
                        'User Started Password Reset',
                        'user',
                        $this->session->userdata('user_id')
                    );
                    redirect('/user/pwemail/', 'refresh');
                }
                else
                {
                    $this->data['msg'] = $this->ion_auth->errors();
                    redirect('/user/forgotpw/');
                }
            }
        }

        $this->load->helper('form');
        $this->load->view('layout/admin/minimal-header.php', $this->data);
        $this->load->view('users/forgotpw.php', $this->data);
        $this->load->view('layout/admin/minimal-footer.php', $this->data);
    }

    public function resetpw()
    {
        $this->data = array();
        $this->data['title'] = "Reset Password";
        $this->data['msg'] = "";
        $this->data['hideform'] = "false";

        $key = $this->input->get('key');
        $keyparts = explode(".", $key);
        $password = $this->input->post('password');
        $password_confirm = $this->input->post('password_confirm');

        if (isset($password) && $password != '')
        {
            if (!preg_match('([a-zA-Z].*[0-9]|[0-9].*[a-zA-Z])', $password) )
            {
                $this->data['msg'] = "Your Password must have uppercase, lowercase and numbers.";
            } else {
                $this->load->library('form_validation');
                $this->form_validation->set_rules('password', 'Password', 'trim|required');
                $this->form_validation->set_rules('password_confirm', 'Password Confirm', 'trim|required|matches[password]');
                if ($this->form_validation->run() === FALSE)
                {
                    // did not validate
                }
                else
                {
                    $user = $this->Users_model->get_user_info_by_key($keyparts[0]);
                    if ($this->Users_model->set_password($user['email'], $password))
                    {
                        $this->ion_auth_model->identity_column = "email";
                        $this->Ion_auth_model->clear_forgotten_password_code($user['email']);
                        redirect('/user/login/', 'refresh');
                    }
                }
            }
        }

        if (isset($key) && $key != '' && $this->Users_model->verify_key($key))
        {
            // nothing yet, just show the default page/form
        } else {
            $this->data['hideform'] = 'true';
            $this->data['msg'] = "There was no reset key in the url!<br><br>Please check the link in the password reset email and try again or contact the webmaster.";
        }

        $this->load->helper('form');
        $this->load->view('layout/admin/minimal-header.php', $this->data);
        $this->load->view('users/resetpw.php', $this->data);
        $this->load->view('layout/admin/minimal-footer.php', $this->data);
    }

    public function pwemail()
    {
        $this->data = array();
        $this->data['title'] = "Check Email for Password Reset Link";

        $this->load->helper('form');
        $this->load->view('layout/admin/minimal-header.php', $this->data);
        $this->load->view('users/pwemail.php', $this->data);
        $this->load->view('layout/admin/minimal-footer.php', $this->data);
    }

}