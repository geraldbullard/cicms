<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Roles extends MY_Controller
{

	function __construct()
	{
		parent::__construct();

        // Make sure the user is logged in for this module
        if (!$this->session->userdata('user_id'))
        {
            // set the url they were trtying to go to in session
            $this->session->set_userdata('page_url', current_url());

            //Redirect to login
            redirect('user/login');
        }

		// Get site wide settings first
		$this->Settings_model->loadSitewideSettings();
	}

	public function index()
	{
        $this->data = array();
        $this->data['title'] = "Roles";

		$this->db->order_by("id", "asc");
        $roles = $this->db->get($this->db->dbprefix . "groups");
        $this->data['roles'] = $roles->result_array();
        // Added for developer dropdown
        $this->data['dev_data'] = $roles->result_array();

        $this->load->view('layout/admin/header.php', $this->data);
        $this->load->view('roles/admin/index', $this->data);
        $this->load->view('layout/admin/footer.php', $this->data);
	}

	public function getRoles()
	{
		$filter = new stdClass();
		//$filter->first_name = ($this->input->get_post('filterfirstname') != '') ? $this->input->get_post('filterfirstname') : null;

		//$this->load->model('Roles_model');
		$data['roles'] = $this->Roles_model->filter($filter);

		return json_encode($this->load->view('roles/admin/filter', $data));
	}

	public function edit()
	{
		$roleid = $this->uri->segment(4);

        if (
            $roleid !== '1'
        )
        {
            $this->data = array();

            // START - The Edit Stuff //////////////////////////////////////////////////////////////////////////////////////
            if ($this->input->post('description') != '') {
                $this->load->library('form_validation');
                //$this->form_validation->set_error_delimiters('<div class="alert alert-danger" role="alert">', '</div>');
                //$this->form_validation->set_rules('description', 'Group Description', 'trim|is_unique[ci_groups.description]');
                $this->form_validation->set_rules('description', 'Group Description', 'trim');
                //$this->form_validation->set_message('is_unique', '%s already exists.');
                if ($this->form_validation->run() !== FALSE)
                {
                    // Set the Role information !!!!! (name is not changeable, delete and re-add for now) !!!!!
                    // any other columns in the groups table not id or name, add any new columns to this array
                    $roleinfo = array(
                        'description' => $this->input->post('description')
                    );

                    // If it doesn't update:
                    $error = false;
                    if (!$this->ion_auth->update_group($roleid, null, $roleinfo))
                    {
                        $error = true;
                    }
                    // If we get here, it means the process was successful.
                    else
                    {
                        if (!$error) {
                            // Let's work on the role permissions, first truncate the table of any permissions for this role
                            $this->db->where('groups_id', $roleid);
                            $this->db->delete('ci_permissions');

                            foreach ($this->input->post('permissions') as $p => $permission) {
                                $data = array(
                                    'groups_id' => $roleid,
                                    'module' => $p,
                                    'access' => $permission
                                );
                                if (!$this->db->insert('ci_permissions', $data))
                                {
                                    $error = true;
                                    break;
                                } else {
                                    continue;
                                }
                            }
                        }
                    }
                    if ($error) {
                        // Set the error to the data array:
                        $this->data['msg'] = "Error Updating the Role Information!";
                        $this->data['msgtype'] = "danger";
                    } else {
                        // Add Audit Trail
                        $this->load->model('Audit_model');
                        $this->Audit_model->addAuditLog(
                            $this->config->config['settings']['auditTypeRecordUpdated'],
                            '/roles/edit',
                            'Role Information Updated',
                            'roles',
                            $roleid
                        );

                        // Set the success message:
                        $this->data['msg'] = "Role Information Successfully Updated!";
                        $this->data['msgtype'] = "success";
                    }
                } else {
                    $this->data['msg'] = "Your submission has the following Problems:<br>" . validation_errors('<div>', '</div>');
                    $this->data['msgtype'] = "danger";
                }
            }
            // END - The Edit Stuff ////////////////////////////////////////////////////////////////////////////////////////
        }
        else
        {
            $this->session->set_flashdata(array("msg" => "You cannot edit the Developer Role!", "msgtype" => "danger"));
            redirect('/roles/', 'refresh');
        }

		$this->data['title'] = "Edit Role";

		$role = $this->db->where("id", $roleid);
		$role = $this->db->get($this->db->dbprefix . "groups");
		$this->data['role'] = $role->result_array();

		// Added for developer dropdown
		$this->data['dev_data'] = $role->result_array();
		$this->data['dev_data']['permissions'] = $this->Roles_model->getRolePermissions($roleid);

		$this->load->view('layout/admin/header.php', $this->data);
		$this->load->view('roles/admin/edit', $this->data);
		$this->load->view('layout/admin/footer.php', $this->data);
	}

	public function add()
	{
		$this->data = array();

		// START - The Edit Stuff //////////////////////////////////////////////////////////////////////////////////////
		if ($this->input->post('name') != '') {
			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<div class="alert alert-danger" role="alert">', '</div>');
			$this->form_validation->set_rules('name', 'Role Name', 'trim|required|is_unique[ci_groups.name]');
			$this->form_validation->set_message('is_unique', '%s already exists.');
			$this->form_validation->set_rules('description', 'Role Description', 'trim|required|is_unique[ci_groups.description]');
			$this->form_validation->set_message('is_unique', '%s already exists.');
			if ($this->form_validation->run() !== FALSE)
			{
				// Set the Role information !!!!! (name is not changeable, delete and re-add for now) !!!!!
				// any other columns in the groups table not id or name, add any new columns to this array
				$rolename = $this->input->post('name');
				$roledescription = $this->input->post('description');

				// If it doesn't update:
				$roleid = $this->ion_auth->create_group($rolename, $roledescription);
				if (!$roleid)
				{
					// Set the error to the data array:
					$this->data['msg'] = "Error Creating the Role!";
					$this->data['msgtype'] = "danger";
				}
				// If we get here, it means the process was successful.
				else
				{
					// Add Audit Trail
					$this->load->model('Audit_model');
					$this->Audit_model->addAuditLog(
						$this->config->config['settings']['auditTypeRecordCreated'],
						'/roles/add',
						'New Role Created',
						'roles',
						$roleid
					);

					// Set the success message:
					$this->data['msg'] = "Role Created Successfully Updated!";
					$this->data['msgtype'] = "success";
				}
			} else {
				$this->data['msg'] = "Your submission has the following Problems:<br>" . validation_errors('<div>', '</div>');
				$this->data['msgtype'] = "danger";
			}
		}
		// END - The Edit Stuff ////////////////////////////////////////////////////////////////////////////////////////

		$this->data['title'] = "Create Role";

		$this->load->view('layout/admin/header.php', $this->data);
		$this->load->view('roles/admin/add', $this->data);
		$this->load->view('layout/admin/footer.php', $this->data);
	}

	public function delete()
	{
		$roleid = $this->uri->segment(4);

        if ($roleid !== '1') {
            if (!$this->Roles_model->delete($roleid)) {
                // Set the error to the flashdata array:
                $this->session->set_flashdata(array("msg" => "Error Deleting the Role!", "msgtype" => "danger"));
            } else {
                // Add Audit Trail
                $this->load->model('Audit_model');
                $this->Audit_model->addAuditLog(
                    $this->config->config['settings']['auditTypeRecordDeleted'],
                    '/roles/delete',
                    'Role Deleted',
                    'roles',
                    $roleid
                );
                // Set the success message to the flashdata array:
                $this->session->set_flashdata(array("msg" => "Role Successfully Deleted!", "msgtype" => "success"));
            }
        }
        else
        {
            $this->session->set_flashdata(array("msg" => "You cannot delete the Developer Role!", "msgtype" => "danger"));
        }
		redirect('/roles/', 'refresh');
	}

}
