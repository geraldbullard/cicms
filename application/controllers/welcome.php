<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Welcome extends MY_Controller {

	function __construct() {
		parent::__construct();

		// Get site wide settings first
		$this->Settings_model->loadSitewideSettings();
	}

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/userguide3/general/urls.html
	 */
	public function index() {
		// First thing let's set the template based on the site settings value, or the default
		if (file_exists(VIEWPATH . $this->config->item('siteTheme', 'settings') . '.php')) {
			$this->template->set_template($this->config->item('siteTheme', 'settings'));
		}

		// Set the title
		$this->template->title = 'Welcome - ' . $this->config->item('siteName', 'settings');

		// Add meta with and without siteWideSettings defines, later make CI_Meta class to possibly control this
		$this->template->meta->add("keywords", $this->config->item('siteKeywords', 'settings'));
		$this->template->meta->add("description", $this->config->item('siteDescription', 'settings'));
		$this->template->meta->add("robots", "index,follow");
		$this->template->meta->add("viewport", "width=device-width, initial-scale=1.0");
		$this->template->meta->add("author", "brokenPIXEL");
		$this->template->meta->add("utf-8", 'charset');

		// Dynamically add a css stylesheet, maybe specifically for this page etc
		//$this->template->stylesheet->add('REPLACE_ME_WITH_URL_OR_PATH');

		// get users data and show in main content area, later will be page html content from database
		if (file_exists(VIEWPATH . 'welcome/' . $this->config->item('siteTheme', 'settings') . '.php')) {
			$this->template->content->view('welcome/' . $this->config->item('siteTheme', 'settings'));
		} else {
			$this->template->content->view('welcome/estartup');
		}

		// Publish the template
		$this->template->publish();
	}
}
