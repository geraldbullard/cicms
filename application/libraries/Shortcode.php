<?php

class Shortcode
{
    /**
     * Shortcode tags array
     *
     * @var shortcode_tags
     */
    public static $shortcode_tags = [];

    /**
     * Public constructor since this is a static class.
     *
     * @access  public
     */
    public function __construct()
    {
        // Nothing here
    }

    /**
     * Protected constructor since this is a static class.
     *
     * @access  protected
     */
    public static function get_shortcodes()
    {
        return self::$shortcode_tags;
    }

    /**
     * Add new shortcode
     *
     *  <code>
     *      function returnSiteUrl() {
     *          return 'http://example.org';
     *      }
     *
     *      // Add shortcode {siteurl}
     *      Shortcode::add('siteurl', 'returnSiteUrl');
     *  </code>
     *
     * @param string $shortcode         Shortcode tag to be searched in content.
     * @param string $callback_function The callback function to replace the shortcode with.
     */
    public static function add($shortcode, $callback_function)
    {
        // Redefine vars
        $shortcode = (string) $shortcode;

        // Add new shortcode
        if (is_callable($callback_function)) {
            Shortcode::$shortcode_tags[$shortcode] = $callback_function;
        }
    }

    /**
     * Remove a specific registered shortcode.
     *
     *  <code>
     *      Shortcode::delete('shortcode_name');
     *  </code>
     *
     * @param string $shortcode Shortcode tag.
     */
    public static function delete($shortcode)
    {
        // Redefine vars
        $shortcode = (string) $shortcode;

        // Delete shortcode
        if (Shortcode::exists($shortcode)) {
            unset(Shortcode::$shortcode_tags[$shortcode]);
        }
    }

    /**
     * Remove all registered shortcodes.
     *
     *  <code>
     *      Shortcode::clear();
     *  </code>
     *
     */
    public static function clear()
    {
        Shortcode::$shortcode_tags = array();
    }

    /**
     * Check if a shortcode has been registered.
     *
     *  <code>
     *      if (Shortcode::exists('shortcode_name')) {
     *          // do something...
     *      }
     *  </code>
     *
     * @param string $shortcode Shortcode tag.
     */
    public static function exists($shortcode)
    {
        // Redefine vars
        $shortcode = (string) $shortcode;

        // Check shortcode
        return array_key_exists($shortcode, Shortcode::$shortcode_tags);
    }

    /**
     * Parse a string, and replace any registered shortcodes within it with the result of the mapped callback.
     *
     *  <code>
     *      $content = Shortcode::parse($content);
     *  </code>
     *
     * @param  string $content Content
     * @return string
     */
    public static function parse($content)
    {
        // Ensure $content is a string
        if (!is_string($content)) {
            return ''; // Return an empty string if $content is not a string
        }

        if (!Shortcode::$shortcode_tags) {
            return $content;
        }

        $shortcodes = implode('|', array_map('preg_quote', array_keys(Shortcode::$shortcode_tags)));
        $pattern = "/(.?)\{([$shortcodes]+)(.*?)(\/)?\}(?(4)|(?:(.+?)\{\/\s*\\2\s*\}))?(.?)/s";

        return preg_replace_callback($pattern, 'Shortcode::_handle', $content);
    }

    /**
     * _handle()
     */
    protected static function _handle($matches)
    {
        $prefix    = $matches[1];
        $suffix    = $matches[6];
        $shortcode = $matches[2];

        // Allow for escaping shortcodes by enclosing them in {{shortcode}}
        if ($prefix == '{' && $suffix == '}') {
            return substr($matches[0], 1, -1);
        }

        $attributes = array(); // Parse attributes into into this array.

        if (preg_match_all('/(\w+) *= *(?:([\'"])(.*?)\\2|([^ "\'>]+))/', $matches[3], $match, PREG_SET_ORDER)) {
            foreach ($match as $attribute) {
                if (! empty($attribute[4])) {
                    $attributes[strtolower($attribute[1])] = $attribute[4];
                } elseif (! empty($attribute[3])) {
                    $attributes[strtolower($attribute[1])] = $attribute[3];
                }
            }
        }

        // Check if this shortcode realy exists then call user function else return empty string
        return (isset(Shortcode::$shortcode_tags[$shortcode])) ? $prefix . call_user_func(Shortcode::$shortcode_tags[$shortcode], $attributes, $matches[5], $shortcode) . $suffix : '';
    }

}