<?php

class Core {

	// Function to validate the post data
	function validate_post($data) {
		/* Validating the devemail, devpass, hostname, the database name and the username. The password is optional. */
		return !empty($data['devemail']) && !empty($data['devpass']) && !empty($data['hostname']) && !empty($data['username']) && !empty($data['database']);
	}

	// Function to show an error
	function show_message($type, $message) {
		return $message;
	}

	// Function to generate the encryption key string
	function random_string() {
		$seed = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		$get = "";
		for ($i = 0; $i < 34; $i++) {
			$get .= $seed[rand(0, strlen($seed))];
		}
		return $get;
	}

	/**
	 * Hashes the password to be stored in the database.
	 *
	 * @param string $password
	 * @param string $identity
	 *
	 * @return false|string
	 * @author Mathew
	 */
	function hash_password($password) {
		// Check for empty password, or password containing null char, or password above limit
		// Null char may pose issue: http://php.net/manual/en/function.password-hash.php#118603
		// Long password may pose DOS issue (note: strlen gives size in bytes and not in multibyte symbol)
		if (empty($password) || strpos($password, "\0") !== FALSE || strlen($password) > 4096) {
			return FALSE;
		}
		$params = array('cost' => 12);
		return password_hash($password, NULL, $params);
	}

	// Function to write the config.php file
	function write_config($data) {
		// Config path
		$template_path = '../install/config/config.php';
		$output_path = '../application/config/config.php';

		// Open the file
		$config_file = file_get_contents($template_path);

		$protocol = strpos(strtolower($_SERVER['SERVER_PROTOCOL']), 'https') === FALSE ? 'http' : 'https';
		$domainLink = $protocol . '://' . $_SERVER['HTTP_HOST'] . '/';

		$new = str_replace("%BASEURL%", $domainLink, $config_file);
		$new = str_replace("%ENCRYPTIONKEY%", self::random_string(), $new);
		$new = str_replace("%COOKIEDOMAIN%", $_SERVER['HTTP_HOST'], $new);

		// Write the new config.php file
		$handle = fopen($output_path, "w+");

		if ($handle) {
			if (@fwrite($handle, $new) !== false) {
				fclose($handle);
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	// Function to write the database.php file
	function write_db_config($data) {
		// Config path
		$template_path = '../install/config/database.php';
		$output_path = '../application/config/database.php';

		// Open the file
		$database_file = file_get_contents($template_path);

		$new = str_replace("%HOSTNAME%", $data['hostname'], $database_file);
		$new = str_replace("%USERNAME%", $data['username'], $new);
		$new = str_replace("%PASSWORD%", $data['password'], $new);
		$new = str_replace("%DATABASE%", $data['database'], $new);

		// Write the new database.php file
		$handle = fopen($output_path, "w+");

		if ($handle) {
			if (@fwrite($handle, $new) !== false) {
				fclose($handle);
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	// Function to create the tables and fill them with the default data
	function update_admin_username($data) {
		// Connect to the database
		$mysqli = new mysqli($data['hostname'], $data['username'], $data['password'], $data['database']);

		// Check for errors
		if (mysqli_connect_errno())
			return false;

		// Update the developer username
		if (!$mysqli->query("UPDATE `ci_users` SET `username` = '" . $data['devuser'] . "' WHERE `id` = '1'") === TRUE)
			return false;

		// Close the connection
		$mysqli->close();

		return true;
	}

	// Function to create the tables and fill them with the default data
	function update_admin_email($data) {
		// Connect to the database
		$mysqli = new mysqli($data['hostname'], $data['username'], $data['password'], $data['database']);

		// Check for errors
		if (mysqli_connect_errno())
			return false;

		// Update the developer email
		if (!$mysqli->query("UPDATE `ci_users` SET `email` = '" . $data['devemail'] . "' WHERE `id` = '1'") === TRUE)
			return false;

		// Close the connection
		$mysqli->close();

		return true;
	}

	// Function to create the tables and fill them with the default data
	function update_admin_password($data) {
		// Connect to the database
		$mysqli = new mysqli($data['hostname'], $data['username'], $data['password'], $data['database']);

		// Check for errors
		if (mysqli_connect_errno())
			return false;

		// Hash the password
		$password = password_hash($data['devpass'], PASSWORD_BCRYPT);

		// Update the developer password
		if (!$mysqli->query("UPDATE `ci_users` SET `password` = '" . $password . "' WHERE `id` = '1'") === TRUE) {
			return false;
		}

		// Close the connection
		$mysqli->close();

		return true;
	}
}
